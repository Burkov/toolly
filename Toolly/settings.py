"""
Django settings for Toolly project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

import mongoengine
import pymongo
import mmap
import json
import sys
import os
import sendgrid

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

reload(sys)
sys.setdefaultencoding('UTF-8')

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

if 'PRODUCTION' in os.environ:
    PRODUCTION = True
else:
    PRODUCTION = False

if PRODUCTION: # we're running on production server: alpha.toolly.ru
    GOOGLE_CLIENT_ID = "131713289177-vcmcffam2oh8ikprbpv8por7631a7tgl.apps.googleusercontent.com"
    GOOGLE_CLIENT_SECRET = "3Kzp1QryiLY04_ZZZvh5WdY3"
else:
    GOOGLE_CLIENT_ID = "766672383686-fdfen2scjo8ahfhn16veqbl012e4ncqg.apps.googleusercontent.com"
    GOOGLE_CLIENT_SECRET = "eO3BntC91mMNSl70W0JnmPso"

if PRODUCTION: # we're running on production server: alpha.toolly.ru
    FACEBOOK_CLIENT_ID = "814985621877553"
    FACEBOOK_CLIENT_SECRET = "3a4d44dfc338445e7f5c0fa7abb60a30"
else:
    FACEBOOK_CLIENT_ID = "319619584891125"
    FACEBOOK_CLIENT_SECRET = "054dfedd28a0d35be18b6ff83c630df6"

if PRODUCTION: # we're running on production server: alpha.toolly.ru
    VK_CLIENT_ID = "4674049"
    VK_CLIENT_SECRET = "cutUMptG4ak3r5CTTg90"
else:
    VK_CLIENT_ID = "4652014"
    VK_CLIENT_SECRET = "phtZfzqGBngqe9WGMBUU"

if PRODUCTION: # we're running on production server: alpha.toolly.ru
    TWITTER_CLIENT_ID = "BAFXYal5vs2mUHeRZg6YbEn7X"
    TWITTER_CLIENT_SECRET = "h80YOG2cAbF3plS88DLhU4X7ENi7i9IIbBpVfaci3Wp5Objh8F"
else: # we're running on development server: sabotage.toolly.ru
    TWITTER_CLIENT_ID = "mBDDk6yXAX5EbF1R3NLwAXPRe"
    TWITTER_CLIENT_SECRET = "7HoVWmsICqmRAGQJCym4OESn6qbhZm9XCvg9AjRcVKM3MG71s1"

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*g(5h-t%q$w^$@rbpsfzj)26j2sdo5ou-gwh__&6$5&sx*m7^c'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = not 'NDEBUG' in os.environ

# TIMEOUT in seconds, waiting for each coordinator's response
# Note: failed connection will drop out faster
COORDINATORS_TIMEOUT = 2.0
TAG_MANAGERS_TIMEOUT = 2.0


SEND_GRID = sendgrid.SendGridClient('knackit-web', '4gfZk52uLjxL1wEh2Nue47oPKA')

# Always in-sync map of active services
# methods return up to date URIs or URL lists
class ServicesMap:
    # mmaps an externally maintained JSON file
    def __init__(self):
        self.f = open("services.json", "r")
        self.mm = mmap.mmap(self.f.fileno(), 0, mmap.MAP_SHARED, mmap.PROT_READ)
    
    # dict of services as listed in JSON
    def allServices(self):
        return json.loads(self.mm[:])

    # internal - list of all hosts matching given prefix
    def instances(self, prefix):
        services = self.allServices()
        keys = filter(lambda x: x.startswith(prefix), services.keys())
        hosts = [services[k]['addr'] for k in keys]
        return hosts

    # internal - get list of hosts as http://$host:$port
    def httpList(self, prefix, port):
        hosts = self.instances(prefix)
        hosts = map(lambda h: "http://%s:%s" % (h, port), hosts)
        return hosts

    # Mongo URI
    def mongo(self):
        hosts = self.instances("/toolly/mongodb")
        return "mongodb://" + ",".join(hosts) + "/Toolly"

    # Coordinators - list of http://$host:$port
    def coo(self):
        return self.httpList("/toolly/coo", 5000)

    # Tag mangers - same as coordiantors
    def tagman(self):
        return self.httpList("/toolly/tag-manager", 8080)

SERVICES = ServicesMap()

print "Services discovered: ", SERVICES.allServices()
MONGODB_URI = SERVICES.mongo()
MONGO_DB = pymongo.MongoClient(MONGODB_URI)
print "Mongo URI: ", MONGODB_URI


# TODO: should disable in production?
TEMPLATE_DEBUG = True

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'template'),
)

ALLOWED_HOSTS = [ '.knackit.com', 'localhost', '.local']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'mongoengine.django.mongo_auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app'
)

AUTH_USER_MODEL = 'mongo_auth.MongoUser'

AUTHENTICATION_BACKENDS = (
    'mongoengine.django.auth.MongoEngineBackend',
)

# modify the following line, if you want custom user model
MONGOENGINE_USER_DOCUMENT = 'app.models.User'#'mongoengine.django.auth.User' 

LOGIN_REDIRECT_URL = "/"

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

SESSION_ENGINE = 'mongoengine.django.sessions'
SESSION_SERIALIZER = 'mongoengine.django.sessions.BSONSerializer'

ROOT_URLCONF = 'Toolly.urls'

WSGI_APPLICATION = 'Toolly.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

#_MONGODB_USER = 'mongouser'
#_MONGODB_PASSWD = 'password'
#_MONGODB_HOST = 'thehost'
#_MONGODB_NAME = 'thedb'
#_MONGODB_DATABASE_HOST = \
#    'mongodb://%s:%s@%s/%s' \
#    % (_MONGODB_USER, _MONGODB_PASSWD, _MONGODB_HOST, _MONGODB_NAME)

#mongoengine.connect(_MONGODB_NAME, host=_MONGODB_DATABASE_HOST)
mongoengine.connect('Toolly', host=MONGODB_URI)


# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True

LOCALE_PATHS = (
    os.path.join(os.path.abspath(BASE_DIR), 'locale'),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(os.path.dirname(__file__), 'static'),
)

TEST_RUNNER = 'app.tests.MongoTestRunner'
