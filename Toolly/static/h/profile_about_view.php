<div class="inner-section">
    <p class="section-header">Общие сведения</p>
    <div class="common-form-cnt">
        <div class="fieldset">
            <p>Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. Наши менеджеры сами предупредят транспортную компанию</p>
        </div>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Квалификация</p>
    <div class="common-form-cnt">
        <div class="col12">
            <div class="fieldset">
                <p class="label">Род деятельности</p>

                <p>ООО «Инновационные технологии»</p>
            </div>
            <div class="fieldset">
                <p class="label">Период работы</p>

                <p>с 3 декабря, 2014 по 3 декабря, 2014</p>
            </div>
        </div>
        <div class="col12">
            <div class="fieldset">
                <p class="label">Комментарий</p>

                <p>Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас.</p>
            </div>
        </div>
    </div>
    <hr/>
    <div class="common-form-cnt">
        <div class="col12">
            <div class="fieldset">
                <p class="label">Род деятельности</p>

                <p>ООО «Инновационные технологии»</p>
            </div>
            <div class="fieldset">
                <p class="label">Период работы</p>

                <p>с 3 декабря, 2014 по 3 декабря, 2014</p>
            </div>
        </div>
        <div class="col12">
            <div class="fieldset">
                <p class="label">Комментарий</p>

                <p>Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас.</p>
            </div>
        </div>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Образование/курсы</p>
    <div class="common-form-cnt">
        <div class="col12">
            <div class="fieldset">
                <p class="label">Организация, выдавшая диплом</p>

                <p>Московский Государственный Институт Стали И Сплавов</p>
            </div>
            <div class="fieldset">
                <p class="label">Период обучения</p>

                <p>с 1 сентября, 2000 по 30 июня, 2005</p>
            </div>
            <div class="text-content">
                <div class="files-list">
                    <a href="file.pdf">Скан диплома <span>PDF, 2 Mb</span></a>
                </div>
            </div>
        </div>
        <div class="col12">
            <div class="fieldset">
                <p class="label">Комментарий</p>

                <p>Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас.</p>
            </div>
        </div>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Язык</p>
    <div class="common-form-cnt">
        <div class="col12">
            <p class="label">Иностранный язык</p>
            <p>Английский</p>
        </div>
        <div class="col12">
            <p class="label">Уровень владения</p>

            <p>Средний уровень</p>
        </div>
    </div>
</div>
