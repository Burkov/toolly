<div class="inner-section">
    <p class="section-header">Контакты</p>
    <ul class="profile-cont-types-list">
        <li class="cont-type-item phone">
            <i class="cont-type-status" title="Предпочтительный способ связи"></i>

            <div class="cont-type-inner">
                <p>+7 926-456-56-78<span class="profile-plus">&plus;</span></p>
<!---->
<!--                <p>-->
<!--                    <a class="profile-change-link js-profile-change-link" href="#">Изменить</a>|<a class="profile-change-link js-profile-remove-link remove-link" href="#">Удалить</a>-->
<!--                </p>-->
            </div>
            <div class="cont-type-inner">
                <p>+7 924-778-16-54<span class="profile-plus">+</span></p>

                <div class="input-group">
                    <input type="text" class="form-control" name="account-phone" id="account-phone" placeholder="телефонный номер">
                    <span class="input-group-btn">
                        <button class="btn" type="button">Ок</button>
                    </span>
                </div>
            </div>
        </li>
        <li class="cont-type-item skype">
            <i class="cont-type-status" title="Предпочтительный способ связи"></i>

            <div class="cont-type-inner">
                <p>skype_login<span class="profile-plus">+</span></p>
            </div>
            <div class="cont-type-inner">
                <p>skype_login 2<span class="profile-plus">+</span></p>
            </div>
        </li>
        <li class="cont-type-item vkontakte default">
            <i class="cont-type-status"></i>

            <div class="cont-type-inner">
                <p>vk.com/id12900534<span class="profile-plus">+</span></p>
            </div>
        </li>
        <li class="cont-type-item mail default">
            <i class="cont-type-status"></i>

            <div class="cont-type-inner">
                <p>example@mail.ru<span class="profile-plus">+</span></p>
            </div>
        </li>
        <li class="cont-type-item facebook">
            <i class="cont-type-status" title="Предпочтительный способ связи"></i>

            <div class="cont-type-inner">
                <p>facebook.com/login<span class="profile-plus">+</span></p>
            </div>
        </li>
        <li class="cont-type-item linkedin default">
            <i class="cont-type-status"></i>

            <div class="cont-type-inner">
                <p>linkedin.com/id12900534<span class="profile-plus">+</span></p>
            </div>
        </li>
        <li class="cont-type-item twitter">
            <i class="cont-type-status" title="Предпочтительный способ связи"></i>

            <div class="cont-type-inner">
                <p>twitter.com/login<span class="profile-plus">+</span></p>
            </div>
        </li>
    </ul>
</div>
<div class="inner-section no-padding">
    <div class="profile-address-cnt">
        <p class="section-header">Адрес</p>

        <div class="profile-address">
            <p>Россия</p>

            <p>Московская область, <br/>г. Красногорск, ул. Заречная, д. 1</p>

<!--            <p>-->
<!--                <a class="profile-change-link js-profile-change-link" href="#">Изменить</a>|<a class="profile-change-link js-profile-remove-link remove-link" href="#">Удалить</a>-->
<!--            </p>-->
        </div>
    </div>
    <div class="profile-address-map" data-lat="55.749266" data-long="37.568893"></div>
</div>
<div class="inner-section">
    <p class="section-header">Услуги</p>
    <div class="profile-service-logo-cnt">
        <div class="img-cnt">
            <label class="profile-add-logo"><input type="file" name="profile-add-logo" id="profile-add-logo"/></label>
        </div>
    </div>
    <div class="profile-service-inner">
        <div class="profile-service-rating">9.8</div>
        <div class="header">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Введите заголовок..." value="Путевки и экскурсии в Санкт-Петерург, организация личного транспорта"/>
            <span class="input-group-btn">
                <button class="btn" type="button">Ок</button>
            </span>
            </div>
        </div>
<!--        <p class="header">Путевки и экскурсии в Санкт-Петерург, организация личного транспорта </p>-->

        <p class="profile-meta">Размещено сегодня в 09:38</p>

        <p class="small-header">Категории</p>
        <ul class="tags-list">
            <li class="tag-item">Санкт-Петербург<a class="tag-remove js-tag-remove" href="#"></a></li>
            <li class="tag-item">Москва<a class="tag-remove js-tag-remove" href="#"></a></li>
            <li class="tag-item">Магазин<a class="tag-remove js-tag-remove" href="#"></a></li>
            <li class="tag-item">Коммерция<a class="tag-remove js-tag-remove" href="#"></a></li>
            <li class="tag-item">Торговый центр<a class="tag-remove js-tag-remove" href="#"></a></li>
            <li class="tag-item">Магазин<a class="tag-remove js-tag-remove" href="#"></a></li>
        </ul>
        <div class="service-category-input">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Название метки...">
                <span class="input-group-btn">
                    <button class="btn" type="button">Добавить</button>
                </span>
            </div>
        </div>
    </div>
    <div class="profile-service-info">
        <div class="col12">
            <p class="small-header">Список услуг</p>

            <div class="profile-service-edit-cnt common-form-cnt">
                <form action="">
                    <fieldset class="service-name-cnt">
                        <label for="service-name">Наименование услуги</label>
                        <textarea name="service-name" id="service-name">Круглосуточная поддержка туристов, находящихся на отдыхе</textarea>
                    </fieldset>
                    <fieldset class="service-price-cnt">
                        <label for="service-price">Стоимость</label>
                        <input type="text" name="service-price" id="service-price"/>
                    </fieldset>
                    <fieldset class="service-submit-cnt">
                        <a class="btn" href="#">Сохранить</a>
                        <a class="btn btn-secondary" href="#">Удалить</a>
                        <a class="btn btn-secondary" href="#">Отменить</a>
                    </fieldset>
                </form>
            </div>
            <div class="profile-service-edit-cnt common-form-cnt">
                <form action="">
                    <fieldset class="service-name-cnt">
                        <label for="service-name">Наименование услуги</label>
                        <textarea name="service-name" id="service-name">Экскурсионные авиа и автобусные туры</textarea>
                    </fieldset>
                    <fieldset class="service-price-cnt">
                        <label for="service-price">Стоимость</label>
                        <input type="text" name="service-price" id="service-price"/>
                    </fieldset>
                    <fieldset class="service-submit-cnt">
                        <a class="btn" href="#">Сохранить</a>
                        <a class="btn btn-secondary" href="#">Удалить</a>
                        <a class="btn btn-secondary" href="#">Отменить</a>
                    </fieldset>
                </form>
            </div>
            <div class="add-more-cnt">
                <a class="add-more" href="#">Добавить услугу</a>
            </div>
        </div>
        <div class="col12">
            <p class="small-header">Описание</p>

            <div class="profile-service-edit-cnt common-form-cnt">
                <form action="">
                    <fieldset class="service-descr-cnt">
                        <textarea name="service-descr" id="service-descr">Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. Наши менеджеры сами предупредят транспортную компанию </textarea>
                    </fieldset>
                    <fieldset class="service-submit-cnt">
                        <a class="btn" href="#">Сохранить</a>
                        <a class="btn btn-secondary" href="#">Отменить</a>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="profile-service-preview">
        <p><a class="preview-link" href="#">Предпросмотр объявления</a></p>

        <div class="preview-cnt">
            <div class="search-result-item">
                <aside class="item-actions">
                    <div class="actions-block">
                        <div class="logo-cnt">
                            <img src="/img/img_sr_logo1.jpg" alt="logo1"/>
                        </div>
                        <ul class="actions-list">
                            <li class="action-item">
                                <a class="action-fold js-action" data-action="fold" href="#"></a></li>
                            <li class="action-item">
                                <a class="action-add-user js-action" data-action="add-user" href="#"></a></li>
                            <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a>
                            </li>
                            <li class="action-item">
                                <a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a>
                            </li>
                        </ul>
                    </div>
                    <a class="btn btn-small" href="#">Заказать услугу</a>
                </aside>
                <div class="item-descr">
                    <div class="descr-header">
                        <div class="item-rating">9.8</div>
                        <div class="item-name">
                            <a href="#">Экскурсии в Санкт-Петербурге</a>
                        </div>
                    </div>
                    <div class="person-status person-offline">Константин Константинов</div>
                    <ul class="item-services-list">
                        <li>
                            <span>Экскурсии по Санкт-Петербургу</span>
                            <span>50 000 руб.</span>
                        </li>
                        <li>
                            <span>Новогодние и рождественские туры по Санкт-Петербургу</span>
                            <span>23 000 руб.</span>
                        </li>
                        <li>
                            <span>Бронирование отелей</span>
                            <span>500 руб.</span>
                        </li>
                        <li>
                            <span>Тур от</span>
                            <span>5 000 руб.</span>
                        </li>
                    </ul>
                    <div class="item-expander">
                        <a class="item-expand js-item-expand" href="#"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="inner-section add-section"></div>
