<div class="inner-section">
    <p class="section-header">Контакты</p>
    <ul class="profile-cont-types-list">
        <li class="cont-type-item phone">
            <div class="cont-type-inner">
                <p>+7 926-456-56-78</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item skype">
            <div class="cont-type-inner">
                <p>skype_login</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item vkontakte default">
            <div class="cont-type-inner">
                <p>vk.com/id12900534</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item mail default">
            <div class="cont-type-inner">
                <p>example@mail.ru</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item facebook">
            <div class="cont-type-inner">
                <p>facebook.com/login</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item linkedin default">
            <div class="cont-type-inner">
                <p>linkedin.com/id12900534</p>
            </div>
            <i></i>
        </li>
        <li class="cont-type-item twitter">
            <div class="cont-type-inner">
                <p>twitter.com/login</p>
            </div>
            <i></i>
        </li>
    </ul>
</div>
<div class="inner-section no-padding">
    <div class="profile-address-cnt">
        <p class="section-header">Адрес</p>

        <div class="profile-address">
            <p>Россия</p>

            <p>Московская область, <br/>г. Красногорск, ул. Заречная, д. 1</p>
        </div>
    </div>
    <div class="profile-address-map" data-lat="55.749266" data-long="37.568893"></div>
</div>
<div class="inner-section">
    <div class="profile-service-logo-cnt">
        <div class="img-cnt">
            <img src="/img/img_sr_logo1.jpg" alt="logo"/>
        </div>
    </div>
    <div class="profile-service-inner">
        <p class="header">Путевки и экскурсии в Санкт-Петерург, организация личного транспорта </p>

        <div class="profile-service-rating">9.8</div>
        <p class="profile-meta">Размещено сегодня в 09:38</p>
        <p class="small-header">Категории<i class="js-info info-trigger" title="Партисипативное планирование определяет план размещения"></i></p>
        <ul class="tags-list">
            <li class="tag-item">Санкт-Петербург</li>
            <li class="tag-item">Москва</li>
            <li class="tag-item">Магазин</li>
            <li class="tag-item">Коммерция</li>
            <li class="tag-item">Торговый центр</li>
            <li class="tag-item">Магазин</li>
        </ul>
        <div class="profile-service-order-btn">
            <a class="btn js-search-popup fancybox.ajax" href="order_service_popup.php">Заказать услугу</a>
        </div>
    </div>
    <div class="profile-service-info">
        <div class="col12">
            <p class="small-header">Список услуг</p>
            <ul class="profile-services-list">
                <li>
                    <span>Круглосуточная  поддержка туристов, находящихся на отдыхе</span>
                    <span>1 500 руб.</span>
                </li>
                <li>
                    <span>Экскурсионные авиа и автобусные туры</span>
                    <span>15 000 руб.</span>
                </li>
                <li>
                    <span>Лечебные туры</span>
                    <span>30 000 руб.</span>
                </li>
                <li>
                    <span>Бронирование авиа и железнодорожных билетов</span>
                    <span>500 руб.</span>
                </li>
                <li>
                    <span>Бронирование отелей в любой стране мира</span>
                    <span>2 500 руб.</span>
                </li>
            </ul>
        </div>
        <div class="col12">
            <p class="small-header">Описание</p>

            <div class="profile-service-descr">
                <p>Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. Наши менеджеры сами предупредят транспортную компанию об этих изменениях. Договор с транспортной компанией Вы можете оформить в нашем офисе.</p>
            </div>
        </div>
    </div>
</div>