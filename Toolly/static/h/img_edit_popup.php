<div class="popup-cnt img-edit-popup common-form-cnt">
    <form action="">
        <div class="image-edit-cnt">
            <img src="http://placehold.it/640x360" alt="dummy"/>
        </div>
        <fieldset>
            <input type="text" name="image-descr" id="image-descr" placeholder="Описание фото"/>
            <input class="css-checkbox" type="checkbox" name="make-cover" id="make-cover"/><label class="css-label" for="make-cover">Сделать обложкой</label>
        </fieldset>
        <div class="img-thumbs-cnt">
            <ul class="img-thumbs-list">
                <li class="img-thumb"><a href="#"><img src="/img/img_gallery1.jpg" alt="image 1"/></a></li>
                <li class="img-thumb"><a href="#"><img src="/img/img_gallery2.jpg" alt="image 2"/></a></li>
                <li class="img-thumb"><a href="#"><img src="/img/img_gallery3.jpg" alt="image 3"/></a></li>
                <li class="img-thumb"><a href="#"><img src="/img/img_gallery4.jpg" alt="image 4"/></a></li>
                <li class="img-thumb"><a href="#"><img src="/img/img_gallery5.jpg" alt="image 5"/></a></li>
            </ul>
        </div>
        <fieldset class="form-submit-cnt">
            <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отмена</a>
        </fieldset>
    </form>
</div>