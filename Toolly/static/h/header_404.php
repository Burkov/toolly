<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Knackit - Main</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/plugins/jquery.raty.css">
    <link rel="stylesheet" href="/css/plugins/jquery.mCustomScrollbar.min.css"/>
    <link rel="stylesheet" href="/css/plugins/chosen.min.css">
    <link rel="stylesheet" href="/css/plugins/jquery.fancybox3.css"/>
    <link rel="stylesheet" href="/css/plugins/jquery.datetimepicker.css"/>
    <link rel="stylesheet" href="/css/main.css">
    <!--[if lt IE 9]>
    <script>
        document.createElement('main');
    </script>
    <script src="/js/libs/es5-shim.min.js"></script>
    <![endif]-->
    <script src="/js/libs/require.js" data-main="/js/main.js"></script>

</head>
<body>

<div id="root-wrapper">
    <div id="site-wrapper">
        <div class="js-page" data-page="<?=$page?>"></div>
        <header class="global-header">
            <a class="logo" href="/"></a>
            <a class="header-search-trigger js-search-panel-trigger mobile-show" href="#"></a>
            <div class="header-search-cnt">
                <form action="">
                    <input class="header-search js-header-search" type="text" name="header-search" placeholder="Я ищу..."/>
                    <div class="city-select-cnt">
                        <input class="city-select" type="text" name="city-select" id="city-select"/>
                        <i></i>
                    </div>
                    <input type="submit" value="" class="mobile-hide"/>
                    <input type="submit" value="Поиск" class="mobile-show"/>
                </form>
            </div>
        </header>
        <div class="overlay"></div>