<div class="inner-section">
    <div class="settings-section common-form-cnt">
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Тип аккаунта</p>
                <div class="profile-account-types">
                    <input type="radio" checked name="account-type" id="account-type-finder"/><label for="account-type-finder">Соискатель</label>
                    <input type="radio" name="account-type" id="account-type-taker"/><label for="account-type-taker">Предприниматель</label>
                </div>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Имя страницы</p>
                <p>pagename.knackit.com</p>
            </div>
        </div>
        <div class="profile-subsection personal-info">
            <div class="fieldset">
                <p class="label">Личная информация</p>
                <p><strong>ФИО: </strong>Иванов Петр</p>
                <p><strong>Пол: </strong>мужской</p>
                <p><strong>Дата рождения: </strong>12 июня 1988 г.</p>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">E-mail</p>
                <p>p*****e@mail.ru</p>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Пароль</p>
                <p><a href="#change-pass-popup" class="change-pass-link js-changepass-popup">Изменить пароль</a></p>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Привязка к номеру телефона</p>
                <p>+7 (917)-***-*5-32</p>
                <p><a href="#change-phone-popup" class="change-pass-link js-changepass-popup">Изменить номер</a></p>
            </div>
        </div>
    </div>
</div>
