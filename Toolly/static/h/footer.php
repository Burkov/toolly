</div> <!-- #site-wrapper -->
<div id="footer-struggle">

</div>
</div> <!-- #root-wrapper -->
<div id="footer">
	<div class="footer-nav-cnt">
		<ul class="footer-nav">
			<li class="footer-nav-item"><a href="#">О нас</a></li>
			<li class="footer-nav-item"><a href="#">Помощь</a></li>
			<li class="footer-nav-item"><a href="#">Условия использования</a></li>
		</ul>
	</div>
	<div class="footer-socials">
		<div class="copyrights">© Knackit 2015 – All Right Reserved</div>
		<ul class="footer-socials-list">
			<li><a href="#" class="social gp"></a></li>
			<li><a href="#" class="social tw"></a></li>
			<li><a href="#" class="social fb"></a></li>
			<li><a href="#" class="social vk"></a></li>
		</ul>
	</div>
	<div class="footer-lang-panel">
		<p><a>RU</a> / <a href="#">EN</a> / <a href="#">LT</a></p>
	</div>
    <div id="register-popup" class="fbx-content">
        <div class="register-form-cnt">
            <p class="form-header">Форма регистрации</p>
            <form action="">
                <fieldset><input type="text" name="register-name" id="register-name" placeholder="Ваше имя"/></fieldset>
                <fieldset><input type="text" name="register-last-name" id="register-last-name" placeholder="Ваша фамилия"/></fieldset>
                <fieldset><input type="text" name="register-email" id="register-email" placeholder="E-mail"/></fieldset>
                <fieldset><input type="password" name="register-pass" id="register-pass" placeholder="Пароль"/><a class="pass-eye js-show-pass" href="#"></a></fieldset>
                <fieldset><input type="password" name="register-pass2" id="register-pass2" placeholder="Подтверждение пароля"/><a class="pass-eye js-show-pass" href="#"></a></fieldset>
                <fieldset><input class="btn-big" type="submit" value="Зарегистрироваться"/></fieldset>
            </form>
        </div>
        <div class="social-register-cnt">
            <div class="separator"><span>или</span></div>
            <div class="social-buttons-list">
                <a class="btn btn-big btn-social fb" href="#">Facebook</a><a class="btn btn-big btn-social vk" href="#">Вконтакте</a><a class="btn btn-big btn-social gp" href="#">Google</a><a class="btn btn-big btn-social tw" href="#">Twitter</a>
            </div>
        </div>
    </div>
    <div id="auth-popup" class="fbx-content">
        <div class="auth-form-cnt">
            <p class="form-header">Войти</p>
            <form action="">
                <fieldset><input type="text" name="auth-email" id="auth-email" placeholder="E-mail"/></fieldset>
                <fieldset><input type="password" name="auth-pass" id="auth-pass" placeholder="Пароль"/><a class="pass-eye js-show-pass" href="#"></a></fieldset>
                <fieldset><input class="btn-big" type="submit" value="Войти"/><a class="forgot-pass-link js-register-popup" href="#forgot-pass-popup">Забыли пароль?</a></fieldset>
            </form>
        </div>
        <div class="social-auth-cnt">
            <div class="separator"><span>или</span></div>
            <div class="social-buttons-list">
                <a class="btn btn-big btn-social fb" href="#">Facebook</a><a class="btn btn-big btn-social vk" href="#">Вконтакте</a><a class="btn btn-big btn-social gp" href="#">Google</a><a class="btn btn-big btn-social tw" href="#">Twitter</a>
            </div>
        </div>
        <div class="auth-register-link">
            <a href="#register-popup" class="js-register-popup">Еще не зарегистрированы на Knackit.com?</a>
        </div>
    </div>
    <div id="forgot-pass-popup" class="fbx-content">
        <div class="forgot-pass-form-cnt common-form-cnt">
            <p class="form-header">Сброс пароля</p>
            <form action="">
                <fieldset>
                    <label for="forgot-pass-email">E-mail, указанный при  регистраци</label>
                    <input type="text" name="forgot-pass-email" id="forgot-pass-email" placeholder="E-mail"/>
                </fieldset>
                <fieldset><input class="btn-big" type="submit" value="Отправить"/></fieldset>
            </form>
        </div>
    </div>

    <div id="change-pass-popup" class="fbx-content">
        <div class="change-pass-form-cnt common-form-cnt">
            <p class="form-header">Изменить пароль</p>

            <form action="">
                <fieldset><input type="text" name="change-pass" id="change-pass" placeholder="Текущий пароль"/></fieldset>
                <fieldset><input type="text" name="change-newpass" id="change-newpass" placeholder="Новый пароль"/></fieldset>
                <fieldset><input type="text" name="change-newpass2" id="change-newpass2" placeholder="Повторите ввод нового пароля"/></fieldset>
                <fieldset class="change-submit-cnt">
                    <input type="submit" value="Сохранить"/>
                </fieldset>
            </form>
        </div>
    </div>
    <div id="change-phone-popup" class="fbx-content">
        <div class="change-phone-form-cnt common-form-cnt">
            <p class="form-header">Изменить пароль</p>

            <form action="">
                <fieldset>
                    <input type="text" name="change-phone" id="change-phone" placeholder="Номер телефона"/>
                    <p><a href="#">Отправить код проверки</a></p>
                </fieldset>
                <fieldset><input type="text" name="change-phone-code" id="change-phone-code" placeholder="Код проверки"/></fieldset>
                <fieldset class="change-submit-cnt">
                    <input type="submit" value="Сохранить"/>
                </fieldset>
            </form>
        </div>
    </div>
</div>


</body>
</html>
