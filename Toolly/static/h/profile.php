<?php
$page = 'profile';
include('header.php');
?>
    <div class="page-inner-cnt">
        <div class="page-inner-wrapper">
            <div class="inner-section profile-header no-padding">
                <div class="profile-user-info-cnt" style="background-image: url('/img/bg_lk_service.jpg');">
                    <div class="profile-user-info">
                        <div class="photo-cnt">
                            <img src="/img/img_photo1.jpg" alt="photo"/>
                        </div>
                        <!-- В режиме редактирования
                        <div class="photo-cnt nophoto">
                            <label class="add-photo-form"><input type="file" name="user-photo" id="user-photo"/></label>
                        </div>-->

                        <p class="username">Константин Александров</p>
                        <!-- В режиме редактирования
                        <div>
                            <input type="text" name="profile-name" id="profile-name" placeholder="Ваше имя" value="Константин Александров"/>
                        </div>

                        или

                        <div class="input-group">
                            <input type="text" class="form-control" name="profile-name" id="profile-name" placeholder="Ваше имя" value="Константин Александров">
                            <span class="input-group-btn">
                                <button class="btn" type="button">Ок</button>
                            </span>
                        </div>
                        -->

                        <div class="user-address">
                            <p class="profile-meta">Московская область,<br/>г. Красногорск, ул. Заречная, д. 1</p>
                        </div>
                        <!-- В режиме редактирования разметка аналогичная (см. выше)-->

                        <div class="control-cnt">
                            <a class="btn btn-small js-editable-trigger" href="#">Редактировать профиль</a>
                            <!-- Активное состояние:
                            <a class="btn btn-small active js-editable-trigger" href="#">Редактировать профиль</a>
                            -->
                        </div>
                    </div>

                    <!-- В режиме редактирования раскомментировать здесь
                    <div class="add-profile-background">
                        <label class="add-profile-bg-file" >Добавить фотографию обложки<i></i><input type="file" name="profile-bg" id="profile-bg"/></label>
                    </div>
                    -->
                </div>

                <div class="profile-top-nav-cnt">
                    <ul class="js-panes-control profile-top-nav" data-panes="profile">
                        <li class="js-tab-item active">
                            <a class="js-pane-trigger active" href="#services_view">Услуги</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#services_edit">Услуги (ред.)</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#about_view">О нас</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#about_edit">О нас (ред.)</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#portfolio_view">Портфолио</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#portfolio_edit">Портфолио (ред.)</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#settings_view">Настройки</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#settings_edit">Настройки (ред.)</a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="js-common-panes" id="profile">
                <div class="js-pane active" id="services_view">
                    <?php include('profile_services_view.php'); ?>
                </div>


                <div class="js-pane editable" id="services_edit">
                    <?php include('profile_services_edit.php'); ?>
                </div>


                <div class="js-pane" id="about_view">
                    <?php include('profile_about_view.php'); ?>
                </div>


                <div class="js-pane" id="about_edit">
                    <?php include('profile_about_edit.php'); ?>
                </div>


                <div class="js-pane" id="portfolio_view">
                    <?php include('profile_portfolio_view.php'); ?>
                </div>


                <div class="js-pane" id="portfolio_edit">
                    <?php include('profile_portfolio_edit.php'); ?>
                </div>


                <div class="js-pane" id="settings_view">
                    <?php include('profile_settings_view.php'); ?>
                </div>


                <div class="js-pane" id="settings_edit">
                    <?php include('profile_settings_edit.php'); ?>
                </div>
            </div>
        </div>
    </div>

<?php
include('footer.php');
?>