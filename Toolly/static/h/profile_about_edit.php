<div class="inner-section">
    <p class="section-header">Общие сведения</p>
    <div class="common-form-cnt">
        <form action="">
            <fieldset>
                <textarea name="bio-common" id="bio-common">Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. Наши менеджеры сами предупредят транспортную компанию </textarea>
            </fieldset>
            <fieldset class="profile-submit-cnt">
                <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отменить</a>
            </fieldset>
        </form>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Квалификация<a class="header-edit-link" href="#">Редактировать</a></p>
    <div class="common-form-cnt">
        <form action="">
            <div class="col12">
                <fieldset>
                    <label for="bio-qual1">Род деятельности</label>
                    <input type="text" name="bio-qual1" id="bio-qual1"/>
                </fieldset>
                <fieldset>
                    <label for="bio-period1">Период работы</label>

                    <div class="period-cnt">
                        <span>с</span>
                        <input class="date-input date-from mobile-hide" type="text" name="bio-date-from" id="bio-date-from"/>
                        <input class="date-input mobile-show" type="date" name="bio-date-from" id="bio-date-from"/>
                        <span>по</span>
                        <input class="date-input date-to mobile-hide" type="text" name="bio-date-to" id="bio-date-to"/>
                        <input class="date-input mobile-show" type="date" name="bio-date-to" id="bio-date-to"/>
                    </div>
                </fieldset>
            </div>
            <div class="col12">
                <fieldset>
                    <label for="bio-qual-comment">Комментарий</label>
                    <textarea name="bio-qual-comment" id="bio-qual-comment">Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. </textarea>
                </fieldset>
            </div>
            <fieldset class="profile-submit-cnt">
                <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отменить</a><a class="btn" href="#">Удалить</a>
            </fieldset>
        </form>
    </div>
    <div class="add-more-cnt">
        <a class="add-more" href="#">Добавить квалификацию</a>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Образование/курсы<a class="header-edit-link" href="#">Редактировать</a></p>
    <div class="common-form-cnt">
        <form action="">
            <div class="col12">
                <fieldset>
                    <label for="bio-ed-org">Организация, выдавшая диплом</label>
                    <input type="text" name="bio-ed-org" id="bio-ed-org" value="Московский Государственный Институт Стали И Сплавов"/>
                </fieldset>
                <fieldset>
                    <label for="bio-ed-period">Период обучения</label>

                    <div class="period-cnt">
                        <span>с</span>
                        <input class="date-input date-from mobile-hide" type="text" name="bio-ed-from" id="bio-ed-from"/>
                        <input class="date-input mobile-show" type="date" name="bio-ed-from" id="bio-ed-from"/>
                        <span>по</span>
                        <input class="date-input date-to mobile-hide" type="text" name="bio-ed-to" id="bio-ed-to"/>
                        <input class="date-input mobile-show" type="date" name="bio-ed-to" id="bio-ed-to"/>
                    </div>
                </fieldset>
                <fieldset>
                    <label class="file-label">Прикрепить файл<input type="file" name="bio-ed-file" id="bio-ed-file"/><span class="filename">file_12_22_14.png (12kb)</span></label>
                </fieldset>
            </div>
            <div class="col12">
                <fieldset>
                    <label for="bio-ed-comment">Комментарий</label>
                    <textarea name="bio-ed-comment" id="bio-ed-comment">Туристическое агентство «Пример» сотрудничает странспортными компаниями нашего города («ЯМЩИК» и «МАГИСТРАЛЬ»), поэтому дорога до пункта отправления в Ваше путешествие и обратно будет решена максимально комфортно для Вас. Даже если у Вас произойдет замена рейса на вылет или на возврат, Вам не придется беспокоиться. </textarea>
                </fieldset>
            </div>
            <fieldset class="profile-submit-cnt">
                <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отменить</a><a class="btn" href="#">Удалить</a>
            </fieldset>
        </form>
    </div>
    <div class="add-more-cnt">
        <a class="add-more" href="#">Добавить курсы</a>
    </div>
</div>

<div class="inner-section">
    <p class="section-header">Язык<a class="header-edit-link" href="#">Редактировать</a></p>
    <div class="common-form-cnt">
        <form action="">
            <div class="col12">
                <fieldset>
                    <label for="bio-lang">Иностранный язык</label>
                    <input type="text" name="bio-lang" id="bio-lang" value="Английский"/>
                </fieldset>
            </div>
            <div class="col12">
                <fieldset>
                    <label for="bio-lang-level">Уровень владения</label>
                    <select name="bio-lang-level" id="bio-lang-level" class="single-select">
                        <option value="1">Начальный уровень</option>
                        <option value="2">Средний уровень</option>
                        <option value="3">Продвинутый уровень</option>
                    </select>
                </fieldset>
            </div>
            <fieldset class="profile-submit-cnt">
                <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отменить</a><a class="btn" href="#">Удалить</a>
            </fieldset>
        </form>
    </div>
    <div class="add-more-cnt">
        <a class="add-more" href="#">Добавить язык</a>
    </div>
</div>
