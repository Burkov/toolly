<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Knackit - Main</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/plugins/jquery.raty.css">
    <link rel="stylesheet" href="/css/plugins/jquery.mCustomScrollbar.min.css"/>
    <link rel="stylesheet" href="/css/plugins/chosen.min.css">
    <link rel="stylesheet" href="/css/plugins/jquery.fancybox3.css"/>
    <link rel="stylesheet" href="/css/plugins/jquery.datetimepicker.css"/>
    <link rel="stylesheet" href="/css/main.css">
    <!--[if lt IE 9]>
    <script>
        document.createElement('main');
    </script>
    <script src="/js/libs/es5-shim.min.js"></script>
    <![endif]-->
    <script src="/js/libs/require.js" data-main="/js/main.js"></script>

</head>
<body>

<div id="root-wrapper">
    <div id="site-wrapper">
        <div class="js-page" data-page="<?=$page?>"></div>
        <header class="global-header">
            <a class="logo" href="/"></a>
            <div class="header-actions">
                <a class="login-btn js-auth-popup" href="#auth-popup">Войти</a>
            </div>
            <a class="header-search-trigger js-search-panel-trigger mobile-show" href="#"></a>
            <div class="header-search-cnt">
                <form action="">
                    <input class="header-search js-header-search" type="text" name="header-search" placeholder="Я ищу..."/>
                    <div class="city-select-cnt">
                        <input class="city-select" type="text" name="city-select" id="city-select"/>
                        <i></i>
                    </div>
                    <input type="submit" value="" class="mobile-hide"/>
                    <input type="submit" value="Поиск" class="mobile-show"/>
                </form>
            </div>
            <div class="top-profile-dropdown mobile-show">
                <div class="top-profile-info">
                    <p class="top-profile-name">Артур Семенов</p>
                    <p class="top-profile-meta">Спасибо 200</p>
                </div>
                <ul class="top-profile-menu">
                    <li><a href="#">Услуги</a></li>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Портфолио</a></li>
                </ul>
                <a class="top-profile-bar-link"><i class="icon icon-settings"></i>Настройки</a>
                <a class="top-profile-bar-link"><i class="icon icon-logout"></i>Выйти</a>
            </div>
            <div class="utils-sidebar">
                <ul class="js-panes-control utils-tabs" data-panes="utils">
                    <li class="js-tab-item utils-tab-item active">
                        <a class="js-pane-trigger active" href="#contacts">Контакты</a>
                    </li>
                    <li class="js-tab-item utils-tab-item">
                        <a class="js-pane-trigger" href="#messages">Сообщения<span class="counter">12</span></a>
                    </li>
                    <li class="js-tab-item utils-tab-item">
                        <a class="js-pane-trigger" href="#events">События<span class="counter">10</span></a>
                    </li>
                </ul>
                    
                <div class="js-panes" id="utils">
                    <div class="utils-pane js-pane active" id="contacts">
                        <div class="utils-top-actions">
                            <div class="left-block">
                                <div class="search-cnt">
                                    <form action="">
                                        <input type="text" name="contacts-search" placeholder="Поиск по контактам"/><input type="submit" value=""/>
                                    </form>
                                </div>
                            </div>
                            <div class="right-block">
                                <a class="utils-settings js-utils-operate" data-operate="remove" href="#"><i class="icon icon-settings"></i></a>
                            </div>
                        </div>
                        <div class="utils-inner-content">
                            <div class="contacts-list-cnt">
                                <div class="contacts-list-section">
                                    <p class="section-header">А</p>

                                    <div class="contact-card js-operate-item">
                                        <div class="contact-avatar-cnt">
                                            <div class="img-cnt"><img src="/img/img_avatar2.jpg" alt="avatar"/></div>
                                        </div>
                                        <div class="contact-inner">
                                            <p class="contact-name"><a href="#">Константин <strong>Александров</strong></a></p>
                                            <div class="person-status">Online</div>
                                            <ul class="actions-list left">
                                                <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                                <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                                <li class="action-item"><a class="action-remove-user js-action" data-action="remove-user" href="#"></a></li>
                                                <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                            </ul>
                                            <div class="add-note-link"><a class="add-contact-note js-add-contact-note" href="#">Оставить заметку</a></div>
                                            <div class="contact-note-form-cnt">
                                                <form action="">
                                                    <textarea name="contact-note" placeholder="Текст комментария"></textarea>
                                                    <input type="submit" value="Оставить заметку"/>
                                                </form>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </div>
                                    <div class="contact-card js-operate-item">
                                        <div class="contact-avatar-cnt">
                                            <div class="img-cnt"><img src="/img/img_avatar6.jpg" alt="avatar"/></div>
                                        </div>
                                        <div class="contact-inner">
                                            <p class="contact-name"><a href="#">Ирина <strong>Аникина</strong></a></p>
                                            <div class="person-status">Online</div>
                                            <ul class="actions-list left">
                                                <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                                <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                                <li class="action-item"><a class="action-remove-user js-action" data-action="remove-user" href="#"></a></li>
                                                <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                            </ul>
                                            <p class="contact-note">Помогла с оформлением письма и туристической визы.</p>

                                            <div class="add-note-link"><a class="add-contact-note js-add-contact-note" href="#">Изменить заметку</a></div>
                                            <div class="contact-note-form-cnt">
                                                <form action="">
                                                    <textarea name="contact-note" placeholder="Текст комментария"></textarea>
                                                    <input type="submit" value="Оставить заметку"/>
                                                </form>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </div>
                                </div>
                                <div class="contacts-list-section">
                                    <p class="section-header">Б</p>

                                    <div class="contact-card js-operate-item">
                                        <div class="contact-avatar-cnt">
                                            <div class="img-cnt"><img src="/img/img_avatar3.jpg" alt="avatar"/></div>
                                        </div>
                                        <div class="contact-inner">
                                            <p class="contact-name"><a href="#">Станислав <strong>Баринов</strong></a></p>
                                            <div class="person-status">Online</div>
                                            <ul class="actions-list left">
                                                <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                                <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                                <li class="action-item"><a class="action-remove-user js-action" data-action="remove-user" href="#"></a></li>
                                                <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                            </ul>
                                            <div class="add-note-link"><a class="add-contact-note js-add-contact-note" href="#">Оставить заметку</a></div>
                                            <div class="contact-note-form-cnt">
                                                <form action="">
                                                    <textarea name="contact-note" placeholder="Текст комментария"></textarea>
                                                    <input type="submit" value="Оставить заметку"/>
                                                </form>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </div>
                                    <div class="contact-card js-operate-item">
                                        <div class="contact-avatar-cnt">
                                            <div class="img-cnt"><img src="/img/img_avatar4.jpg" alt="avatar"/></div>
                                        </div>
                                        <div class="contact-inner">
                                            <p class="contact-name"><a href="#">Андрей <strong>Бикулов</strong></a></p>
                                            <div class="person-status person-offline">Offline</div>
                                            <ul class="actions-list left">
                                                <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                                <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                                <li class="action-item"><a class="action-remove-user js-action" data-action="remove-user" href="#"></a></li>
                                                <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                            </ul>

                                            <div class="add-note-link"><a class="add-contact-note js-add-contact-note" href="#">Оставить заметку</a></div>
                                            <div class="contact-note-form-cnt">
                                                <form action="">
                                                    <textarea name="contact-note" placeholder="Текст комментария"></textarea>
                                                    <input type="submit" value="Оставить заметку"/>
                                                </form>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </div>
                                    <div class="contact-card js-operate-item">
                                        <div class="contact-avatar-cnt">
                                            <div class="img-cnt"><img src="/img/img_avatar4.jpg" alt="avatar"/></div>
                                        </div>
                                        <div class="contact-inner">
                                            <p class="contact-name"><a href="#">Артем <strong>Булатов</strong></a></p>
                                            <div class="person-status">Online</div>
                                            <ul class="actions-list left">
                                                <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                                <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                                <li class="action-item"><a class="action-remove-user js-action" data-action="remove-user" href="#"></a></li>
                                                <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                            </ul>

                                            <div class="add-note-link"><a class="add-contact-note js-add-contact-note" href="#">Оставить заметку</a></div>
                                            <div class="contact-note-form-cnt">
                                                <form action="">
                                                    <textarea name="contact-note" placeholder="Текст комментария"></textarea>
                                                    <input type="submit" value="Оставить заметку"/>
                                                </form>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="utils-pane js-pane" id="messages">
                        <div class="chat-list-cnt">
                            <div class="utils-top-actions">
                                <div class="left-block">
                                    <div class="search-cnt">
                                        <form action="">
                                            <input type="text" name="chats-search" placeholder="Поиск по контактам"/><input type="submit" value=""/>
                                        </form>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <a class="utils-settings js-utils-operate" data-operate="remove" href="#"><i class="icon icon-settings"></i></a>
                                </div>
                            </div>
                            <div class="utils-inner-content">
                                <ul class="chat-list">
                                    <li class="chat-item js-operate-item">
                                        <div class="chat-item-wrapper">
                                            <div class="chat-avatar-cnt">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt=""/>
                                                </div>
                                            </div>
                                            <div class="chat-item-inner">
                                                <p class="contact-name"><a href="#">Константин <strong>Александров</strong></a></p>
                                                <div class="person-status">Online</div>
                                                <p class="last-message-time">Вчера в 22:43</p>
                                                <div class="last-message-cnt">
                                                    <div class="avatar-cnt">
                                                        <div class="img-cnt"><img src="/img/img_avatar3.jpg" alt=""/></div>
                                                    </div>
                                                    <div class="last-message js-chat-trigger">
                                                        Согласен, можно обсудить ценник на данную услугу
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                    <li class="chat-item js-operate-item">
                                        <div class="chat-item-wrapper">
                                            <div class="chat-avatar-cnt x4">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt=""/>
                                                    <img src="/img/img_avatar2.jpg" alt=""/>
                                                    <img src="/img/img_avatar5.jpg" alt=""/>
                                                    <img src="/img/img_avatar6.jpg" alt=""/>
                                                </div>
                                            </div>
                                            <div class="chat-item-inner">
                                                <p class="contact-name"><a href="#">Константин</a>,
                                                    <a href="#">Арина</a>, <a href="#">Кирилл</a>,
                                                    <a href="#">Евлампий</a></p>

                                                <div class="person-status no-status">4 участника</div>
                                                <p class="last-message-time">Вчера в 22:43</p>

                                                <div class="last-message-cnt">
                                                    <div class="avatar-cnt">
                                                        <div class="img-cnt"><img src="/img/img_avatar3.jpg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div class="last-message js-chat-trigger">
                                                        Согласен, можно обсудить ценник на данную услугу
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                    <li class="chat-item js-operate-item" data-ro="true">
                                        <div class="chat-item-wrapper">
                                            <div class="chat-avatar-cnt x2">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt=""/>
                                                    <img src="/img/img_avatar.jpg" alt=""/>
                                                </div>
                                            </div>
                                            <div class="chat-item-inner">
                                                <p class="contact-name"><a href="#">Борис</a>, <a href="#">Владимир</a>,
                                                    <a href="#">Елена</a></p>

                                                <div class="person-status no-status">2 или 3 участника</div>
                                                <p class="last-message-time">Вчера в 22:43</p>

                                                <div class="last-message-cnt no-avatar">
                                                    <div class="last-message js-chat-trigger">
                                                        Согласен, можно обсудить ценник на данную услугу
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chat">
                            <div class="utils-top-actions">
                                <div class="left-block">
                                    <div class="backlink-cnt">
                                        <a href="#" class="js-close-chat">Назад</a>
                                    </div>
                                    <div class="chat-persons">
                                        <ul class="tags-list">
                                            <li class="tag-item">Артем</li>
                                            <li class="tag-item">Александр</li>
                                            <li class="tag-item">Иван</li>
                                            <li class="tag-item">Александр</li>
                                            <li class="tag-item">Иван</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <a class="utils-settings js-utils-operate" data-operate="remove" href="#"><i class="icon icon-settings"></i></a>
                                </div>
                            </div>
                            <div class="utils-inner-content chat-content">
                                <ul class="chat-messages-list">
                                    <li class="chat-message-item js-operate-item" data-ro="true">
                                        <div class="chat-message-wrapper">
                                            <div class="avatar-cnt">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt="avatar"/>
                                                </div>
                                            </div>
                                            <div class="chat-message">
                                                Несмотря на сложности, таргетирование индуктивно обуславливает рекламный макет
                                                <p class="chat-message-timestamp">Вчера в 22:43</p>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                    <li class="chat-message-item my-message js-operate-item">
                                        <div class="chat-message-wrapper">
                                            <div class="avatar-cnt">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt="avatar"/>
                                                </div>
                                            </div>
                                            <div class="chat-message">
                                                Несмотря на сложности, таргетирование индуктивно обуславливает рекламный макет
                                                <p class="chat-message-timestamp">Вчера в 22:43</p>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                    <li class="chat-message-item js-operate-item" data-ro="true">
                                        <div class="chat-message-wrapper">
                                            <div class="avatar-cnt">
                                                <div class="img-cnt">
                                                    <img src="/img/img_avatar4.jpg" alt="avatar"/>
                                                </div>
                                            </div>
                                            <div class="chat-message">
                                                Несмотря на сложности, таргетирование индуктивно обуславливает рекламный макет
                                                <p class="chat-message-timestamp">Вчера в 22:43</p>
                                            </div>
                                        </div>
                                        <a class="remove-item js-remove-item" href="#"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="chat-message-form">
                                <form action="">
                                    <div class="chat-message-cnt">
                                        <textarea name="chat-message" id="chat-message" placeholder="Текст сообщения..."></textarea>
                                        <div class="smile-trigger js-smile-trigger">
                                            <div class="smiles-panel">
                                                <ul class="smiles-list">
                                                    <li class="js-smile smile-item"><i class="icon-emo-happy"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-wink"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-wink2"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-unhappy"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-sleep"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-thumbsup"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-devil"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-surprised"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-tongue"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-coffee"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-sunglasses"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-displeased"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-beer"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-grin"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-angry"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-saint"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-cry"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-shoot"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-squint"></i></li>
                                                    <li class="js-smile smile-item"><i class="icon-emo-laugh"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-submit-cnt">
                                        <input type="submit" value="Отправить сообщение"/>
                                        <label class="file-label">Прикрепить файл<input type="file" name="chat-file" id="chat-file"/></label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="utils-pane js-pane" id="events">
                        <div class="utils-top-actions">
                            <div class="left-block">
                                <div class="search-cnt">
                                    <form action="">
                                        <input type="text" name="contacts-search" placeholder="Поиск по контактам"/><input type="submit" value=""/>
                                    </form>
                                </div>
                            </div>
                            <div class="right-block">
                                <a class="utils-settings js-utils-operate" data-operate="remove" href="#"><i class="icon icon-settings"></i></a>
                            </div>
                        </div>
                        <div class="utils-inner-content">
                            <div class="events-list-cnt">
                                <ul class="events-list">
                                    <li class="event-item js-operate-item" data-ro="true">
                                        <div class="event-inner">
                                            <div class="event-params">
                                                <div class="row">
                                                    <div class="param-name">Дата</div>
                                                    <div class="param-value param-date">21 декабря</div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Заказчик</div>
                                                    <div class="param-value"><a href="#">Васильев Николай <span class="js-client-count">(20)</span></a></div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Услуга</div>
                                                    <div class="param-value">Ремонт мобильных телефонов</div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Прайс</div>
                                                    <div class="param-value">Замена экрана, Ремонт зарядного устройства</div>
                                                </div>
                                            </div>
                                            <div class="event-message collapsed">
                                                <div class="message">Добрый день Константин. Хочу уточнить у Вас насчёт Ремонта мобильных телефонов и замены экрана на новый</div>
                                            </div>
                                            <div class="event-actions">
                                                <a class="btn btn-small" href="#">Принять</a><a class="btn btn-small btn-secondary" href="#">Отклонить</a>
                                            </div>
                                        </div>
                                        <a href="#" class="remove-item js-remove-item" data-operate="remove"></a>
                                        <a href="#" class="make-pinned js-make-pinned"></a>
                                    </li>
                                    <li class="event-item js-operate-item item-info">
                                        <div class="event-inner">
                                            <div class="event-params">
                                                <div class="row">
                                                    <div class="param-value param-date">18 декабря</div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-value">
                                                        В соседнем регионе очень популярна ваша новая услуга
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="remove-item js-remove-item" data-operate="remove"></a>
                                        <a href="#" class="make-pinned js-make-pinned"></a>
                                    </li>
                                    <li class="event-item js-operate-item" data-ro="true">
                                        <div class="event-inner">
                                            <div class="event-params">
                                                <div class="row">
                                                    <div class="param-name">Дата</div>
                                                    <div class="param-value param-date">12 декабря</div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Заказчик</div>
                                                    <div class="param-value"><a href="#">Константин Рустамов <span class="js-client-count">(20)</span></a></div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Услуга</div>
                                                    <div class="param-value">Фриланс копирайт</div>
                                                </div>
                                                <div class="row">
                                                    <div class="param-name">Прайс</div>
                                                    <div class="param-value">Все</div>
                                                </div>
                                                <div class="row row-marked">
                                                    <div class="param-name">Статус</div>
                                                    <div class="param-value">Завершено</div>
                                                </div>
                                            </div>
                                            <div class="event-rating-cnt">
                                                <p class="header">Оцените качество работы</p>
                                                <div class="event-rating"></div>
                                            </div>
                                        </div>
                                        <a href="#" class="remove-item js-remove-item" data-operate="remove"></a>
                                        <a href="#" class="make-pinned js-make-pinned"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </header>
        <div class="overlay"></div>