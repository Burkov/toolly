<div class="inner-section">
    <div class="portfolio-catalog-slider-cnt">
        <ul class="portfolio-gallery catalog-gallery">
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#">
                        <img src="/img/img_portfolio1.jpg" alt=""/>
                    </a>
                    <div class="portfolio-item-actions">
                        <a class="item-edit" href="#">Редактировать</a>
                        <a class="item-remove" href="#">Удалить</a>
                    </div>
                </div>
                <div class="portfolio-item-descr">
                    <div class="common-form-cnt">
                        <fieldset>
                            <input type="text" name="" id="" value="Фотографии с тура по Санкт-Петербурга"/>
                        </fieldset>
                        <fieldset>
                            <input type="text" name="" id="" value="Короткая аннотация к альбому, которая растягивается на две строки"/>
                        </fieldset>
                        <fieldset>
                            <a class="btn" href="#">Сохранить</a>
                            <a class="btn btn-secondary" href="#">Отменить</a>
                        </fieldset>
                    </div>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                    <div class="portfolio-item-actions">
                        <a class="item-edit" href="#">Редактировать</a>
                        <a class="item-remove" href="#">Удалить</a>
                    </div>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Изготовление логотипов</a></p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                    <div class="portfolio-item-actions">
                        <a class="item-edit" href="#">Редактировать</a>
                        <a class="item-remove" href="#">Удалить</a>
                    </div>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Фотографии с тура по Санкт-Петербурга</a></p>
                    <p>Короткая аннотация к альбому, которая растягивается на две строки</p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt empty-cnt">
                    <a href="#"></a>
                </div>
            </li>
        </ul>
    </div>
    <ul class="portfolio-gallery">
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery1.jpg">
                    <img src="/img/img_gallery1.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery2.jpg">
                    <img src="/img/img_gallery2.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery3.jpg">
                    <img src="/img/img_gallery3.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery4.jpg">
                    <img src="/img/img_gallery4.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery5.jpg">
                    <img src="/img/img_gallery5.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery6.jpg">
                    <img src="/img/img_gallery6.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery7.jpg">
                    <img src="/img/img_gallery7.jpg" alt="photo1"/>
                </a>
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax" href="img_edit_popup.php">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove" href="#">Удалить</a>
                </div>
            </div>
        </li>
    </ul>
</div>
