<?php
$page = '';
include('header.php');
?>
    <div class="js-page" data-page="<?= $page ?>"></div>
    <div class="page-inner-cnt">
        <div class="page-inner-wrapper">
        <div class="inner-section">
            <div class="text-content">
                <h1>Интересный заголовок для статей и отдельных разделов не отображенных в отдельных макетах (заголовок H1)</h1>
                <p class="lead">Выделение параграфа текста. Представляется логичным, что PR абстрактен. Практика однозначно показывает, что узнаваемость марки отталкивает PR, не считаясь с затратами</p>
                <p>Размещение масштабирует медиаплан, <a href="#">не считаясь (ссылка в тексте)</a> с затратами. В соответствии с законом Мерфи, молодежная аудитория откровенно цинична (посещенная ссылка). Продвижение проекта директивно стабилизирует пул лояльных изданий.</p>
                <p>К тому же рекламное сообщество оправдывает из ряда вон выходящий повторный контакт. Интеграция, на первый взгляд, <a href="">тормозит</a> стратегический социальный статус, размещаясь во всех медиа. Охват аудитории ускоряет конвергентный медиамикс</p>
                <h2>Медийный канал, пренебрегая деталями (заголовок H2)</h2>
                <div class="full-width-img">
                    <img src="/img/img_styles1.jpg" alt="styles1"/>
                </div>
                <h3>Медийный канал, пренебрегая деталями (заголовок H2)</h3>
                <ul>
                    <li>Интеграция, на первый взгляд, тормозит стратегический социальный статус, размещаясь во всех медиа</li>
                    <li>Охват аудитории ускоряет конвергентный медиамикс<sup>1</sup></li>
                </ul>
                <cite class="under">1. Емкость рынка упорядочивает повседневный клиентский спрос. Партисипативное планирование определяет план размещения.</cite>
                <ol>
                    <li>Концепция развития концентрирует конвергентный<i class="js-info info-trigger" title="Партисипативное планирование определяет план размещения"></i></li>
                    <li>Взаимодействие корпорации и клиента усиливает анализ рыночных цен</li>
                </ol>
                <img src="/img/img_styles2.jpg" alt="styles2"/>
                <h4>Почему недостижимо стимулирование сбыта? Практика однозначно показывает (заголовок H4)</h4>

                <p>Стратегический рыночный план сбалансирован. SWOT-анализ правомочен. Департамент маркетинга и продаж индуцирует креативный презентационный материал.</p>

                <p>VIP-мероприятие наиболее полно отражает повседневный бизнес-план. В рамках концепции Акоффа и Стэка, диверсификация бизнеса упорядочивает сублимированный традиционный канал, оптимизируя бюджеты. Нишевый проект одновременно притягивает conversion rate. Стратегическое планирование решительно притягивает инвестиционный продукт. Продвижение проекта, не меняя концепции, изложенной выше, специфицирует из ряда вон выходящий план размещения.</p>

                <p>заимодействие корпорации и клиента, суммируя приведенные примеры, притягивает бизнес-план. Адекватная ментальность непосредственно определяет PR. Перераспределение бюджета естественно притягивает комплексный системный анализ, осознав маркетинг как часть производства. </p>

                <p>Концепция развития концентрирует конвергентный медиабизнес. Взаимодействие корпорации и клиента усиливает анализ рыночных цен. Имидж экономит диктат потребителя. Стратегия сегментации нейтрализует рыночный продукт.</p>
                <h2>Цитаты и выделение в тексте</h2>
                <p>Стратегический рыночный план сбалансирован. SWOT-анализ правомочен. Департамент маркетинга и продаж индуцирует креативный презентационный материал.</p>
                <cite>
                    Нишевый проект одновременно притягивает conversion rate. Стратегическое планирование решительно притягивает инвестиционный продукт. Продвижение проекта, не меняя концепции, изложенной выше, специфицирует из ряда вон выходящий план размещения»<span>Андреев Иван Иванович — менеджер по продажам</span>
                </cite>
                <p>VIP-мероприятие наиболее полно отражает повседневный бизнес-план. В рамках концепции Акоффа и Стэка, диверсификация бизнеса упорядочивает сублимированный традиционный канал, оптимизируя бюджеты.</p>
                <blockquote>
                    Метод изучения рынка индуцирует рекламоноситель. Маркетинг продуцирует пресс-клиппинг. Потребительская культура выражена наиболее полно. Еще Траут показал, что рекламоноситель конкурентоспособен. Креативная концепция решительно искажает конструктивный рейтинг, полагаясь на инсайдерскую информацию.
                </blockquote>
                <div class="tabs-cnt">
                    <ul class="js-panes-control tabs-list" data-panes="tabs">
                        <li class="js-tab-item active">
                            <a class="js-pane-trigger active" href="#tab1">Первый таб</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#tab2">Второй таб</a>
                        </li>
                        <li class="js-tab-item">
                            <a class="js-pane-trigger" href="#tab3">Третий таб</a>
                        </li>
                    </ul>

                    <div class="js-common-panes" id="tabs">
                        <div class="js-pane active" id="tab1">
                            <h4>Содержимое первого таба</h4>
                            <p>Фосфоритообразование убывающе варьирует метаморфический голоцен. Капиллярное поднятие поступает в фитолитный трог. Тем не менее, нужно учитывать и то обстоятельство, что коллювий растрескан.</p>
                            <p>Неоцен фиксирован. Магма, используя геологические данные нового типа, причленяет к себе лакколит. Рифт полого обедняет триас, что, однако, не уничтожило доледниковую переуглубленную гидросеть древних долин. Текстура достаточно хорошо аккумулирует базис эрозии. Исток залегает в триас. Выклинивание существенно обедняет надвиг.</p>
                        </div>
                        <div class="js-pane" id="tab2">
                            <h4>Содержимое второго таба</h4>
                            <p>По сути, оледенение высвобождает цокольный коммунизм. Политическая коммуникация систематически слагает классический мусковит. Понятие политического участия отражает межпластовый парагенезис. Политический процесс в современной России теоретически опускает кислый бихевиоризм, подчеркивает президент. Управление политическими конфликтами, так же, как и в других регионах, верифицирует теоретический авгит. Наконец, понятие политического участия сбрасывает третичный континентально-европейский тип политической культуры, впрочем, не все политологи разделяют это мнение.</p>
                        </div>
                        <div class="js-pane" id="tab3">
                            <h4>Содержимое третьего таба</h4>
                            <p>Политическое манипулирование поднимает электрон. Самосогласованная модель предсказывает, что при определенных условиях элювиальное образование теоретически доказывает резонатор. Как уже подчеркивалось, политическая модернизация отчетливо и полно выталкивает погранслой. Правовое государство, согласно традиционным представлениям, разнонаправленно. Магматическая дифференциация верифицирует экситон. Политическое учение Аристотеля излучает адронный субъект политического процесса.</p>
                            <p>Принимая во внимание позицию Ф.Фукуямы, осциллятор излучает грубообломочный субъект политического процесса. Как писал С.Хантингтон, глобализация отражает гейзер. Политическое учение Руссо, вследствие квантового характера явления, фактически переоткладывает квантовый трог так, как это могло бы происходить в полупроводнике с широкой запрещенной зоной.</p>
                            <p>Англо-американский тип политической культуры, разделенные узкими линейновытянутыми зонами выветрелых пород, стягивает стеклянный авторитаризм в полном соответствии с законом сохранения энергии. Иначе говоря, тело переоткладывает лазер. Вихрь, однако, квантово разрешен.</p>
                        </div>
                    </div>
                </div>
                <div class="accordeon-cnt">
                    <div class="accordeon-item">
                        <a class="accordeon-trigger" href="#">Первый пункт аккордеона</a>
                        <div class="accordeon-inner">
                            <div class="accordeon-content">
                                <p>Дифференциальное уравнение учитывает нестационарный кожух, при котором центр масс стабилизируемого тела занимает верхнее положение. Необходимым и достаточным условием отрицательности действительных частей корней рассматриваемого характеристического уравнения является то, что кинематическое уравнение Эйлера характеризует угол крена, поэтому энергия гироскопического маятника на неподвижной оси остаётся неизменной.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordeon-item">
                        <a class="accordeon-trigger" href="#">Второй пункт аккордеона</a>
                        <div class="accordeon-inner">
                            <div class="accordeon-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cupiditate deleniti, eos laudantium maiores maxime odit quas suscipit vero. Ad dolor est expedita iure nisi quae quos sequi tempore voluptates.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordeon-item">
                        <a class="accordeon-trigger" href="#">Третий пункт аккордеона</a>
                        <div class="accordeon-inner">
                            <div class="accordeon-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi asperiores dignissimos dolorum id minima nam optio quam? Architecto asperiores ea fugiat libero, porro praesentium quis repellendus sunt. Autem reprehenderit, similique?</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="common-form-cnt">
                    <form action="">
                        <div class="row">
                            <div class="col12">
                                <fieldset>
                                    <input type="radio" class="css-checkbox" name="radio" id="radio1"/><label class="css-label" for="radio1">Радиобатн не отмечен, и имеет описание больше двух строчек. И много полезной информации</label>
                                </fieldset>
                                <fieldset>
                                    <input disabled type="radio" class="css-checkbox disabled" name="radio" id="radio2"/><label class="css-label" for="radio2">Радиобатн не активен</label>
                                </fieldset>
                                <fieldset>
                                    <input checked type="radio" class="css-checkbox" name="radio" id="radio3"/><label class="css-label" for="radio3">Радиобатн отмечен</label>
                                </fieldset>
                                <fieldset class="error">
                                    <input type="radio" class="css-checkbox" name="radio" id="radio4"/><label class="css-label" for="radio4">Ошибка в пункте</label>
                                </fieldset>
                            </div>
                            <div class="col12">
                                <fieldset>
                                    <input checked type="checkbox" class="css-checkbox" name="check[]" id="check1"/><label class="css-label" for="check1">Чекбокс отмечен</label>
                                </fieldset>
                                <fieldset>
                                    <input disabled type="checkbox" class="css-checkbox disabled" name="check[]" id="check2"/><label class="css-label" for="check2">Чекбокс не активен</label>
                                </fieldset>
                                <fieldset>
                                    <input type="checkbox" class="css-checkbox" name="check[]" id="check3"/><label class="css-label" for="check3">Чекбокс не отмечен, и имеет описание больше двух строчек. И много полезной информации</label>
                                </fieldset>
                                <fieldset class="error">
                                    <input type="checkbox" class="css-checkbox" name="check[]" id="check4"/><label class="css-label" for="check4">Ошибка в пункте</label>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col12">
                                <fieldset>
                                    <label for="field1">Номер телефона*</label>
                                    <input type="text" name="field1" id="field1" placeholder="+7 917 654-15-88"/>
                                </fieldset>
                            </div>
                            <div class="col12">
                                <fieldset class="error">
                                    <label for="field2">Ошибка</label>
                                    <input type="text" name="field2" id="field2" placeholder="Ваше имя"/>
                                    <ul class="errors-list filled">
                                        <li>Неправильно заполнено поле</li>
                                        <li>Вторая ошибка</li>
                                    </ul>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col12">
                                <fieldset>
                                    <label for="text">Комментарий</label>
                                    <textarea name="text" id="text"></textarea>
                                </fieldset>
                            </div>
                        </div>
                        <fieldset>
                            <a class="btn" href="#">a.btn</a>
                            <a class="btn disabled" href="#">a.btn.disabled</a>
                            <input type="submit" value="input:submit"/>
                            <input type="submit" disabled value="input:submit:disabled"/>
                            <button>button</button>
                            <button disabled>button:disabled</button>
                        </fieldset>
                    </form>
                </div>
                <div class="files-list">
                    <a href="file.doc">Документ MS Office <span>DOC, 2 Mb</span></a>
                    <a href="file.pdf">Документ dobe acrobat <span>PDF, 2 Mb</span></a>
                    <a href="file.xls">Таблица MS Office <span>XLS, 2 Mb</span></a>
                    <a href="file.rar">Файл архив <span>RAR, 2 Mb</span></a>
                    <a href="file.zip">Файл архив <span>ZIP, 2 Mb</span></a>
                    <a href="file.txt">Остальные файлы <span>120 kb</span></a>
                </div>
                <div class="table-cnt">
                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Первый пункт</th>
                        <th>Второй пункт</th>
                        <th>Третий пункт</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Первый пункт</td>
                        <td>Первый пункт</td>
                        <td>Первый пункт</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Второй пункт</td>
                        <td>Второй пункт</td>
                        <td>Второй пункт</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Третий пункт</td>
                        <td>Третий пункт</td>
                        <td>Третий пункт</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Четвертый пункт</td>
                        <td>Четвертый пункт</td>
                        <td>Четвертый пункт</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Пятый пункт</td>
                        <td>Пятый пункт</td>
                        <td>Пятый пункт</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        </div>
    </div>

<?php
include('footer.php');
?>