<?php
    include('header_index.php');
?>

<div class="full-sized-site-inner">
    <div class="welcome-block">
        <h1 class="header">Добро пожаловать в Knackit</h1>
        <p>Ежеминутно тысячи людей по всему миру ищут возможность <br/>
            мгновенного сотрудничества</p>
        <p>Knackit помогает найти актуальные предложения поблизости</p>
        <p>Создайте страницу - зарабатывайте, оказывая услуги</p>
        <a class="btn register-btn js-register-popup" href="#register-popup">Регистрация</a>
    </div>
</div>


<?php
    include('footer.php');
?>