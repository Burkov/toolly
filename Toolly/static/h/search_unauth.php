<?php
$page = 'search';

    include('header_unauth.php');
?>

<div class="full-sized-site-inner">
    <aside class="search-results-cnt">
        <a class="search-results-trigger js-search-results-trigger" href="#"></a>
        <div class="search-results-inner">
            <div class="search-tags-cnt">
                <div class="tags-group tags-request">
                    <p class="tags-group-header">Категории по запросу</p>
                    <ul class="tags-list">
                        <li class="tag-item">Туризм<a class="tag-remove js-tag-remove" href="#"></a></li>
                        <li class="tag-item">Навигация<a class="tag-remove js-tag-remove" href="#"></a></li>
                        <li class="tag-item">Экскурсии<a class="tag-remove js-tag-remove" href="#"></a></li>
                        <li class="tag-item">Тур<a class="tag-remove js-tag-remove" href="#"></a></li>
                    </ul>
                </div>
                <div class="tags-group tags-fine">
                    <p class="tags-group-header">Уточнить</p>
                    <ul class="tags-list">
                        <li class="tag-item js-tag-fine">Санкт-Петербург</li>
                        <li class="tag-item js-tag-fine">Москва</li>
                        <li class="tag-item js-tag-fine">Магазин</li>
                        <li class="tag-item js-tag-fine">Коммерция</li>
                        <li class="tag-item js-tag-fine">Торговый центр</li>
                        <li class="tag-item js-tag-fine">Магазин</li>
                    </ul>
                </div>
            </div>
            <div class="search-results scrollable left-scroll">
                <ul class="search-results-list">
                    <li class="search-result-item">
                        <aside class="item-actions">
                            <div class="actions-block">
                                <div class="logo-cnt">
                                    <img src="/img/img_sr_logo1.jpg" alt="logo1"/>
                                </div>
                                <ul class="actions-list">
                                    <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                    <li class="action-item"><a class="action-add-user js-action" data-action="add-user" href="#"></a></li>
                                    <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                    <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                </ul>
                            </div>
                            <a class="btn btn-small js-search-popup fancybox.ajax" href="order_service_popup.php">Заказать услугу</a>
                        </aside>
                        <div class="item-descr">
                            <div class="descr-header">
                                <div class="item-rating">9.8</div>
                                <div class="item-name">
                                    <a href="#">Экскурсии в Санкт-Петербурге</a>
                                </div>
                            </div>
                            <div class="person-status person-offline">Константин Константинов</div>
                            <ul class="item-services-list">
                                <li>
                                    <span>Экскурсии по Санкт-Петербургу</span>
                                    <span>50 000 руб.</span>
                                </li>
                                <li>
                                    <span>Новогодние и рождественские туры по Санкт-Петербургу</span>
                                    <span>23 000 руб.</span>
                                </li>
                                <li>
                                    <span>Бронирование отелей</span>
                                    <span>500 руб.</span>
                                </li>
                                <li>
                                    <span>Тур от</span>
                                    <span>5 000 руб.</span>
                                </li>
                            </ul>
                            <div class="item-expander">
                                <a class="item-expand js-item-expand" href="#"></a>
                            </div>
                        </div>
                    </li>

                    <li class="search-result-item">
                        <aside class="item-actions">
                            <div class="actions-block">
                                <div class="logo-cnt">
                                    <img src="/img/img_sr_logo2.jpg" alt="logo1"/>
                                </div>
                                <ul class="actions-list ">
                                    <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                    <li class="action-item"><a class="action-add-user js-action" data-action="add-user" href="#"></a></li>
                                    <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                    <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                </ul>
                            </div>
                            <a class="btn btn-small" href="#">Заказать услугу</a>
                        </aside>
                        <div class="item-descr">
                            <div class="descr-header">
                                <div class="item-rating">9.8</div>
                                <div class="item-name">
                                    <a href="#">Экскурсии в Санкт-Петербурге</a>
                                </div>
                            </div>
                            <div class="person-status">Константин Константинов</div>
                            <ul class="item-services-list">
                                <li>
                                    <span>Экскурсии по Санкт-Петербургу</span>
                                    <span>50 000 руб.</span>
                                </li>
                                <li>
                                    <span>Новогодние и рождественские туры по Санкт-Петербургу</span>
                                    <span>23 000 руб.</span>
                                </li>
                            </ul>
                            <div class="item-expander">
                                <a class="item-expand js-item-expand" href="#"></a>
                            </div>
                        </div>
                    </li>

                    <li class="search-result-item">
                        <aside class="item-actions">
                            <div class="actions-block">
                                <div class="logo-cnt">
                                    <img src="/img/img_sr_logo3.jpg" alt="logo1"/>
                                </div>
                                <ul class="actions-list ">
                                    <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                    <li class="action-item"><a class="action-add-user js-action" data-action="add-user" href="#"></a></li>
                                    <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                    <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                </ul>
                            </div>
                            <a class="btn btn-small" href="#">Заказать услугу</a>
                        </aside>
                        <div class="item-descr">
                            <div class="descr-header">
                                <div class="item-rating">9.6</div>
                                <div class="item-name">
                                    <a href="#">Купить туры по России</a>
                                </div>
                            </div>
                            <div class="person-status person-offline">Ирина Гамзина</div>
                            <ul class="item-services-list">
                                <li>
                                    <span>Бронирование отелей</span>
                                    <span>500 руб.</span>
                                </li>
                                <li>
                                    <span>Тур от</span>
                                    <span>5 000 руб.</span>
                                </li>
                            </ul>
                            <div class="item-expander">
                                <a class="item-expand js-item-expand" href="#"></a>
                            </div>
                        </div>
                    </li>
                    <li class="search-result-item">
                        <aside class="item-actions">
                            <div class="actions-block">
                                <div class="logo-cnt">
                                    <img src="/img/img_sr_logo3.jpg" alt="logo1"/>
                                </div>
                                <ul class="actions-list ">
                                    <li class="action-item"><a class="action-fold js-action" data-action="fold" href="#"></a></li>
                                    <li class="action-item"><a class="action-add-user js-action" data-action="add-user" href="#"></a></li>
                                    <li class="action-item"><a class="action-pin js-action" data-action="pin" href="#"></a></li>
                                    <li class="action-item"><a class="action-mail js-action" data-action="mail" href="#" title="Сообщение"></a></li>
                                </ul>
                            </div>
                            <a class="btn btn-small" href="#">Заказать услугу</a>
                        </aside>
                        <div class="item-descr">
                            <div class="descr-header">
                                <div class="item-rating">9.6</div>
                                <div class="item-name">
                                    <a href="#">Купить туры по России</a>
                                </div>
                            </div>
                            <div class="person-status person-offline">Ирина Гамзина</div>
                            <ul class="item-services-list">
                                <li>
                                    <span>Бронирование отелей</span>
                                    <span>500 руб.</span>
                                </li>
                                <li>
                                    <span>Тур от</span>
                                    <span>5 000 руб.</span>
                                </li>
                            </ul>
                            <div class="item-expander">
                                <a class="item-expand js-item-expand" href="#"></a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="mobile-map-cnt" data-lat="55.749266" data-long="37.568893">

                </div>
                <div class="search-view-switch-cnt mobile-show">
                    <a class="search-view-switch active js-search-view-switch" href="#" data-target="list">Список</a><a class="search-view-switch js-search-view-switch" href="#" data-target="map">Карта</a>
                </div>
            </div>
        </div>
    </aside>
    <div class="map-cnt" data-lat="55.749266" data-long="37.568893">

    </div>
</div>


<?php
    include('footer.php');
?>