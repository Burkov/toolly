<div class="inner-section">
    <div class="settings-section common-form-cnt">
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Тип аккаунта</p>
                <div class="profile-account-types">
                    <input type="radio" checked name="account-type" id="account-type-finder"/><label for="account-type-finder">Соискатель</label>
                    <input type="radio" name="account-type" id="account-type-taker"/><label for="account-type-taker">Предприниматель</label>
                </div>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="row">
                <div class="col12">
                    <fieldset>
                        <label for="account-page-name">Имя страницы</label>
                        <div class="input-group with-helper">
                            <input type="text" class="form-control" placeholder="Имя страницы..." value="pagename">
                                <span class="input-group-btn">
                                    <button class="btn" type="button">Ок</button>
                                </span>
                            <span class="helper">.knackit.com</span>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="profile-subsection personal-info">
            <fieldset>
                <label>Личная информация</label>
                <div class="row">
                    <div class="col12">
                        <input type="text" name="account-firstname" id="account-firstname" placeholder="Ваше имя"/>
                    </div>
                    <div class="col12">
                        <input type="text" name="account-lastname" id="account-lastname" placeholder="Ваша фамилия"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col12">
                        <span>Ваш пол:</span>
                        <input class="css-checkbox" checked type="radio" name="account-gender" id="account-gender-male"/><label for="account-gender-male" class="css-label">Мужской</label>
                        <input class="css-checkbox" type="radio" name="account-gender" id="account-gender-female"/><label class="css-label" for="account-gender-female">Женский</label>
                    </div>
                    <div class="col12">
                        <span>Дата рождения:</span>
                        <div class="date-input-cnt"><input class="date-input mobile-hide birth-date" type="text"/><input class="mobile-show" type="date"/></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="profile-subsection">
            <div class="row">
                <div class="col12">
                    <fieldset>
                        <label for="account-email">E-mail</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="account-email" id="account-email" placeholder="Введите электронный почтовый адрес">
                                <span class="input-group-btn">
                                    <button class="btn" type="button">Ок</button>
                                </span>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="profile-subsection">
            <div class="fieldset">
                <p class="label">Пароль</p>
                <p><a href="#change-pass-popup" class="change-pass-link js-changepass-popup">Изменить пароль</a></p>
            </div>
        </div>
        <div class="profile-subsection">
            <fieldset>
                <label>Привязка к номеру телефона</label>
                <div class="row">
                    <div class="col12">
                        <div class="input-group">
                            <input type="text" class="form-control" name="account-phone" id="account-phone" placeholder="телефонный номер">
                                        <span class="input-group-btn">
                                            <button class="btn" type="button">Ок</button>
                                        </span>
                        </div>
                    </div>
                    <div class="col12">
                        <div class="input-group">
                            <input type="text" class="form-control" name="account-check-pass" id="account-check-pass" placeholder="Введите пароль проверки">
                                        <span class="input-group-btn">
                                            <button class="btn" type="button">Ок</button>
                                        </span>
                        </div>
                    </div>
                </div>
                <p class="meta">К Вам на телефон в ближайшее время поступит sms с кодом проверки. </p>
            </fieldset>
        </div>
        <fieldset class="submit-cnt">
            <input type="submit" value="Сохранить"/><p class="meta">Все поля обязательны для заполнения</p>
        </fieldset>
    </div>
</div>
