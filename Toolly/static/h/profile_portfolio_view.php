<div class="inner-section">
    <div class="portfolio-catalog-slider-cnt">
        <ul class="portfolio-gallery catalog-gallery">
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#">
                        <img src="/img/img_portfolio1.jpg" alt=""/>
                    </a>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Фотографии с тура по Санкт-Петербурга</a></p>
                    <p>Короткая аннотация к альбому</p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Изготовление логотипов</a></p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Фотографии с тура по Санкт-Петербурга</a></p>
                    <p>Короткая аннотация к альбому, которая растягивается на две строки</p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Изготовление логотипов</a></p>
                </div>
            </li>
            <li class="portfolio-item">
                <div class="img-cnt">
                    <a href="#"><img src="/img/img_portfolio1.jpg" alt=""/></a>
                </div>
                <div class="portfolio-item-descr">
                    <p class="header"><a href="#">Фотографии с тура по Санкт-Петербурга</a></p>
                    <p>Короткая аннотация к альбому, которая растягивается на две строки</p>
                </div>
            </li>
        </ul>
    </div>
    <ul class="portfolio-gallery">
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery1.jpg">
                    <img src="/img/img_gallery1.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery2.jpg">
                    <img src="/img/img_gallery2.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery3.jpg">
                    <img src="/img/img_gallery3.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery4.jpg">
                    <img src="/img/img_gallery4.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery5.jpg">
                    <img src="/img/img_gallery5.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery6.jpg">
                    <img src="/img/img_gallery6.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
        <li class="portfolio-item">
            <div class="img-cnt">
                <a class="js-gallery-image" rel="gallery1" href="/img/img_gallery7.jpg">
                    <img src="/img/img_gallery7.jpg" alt="photo1"/>
                </a>
            </div>
        </li>
    </ul>
</div>
