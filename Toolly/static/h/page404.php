<?php
$page = 404;
    include('header_404.php');
?>

<div class="full-sized-site-inner page404-inner">
    <div class="page404-content">
        <div class="page404-img">
            <img src="/img/img_404.jpg" alt="page404"/>
        </div>
        <div class="text-content page404-text">
            <p class="header">Страница не найдена</p>
            <p>Если вы ввели адрес вручную в адресной строке браузера, проверьте, <br/>
                всё ли вы написали правильно.
            </p>
            <p>Если вы пришли по ссылке с другого ресурса, попробуйте перейти на <br/>
                <a href="/">главную страницу</a> или <a href="#">карту сайта</a>,  вполне вероятно, что там вы найдёте нужный вам материал.
            </p>
        </div>
    </div>
</div>


<?php
    include('footer.php');
?>