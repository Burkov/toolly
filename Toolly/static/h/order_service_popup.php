<div class="popup-cnt search-popup">
    <form action="">
    <div class="row">
        <div class="logo-cnt">
            <div class="img-cnt">
                <img src="/img/img_sr_logo1.jpg" alt="logo"/>
            </div>
        </div>
        <div class="contact-card">
            <div class="contact-avatar-cnt">
                <div class="img-cnt"><img src="/img/img_avatar3.jpg" alt="avatar"/></div>
            </div>
            <div class="contact-inner">
                <p class="contact-name"><a href="#">Ирина <strong>Самаркина</strong></a></p>
                <div class="person-status person-offline">Offline</div>
            </div>
        </div>
        <div class="service-date-cnt mobile-show">
            <label for="service-date">Желаемая дата</label>
            <input type="date" class="service-date" name="service-date" id="service-date"/>
        </div>
        <div class="service-name-cnt">
            <label for="service-name">Услуга</label>
            <select name="service-name" id="service-name" class="single-select">
                <option value="1">Экскурсии в Санкт-Петербурге</option>
                <option value="2">Бронирование авиарейсов</option>
                <option value="3">Путевки по «Золотому кольцу»</option>
            </select>
        </div>
        <div class="service-date-cnt mobile-hide">
            <label for="service-date">Желаемая дата</label>
            <input type="text" class="date-input service-date" name="service-date" id="service-date"/>
        </div>
        <div class="service-price-cnt">
            <label for="service-price">Выбрать из прайса</label>
            <select name="service-price" id="service-price" class="multiple-select" multiple>
                <option value="0">Все</option>
                <option value="1">Экскурсии в Санкт-Петербурге</option>
                <option value="2">Бронирование авиарейсов</option>
                <option value="3">Путевки по «Золотому кольцу»</option>
            </select>
        </div>
    </div>
    <div class="service-comment-cnt">
        <textarea name="service-comment" id="service-comment" placeholder="Текст комментария..."></textarea>
        <div class="smile-trigger js-smile-trigger">
            <div class="smiles-panel">
                <ul class="smiles-list">
                    <li class="js-smile smile-item"><i class="icon-emo-happy"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-wink"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-wink2"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-unhappy"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-sleep"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-thumbsup"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-devil"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-surprised"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-tongue"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-coffee"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-sunglasses"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-displeased"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-beer"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-grin"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-angry"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-saint"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-cry"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-shoot"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-squint"></i></li>
                    <li class="js-smile smile-item"><i class="icon-emo-laugh"></i></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="service-submit-cnt">
        <input type="submit" value="Заказать услугу"/>
        <label class="file-label">Прикрепить файл<input type="file" name="service-file" id="service-file"/></label>
    </div>
    </form>
</div>