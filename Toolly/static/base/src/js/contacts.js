define(['jquery', 'underscore', 'Backbone', render, global_context, client_state, temp_state], function($, _, Backbone, render, global_context, client_state, temp_state){

	var ContactView = new Backbone.View.extend({
		template: function(){/*
	        <p>{{conent}}{{#is_owner}}<span class="lk-plus">+</span>{{/is_owner}}</p>
	        {{#is_owner}}{{#editable}}<p><a class="lk-change-link" href="#">Изменить</a></p>{{/editable}}
		*/},
		initialize: function(options){
			this.contactsView = options.contactsView;

			// event handlers - switch editability


			this.$el.addClass('cont-type-inner');


		},
		render: function(editable){
			var newHtml = render(this.template, {content: this.model.get("content"), editable: client_state.editable})
			this.$el.html(newHtml);
		},
		events: {
			"click a.lk-change-link editContact",
			"click a.lk-delete-link discardContact" //TODO!!!
		},
		editContact: function(e){
			// check that editFocus is false
			// re-render
		},
		discardContact: function(e){
			// check that editFocus is false
			// delete contactModel from server
			// on .done() remove contactView from contactsView and contactModel from contactsCollection
			// destroy contactView
			// remove this contact type, if necessary
		}		
	});

	var ContactModel = new Backbone.Model.extend({
        setId: function(model, response, options){
            model.set("id", response.id);
        }
	});

	var ContactTypeView = new Backbone.View.extend({
		template: function(){/*
        	<i {{#is_owner}}class="cont-type-status"{{/is_owner}}></i>
		*/},
		tagName: "li",
		className: "cont-type-item", // TODO add {{contact_type}} class to className upon creation
		initialize: function(options){
			this.contactType = options.contactType;
			this.contactsView = options.contactsView;
			this.contactViews = options.contactViews || [];
			this.display = "" // value of the display property

			this.$el.addClass(this.contactType);
		},
		render: function(){
			var newHtml = render(this.template, {is_owner: global_context: is_owner});
			this.$el.html(newHtml);
			for (var contactView in this.contactViews[contactType]){
				this.$el.append(contactView.$el);
				contactView.render();
			}
		},
		cache_display_value: function(){
			this.display = this.$el.css("display");
		},
		events: {
			"click span.lk-plus createContact",
		},
		createContact: function(e){
			if (temp_state.editFocus) { // check that editFocus is false
				alert("Another element is being edited. Finish editing first, before creating a new contact.\nВы начали редактировать другой элемент. Прежде чем создать новый контакт, завершите редактирование.")
				return;
			}
			var contactModel = new ContactModel({type: this.contactType, content: ""}, {collection: this.contactsView.collection});

		},		
	});

	var ContactsView = new Backbone.View.extend({
		className: "inner-section",
		initialize: function(options){
			var innerTemplate = function(){/*
                <p class="section-header">Контакты</p>
                <ul class="lk-cont-types-list {{editable}}">
                </ul>
			*/}
			this.$el.html(render(innerTemplate, {editable: client_state.editable}));

			this.contactTypeViews = {}; // grouped by type

			for (var contactType in global_context.contact_types){
				// create contactTypeViews for each contactType
				newContactTypeView = new ContactTypeView({contactType:contactType, contactsView:this});
				this.contactTypeViews[contactType] = newContactTypeView;

				// create contactViews for existing contact types, render them
				this.collection.forEach(function(contactModel) {
					if (contactModel.get("type") === contactType) {
						var newContactView = new ContactView({
	                    	model: contactModel,
	                    	tagName: 'div',
	                    	className: 'cont-type-inner',
	                    	id: contactModel.get("id"),
	                    	contactTypeView: newContactTypeView
						});
						newContactTypeView.push(newContactView);
					}
				});

				newContactTypeView.render();
				newContanctTypeView.cache_display_value();
				// set display: none if necessary
				if (client_state.editable === false and newContactTypeView.contactViews.length > 0){
					newContactTypeView.$el.css("display", "none");
					// TODO whether this contact type is default?
				}
			}
			// when profile view/edit mode switched, repaint self			
			if (global_context.is_owner) client_state.on("editable", this.toggleEditable);
		},
		events: {

		},
		toggleEditable: function(editable){
			if (editable) {
				this.$('ul').addClass("editable");
				// find all <li> nodes of contactTypes without contacts in them and remove display: none; from their style
				// upon toggle of editability, create a new list of <li> nodes corresponding to contactTypes
				// if <li> already exists, add it to the list, else, create a new node
				// populate <ul> with them
				var liNodes = [];
				for (var contactType in global_context.contact_types){
					if (this.$('ul > li.cont-type-item.' + contactType).length > 0){ // if <li> node already exists, push it to the list
						var contactTypeNode = this.$('ul > li.cont-type-item.' + contactType).get(0);
						liNodes.push(contactTypeNode);
					}
					else { // if <li> doesn't exist yet, create it
						var contactTypeNode = $.parseHtml(render(ContactTypeTemplate, {contact_type: contactType, is_owner: global_context: is_owner}));
						liNodes.push(contactTypeNode);
					}
				}
			}
			else { // if user profile page in view mode
				this.$('ul').removeClass("editable");
			}
			this.render(); // so that each view re-renders itself
			// TODO attach listenTo's to each contactType <li> node's interactable elements

		},
	});

	var ContactsCollection = new Backbone.Collection.extend({
		model: ContactModel,
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output;
        },

	});

	if (global_context.hasOwnProperty("contacts_url")){
		var contactsCollection = new ContactsCollection();
		contactsCollection.url = global_context.contacts_url;
		contactsCollection.fetch();
		contactsCollection.comparator = 'id'; 
		contactsCollection.sort(); // first digits of mongodb ids are the timestamp, thus sorting by id provides chronological order
	}
});