$(document).ready(function(){

    var ImageModel = Backbone.Model.extend({
        initialize: function(){
        },
        /*
         * @param data {FormData} - FormData, received from form with <input type="file" name="image_file"> 
         *  that contains user-selected image.
         */     
        save: function(data){
            // first create File object in the database {"name": "something"}
            var result = this.createFileObject();
            // bind postFileData to this and data
            result.done(_.bind(function(received_data){
                this.postFileData(received_data, data);
            }, this));
        },
        createFileObject: function(){
            var result = $.ajax({
                url: this.url(),
                type: 'post',
                data: JSON.stringify({ 'name': 'No name' }),
                contentType: 'application/json'
            });
            return result;
        },
        postFileData: function(received_data, data){
            this.set("id", received_data.id);
            this.set("name", 'No name');
            // then populate the "id" field of that File object with content of the real file
            var result = $.ajax({
                url: this.url()+"/"+"fs",
                type: 'put',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
            });
            // after posting the image, add this model to collection; collection.add
            // triggers callback, ImageUploadModalView.onUploadDone, set in ImageUploadModalView.onUpload
            result.done(_.bind(function() {this.collection.add(this)}, this)); 
            
            return result;
        },
        imageSrc: function(){
            // extract album_id and image_id from this.url() and create imageSrc path from them         
            var urlComponents = this.url().replace(/([^\/])$/, '$1').split("/");
            var imageSrcPathEnding = urlComponents[urlComponents.length-3] + "/" + urlComponents[urlComponents.length-1] + "/";
            var imageSrcPath = window.location.host + '/img/' + global_context.owner + "/" + imageSrcPathEnding;
            return imageSrcPath;
        }
    });

    var ImageViewTemplate = templateFromFunction(function(){/*
      <div class="col-lg-3 col-md-4 col-xs-6 thumb">
          <a class="thumbnail" href="http://{{url}}" data-gallery> <!-- TODO: make href=# for gallery when ready -->
              <img class="img-responsive" src="http://{{url}}" alt="">
          </a>
      </div>*/});

    var ImageView = Backbone.View.extend({
        render: function(){
            //alert("ImageView.model.attributes = " + JSON.stringify(this.model.attributes));
            //alert("ImageView.model.collection = " + JSON.stringify(this.model.collection));
            this.$el.html(render(ImageViewTemplate, {url: this.model.imageSrc()}));
            return this;
        }
    });

    var ImageUploadModalElementTemplate = templateFromFunction(function(){/*
<div class="modal fade" id="imageUploadModal" tabindex="-1" role="dialog" aria-labelledby="imageUploadModalLabel" aria-hidden="true">  
</div> <!-- modal-dialog -->
    */});

    var ImageUploadModalTemplate = templateFromFunction(function(){/*
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
        <h4 class="modal-title" id="imageUploadModalLabel">Загрузить изображение:</h4>
      </div>
      <div id="imageUploadModalBody" class="modal-body next">
        <form method="post" action="{{url}}" id="portfolio-upload-form" enctype="multipart/form-data">
        <input type="file" name="image_file" class="image_file" />
        </form>
      </div><!-- modal body -->
      <div class="modal-footer">
        <button type="submit" form="portfolio-upload-form" class="btn btn-primary portfolio-upload-btn" name='portfolio-upload-btn' value='portfolio-upload-btn'>Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>        
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
*/});

// We don't need this anymore
// WARNING! input 'value' is both DOM attribute and Javascript property. 
// Changing attr, you change prop, too. Changing prop  DOESN'T affect attr.
//      <input type="hidden" id="album_id" name="album_id" value="{{album_id}}" />

    var ImageUploadModalView = Backbone.View.extend({
        initialize: function(){
            var $element = $(ImageUploadModalElementTemplate);
            $('body').prepend($element);
            this.el = $("div#imageUploadModal").get(0);
            this.$el = $("div#imageUploadModal");
        },
        render: function(){
            var context;
            if (this.model !== undefined){ context = {url: this.model.collection.url()}}
            else {context = {url:""}}
            var newHtml = render(ImageUploadModalTemplate, context);
            this.$el.html(newHtml);
            return this;
        },
        events: {
            "submit #portfolio-upload-form": "onUpload"
        },
        onUpload: function(e){
            e.preventDefault();
            // use e.currentTarget or e.target to reference the DOM element that triggered this event
            console.log("onUpload: this = " + this);
            var data = new FormData(e.target); // or $('#gallery-upload-form').get(0)
            var imagesCollection = this.albumModel.get("files");
            var imageModel = new ImageModel({}, {collection:imagesCollection});
            this.listenToOnce(imagesCollection, "add", this.onUploadDone);
            imageModel.save(data);
            return false;
        },
        onUploadDone: function(model){
            //alert("In the beginning of onUploadDone");
            var imageView = new ImageView({model: model});
            this.albumView.imageViews.push(imageView);
            this.albumView.$('.images').append(imageView.$el);
            imageView.render();
            $('#imageUploadModal').modal('hide');
        }
    });

    var AlbumViewTemplate = templateFromFunction(function(){/*
            <div class="album" id="{{id}}"> 
              <div class="page-header">
                <h4>
                  {{#is_owner}}
                  <a href="#" class="album_title editable" id="service_name" data-type="text" data-pk="" data-name="name" data-url="{{url}}" data-original-title="Enter service name here">
                    {{title}}
                  </a>
                  {{/is_owner}}
                  {{^is_owner}}
                  {{title}}
                  {{/is_owner}}
                </h4>
              </div>
              <div class="images">
              </div>
              {{#is_owner}}
              <button type="button" class="btn btn-primary image-upload-modal-button">Загрузить изображение...</button>
              {{/is_owner}}
            </div>
    */});

    var ImagesCollection = Backbone.Collection.extend({
        model: ImageModel,
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output;
        }
    });
    // WARNING!!! Don't use .add() method of ImagesCollection unless you're
    // creating a new image with upload button. AlbumView's onUploadDone
    // is attached to the add event of the collection and will fire.
    var AlbumModel = Backbone.Model.extend({
        initialize: function(){
        },
        /*
        removeImage: function(image_url){
            $.ajax({
                url: image_url,
                type: 'post',
                data: {'_method': 'delete'},
                dataType: 'text',
                success: function(data, textStatus, jqXHR){
                    var removed_image = this.get("files").get(image_url);
                    this.get("files").remove(removed_image);
                }
            });
        },
        */
        setId: function(model, response, options){
            model.set("id", response.id);
        },
        toJSON: function(){
            return {title: this.get('title'), files: this.get("files").toJSON()};
        }
/*        save: function(){
            var serial = JSON.stringify(this.toJSON());
            alert(serial);
            $.ajax({
                url: this.url(),
                type: 'post',
                data: JSON.stringify(this.toJSON()),
                contentType: 'application/json',
                dataType: 'json',               
                success: function(data, textStatus, jqXHR){
                    alert("success!");
                }
            });
        },
*/
    });

    var AlbumView = Backbone.View.extend({
        initialize: function(){
            //this.listenTo(this.model.get("files"), "add", this.addImage);
            this.imageViews = [];
            for (var i=0;i<this.model.get("files").length;i++){
                var imageView = new ImageView({model: this.model.get("files").at(i)});
                this.imageViews.push(imageView);
            }
        },
        render: function(){
            var context = {
                "title": this.model.get("title"),
                "id": this.model.get("id"),
                "is_owner": global_context.is_owner,
                "url": this.model.url().replace(/([^\/])$/, '$1/') + "title/"
            };
            var newHtml = render(AlbumViewTemplate, context);
            this.$el.html(newHtml);
            // activate bootstrap-editable and attach signal handler to it
            this.$('a.album_title.editable').editable();
            var self = this; // to see the view inside event callback
            this.$('a.album_title.editable').on('render', function(e, editable){
                self.model.set('title', $(this).text());
            });
            // render all imageViews in the album
            for (var i=0;i<this.model.get("files").length;i++){
                this.$('.images').append(this.imageViews[i].$el);
                this.imageViews[i].render();
            }
            return this;
        },
        events: {
            "click .image-upload-modal-button": "show_upload_dialog",
            "submit #gallery-upload-form": "upload_image_to_album",
        },
        show_upload_dialog: function(e){
            imageUploadModalView.albumModel = this.model;
            imageUploadModalView.albumView = this;
            imageUploadModalView.render();
            $('#imageUploadModal').modal('show');
        },
        addImage: function(imageModel){
            var imageView = new ImageView({model: imageModel});
            this.$('.images').append(render(ImageViewTemplate, {src: imageModel.src}));
        }
    });


    var PortfolioTemplate = templateFromFunction(function(){/*
        <div id="albums">
        </div>
        <p></p>     
        {{#is_owner}}       
        <button id="create-album" type="button" class="btn btn-default">Создать альбом</button>
        {{/is_owner}}
    */});
    
    var PortfolioCollection = Backbone.Collection.extend({
        model: AlbumModel,
        initialize: function(){
            this.url = this.urlRoot; // portfolioCollection itself doesn't need url, but its AlbumModels do
        },
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, textStatus, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                // create array from files object
                var files = value.files;
                var files_models = [];
                var newImagesCollection = new ImagesCollection(); // we create it here and .set() later in order to set .collection property on each model
                for (var file_id in files){
                    var newImageModel = new ImageModel(files[file_id]);
                    newImageModel.set("id", file_id);
                    newImageModel.collection = newImagesCollection;
                    files_models.push(newImageModel);
                }
                newImagesCollection.set(files_models, {silent:true}); //don't fire event
                var newAlbum = new AlbumModel({files: newImagesCollection, title: value.title},
                                              {collection: this});
                newAlbum.set("id", key);
                newImagesCollection.url = newAlbum.url().replace(/([^\/])$/, '$1/') + "files/";
                models.push(newAlbum);
            }
            this.set(models);
        }
        //TODO MAKE SAVE METHOD THAT STORES THE WHOLE PortfolioCollection AS AN {}, NOT AS []
    });

    var PortfolioView = Backbone.View.extend({
        initialize: function(){
            var newHtml = render(PortfolioTemplate, {is_owner: global_context.is_owner});
            this.$el.html(newHtml);
            this.albumViews = [];
            for (var i=0;i<this.collection.length;i++){
                var newAlbumView = new AlbumView({model: this.collection.at(i)});
                this.albumViews.push(newAlbumView);
                this.$('#albums').append(newAlbumView.$el);
                newAlbumView.render();
            }
        },
        events:{
            "click #create-album": "createAlbum"
        },
        render: function(){
            for (var i=0;i<this.collection.length;i++){
                this.albumViews[i].render();
            }
            return this;
        },
        createAlbum: function(e){
            var newAlbum = new this.collection.model({title: "Альбом", files: new ImagesCollection()}, {collection: this.collection});
            this.collection.add(newAlbum);
            var response = newAlbum.save(null, {success:newAlbum.setId});
            response.done(_.bind(function(data){
                newAlbum.set("id", data.id);
                newAlbum.get("files").url = newAlbum.url().replace(/([^\/])$/, '$1/') + "files/";               
                var newAlbumView = new AlbumView({
                    model: newAlbum,
                    tagName: 'div',
                    className: 'album',
                    id: newAlbum.get("id")
                });
                this.albumViews.push(newAlbumView);
                this.$('#albums').append(newAlbumView.$el);
                newAlbumView.render();
            }, this));
        }
    });

    // "main" entry point

    if (global_context.hasOwnProperty("portfolio_url")){
        var imageUploadModalView = new ImageUploadModalView(); // to prepare dialog window
        var portfolioCollection = new PortfolioCollection([]);
        portfolioCollection.url = global_context.portfolio_url;

        portfolioCollection.fetch({}).done(function(){
            var portfolioView = new PortfolioView({collection: portfolioCollection, el: $("div#portfolio").get(0)});
            portfolioView.render();
        });
    }

});