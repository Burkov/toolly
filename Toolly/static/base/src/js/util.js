/**
 * This is a hack used to write multiline template html inside javascript.
 * Given a function, whose body is a commented html template, 
 * template_from_html extracts html from it and returns it as a string.
 * See: http://tomasz.janczuk.org/2013/05/multi-line-strings-in-javascript-and.html
 *
 * @param func {function} - function that contains commented-out html template
 */
function templateFromFunction(func){
	return func.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
}

/**
 * Renders template with Mustache and returns string with html.

 * @param template {string} Mustache-formatted template of created element.
 * @param context {Object} dictionary with variables to be inserted into the template.
 * @return {str} string with html
 */
function render(template, context){
    return Mustache.to_html(template, context);
}

/**
 * Creates and returns element, described by template and context, and inserts it into the document before or after each
 * element, selected by $(insertion_point).
 *
 * @param template {string} Mustache-formatted template of created element.
 * @param context {Object} dictionary with variables to be inserted into the template.
 * @param insertion_point {jquery selector}; if multiple elements are selected, inserts before/after each of them.
 * @param before_or_after {boolean} [before_of_after=false] true, if new element is inserted before insertion point, false if after.
 * @return newly created element
 */
function renderAndInsert(template, context, insertion_point, before_or_after){
    var newHtml = render(template, context);
    var $newElement = $(newHtml);
    if (before_or_after) {
        $(insertion_point).before($newElement);
    }
    else {
        $(insertion_point).after($newElement);
    }
    return $newElement;
}

/**
* Creates and returns element, described by template and context, and appends it to the contents of container element.
*
* @param template {string} Mustache-formatted template of created element.
* @param context {Object} dictionary with variables to be inserted into the template.
* @param container {Dom element or jquery selector} element, to which the newly created element should be appended.
* @return newly created element
*/
function renderAndAppend(template, context, container){
    var newHtml = render(template, context);
    var $newElement = $(newHtml);
    $(container).append($newElement);
    return $newElement;
}