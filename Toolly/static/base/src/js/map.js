/**
    Map widget with thubnail preview, and modal popup on click.
*/
$(function(){
    // add MapBox or our own tile layers
    function addTileLayers(map){
        L.mapbox.accessToken = 'pk.eyJ1IjoiYmxhY2t3aGFsZSIsImEiOiJJQTViMG1BIn0.-jSsQ86WpuMIqUERauTVdw';
        var mapboxTiles = L.tileLayer('https://{s}.tiles.mapbox.com/v3/blackwhale.ipl93iie/{z}/{x}/{y}.png');
        //MapBox connection (needs APIs see above)
        map.addLayer(mapboxTiles);
        //Our map server
        /*
        L.tileLayer.wms("http://mapper1.toolly.ru/mapcache/", {
            layers: 'default',
            format: 'image/png',
            transparent: true,
        }).addTo(map);
        */
    }

    var MapModel = Backbone.Model.extend({
        defaults:{
            latlon: [90.0, 0.0],
            popup: "Some html<b>!</b>"
        },

    });

    /**
        Custom event "enterMap".
    */
    var MapMiniView = Backbone.View.extend({
        tagName: "div",

        className: "",

        events: {
            "click #map" : 'enterMap'
        },

        initialize: function(hash){
            this.width = hash.width;
            this.height = hash.height;
            this.$el.html('<div id="map"></div>');
            this.$("#map").css({ width: this.width, height: this.height, cursor: 'pointer' });
            var map = L.map(this.$("#map").get(0), {
                zoomControl: false,
                attributionControl: false
            });
            map.dragging.disable();
            map.touchZoom.disable();
            map.doubleClickZoom.disable();
            map.scrollWheelZoom.disable();
            if (map.tap) map.tap.disable();
            addTileLayers(map);
            var marker = L.marker(this.model.get("latlon"));
            marker.addTo(map);
            map.setView(this.model.get("latlon"), 12);
            this.map = map;
            this.marker = marker;
            this.listenTo(this.model, "change", this.panToLocation)
        },

        enterMap: function(){
            console.info("enterMap event");
            this.model.trigger("enterMap");
        },

        updateMapSize: function(){
            this.map.invalidateSize();
        },

        panToLocation: function(){
            var loc = this.model.get("latlon");
            this.marker.setLatLng(loc);
            this.map.panTo(loc);
            return this;
        },

        render: function(){
            return this;
        }
    });

    var modalMapTmpl = templateFromFunction(function(){/*
    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="mapHeader" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
            <h4 class="modal-title" id="mapHeader">Координаты</h4>
          </div>
          <div id="mapBody" class="modal-body">
                <div id="map"></div>
          </div>
          {{#editable}}
          <div class="modal-footer">
            <div class="pull-left">
                <button type="button" class="btn btn-primary" id="save">
                  Сохранить
                </button>
                <i>
                    Уточните свое положение на карте перетащив подвижный маркер в желаемое место.
                </i>
            </div>
          </div>
          {{/editable}}
        </div>
      </div>
    </div>
    */});
    var ModalMapView = Backbone.View.extend({
        tagName: "div",
        className: "",

        events: {
            "click #save" : 'saveLocation'
        },

        initialize: function(hash){
            var modalH = $(window).innerHeight();
            modalH = modalH > 320 ? modalH - 240 : modalH - 20;
            this.height = modalH;
            this.editable = hash.editable;
            this.listenTo(this.model, "change", this.panToLocation);
            this.$el.html(Mustache.to_html(modalMapTmpl, { editable: this.editable }));
            this.map = L.map(this.$("#map").get(0), {
                attributionControl: false
            });
            addTileLayers(this.map);
            var marker;
            var _this = this;
            if(this.editable){
                marker = L.marker(this.model.get("latlon"), {draggable:'true'});
                marker.on('dragend', function(event){
                    var marker = event.target;
                    var position = marker.getLatLng();
                    _this.model.set({ latlon : [position.lat, position.lng] });
                });
            }
            else{
                marker = L.marker(this.model.get("latlon"));
            }            
            marker.addTo(this.map);
            this.map.setView(this.model.get("latlon"), 12);
            this.marker = marker;
            // issue AJAX to change model
            $.ajax({
                type: "GET",
                url: "/api/"+global_context.owner+"/profile/main/location/",
            }).done(function(data){
                if(data.length){
                    var loc = data[0].center;
                    console.log(data);
                    _this.model.set({ latlon: [loc[1], loc[0]]}) //lonlat --> latlon
                }
            });
        },

        updateMapSize:function(){
            this.$("#map").css({ height: this.height });
            this.$("#map").css({ width: this.$("#mapBody").width() });
            //TODO: may optimize by checking what is the net change in size
            this.map.invalidateSize();
            return this;
        },

        //
        panToLocation: function(){
            var loc = this.model.get("latlon");
            this.marker.setLatLng(loc, {draggable:'true'});
            this.map.panTo(loc);
        },


        saveLocation: function(){
            // use REST API
            console.log(global_context)
            var point = this.model.get("latlon");
            $.ajax({
                type: "PUT",
                url: "/api/"+global_context.owner+"/profile/main/location/0",
                data: JSON.stringify({
                    center: [point[1], point[0]],//lng, lat in elastic
                    shape: {
                        type: "Point",
                        coordinates: [point[1], point[0]]
                    }
                })
            });
            this.$("#mapModal").modal('hide');
        },

        render: function(){
            return this;
        }
    });

// Map widgets entry point
//======================================================
    var position = new MapModel({ latlon: [55.77, 37.7] });
    var view = new MapMiniView({ model:position, width:$("#Main").width(), height:265 });
    var modal;
    if(global_context.user_pk == global_context.owner_pk)
        modal = new ModalMapView({model: position, editable: true });
    else{
        modal = new ModalMapView({model: position, editable: false });
    }
    $("#Main").append($("<h3>Координаты</h3>"));
    $("#Main").append(view.render().$el);
    $("#Main").append($("<p></p>"));
    $("#Main").append(modal.render().$el);
    position.on("enterMap", function(){
        // leafleat requires extra draw command _after_ element is displyed in DOM
        modal.$("#mapModal").modal();
    })
    modal.$('#mapModal').on('shown.bs.modal', function() {
        modal.updateMapSize();
    });
    // leafleat requires extra draw command _after_ element is displyed in DOM
    view.updateMapSize();
})