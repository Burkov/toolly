$(function(){

    var TagView = Backbone.View.extend({
        template: templateFromFunction(function(){/*
            <span class="badge"><span class="tag-text">{{tag_name}}</span> x{{tag_multiplicity}} {{#is_owner}}<a href="#" class="discard-service-tag"><span class="glyphicon glyphicon-remove"></span></a>{{/is_owner}}</span>
        */}),
        className: 'tag',
        /**
         * Expected parameters:
         * @param options {object} e.g. {service: ServiceModel, tag_name: "NameOfTag", tag_multiplicity: 1}
         */
        initialize: function(options){
            this.service = options.service;
            this.service_view = options.service_view;            
            this.tag_name = options.tag_name;
            this.tag_multiplicity = options.tag_multiplicity;
        },
        render: function(){
            this.$el.css({'display': 'inline-block'});
            var newHtml = render(this.template, {
                tag_name: this.tag_name,
                tag_multiplicity: this.tag_multiplicity.toString(),
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);
        },
        events: {
            "click a.discard-service-tag": "discardTag"
        },
        discardTag: function(e){
            e.preventDefault();
            var self = this; // to propagate this into event handlers
            var tags = this.service.get("tags");
            tags.splice(tags.indexOf(this.tag_name), 1); // remove tag from service.attributes.tags
            this.service_view.tagViews.splice(this.service_view.tagViews.indexOf(this), 1) ;// remove tagView from tagViews

            $.ajax({
                type: "POST",
                url: global_context.discard_service_tag_url,
                data: JSON.stringify({id: this.service.id, tag: this.tag_name}),
                contentType: 'application/json; charset=utf-8',
            }).done(function(){
                // remove tag name from servicesCollection.tagMultiplicities
                self.service.collection.fetchMultiplicities().done(function(){
                    var tagMultiplicities = self.service.collection.tagMultiplicities;
                    // removed the following just not to think about it:
                    //if (tagMultiplicities[self.tag_name] === 0) {delete tagMultiplicities[self.tag_name];}
                    // remove tag from DOM
                    self.remove();
                });
            });
        }
    });

    var ServiceModel = Backbone.Model.extend({
        setId: function(model, response, options){
            model.set("id", response.id);
        }
    });
    
    var ServiceView = Backbone.View.extend({
        /**
         * @param options {object} contains single key {tagMultiplicities: ServicesCollection.tagMultiplicities}
         */
        template: templateFromFunction(function(){/*
            <div class="panel-heading"> 
              <h3 class="panel-title">
                {{#is_owner}}
                <a href="#" class="service_name editable" id="service_name" data-type="text" data-pk="" data-name="name" data-url="{{url}}" data-original-title="Enter service name here">
                  {{name}}
                </a>
                {{/is_owner}}
                {{^is_owner}}
                  {{name}}
                {{/is_owner}}
                {{#is_owner}}
                <button type="button" class="btn btn-xs btn-default pull-right discard-service"><span class="glyphicon glyphicon-remove"></span></button> 
                {{/is_owner}}
              </h3> 
            </div> 
            <div class="panel-body">
              {{#is_owner}}
              <form method="post" action="" class="services-tag-form"> 
                <div>
                  Добавить тег:
                  <input type="text" name="tag" class="form-control typeahead tag-input" placeholder="">
                  <button class="btn btn-primary">
                    <span class="glyphicon glyphicon-ok"></span>
                  </button>
                </div>       
              </form>
              {{/is_owner}}
              <BR>
              <div class="tag-box">
              </div>
            </div>
        */}),
        initialize: function(options){
            this.$el.addClass("panel panel-default service-panel");
            this.tagViews = [];
            // set view content only once upon initialize
            var newHtml = render(this.template, {
                name: this.model.get("name"),
                url: this.model.url().replace(/([^\/])$/, '$1/') + "name/",
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);
            // create tagViews for each tag in model
            this.tagViews = [];
            for (var i=0;i<this.model.get("tags").length;i++){
                var tagName = this.model.get("tags")[i];

                var tagMultiplicity = 1; // default if tag is not in tagMultiplicities
                if (tagName in options.tagMultiplicities){
                    tagMultiplicity = options.tagMultiplicities[tagName]; //TODO fix bugs with tagMultiplicities request!!!  
                }
                var newTagView = new TagView({service: this.model, service_view: this, tag_name: tagName, tag_multiplicity: tagMultiplicity});
                this.tagViews.push(newTagView);
                this.$(".tag-box").append(newTagView.$el);
            }
            // activate bootstrap-editable and attach signal handler to it
            this.$('a.service_name.editable').editable();
            var self = this; // to see the view inside event callback
            this.$('a.service_name.editable').on('render', function(e, editable){
                self.model.set('name', $(this).text());
            });
        },
        render: function(){
            for (var i=0;i<this.tagViews.length;i++){
                this.tagViews[i].render();
            }
        },
        events: {
            "submit form.services-tag-form": "addTag",
            "click button.discard-service": "discardService"
        },
        addTag: function(e){
            e.stopPropagation();
            e.preventDefault();
            var self = this; // for callbacks

            var newTagName = this.$("input.tag-input").val();

            if (!_.contains(this.model.get("tags"), newTagName)){ //prevent duplicate tags in the same service
                this.saveTag(newTagName).done(function(){
                    var tags = self.model.get("tags");
                    tags.push(newTagName);
                    self.model.collection.fetchMultiplicities().done(function(){
                        var newTagMultiplicity = 1; // if tag isn't in tagMultiplicities, set its multiplicity to 1 by default
                        if (newTagName in self.model.collection.tagMultiplicities){
                            newTagMultiplicity = self.model.collection.tagMultiplicities[newTagName];
                        }
                        var newTagView = new TagView({service: self.model, service_view: self, tag_name: newTagName, tag_multiplicity: newTagMultiplicity});
                        self.tagViews.push(newTagView);
                        self.$('div.tag-box').append(newTagView.$el);
                        newTagView.render();
                    });
                });
            }
            else {
                //TODO display an explicit warning about Tag duplication!!!
            }
        },
        saveTag: function(tag_name){
            var result = $.ajax({
                type: "POST",
                url: global_context.service_submit_tag_url,
                data: JSON.stringify({id: this.model.id, tag: tag_name}),
                contentType: 'application/json; charset=utf-8',
            });
            return result
        },
        discardService: function(e){
            var self = this; // to let .done callback see this
            $.ajax({
                type: "POST",
                url: global_context.discard_service_url,
                data: JSON.stringify({"id":this.model.id}),
                contentType: 'application/json; charset=utf-8'
            }).done(function(){
                //TODO write a destructor, preventing memory leaks
                self.remove();
            });
        }
    });
    
    var ServicesCollection = Backbone.Collection.extend({
        model: ServiceModel,
        initialize: function(){
            _.extend(this, Backbone.Events); // to trigger signals
        },
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, text_status, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                //each service is
                //{"name":"programmer", "tags":["html","css","js"], "points":["freelance: 2k$", "full-time: 3k$"], "extra":"text description"}
                var serviceModel = new ServiceModel({name: value.name,
                                                      tags: value.tags,
                                                      points: value.points,
                                                      extra: value.extra},
                                                      {collecion: this});
                serviceModel.set("id", key); //this should automatically set serviceModel.url() as well
                models.push(serviceModel);
            }
            this.set(models);
            this.comparator = 'id';
            this.sort();
        },
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output
        },
        fetchMultiplicities: function(){
            var self = this; // to reach this in event handler
            var allTags = _.flatten(this.map(function(service){
                return service.get('tags');
            })); // this "_.pluck"s tags from all services
            // TODO WHAT ABOUT DUPLICATES???
            if (allTags.length > 0){
                var tagParams = { tags: allTags };
                var result = $.ajax({
                    type: "GET",
                    url: "/get_tags/tags/tag",
                    data: $.param(tagParams, true)
                }).done(function(data){
                    self.tagMultiplicities = self.parseMultiplicities(data);
                    // trigger multiplicitiesFetched event
                    self.trigger("multiplicitiesFetched");
                });
                return result;
            }
            else{
                this.tagMultiplicities = {};
                // trigger multiplicitiesFetched event
                self.trigger("multiplicitiesFetched");
                return $.Deferred().resolve().promise();
            }
        },
        parseMultiplicities: function(data){
            //tag-manager returns multiplicities as [{"name": tag_name, "counter": 1}], we parse it into {"tag_name1": 1, "tag_name2": 2}
            var output = {};
            for (var i=0;i<data.length;i++){
                output[data[i].name] = data[i].counter;
            }
            return output;
        }
    });

    var ServicesView = Backbone.View.extend({
        template: templateFromFunction(function(){/*
            <div id="services-collection">
            </div>
            <p></p>           
            {{#is_owner}}
            <button type="button" class="btn btn-default" id="add-service"><span class="glyphicon glyphicon-plus"></span></button>
            {{/is_owner}}
        */}),
        initialize: function(){  
            var newHtml = render(this.template, {is_owner: global_context.is_owner});
            this.$el.html(newHtml);
            this.serviceViews = [];
            for (var i=0;i<this.collection.length;i++){
                var newServiceView = new ServiceView({model: this.collection.at(i), tagMultiplicities: this.collection.tagMultiplicities});
                this.serviceViews.push(newServiceView);
                this.$('#services-collection').append(newServiceView.$el);
            }
            this.render();
            // upon every creation of tag or removal of tag or service, we should re-calculate the multiplicities of each other tag
            // in ServiceView's services, cause they might've changed
            this.listenTo(this.collection, "multiplicitiesFetched", _.bind(this.redrawMultiplicities, this));
        },
        render: function(){
            for (var i=0;i<this.collection.length;i++){
                this.serviceViews[i].render();
            }
            return this;
        },
        events:{
            "click #add-service": "addService"
        },
        addService: function(e){
            var newService = new this.collection.model({name: "Название услуги", tags: [], points: [], extra: ""},
                                                       {collection: this.collection});
            this.collection.add(newService);
            var response = newService.save(null, {success: newService.setId});
            response.done(_.bind(function(data){
                newService.set("id", data.id);
                var newServiceView = new ServiceView({model: newService, id: newService.get("id")});
                this.serviceViews.push(newServiceView);
                this.$('#services-collection').append(newServiceView.$el);              
                newServiceView.render();
            }, this));
        },
        redrawMultiplicities: function(e){
            for (var i=0;i<this.collection.length;i++){
                var service = this.collection.at(i);
                var serviceView = this.serviceViews[i];
                for (var j=0;j<serviceView.tagViews.length;j++){
                    var tagView = serviceView.tagViews[j];
                    if (tagView.tag_name in this.collection.tagMultiplicities){
                        tagView.tag_multiplicity = this.collection.tagMultiplicities[tagView.tag_name];                        
                    }
                    else{
                        tagView.tag_multiplicity = 1; // if tag not in tagMultiplicities, set its multiplicity to 1
                    }
                    tagView.render();
                }
            }
        }
    });


    // TODO: fix random order of services!!!!!!!
    if (global_context.hasOwnProperty("services_url")){
        var servicesCollection = new ServicesCollection([]);
        servicesCollection.url = global_context.services_url;

        servicesCollection.fetch({})
        .done(function(){
            servicesCollection.fetchMultiplicities().done(function(){
                var servicesView = new ServicesView({collection: servicesCollection, el: $("div#services").get(0)});
            });
        });
    }

});