	/*
	 * Gallery is a modal that displays a single album at a time and allows to scroll images from that album
	 */

	var GalleryTemplate = templateFromFunction(function(){/*
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="galleryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
        <h4 class="modal-title" id="galleryModalLabel">Gallery</h4>
      </div>
      <div id="galleryModalBody" class="modal-body next">

        <div id="myToollyGallery" class="toolly-gallery slide" data-ride="tolly-gallery">
          <div class="toolly-gallery-inner">
          </div>
          <a class="left toolly-gallery-control" href="#myCarousel" role="button" data-slide="prev" data-target="#myToollyGallery"><span class="glyphicon glyphicon-chevron-left"></span></a>
          <a class="right toolly-gallery-control" href="#myCarousel" role="button" data-slide="next" data-target="#myToollyGallery"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div><!-- /.myToollyGallery -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left prev" onclick="galleryModalPrev(this)">
          <i class="glyphicon glyphicon-chevron-left"></i>
          Previous
        </button>
        <button type="button" class="btn btn-primary next" onclick="galleryModalNext(this)">
          Next
          <i class="glyphicon glyphicon-chevron-right"></i>
        </button>
      </div>
    </div>
  </div>
</div>	
	*/});

	var GalleryImageTemplate = templateFromFunction(function(){/*
            <div class="item {{active}}">
              <img src="Bokeh-1-JodyDole-1.jpg" alt="First slide" style="max-width:100%; height: auto; width: auto;">
              <div class="container"> <!-- remove container to unfix width -->
                <div class="toolly-gallery-caption">
                  <h1>Example headline.</h1>
                  <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                </div>
              </div>
            </div>
	*/});

	var GalleryView = Backbone.View.extend({
		initialize: function(){
		}
	});