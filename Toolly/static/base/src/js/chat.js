/**
    Backbone-ish chat applet.

    Custom events from UI are triggered only on models and propagated to ChatListModel.
    Events:
        'chatBack'("chat-id-to-leave")
        'chatEnter'("chat-id")
*/
$(function(){

//dirty helper function
function templateFrom(funcBody){
    return funcBody.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
}
var months = [
    "Янв", "Фев",
    "Мар", "Апр", "Май",
    "Июн", "Июл", "Авг",
    "Сен", "Окт", "Ноя",
    "Дек"
];

// pad string or number with zeros up to 'len'
function zeroPadFront(str, len){
    str = ""+str;
    if(str.length < len){
        var zeros = len - str.length;
        var pad = "";
        while(zeros > 0){
            zeros--;
            pad += "0"
        }
        str = pad + str;
    }
    return str;
}

// chat notation for dates
function toChatTime(date){
    //TODO: use X days ago, and the like?
    return Mustache.render("{{mon}} {{day}} {{hh}}:{{mm}}:{{ss}}", {
        mon: months[date.getMonth()],
        day: date.getDate(),
        hh: date.getHours(),
        mm: zeroPadFront(date.getMinutes(), 2),
        ss: zeroPadFront(date.getSeconds(), 2)
    });
}

// Loads resources and caches them internally. So far only user names.
// TODO: use plain URLs
var assetFetcher = {
    table: {}, // cache of data for given user id
    requests: {}, // unique user id -> ajax request

    // translate a message and call callback with the resulting object
    fetch: function(id){
        var res = { }
        var fetcher = this;
        if(id in fetcher.table){ // resource is already loaded
            // immedate call version
            return {
                done: function(callback){
                    callback(fetcher.table[id]);    
                    return this; // finally I meant 'this' to chain calls to done
                }
            }
        }
        if(!(id in fetcher.requests)){ // never asked before
            // issue new AJAX request
            fetcher.requests[id] = $.ajax({
                type: "GET",
                url: "/username_crutch/"+id+"/"
                //TODO: graceful error handling:
                // retry failed requests?
            }).done(function(res){
                fetcher.table[id] = res;
            });
        }
        return {
            done: function(callback){
                fetcher.requests[id].done(callback);
                return this; // chain calls
            }
        };
    }
};

var ChatMessageModel = Backbone.Model.extend({
    defaults: {
        send_time : 0, 
        sender: 0,
        msg: ""
    },

});

var ChatMessageView = Backbone.View.extend({
    tagName: "div",

    className: "media",

    render: function(){
        var templateMessage = templateFrom(function(){/*
          <a href="/profile/{{sender}}" class="pull-left">
            <img width=64 src="/img/{{sender}}/avatar/" class="media-object img-thumbnail">
          </a>
          <div class="media-body">
              <h4 class="media-heading">{{sender}} <small><i> {{date}} </i></small></h4>
              <p>{{msg}}</p>
          </div>
        */});
        var _this = this;
        _this.$el.html(' ');
        assetFetcher.fetch(this.model.get("sender")).done(function(user){
            var e = _this.model.toJSON()
            var data = {
                sender : user.username,
                date : toChatTime(new Date(e.send_time)),
                msg : e.msg
            }   
            var msg = Mustache.to_html(templateMessage, data);
            _this.$el.html(msg);
        });
        return this;
    }
});


var MessageList = Backbone.Collection.extend({ model: ChatMessageModel });
/**
    A model of a chat instance:
    a list of messages plus few extra methods
*/
var ChatModel = Backbone.Model.extend({
    defaults: function(){
        var col = new MessageList();
        return {
            messages: col,
            id: 'BAD_CHAT_ID', // for debugging
            kind: 'dialogue',
            lastMessage: { send_time : 0, sender: 0, msg: "" }
        };
    },

    constructor: function(){
        var _this = this;
        Backbone.Model.apply(this, arguments);
        $.ajax({
          type: "GET",
          url: '/chat/'+this.get("kind")+'/'+this.id+'/history',
          success: function(data){
              _this.refresh(data);
          }
          //TODO: handle failure with events?
        });
    },
    post: function(msg){
        $.ajax({
            type: "POST",
            url: '/chat/'+this.get("kind")+'/'+this.id+'/post',
            data: JSON.stringify({ 'msg' : msg }),
            contentType: 'application/json'
        }).done(function () {
            //TODO: trigger some event
        }).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO: handle failure with events?
            console.error("Chat inaccessible: "+errorThrown);
        });
    },
    // update with array of "recent" messages
    // they may overlap with current array
    refresh: function(recentMessages){
        var i = 0;
        var messages = this.get("messages")
        // what equal messages means
        function equal(m1, m2){
            return m1.send_time == m2.send_time && m1.msg == m2.msg;
        }
        var last = this.get("lastMessage");
        if(messages.length){
            // need to check for stale messages in recentMessages
            last = messages.at(messages.length-1);
            for(i=0; i<recentMessages.length; i++)
                if(equal(last, recentMessages[i]))
                    break;
            // not found - all fresh
            if(i == recentMessages.length)
                i = 0;
            else
                i++; // all after i are fresh
        }
        if(recentMessages.length){
            last = recentMessages[recentMessages.length-1];
        }
        // append all fresh messages
        messages.add(recentMessages.slice(i));
        // update last message
        this.set({
            lastMessage: last,
        });
    }
})

// A full display of a singular chat as 
// a list of messages with the new message area
var ChatView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",

    initialize: function() {
        this.listenTo(this.model.get("messages"), "add", this.appendMessage);
        _.bindAll(this, 'postOnEnter', 'postOnSubmit', 
            'fireBackEvent', 'appendMessage', 'onUpdate');
        var panel = templateFrom(function(){/*
  <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            <button class="btn btn-primary" id="back">Back</button>
        </h3>
      </div>
      <div class="panel-body">
        <div class="media" id="chat_message_list">
        </div>
      </div>
      <div class="panel-heading">
        <h3 class="panel-title">New message</h3>
      </div>
      <div class="panel-body">
        <form id="chat_input_form">
            <textarea class="form-control" rows="3" id="chat_input" placeholder="Enter message text"></textarea>
            <p></p>
            <button class="btn btn-primary pull-right">
              Отправить
            </button>
          </form>
      </div>
  </div>
      */})
        //create a view per existing element
        this.$el.html(panel);
        this.onUpdate();
    },

    onUpdate: function(){
        var elements = this.model.get("messages").map(function(m){
            return new ChatMessageView({model : m}).render().$el;
        });
        this.$("#chat_message_list").append(elements);
        this.tweakViewport();
    },

    appendMessage: function(message){
        var view = new ChatMessageView({model: message}).render();
        this.$("#chat_message_list").append(view.$el);
        this.tweakViewport();
    },

    tweakViewport: function(){
        //TODO: viewport tweaking doesn't belong here
        var n = parseFloat($('body').css('padding-top'))
        var h = $(window).height() - 330 - n;
        var obj = this.$('#chat_message_list');
        obj.css({ "overflow-y": "scroll", "height" : h });
        obj.scrollTop(232768); //TODO: silly hackzzz
    },

    postMessage: function(){
        var el = this.$("#chat_input")
        if(!/^\s*$/.test(el.val())){
            this.model.post(el.val())
            el.val("")
        }
    },

    // keypress callback
    postOnEnter: function(e){
        if(e.which == 13 && !e.shiftKey){
            this.postMessage();
            return false;
        }
        return true;
    },

    postOnSubmit: function(e){
        this.postMessage();
        return false;
    },

    fireBackEvent: function(e){
        //console.info("Trigger chatBack");
        this.model.trigger("chatBack", this.model.get("id"));
    },

    events: {
        "submit #chat_input_form" : "postOnSubmit",
        "keyup #chat_input" : "postOnEnter",
        "click #back" : "fireBackEvent"
    },

    render: function() {
        // chat_message_list
        return this;
    }
})

// A short display of chat 
// only the last message and (if not your own) sender avatar
var ChatMiniView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",


    initialize: function() {
        this.listenTo(this.model, "change", this.render);
        _.bindAll(this, 'openChat');
    },

    openChat: function(e){
        //console.info("Trigger chatEnter")
        this.model.trigger("chatEnter", this.model.get("id"));
        return true;
    },

    events: {
        "click #my_chat_id" : "openChat"
    },
    
    render: function(){
        var tmpl = templateFrom(function(){/*
            {{#data}}
            <button class="btn btn-default btn-block" id="my_chat_id" >
                <div class="pull-right">
                    <img width=64 src="/img/{{user}}/avatar/"  class="img-thumbnail">
                    <div><b> {{user}} </b></div>
                </div>
                <div>
                <h4><i>{{date}}</i></h4>
                <h4><i>{{poster}}</i>: {{msg}}</h4>
                </div>
            </button>
            {{/data}}
        */});
        var last = this.model.get("lastMessage");
        var _this = this;
        var chatId = _this.model.get("id"); //TODO: dialog only!
        if(last.send_time !== 0){
            var res = assetFetcher.fetch(last.sender);
            res.done(function(user){
                var lastPoster = user.username;
                assetFetcher.fetch(chatId).done(function(user){
                    //previous call happens before this one, so lastPoster is set
                    var obj = {
                        user : user.username,
                        poster: lastPoster,
                        date : toChatTime(new Date(last.send_time)), 
                        msg : last.msg
                    }
                    var content = Mustache.to_html(tmpl, { data: obj });
                    _this.$el.html(content);
                });    
            })

        }
        return this;
    }
});

var ChatCollection = Backbone.Collection.extend({
    model: ChatModel
})

var ChatListModel = Backbone.Model.extend({
    defaults: {
        chats: new ChatCollection(),
        user: "BAD_USER"
    },

    constructor: function(){
        var _this = this;
        Backbone.Model.apply(this, arguments);
        // forward all events from chat models (that are forwarded to collection)
        this.listenTo(this.get("chats"), "chatEnter", function(arg){
            _this.trigger("chatEnter", arg);
        });
        this.listenTo(this.get("chats"), "chatBack", function(arg){
            _this.trigger("chatBack", arg);
        });
        $.ajax({
            url:"/chat/info",
            type:"GET",
            success: function(data){
                var chats = _this.get("chats")
                var usr = _this.get("user")
                var dialogs = data.user_info.dialogs;
                var msg;
                for (var k in dialogs){
                    if (dialogs.hasOwnProperty(k)){
                        var id = k.split("-")
                        if(id[0] != usr)
                            id = id[0];
                        else if(id[1] != usr)
                            id = id[1];
                        msg = {
                            sender: dialogs[k][0],
                            send_time: dialogs[k][1],
                            msg: dialogs[k][2]
                        }
                        chats.add({
                            id: id,
                            kind: "dialogue",
                            lastMessage: msg
                        })
                    }
                }
                _this.longPoll()
            }
        })
    },

    longPoll: function(){
        var dialogs = { };
        var chats = this.get("chats");
        chats.forEach(function(e){
            //console.log("POLL: "+new Date(e.get("lastMessage").send_time).toTimeString());
            dialogs[e.get("id")] = e.get("lastMessage").send_time + 1;
        });
        var requestData = 
          '{ "dialogs": '+ JSON.stringify(dialogs) + ', "chats": {} }';
        //console.log(requestData)
        var _this = this;
        $.ajax({
            url:"/chat/refresh",
            type:"POST",
            data: requestData,
            contentType:"application/json; charset=utf-8",
            success: function(data){
                var chat = null;
                $.each(data, function(index, val) {
                    chat = _this.get("chats").find(function(model){
                        return index.search(model.get("id")) >= 0;
                    });
                    if(chat){
                        chat.refresh(val);
                    } else{
                        var ids = index.split("-");
                        var id;
                        if(ids[0] == global_context.user_pk)
                            id = ids[1];
                        else
                            id = ids[0];
                        chat = new ChatModel({
                            id: id,
                            kind: "dialogue"
                        })
                        chat.refresh(val);
                        chats.add(chat);
                    }
                });
                _this.longPoll();
            },
            error: function(error){
                if(error.status >= 400 && error.status < 500)
                    alert(JSON.stringify(error));
                else
                    _this.longPoll();
            }
        });
    },

    add: function(model){
        this.get("chats").add(model);
    }
})

var ChatListView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",

    initialize: function(){
        this.listenTo(this.model, "change", this.render);
        this.listenTo(this.model.get("chats"), "add", this.addView)
    },

    addView: function(obj){
        this.$el.append(new ChatMiniView({ model: obj }).render().$el);
    },

    render: function(){
        return this;
    }
    // removeView: // TODO:
    //
})

// Entry point of chat applet
//=============================================================

console.log(global_context);
if("user_pk" in global_context){ // chat needs authenticated user
    var chats = new ChatListModel({ user: global_context.user_pk });
    var chatListView = new ChatListView({ model: chats });
    chats.on("chatEnter", function(a){
        console.info("chatEnter: "+a)
        var which = chats.get("chats").find(function(chat){
            return chat.get("id") == a;
        });
        //console.log(which);
        if(!which){
            console.error("Internal error: chat #"+a+" not found!");
        }
        else{
            $("#Messages").children().detach();
            $("#Messages").append(new ChatView({ model: which}).render().$el);
        }
    });
    chats.on("chatBack", function(a){
        console.info("chatBack: "+a);
        $("#Messages").children().detach();
        //console.log(chatListView.render().$el);
        $("#Messages").append(chatListView.render().$el);
    });

    //alert(JSON.stringify(global_context));
    if(global_context.is_owner){
        $("#Messages").append(chatListView.render().$el);
    }
    else{ // open current chat
        //var chatModel = chats.get("chats").find())
        //chat with page owner
        var chatModel = new ChatModel({ id : global_context.owner_pk });
        console.log("Msg", chatModel.get("lastMessage"));
        var chatView  = new ChatView({ model : chatModel });
        chats.add(chatModel); // add current chat to the chat list
        $("#Messages").append(chatView.render().$el);
    }
}

// end of jquery on DOM ready callback
});