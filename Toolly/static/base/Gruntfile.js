module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: '\n',
            },
            dist: {
                src: [
                    'src/js/util.js',
                    'src/js/portfolio.js',
                    'src/js/gallery.js',
                    'src/js/services.js',                    
                    'src/js/chat.js',
                    'src/js/map.js'
                ],
                dest: '<%= pkg.name %>.js'
            }
        },
        jshint: {
            files: ['<%= concat.dist.dest %>'],
            options: {
                force: true, // continue even upon warnings
                asi: true, // do not warn on semicolons
                undef: true, // warn on undefined variables
                globals: {
                    window: true,
                    document: true,
                    alert: true,
                    console: true,
                    module: true,
                    browser: true,
                    FormData: true, // common browser objects
                    // some more stuff            
                    '$' : true, // jQuery
                    "_" : true, // Underscore library
                    L : true, //Leaflet library
                    jQuery: true,                    
                    Backbone: true,
                    Mustache: true,
                    global_context: true // our context with global variables for client-side javascript
                }
            }
        },
        watch: {
            files: ['<%= concat.dist.src %>'],
            tasks: ['concat', 'jshint'],
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['jshint', 'concat']);

};
