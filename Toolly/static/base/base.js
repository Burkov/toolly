/**
 * This is a hack used to write multiline template html inside javascript.
 * Given a function, whose body is a commented html template, 
 * template_from_html extracts html from it and returns it as a string.
 * See: http://tomasz.janczuk.org/2013/05/multi-line-strings-in-javascript-and.html
 *
 * @param func {function} - function that contains commented-out html template
 */
function templateFromFunction(func){
	return func.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
}

/**
 * Renders template with Mustache and returns string with html.

 * @param template {string} Mustache-formatted template of created element.
 * @param context {Object} dictionary with variables to be inserted into the template.
 * @return {str} string with html
 */
function render(template, context){
    return Mustache.to_html(template, context);
}

/**
 * Creates and returns element, described by template and context, and inserts it into the document before or after each
 * element, selected by $(insertion_point).
 *
 * @param template {string} Mustache-formatted template of created element.
 * @param context {Object} dictionary with variables to be inserted into the template.
 * @param insertion_point {jquery selector}; if multiple elements are selected, inserts before/after each of them.
 * @param before_or_after {boolean} [before_of_after=false] true, if new element is inserted before insertion point, false if after.
 * @return newly created element
 */
function renderAndInsert(template, context, insertion_point, before_or_after){
    var newHtml = render(template, context);
    var $newElement = $(newHtml);
    if (before_or_after) {
        $(insertion_point).before($newElement);
    }
    else {
        $(insertion_point).after($newElement);
    }
    return $newElement;
}

/**
* Creates and returns element, described by template and context, and appends it to the contents of container element.
*
* @param template {string} Mustache-formatted template of created element.
* @param context {Object} dictionary with variables to be inserted into the template.
* @param container {Dom element or jquery selector} element, to which the newly created element should be appended.
* @return newly created element
*/
function renderAndAppend(template, context, container){
    var newHtml = render(template, context);
    var $newElement = $(newHtml);
    $(container).append($newElement);
    return $newElement;
}
$(document).ready(function(){

    var ImageModel = Backbone.Model.extend({
        initialize: function(){
        },
        /*
         * @param data {FormData} - FormData, received from form with <input type="file" name="image_file"> 
         *  that contains user-selected image.
         */     
        save: function(data){
            // first create File object in the database {"name": "something"}
            var result = this.createFileObject();
            // bind postFileData to this and data
            result.done(_.bind(function(received_data){
                this.postFileData(received_data, data);
            }, this));
        },
        createFileObject: function(){
            var result = $.ajax({
                url: this.url(),
                type: 'post',
                data: JSON.stringify({ 'name': 'No name' }),
                contentType: 'application/json'
            });
            return result;
        },
        postFileData: function(received_data, data){
            this.set("id", received_data.id);
            this.set("name", 'No name');
            // then populate the "id" field of that File object with content of the real file
            var result = $.ajax({
                url: this.url()+"/"+"fs",
                type: 'put',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
            });
            // after posting the image, add this model to collection; collection.add
            // triggers callback, ImageUploadModalView.onUploadDone, set in ImageUploadModalView.onUpload
            result.done(_.bind(function() {this.collection.add(this)}, this)); 
            
            return result;
        },
        imageSrc: function(){
            // extract album_id and image_id from this.url() and create imageSrc path from them         
            var urlComponents = this.url().replace(/([^\/])$/, '$1').split("/");
            var imageSrcPathEnding = urlComponents[urlComponents.length-3] + "/" + urlComponents[urlComponents.length-1] + "/";
            var imageSrcPath = window.location.host + '/img/' + global_context.owner + "/" + imageSrcPathEnding;
            return imageSrcPath;
        }
    });

    var ImageViewTemplate = templateFromFunction(function(){/*
      <div class="col-lg-3 col-md-4 col-xs-6 thumb">
          <a class="thumbnail" href="http://{{url}}" data-gallery> <!-- TODO: make href=# for gallery when ready -->
              <img class="img-responsive" src="http://{{url}}" alt="">
          </a>
      </div>*/});

    var ImageView = Backbone.View.extend({
        render: function(){
            //alert("ImageView.model.attributes = " + JSON.stringify(this.model.attributes));
            //alert("ImageView.model.collection = " + JSON.stringify(this.model.collection));
            this.$el.html(render(ImageViewTemplate, {url: this.model.imageSrc()}));
            return this;
        }
    });

    var ImageUploadModalElementTemplate = templateFromFunction(function(){/*
<div class="modal fade" id="imageUploadModal" tabindex="-1" role="dialog" aria-labelledby="imageUploadModalLabel" aria-hidden="true">  
</div> <!-- modal-dialog -->
    */});

    var ImageUploadModalTemplate = templateFromFunction(function(){/*
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
        <h4 class="modal-title" id="imageUploadModalLabel">Загрузить изображение:</h4>
      </div>
      <div id="imageUploadModalBody" class="modal-body next">
        <form method="post" action="{{url}}" id="portfolio-upload-form" enctype="multipart/form-data">
        <input type="file" name="image_file" class="image_file" />
        </form>
      </div><!-- modal body -->
      <div class="modal-footer">
        <button type="submit" form="portfolio-upload-form" class="btn btn-primary portfolio-upload-btn" name='portfolio-upload-btn' value='portfolio-upload-btn'>Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>        
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
*/});

// We don't need this anymore
// WARNING! input 'value' is both DOM attribute and Javascript property. 
// Changing attr, you change prop, too. Changing prop  DOESN'T affect attr.
//      <input type="hidden" id="album_id" name="album_id" value="{{album_id}}" />

    var ImageUploadModalView = Backbone.View.extend({
        initialize: function(){
            var $element = $(ImageUploadModalElementTemplate);
            $('body').prepend($element);
            this.el = $("div#imageUploadModal").get(0);
            this.$el = $("div#imageUploadModal");
        },
        render: function(){
            var context;
            if (this.model !== undefined){ context = {url: this.model.collection.url()}}
            else {context = {url:""}}
            var newHtml = render(ImageUploadModalTemplate, context);
            this.$el.html(newHtml);
            return this;
        },
        events: {
            "submit #portfolio-upload-form": "onUpload"
        },
        onUpload: function(e){
            e.preventDefault();
            // use e.currentTarget or e.target to reference the DOM element that triggered this event
            console.log("onUpload: this = " + this);
            var data = new FormData(e.target); // or $('#gallery-upload-form').get(0)
            var imagesCollection = this.albumModel.get("files");
            var imageModel = new ImageModel({}, {collection:imagesCollection});
            this.listenToOnce(imagesCollection, "add", this.onUploadDone);
            imageModel.save(data);
            return false;
        },
        onUploadDone: function(model){
            //alert("In the beginning of onUploadDone");
            var imageView = new ImageView({model: model});
            this.albumView.imageViews.push(imageView);
            this.albumView.$('.images').append(imageView.$el);
            imageView.render();
            $('#imageUploadModal').modal('hide');
        }
    });

    var AlbumViewTemplate = templateFromFunction(function(){/*
            <div class="album" id="{{id}}"> 
              <div class="page-header">
                <h4>
                  {{#is_owner}}
                  <a href="#" class="album_title editable" id="service_name" data-type="text" data-pk="" data-name="name" data-url="{{url}}" data-original-title="Enter service name here">
                    {{title}}
                  </a>
                  {{/is_owner}}
                  {{^is_owner}}
                  {{title}}
                  {{/is_owner}}
                </h4>
              </div>
              <div class="images">
              </div>
              {{#is_owner}}
              <button type="button" class="btn btn-primary image-upload-modal-button">Загрузить изображение...</button>
              {{/is_owner}}
            </div>
    */});

    var ImagesCollection = Backbone.Collection.extend({
        model: ImageModel,
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output;
        }
    });
    // WARNING!!! Don't use .add() method of ImagesCollection unless you're
    // creating a new image with upload button. AlbumView's onUploadDone
    // is attached to the add event of the collection and will fire.
    var AlbumModel = Backbone.Model.extend({
        initialize: function(){
        },
        /*
        removeImage: function(image_url){
            $.ajax({
                url: image_url,
                type: 'post',
                data: {'_method': 'delete'},
                dataType: 'text',
                success: function(data, textStatus, jqXHR){
                    var removed_image = this.get("files").get(image_url);
                    this.get("files").remove(removed_image);
                }
            });
        },
        */
        setId: function(model, response, options){
            model.set("id", response.id);
        },
        toJSON: function(){
            return {title: this.get('title'), files: this.get("files").toJSON()};
        }
/*        save: function(){
            var serial = JSON.stringify(this.toJSON());
            alert(serial);
            $.ajax({
                url: this.url(),
                type: 'post',
                data: JSON.stringify(this.toJSON()),
                contentType: 'application/json',
                dataType: 'json',               
                success: function(data, textStatus, jqXHR){
                    alert("success!");
                }
            });
        },
*/
    });

    var AlbumView = Backbone.View.extend({
        initialize: function(){
            //this.listenTo(this.model.get("files"), "add", this.addImage);
            this.imageViews = [];
            for (var i=0;i<this.model.get("files").length;i++){
                var imageView = new ImageView({model: this.model.get("files").at(i)});
                this.imageViews.push(imageView);
            }
        },
        render: function(){
            var context = {
                "title": this.model.get("title"),
                "id": this.model.get("id"),
                "is_owner": global_context.is_owner,
                "url": this.model.url().replace(/([^\/])$/, '$1/') + "title/"
            };
            var newHtml = render(AlbumViewTemplate, context);
            this.$el.html(newHtml);
            // activate bootstrap-editable and attach signal handler to it
            this.$('a.album_title.editable').editable();
            var self = this; // to see the view inside event callback
            this.$('a.album_title.editable').on('render', function(e, editable){
                self.model.set('title', $(this).text());
            });
            // render all imageViews in the album
            for (var i=0;i<this.model.get("files").length;i++){
                this.$('.images').append(this.imageViews[i].$el);
                this.imageViews[i].render();
            }
            return this;
        },
        events: {
            "click .image-upload-modal-button": "show_upload_dialog",
            "submit #gallery-upload-form": "upload_image_to_album",
        },
        show_upload_dialog: function(e){
            imageUploadModalView.albumModel = this.model;
            imageUploadModalView.albumView = this;
            imageUploadModalView.render();
            $('#imageUploadModal').modal('show');
        },
        addImage: function(imageModel){
            var imageView = new ImageView({model: imageModel});
            this.$('.images').append(render(ImageViewTemplate, {src: imageModel.src}));
        }
    });


    var PortfolioTemplate = templateFromFunction(function(){/*
        <div id="albums">
        </div>
        <p></p>     
        {{#is_owner}}       
        <button id="create-album" type="button" class="btn btn-default">Создать альбом</button>
        {{/is_owner}}
    */});
    
    var PortfolioCollection = Backbone.Collection.extend({
        model: AlbumModel,
        initialize: function(){
            this.url = this.urlRoot; // portfolioCollection itself doesn't need url, but its AlbumModels do
        },
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, textStatus, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                // create array from files object
                var files = value.files;
                var files_models = [];
                var newImagesCollection = new ImagesCollection(); // we create it here and .set() later in order to set .collection property on each model
                for (var file_id in files){
                    var newImageModel = new ImageModel(files[file_id]);
                    newImageModel.set("id", file_id);
                    newImageModel.collection = newImagesCollection;
                    files_models.push(newImageModel);
                }
                newImagesCollection.set(files_models, {silent:true}); //don't fire event
                var newAlbum = new AlbumModel({files: newImagesCollection, title: value.title},
                                              {collection: this});
                newAlbum.set("id", key);
                newImagesCollection.url = newAlbum.url().replace(/([^\/])$/, '$1/') + "files/";
                models.push(newAlbum);
            }
            this.set(models);
        }
        //TODO MAKE SAVE METHOD THAT STORES THE WHOLE PortfolioCollection AS AN {}, NOT AS []
    });

    var PortfolioView = Backbone.View.extend({
        initialize: function(){
            var newHtml = render(PortfolioTemplate, {is_owner: global_context.is_owner});
            this.$el.html(newHtml);
            this.albumViews = [];
            for (var i=0;i<this.collection.length;i++){
                var newAlbumView = new AlbumView({model: this.collection.at(i)});
                this.albumViews.push(newAlbumView);
                this.$('#albums').append(newAlbumView.$el);
                newAlbumView.render();
            }
        },
        events:{
            "click #create-album": "createAlbum"
        },
        render: function(){
            for (var i=0;i<this.collection.length;i++){
                this.albumViews[i].render();
            }
            return this;
        },
        createAlbum: function(e){
            var newAlbum = new this.collection.model({title: "Альбом", files: new ImagesCollection()}, {collection: this.collection});
            this.collection.add(newAlbum);
            var response = newAlbum.save(null, {success:newAlbum.setId});
            response.done(_.bind(function(data){
                newAlbum.set("id", data.id);
                newAlbum.get("files").url = newAlbum.url().replace(/([^\/])$/, '$1/') + "files/";               
                var newAlbumView = new AlbumView({
                    model: newAlbum,
                    tagName: 'div',
                    className: 'album',
                    id: newAlbum.get("id")
                });
                this.albumViews.push(newAlbumView);
                this.$('#albums').append(newAlbumView.$el);
                newAlbumView.render();
            }, this));
        }
    });

    // "main" entry point

    if (global_context.hasOwnProperty("portfolio_url")){
        var imageUploadModalView = new ImageUploadModalView(); // to prepare dialog window
        var portfolioCollection = new PortfolioCollection([]);
        portfolioCollection.url = global_context.portfolio_url;

        portfolioCollection.fetch({}).done(function(){
            var portfolioView = new PortfolioView({collection: portfolioCollection, el: $("div#portfolio").get(0)});
            portfolioView.render();
        });
    }

});
	/*
	 * Gallery is a modal that displays a single album at a time and allows to scroll images from that album
	 */

	var GalleryTemplate = templateFromFunction(function(){/*
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="galleryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
        <h4 class="modal-title" id="galleryModalLabel">Gallery</h4>
      </div>
      <div id="galleryModalBody" class="modal-body next">

        <div id="myToollyGallery" class="toolly-gallery slide" data-ride="tolly-gallery">
          <div class="toolly-gallery-inner">
          </div>
          <a class="left toolly-gallery-control" href="#myCarousel" role="button" data-slide="prev" data-target="#myToollyGallery"><span class="glyphicon glyphicon-chevron-left"></span></a>
          <a class="right toolly-gallery-control" href="#myCarousel" role="button" data-slide="next" data-target="#myToollyGallery"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div><!-- /.myToollyGallery -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left prev" onclick="galleryModalPrev(this)">
          <i class="glyphicon glyphicon-chevron-left"></i>
          Previous
        </button>
        <button type="button" class="btn btn-primary next" onclick="galleryModalNext(this)">
          Next
          <i class="glyphicon glyphicon-chevron-right"></i>
        </button>
      </div>
    </div>
  </div>
</div>	
	*/});

	var GalleryImageTemplate = templateFromFunction(function(){/*
            <div class="item {{active}}">
              <img src="Bokeh-1-JodyDole-1.jpg" alt="First slide" style="max-width:100%; height: auto; width: auto;">
              <div class="container"> <!-- remove container to unfix width -->
                <div class="toolly-gallery-caption">
                  <h1>Example headline.</h1>
                  <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                </div>
              </div>
            </div>
	*/});

	var GalleryView = Backbone.View.extend({
		initialize: function(){
		}
	});
$(function(){

    var TagTemplate = templateFromFunction(function(){/*
            <span class="badge"><span class="tag-text">{{tag_name}}</span> x{{tag_multiplicity}} {{#is_owner}}<a href="#" class="discard-service-tag"><span class="glyphicon glyphicon-remove"></span></a>{{/is_owner}}</span>
    */});

    var TagView = Backbone.View.extend({
        className: 'tag',
        /**
         * Expected parameters:
         * @param options {object} e.g. {service: ServiceModel, tag_name: "NameOfTag", tag_multiplicity: 1}
         */
        initialize: function(options){
            this.service = options.service;
            this.service_view = options.service_view;            
            this.tag_name = options.tag_name;
            this.tag_multiplicity = options.tag_multiplicity;
        },
        render: function(){
            this.$el.css({'display': 'inline-block'});
            var newHtml = render(TagTemplate, {
                tag_name: this.tag_name,
                tag_multiplicity: this.tag_multiplicity.toString(),
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);
        },
        events: {
            "click a.discard-service-tag": "discardTag"
        },
        discardTag: function(e){
            e.preventDefault();
            var self = this; // to propagate this into event handlers
            var tags = this.service.get("tags");
            tags.splice(tags.indexOf(this.tag_name), 1); // remove tag from service.attributes.tags
            this.service_view.tagViews.splice(this.service_view.tagViews.indexOf(this), 1) ;// remove tagView from tagViews

            $.ajax({
                type: "POST",
                url: global_context.discard_service_tag_url,
                data: JSON.stringify({id: this.service.id, tag: this.tag_name}),
                contentType: 'application/json; charset=utf-8',
            }).done(function(){
                // remove tag name from servicesCollection.tagMultiplicities
                self.service.collection.fetchMultiplicities().done(function(){
                    var tagMultiplicities = self.service.collection.tagMultiplicities;
                    // removed the following just not to think about it:
                    //if (tagMultiplicities[self.tag_name] === 0) {delete tagMultiplicities[self.tag_name];}
                    // remove tag from DOM
                    self.remove();
                });
            });
        }
    });

    var ServiceTemplate = templateFromFunction(function(){/*
          <div class="panel-heading"> 
            <h3 class="panel-title">
              {{#is_owner}}
              <a href="#" class="service_name editable" id="service_name" data-type="text" data-pk="" data-name="name" data-url="{{url}}" data-original-title="Enter service name here">
                {{name}}
              </a>
              {{/is_owner}}
              {{^is_owner}}
                {{name}}
              {{/is_owner}}
              {{#is_owner}}
              <button type="button" class="btn btn-xs btn-default pull-right discard-service"><span class="glyphicon glyphicon-remove"></span></button> 
              {{/is_owner}}
            </h3> 
          </div> 
          <div class="panel-body">
            {{#is_owner}}
            <form method="post" action="" class="services-tag-form"> 
              <div>
                Добавить тег:
                <input type="text" name="tag" class="form-control typeahead tag-input" placeholder="">
                <button class="btn btn-primary">
                  <span class="glyphicon glyphicon-ok"></span>
                </button>
              </div>       
            </form>
            {{/is_owner}}
            <BR>
            <div class="tag-box">
            </div>
          </div>
    */});

    var ServiceModel = Backbone.Model.extend({
        setId: function(model, response, options){
            model.set("id", response.id);
        }
    });
    
    var ServiceView = Backbone.View.extend({
        /**
         * @param options {object} contains single key {tagMultiplicities: ServicesCollection.tagMultiplicities}
         */
        initialize: function(options){
            this.$el.addClass("panel panel-default service-panel");
            this.tagViews = [];
            // set view content only once upon initialize
            var newHtml = render(ServiceTemplate, {
                name: this.model.get("name"),
                url: this.model.url().replace(/([^\/])$/, '$1/') + "name/",
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);
            // create tagViews for each tag in model
            this.tagViews = [];
            for (var i=0;i<this.model.get("tags").length;i++){
                var tagName = this.model.get("tags")[i];

                var tagMultiplicity = 1; // default if tag is not in tagMultiplicities
                if (tagName in options.tagMultiplicities){
                    tagMultiplicity = options.tagMultiplicities[tagName]; //TODO fix bugs with tagMultiplicities request!!!  
                }
                var newTagView = new TagView({service: this.model, service_view: this, tag_name: tagName, tag_multiplicity: tagMultiplicity});
                this.tagViews.push(newTagView);
                this.$(".tag-box").append(newTagView.$el);
            }
            // activate bootstrap-editable and attach signal handler to it
            this.$('a.service_name.editable').editable();
            var self = this; // to see the view inside event callback
            this.$('a.service_name.editable').on('render', function(e, editable){
                self.model.set('name', $(this).text());
            });
        },
        render: function(){
            for (var i=0;i<this.tagViews.length;i++){
                this.tagViews[i].render();
            }
        },
        events: {
            "submit form.services-tag-form": "addTag",
            "click button.discard-service": "discardService"
        },
        addTag: function(e){
            e.stopPropagation();
            e.preventDefault();
            var self = this; // for callbacks

            var newTagName = this.$("input.tag-input").val();

            if (!_.contains(this.model.get("tags"), newTagName)){ //prevent duplicate tags in the same service
                this.saveTag(newTagName).done(function(){
                    var tags = self.model.get("tags");
                    tags.push(newTagName);
                    self.model.collection.fetchMultiplicities().done(function(){
                        var newTagMultiplicity = 1; // if tag isn't in tagMultiplicities, set its multiplicity to 1 by default
                        if (newTagName in self.model.collection.tagMultiplicities){
                            newTagMultiplicity = self.model.collection.tagMultiplicities[newTagName];
                        }
                        var newTagView = new TagView({service: self.model, service_view: self, tag_name: newTagName, tag_multiplicity: newTagMultiplicity});
                        self.tagViews.push(newTagView);
                        self.$('div.tag-box').append(newTagView.$el);
                        newTagView.render();
                    });
                });
            }
            else {
                //TODO display an explicit warning about Tag duplication!!!
            }
        },
        saveTag: function(tag_name){
            var result = $.ajax({
                type: "POST",
                url: global_context.service_submit_tag_url,
                data: JSON.stringify({id: this.model.id, tag: tag_name}),
                contentType: 'application/json; charset=utf-8',
            });
            return result
        },
        discardService: function(e){
            var self = this; // to let .done callback see this
            $.ajax({
                type: "POST",
                url: global_context.discard_service_url,
                data: JSON.stringify({"id":this.model.id}),
                contentType: 'application/json; charset=utf-8'
            }).done(function(){
                //TODO write a destructor, preventing memory leaks
                self.remove();
            });
        }
    });
    
    var ServicesTemplate = templateFromFunction(function(){/*
      <div id="services-collection">
      </div>
      <p></p>           
      {{#is_owner}}
      <button type="button" class="btn btn-default" id="add-service"><span class="glyphicon glyphicon-plus"></span></button>
      {{/is_owner}}
    */});

    var ServicesCollection = Backbone.Collection.extend({
        model: ServiceModel,
        initialize: function(){
            _.extend(this, Backbone.Events); // to trigger signals
        },
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, text_status, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                //each service is
                //{"name":"programmer", "tags":["html","css","js"], "points":["freelance: 2k$", "full-time: 3k$"], "extra":"text description"}
                var serviceModel = new ServiceModel({name: value.name,
                                                      tags: value.tags,
                                                      points: value.points,
                                                      extra: value.extra},
                                                      {collecion: this});
                serviceModel.set("id", key); //this should automatically set serviceModel.url() as well
                models.push(serviceModel);
            }
            this.set(models);
            this.comparator = 'id';
            this.sort();
        },
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output
        },
        fetchMultiplicities: function(){
            var self = this; // to reach this in event handler
            var allTags = _.flatten(this.map(function(service){
                return service.get('tags');
            })); // this "_.pluck"s tags from all services
            // TODO WHAT ABOUT DUPLICATES???
            if (allTags.length > 0){
                var tagParams = { tags: allTags };
                var result = $.ajax({
                    type: "GET",
                    url: "/get_tags/tags/tag",
                    data: $.param(tagParams, true)
                }).done(function(data){
                    self.tagMultiplicities = self.parseMultiplicities(data);
                    // trigger multiplicitiesFetched event
                    self.trigger("multiplicitiesFetched");
                });
                return result;
            }
            else{
                this.tagMultiplicities = {};
                // trigger multiplicitiesFetched event
                self.trigger("multiplicitiesFetched");
                return $.Deferred().resolve().promise();
            }
        },
        parseMultiplicities: function(data){
            //tag-manager returns multiplicities as [{"name": tag_name, "counter": 1}], we parse it into {"tag_name1": 1, "tag_name2": 2}
            var output = {};
            for (var i=0;i<data.length;i++){
                output[data[i].name] = data[i].counter;
            }
            return output;
        }
    });

    var ServicesView = Backbone.View.extend({
        initialize: function(){  
            var newHtml = render(ServicesTemplate, {is_owner: global_context.is_owner});
            this.$el.html(newHtml);
            this.serviceViews = [];
            for (var i=0;i<this.collection.length;i++){
                var newServiceView = new ServiceView({model: this.collection.at(i), tagMultiplicities: this.collection.tagMultiplicities});
                this.serviceViews.push(newServiceView);
                this.$('#services-collection').append(newServiceView.$el);
            }
            this.render();
            // upon every creation of tag or removal of tag or service, we should re-calculate the multiplicities of each other tag
            // in ServiceView's services, cause they might've changed
            this.listenTo(this.collection, "multiplicitiesFetched", _.bind(this.redrawMultiplicities, this));
        },
        render: function(){
            for (var i=0;i<this.collection.length;i++){
                this.serviceViews[i].render();
            }
            return this;
        },
        events:{
            "click #add-service": "addService"
        },
        addService: function(e){
            var newService = new this.collection.model({name: "Название услуги", tags: [], points: [], extra: ""},
                                                       {collection: this.collection});
            this.collection.add(newService);
            var response = newService.save(null, {success: newService.setId});
            response.done(_.bind(function(data){
                newService.set("id", data.id);
                var newServiceView = new ServiceView({model: newService, id: newService.get("id")});
                this.serviceViews.push(newServiceView);
                this.$('#services-collection').append(newServiceView.$el);              
                newServiceView.render();
            }, this));
        },
        redrawMultiplicities: function(e){
            for (var i=0;i<this.collection.length;i++){
                var service = this.collection.at(i);
                var serviceView = this.serviceViews[i];
                for (var j=0;j<serviceView.tagViews.length;j++){
                    var tagView = serviceView.tagViews[j];
                    if (tagView.tag_name in this.collection.tagMultiplicities){
                        tagView.tag_multiplicity = this.collection.tagMultiplicities[tagView.tag_name];                        
                    }
                    else{
                        tagView.tag_multiplicity = 1; // if tag not in tagMultiplicities, set its multiplicity to 1
                    }
                    tagView.render();
                }
            }
        }
    });


    // TODO: fix random order of services!!!!!!!
    if (global_context.hasOwnProperty("services_url")){
        var servicesCollection = new ServicesCollection([]);
        servicesCollection.url = global_context.services_url;

        servicesCollection.fetch({})
        .done(function(){
            servicesCollection.fetchMultiplicities().done(function(){
                var servicesView = new ServicesView({collection: servicesCollection, el: $("div#services").get(0)});
            });
        });
    }

});
/**
    Backbone-ish chat applet.

    Custom events from UI are triggered only on models and propagated to ChatListModel.
    Events:
        'chatBack'("chat-id-to-leave")
        'chatEnter'("chat-id")
*/
$(function(){

//dirty helper function
function templateFrom(funcBody){
    return funcBody.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
}
var months = [
    "Янв", "Фев",
    "Мар", "Апр", "Май",
    "Июн", "Июл", "Авг",
    "Сен", "Окт", "Ноя",
    "Дек"
];

// pad string or number with zeros up to 'len'
function zeroPadFront(str, len){
    str = ""+str;
    if(str.length < len){
        var zeros = len - str.length;
        var pad = "";
        while(zeros > 0){
            zeros--;
            pad += "0"
        }
        str = pad + str;
    }
    return str;
}

// chat notation for dates
function toChatTime(date){
    //TODO: use X days ago, and the like?
    return Mustache.render("{{mon}} {{day}} {{hh}}:{{mm}}:{{ss}}", {
        mon: months[date.getMonth()],
        day: date.getDate(),
        hh: date.getHours(),
        mm: zeroPadFront(date.getMinutes(), 2),
        ss: zeroPadFront(date.getSeconds(), 2)
    });
}

// Loads resources and caches them internally. So far only user names.
// TODO: use plain URLs
var assetFetcher = {
    table: {}, // cache of data for given user id
    requests: {}, // unique user id -> ajax request

    // translate a message and call callback with the resulting object
    fetch: function(id){
        var res = { }
        var fetcher = this;
        if(id in fetcher.table){ // resource is already loaded
            // immedate call version
            return {
                done: function(callback){
                    callback(fetcher.table[id]);    
                    return this; // finally I meant 'this' to chain calls to done
                }
            }
        }
        if(!(id in fetcher.requests)){ // never asked before
            // issue new AJAX request
            fetcher.requests[id] = $.ajax({
                type: "GET",
                url: "/username_crutch/"+id+"/"
                //TODO: graceful error handling:
                // retry failed requests?
            }).done(function(res){
                fetcher.table[id] = res;
            });
        }
        return {
            done: function(callback){
                fetcher.requests[id].done(callback);
                return this; // chain calls
            }
        };
    }
};

var ChatMessageModel = Backbone.Model.extend({
    defaults: {
        send_time : 0, 
        sender: 0,
        msg: ""
    },

});

var ChatMessageView = Backbone.View.extend({
    tagName: "div",

    className: "media",

    render: function(){
        var templateMessage = templateFrom(function(){/*
          <a href="/profile/{{sender}}" class="pull-left">
            <img width=64 src="/img/{{sender}}/avatar/" class="media-object img-thumbnail">
          </a>
          <div class="media-body">
              <h4 class="media-heading">{{sender}} <small><i> {{date}} </i></small></h4>
              <p>{{msg}}</p>
          </div>
        */});
        var _this = this;
        _this.$el.html(' ');
        assetFetcher.fetch(this.model.get("sender")).done(function(user){
            var e = _this.model.toJSON()
            var data = {
                sender : user.username,
                date : toChatTime(new Date(e.send_time)),
                msg : e.msg
            }   
            var msg = Mustache.to_html(templateMessage, data);
            _this.$el.html(msg);
        });
        return this;
    }
});


var MessageList = Backbone.Collection.extend({ model: ChatMessageModel });
/**
    A model of a chat instance:
    a list of messages plus few extra methods
*/
var ChatModel = Backbone.Model.extend({
    defaults: function(){
        var col = new MessageList();
        return {
            messages: col,
            id: 'BAD_CHAT_ID', // for debugging
            kind: 'dialogue',
            lastMessage: { send_time : 0, sender: 0, msg: "" }
        };
    },

    constructor: function(){
        var _this = this;
        Backbone.Model.apply(this, arguments);
        $.ajax({
          type: "GET",
          url: '/chat/'+this.get("kind")+'/'+this.id+'/history',
          success: function(data){
              _this.refresh(data);
          }
          //TODO: handle failure with events?
        });
    },
    post: function(msg){
        $.ajax({
            type: "POST",
            url: '/chat/'+this.get("kind")+'/'+this.id+'/post',
            data: JSON.stringify({ 'msg' : msg }),
            contentType: 'application/json'
        }).done(function () {
            //TODO: trigger some event
        }).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO: handle failure with events?
            console.error("Chat inaccessible: "+errorThrown);
        });
    },
    // update with array of "recent" messages
    // they may overlap with current array
    refresh: function(recentMessages){
        var i = 0;
        var messages = this.get("messages")
        // what equal messages means
        function equal(m1, m2){
            return m1.send_time == m2.send_time && m1.msg == m2.msg;
        }
        var last = this.get("lastMessage");
        if(messages.length){
            // need to check for stale messages in recentMessages
            last = messages.at(messages.length-1);
            for(i=0; i<recentMessages.length; i++)
                if(equal(last, recentMessages[i]))
                    break;
            // not found - all fresh
            if(i == recentMessages.length)
                i = 0;
            else
                i++; // all after i are fresh
        }
        if(recentMessages.length){
            last = recentMessages[recentMessages.length-1];
        }
        // append all fresh messages
        messages.add(recentMessages.slice(i));
        // update last message
        this.set({
            lastMessage: last,
        });
    }
})

// A full display of a singular chat as 
// a list of messages with the new message area
var ChatView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",

    initialize: function() {
        this.listenTo(this.model.get("messages"), "add", this.appendMessage);
        _.bindAll(this, 'postOnEnter', 'postOnSubmit', 
            'fireBackEvent', 'appendMessage', 'onUpdate');
        var panel = templateFrom(function(){/*
  <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            <button class="btn btn-primary" id="back">Back</button>
        </h3>
      </div>
      <div class="panel-body">
        <div class="media" id="chat_message_list">
        </div>
      </div>
      <div class="panel-heading">
        <h3 class="panel-title">New message</h3>
      </div>
      <div class="panel-body">
        <form id="chat_input_form">
            <textarea class="form-control" rows="3" id="chat_input" placeholder="Enter message text"></textarea>
            <p></p>
            <button class="btn btn-primary pull-right">
              Отправить
            </button>
          </form>
      </div>
  </div>
      */})
        //create a view per existing element
        this.$el.html(panel);
        this.onUpdate();
    },

    onUpdate: function(){
        var elements = this.model.get("messages").map(function(m){
            return new ChatMessageView({model : m}).render().$el;
        });
        this.$("#chat_message_list").append(elements);
        this.tweakViewport();
    },

    appendMessage: function(message){
        var view = new ChatMessageView({model: message}).render();
        this.$("#chat_message_list").append(view.$el);
        this.tweakViewport();
    },

    tweakViewport: function(){
        //TODO: viewport tweaking doesn't belong here
        var n = parseFloat($('body').css('padding-top'))
        var h = $(window).height() - 330 - n;
        var obj = this.$('#chat_message_list');
        obj.css({ "overflow-y": "scroll", "height" : h });
        obj.scrollTop(232768); //TODO: silly hackzzz
    },

    postMessage: function(){
        var el = this.$("#chat_input")
        if(!/^\s*$/.test(el.val())){
            this.model.post(el.val())
            el.val("")
        }
    },

    // keypress callback
    postOnEnter: function(e){
        if(e.which == 13 && !e.shiftKey){
            this.postMessage();
            return false;
        }
        return true;
    },

    postOnSubmit: function(e){
        this.postMessage();
        return false;
    },

    fireBackEvent: function(e){
        //console.info("Trigger chatBack");
        this.model.trigger("chatBack", this.model.get("id"));
    },

    events: {
        "submit #chat_input_form" : "postOnSubmit",
        "keyup #chat_input" : "postOnEnter",
        "click #back" : "fireBackEvent"
    },

    render: function() {
        // chat_message_list
        return this;
    }
})

// A short display of chat 
// only the last message and (if not your own) sender avatar
var ChatMiniView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",


    initialize: function() {
        this.listenTo(this.model, "change", this.render);
        _.bindAll(this, 'openChat');
    },

    openChat: function(e){
        //console.info("Trigger chatEnter")
        this.model.trigger("chatEnter", this.model.get("id"));
        return true;
    },

    events: {
        "click #my_chat_id" : "openChat"
    },
    
    render: function(){
        var tmpl = templateFrom(function(){/*
            {{#data}}
            <button class="btn btn-default btn-block" id="my_chat_id" >
                <div class="pull-right">
                    <img width=64 src="/img/{{user}}/avatar/"  class="img-thumbnail">
                    <div><b> {{user}} </b></div>
                </div>
                <div>
                <h4><i>{{date}}</i></h4>
                <h4><i>{{poster}}</i>: {{msg}}</h4>
                </div>
            </button>
            {{/data}}
        */});
        var last = this.model.get("lastMessage");
        var _this = this;
        var chatId = _this.model.get("id"); //TODO: dialog only!
        if(last.send_time !== 0){
            var res = assetFetcher.fetch(last.sender);
            res.done(function(user){
                var lastPoster = user.username;
                assetFetcher.fetch(chatId).done(function(user){
                    //previous call happens before this one, so lastPoster is set
                    var obj = {
                        user : user.username,
                        poster: lastPoster,
                        date : toChatTime(new Date(last.send_time)), 
                        msg : last.msg
                    }
                    var content = Mustache.to_html(tmpl, { data: obj });
                    _this.$el.html(content);
                });    
            })

        }
        return this;
    }
});

var ChatCollection = Backbone.Collection.extend({
    model: ChatModel
})

var ChatListModel = Backbone.Model.extend({
    defaults: {
        chats: new ChatCollection(),
        user: "BAD_USER"
    },

    constructor: function(){
        var _this = this;
        Backbone.Model.apply(this, arguments);
        // forward all events from chat models (that are forwarded to collection)
        this.listenTo(this.get("chats"), "chatEnter", function(arg){
            _this.trigger("chatEnter", arg);
        });
        this.listenTo(this.get("chats"), "chatBack", function(arg){
            _this.trigger("chatBack", arg);
        });
        $.ajax({
            url:"/chat/info",
            type:"GET",
            success: function(data){
                var chats = _this.get("chats")
                var usr = _this.get("user")
                var dialogs = data.user_info.dialogs;
                var msg;
                for (var k in dialogs){
                    if (dialogs.hasOwnProperty(k)){
                        var id = k.split("-")
                        if(id[0] != usr)
                            id = id[0];
                        else if(id[1] != usr)
                            id = id[1];
                        msg = {
                            sender: dialogs[k][0],
                            send_time: dialogs[k][1],
                            msg: dialogs[k][2]
                        }
                        chats.add({
                            id: id,
                            kind: "dialogue",
                            lastMessage: msg
                        })
                    }
                }
                _this.longPoll()
            }
        })
    },

    longPoll: function(){
        var dialogs = { };
        var chats = this.get("chats");
        chats.forEach(function(e){
            //console.log("POLL: "+new Date(e.get("lastMessage").send_time).toTimeString());
            dialogs[e.get("id")] = e.get("lastMessage").send_time + 1;
        });
        var requestData = 
          '{ "dialogs": '+ JSON.stringify(dialogs) + ', "chats": {} }';
        //console.log(requestData)
        var _this = this;
        $.ajax({
            url:"/chat/refresh",
            type:"POST",
            data: requestData,
            contentType:"application/json; charset=utf-8",
            success: function(data){
                var chat = null;
                $.each(data, function(index, val) {
                    chat = _this.get("chats").find(function(model){
                        return index.search(model.get("id")) >= 0;
                    });
                    if(chat){
                        chat.refresh(val);
                    } else{
                        var ids = index.split("-");
                        var id;
                        if(ids[0] == global_context.user_pk)
                            id = ids[1];
                        else
                            id = ids[0];
                        chat = new ChatModel({
                            id: id,
                            kind: "dialogue"
                        })
                        chat.refresh(val);
                        chats.add(chat);
                    }
                });
                _this.longPoll();
            },
            error: function(error){
                if(error.status >= 400 && error.status < 500)
                    alert(JSON.stringify(error));
                else
                    _this.longPoll();
            }
        });
    },

    add: function(model){
        this.get("chats").add(model);
    }
})

var ChatListView = Backbone.View.extend({
    tagName: "div",

    className: "panel panel-default",

    initialize: function(){
        this.listenTo(this.model, "change", this.render);
        this.listenTo(this.model.get("chats"), "add", this.addView)
    },

    addView: function(obj){
        this.$el.append(new ChatMiniView({ model: obj }).render().$el);
    },

    render: function(){
        return this;
    }
    // removeView: // TODO:
    //
})

// Entry point of chat applet
//=============================================================

console.log(global_context);
if("user_pk" in global_context){ // chat needs authenticated user
    var chats = new ChatListModel({ user: global_context.user_pk });
    var chatListView = new ChatListView({ model: chats });
    chats.on("chatEnter", function(a){
        console.info("chatEnter: "+a)
        var which = chats.get("chats").find(function(chat){
            return chat.get("id") == a;
        });
        //console.log(which);
        if(!which){
            console.error("Internal error: chat #"+a+" not found!");
        }
        else{
            $("#Messages").children().detach();
            $("#Messages").append(new ChatView({ model: which}).render().$el);
        }
    });
    chats.on("chatBack", function(a){
        console.info("chatBack: "+a);
        $("#Messages").children().detach();
        //console.log(chatListView.render().$el);
        $("#Messages").append(chatListView.render().$el);
    });

    //alert(JSON.stringify(global_context));
    if(global_context.is_owner){
        $("#Messages").append(chatListView.render().$el);
    }
    else{ // open current chat
        //var chatModel = chats.get("chats").find())
        //chat with page owner
        var chatModel = new ChatModel({ id : global_context.owner_pk });
        console.log("Msg", chatModel.get("lastMessage"));
        var chatView  = new ChatView({ model : chatModel });
        chats.add(chatModel); // add current chat to the chat list
        $("#Messages").append(chatView.render().$el);
    }
}

// end of jquery on DOM ready callback
});
/**
    Map widget with thubnail preview, and modal popup on click.
*/
$(function(){
    // add MapBox or our own tile layers
    function addTileLayers(map){
        L.mapbox.accessToken = 'pk.eyJ1IjoiYmxhY2t3aGFsZSIsImEiOiJJQTViMG1BIn0.-jSsQ86WpuMIqUERauTVdw';
        var mapboxTiles = L.tileLayer('https://{s}.tiles.mapbox.com/v3/blackwhale.ipl93iie/{z}/{x}/{y}.png');
        //MapBox connection (needs APIs see above)
        map.addLayer(mapboxTiles);
        //Our map server
        /*
        L.tileLayer.wms("http://mapper1.toolly.ru/mapcache/", {
            layers: 'default',
            format: 'image/png',
            transparent: true,
        }).addTo(map);
        */
    }

    var MapModel = Backbone.Model.extend({
        defaults:{
            latlon: [90.0, 0.0],
            popup: "Some html<b>!</b>"
        },

    });

    /**
        Custom event "enterMap".
    */
    var MapMiniView = Backbone.View.extend({
        tagName: "div",

        className: "",

        events: {
            "click #map" : 'enterMap'
        },

        initialize: function(hash){
            this.width = hash.width;
            this.height = hash.height;
            this.$el.html('<div id="map"></div>');
            this.$("#map").css({ width: this.width, height: this.height, cursor: 'pointer' });
            var map = L.map(this.$("#map").get(0), {
                zoomControl: false,
                attributionControl: false
            });
            map.dragging.disable();
            map.touchZoom.disable();
            map.doubleClickZoom.disable();
            map.scrollWheelZoom.disable();
            if (map.tap) map.tap.disable();
            addTileLayers(map);
            var marker = L.marker(this.model.get("latlon"));
            marker.addTo(map);
            map.setView(this.model.get("latlon"), 12);
            this.map = map;
            this.marker = marker;
            this.listenTo(this.model, "change", this.panToLocation)
        },

        enterMap: function(){
            console.info("enterMap event");
            this.model.trigger("enterMap");
        },

        updateMapSize: function(){
            this.map.invalidateSize();
        },

        panToLocation: function(){
            var loc = this.model.get("latlon");
            this.marker.setLatLng(loc);
            this.map.panTo(loc);
            return this;
        },

        render: function(){
            return this;
        }
    });

    var modalMapTmpl = templateFromFunction(function(){/*
    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="mapHeader" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" aria-hidden="true" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><Close></span></button>
            <h4 class="modal-title" id="mapHeader">Координаты</h4>
          </div>
          <div id="mapBody" class="modal-body">
                <div id="map"></div>
          </div>
          {{#editable}}
          <div class="modal-footer">
            <div class="pull-left">
                <button type="button" class="btn btn-primary" id="save">
                  Сохранить
                </button>
                <i>
                    Уточните свое положение на карте перетащив подвижный маркер в желаемое место.
                </i>
            </div>
          </div>
          {{/editable}}
        </div>
      </div>
    </div>
    */});
    var ModalMapView = Backbone.View.extend({
        tagName: "div",
        className: "",

        events: {
            "click #save" : 'saveLocation'
        },

        initialize: function(hash){
            var modalH = $(window).innerHeight();
            modalH = modalH > 320 ? modalH - 240 : modalH - 20;
            this.height = modalH;
            this.editable = hash.editable;
            this.listenTo(this.model, "change", this.panToLocation);
            this.$el.html(Mustache.to_html(modalMapTmpl, { editable: this.editable }));
            this.map = L.map(this.$("#map").get(0), {
                attributionControl: false
            });
            addTileLayers(this.map);
            var marker;
            var _this = this;
            if(this.editable){
                marker = L.marker(this.model.get("latlon"), {draggable:'true'});
                marker.on('dragend', function(event){
                    var marker = event.target;
                    var position = marker.getLatLng();
                    _this.model.set({ latlon : [position.lat, position.lng] });
                });
            }
            else{
                marker = L.marker(this.model.get("latlon"));
            }            
            marker.addTo(this.map);
            this.map.setView(this.model.get("latlon"), 12);
            this.marker = marker;
            // issue AJAX to change model
            $.ajax({
                type: "GET",
                url: "/api/"+global_context.owner+"/profile/main/location/",
            }).done(function(data){
                if(data.length){
                    var loc = data[0].center;
                    console.log(data);
                    _this.model.set({ latlon: [loc[1], loc[0]]}) //lonlat --> latlon
                }
            });
        },

        updateMapSize:function(){
            this.$("#map").css({ height: this.height });
            this.$("#map").css({ width: this.$("#mapBody").width() });
            //TODO: may optimize by checking what is the net change in size
            this.map.invalidateSize();
            return this;
        },

        //
        panToLocation: function(){
            var loc = this.model.get("latlon");
            this.marker.setLatLng(loc, {draggable:'true'});
            this.map.panTo(loc);
        },


        saveLocation: function(){
            // use REST API
            console.log(global_context)
            var point = this.model.get("latlon");
            $.ajax({
                type: "PUT",
                url: "/api/"+global_context.owner+"/profile/main/location/0",
                data: JSON.stringify({
                    center: [point[1], point[0]],//lng, lat in elastic
                    shape: {
                        type: "Point",
                        coordinates: [point[1], point[0]]
                    }
                })
            });
            this.$("#mapModal").modal('hide');
        },

        render: function(){
            return this;
        }
    });

// Map widgets entry point
//======================================================
    var position = new MapModel({ latlon: [55.77, 37.7] });
    var view = new MapMiniView({ model:position, width:$("#Main").width(), height:265 });
    var modal;
    if(global_context.user_pk == global_context.owner_pk)
        modal = new ModalMapView({model: position, editable: true });
    else{
        modal = new ModalMapView({model: position, editable: false });
    }
    $("#Main").append($("<h3>Координаты</h3>"));
    $("#Main").append(view.render().$el);
    $("#Main").append($("<p></p>"));
    $("#Main").append(modal.render().$el);
    position.on("enterMap", function(){
        // leafleat requires extra draw command _after_ element is displyed in DOM
        modal.$("#mapModal").modal();
    })
    modal.$('#mapModal').on('shown.bs.modal', function() {
        modal.updateMapSize();
    });
    // leafleat requires extra draw command _after_ element is displyed in DOM
    view.updateMapSize();
})