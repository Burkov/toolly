/**
 * Created by SKIff on 10.01.15.
 */
var gulp = require('gulp'),
    compass = require('gulp-compass'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('sync', function(){
    var files = [
        './h/*.php',
        './css/**/*.css',
        './img/**/*.png',
        './js/**/*.js'
    ];
    browserSync.init(files, {
        proxy: "infosys.dev/h/"
    });
});

gulp.task('compass', function(){
    gulp.src('sass/**/*.scss')
        .pipe(plumber())
        .pipe(compass({
            config_file: './config.rb',
            css: 'css',
            sass: 'sass'
        }))
        .pipe(reload({stream: true}));
});

gulp.task('watch', function() {
    gulp.run('sync');
    gulp.watch('sass/**/*.scss', ['compass']);
});
