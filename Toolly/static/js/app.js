/**
 * Created by d.ezhkov on 14.08.14.
 */
define(['jquery', 'underscore', 'sizeObserver', 'sidePanel', 'clientState', 'moment', 'searchBox', 'locationSuggester', 'settings', 'logout', 'oauth', 'popupErrors', 'fancybox',  'datepicker', 'customScroll', 'chosen', 'device', 'tooltip'],
function ($, _, sizeObserver, sidePanel, clientState, moment, searchBox, locationSuggester) {
    var app = {},
        $header = $('.global-header'),
        $topSearch = $('.header-search-cnt'),
        isOverlay = false,
        insertAtCaret = function($area,text) {
            var txtarea = $area[0];
            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
                "ff" : (document.selection ? "ie" : false ) );
            if (br == "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart ('character', -txtarea.value.length);
                strPos = range.text.length;
            }
            else if (br == "ff") strPos = txtarea.selectionStart;

            var front = (txtarea.value).substring(0,strPos);
            var back = (txtarea.value).substring(strPos,txtarea.value.length);
            txtarea.value=front+text+back;
            strPos = strPos + text.length;
            if (br == "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart ('character', -txtarea.value.length);
                range.moveStart ('character', strPos);
                range.moveEnd ('character', 0);
                range.select();
            }
            else if (br == "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }
            txtarea.scrollTop = scrollPos;
        };

    app.init = function () {
        moment.lang('ru');
        Date.parseDate = function( input, format ){
            return moment(input,format).toDate();
        };
        Date.prototype.dateFormat = function( format ){
            return moment(this).format(format);
        };
        /* Инициализация модуля - вычислителя размеров */
        sizeObserver.init();
        /* Инициализация навигатора */
        sidePanel.init();
        searchBox.init();
        locationSuggester.init();
        app.initEvents().initUI();
        if (sizeObserver.page == 'profile') { // Фиксируем шапку в Профайле
            //$header.addClass('fixed');
            require(['profile', 'services', 'portfolio'], function(profile, services, portfolio){
                profile.init(); // Инициализация модуля Профайла
                services.init();
                portfolio.init();
            })
        }

        // require sidebar modules only if logged in
        if("user_pk" in global_context){
            require(['events', 'chat', 'bookmarks'], function(events, chat, bookmarks){
                chat.init();
            });
            // plot notification icons
            require(['notifications'], function(notifications){
                var $triggers = $('.js-pane-trigger');
                $('.js-sidebar-trigger').append(notifications.$counter);
                _.each($triggers, function(e){
                    var $e = $(e);
                    if($e.attr('data-pane') == 'messages'){
                        $e.append(notifications.group("chats").$counter);
                    }
                    else if($e.attr('data-pane') == 'events'){
                        $e.append(notifications.group("events").$counter);   
                    }
                })
            })
        }

        return this;
    };

    function toggleSidebar(active){
        if(active)
            $header.addClass('expanded-sidebar');
        else
            $header.removeClass('expanded-sidebar');
        if (device.mobile()) {
            if (!active) {
                $('.full-sized-site-inner').css('height','auto');
            } else {
                //$('.full-sized-site-inner').height($('.js-pane.active').outerHeight());
            }
        }
    }

    app.initEvents = function(){
        clientState.on('sidebarActive', toggleSidebar);
        $('.js-sidebar-trigger').on('click', function(e){
            e.preventDefault();
            clientState.set({
                sidebarActive: !clientState.get('sidebarActive')
            });
        });

        /* Подсветка синим бордером формы поиска в шапке */
        $('.js-header-search, .city-select').on('focus', function(){
            $topSearch.addClass('focused');
        }).on('blur', function(){
            $topSearch.removeClass('focused');
        });

        /* Кнопка сворачивания/разворачивания результатов поиска */
        $('.js-search-results-trigger').on('click', function(e){
            e.preventDefault();
            $(this).closest('.search-results-cnt').toggleClass('offscreen');
            $(document).trigger('map.resize');
        });

        /* Показываем/скрываем пароль в форме авторизации/регистрации */
        $('.js-show-pass').on('click', function(e){
            e.preventDefault();
            var $me = $(this),
                $input = $me.prev('input'),
                type = $input.attr('type');
            if (type === 'text') {
                $input.attr('type', 'password');
            } else {
                $input.attr('type', 'text');
            }
        });

        /* Выпадашка профайла в шапке для мобильных устройств */
        $('.js-profile-dropdown').on('touchstart', function(e){
            e.preventDefault();
            $header.toggleClass('profile-opened');
            if ($header.hasClass('profile-opened')) {
                if (!isOverlay) {
                    $('.overlay').addClass('visible');
                    isOverlay = true;
                }
            } else {
                if ($header.hasClass('search-opened')) {
                    if (!isOverlay) {
                        $('.overlay').addClass('visible');
                        isOverlay = true;
                    }
                } else {
                    $('.overlay').removeClass('visible');
                    isOverlay = false;
                }
            }
        });

        /* Закрытие выпадашек по клику на оверлей для мобильных устройств */
        $('.overlay').on('touchstart', function(e){
            e.preventDefault();
            $('.overlay').removeClass('visible');
            isOverlay = false;
            $header.removeClass('profile-opened').removeClass('search-opened');
        });

        /* Выпадашка поиска в шапке для мобильных устройств */
        $('.js-search-panel-trigger').on('touchstart', function(e){
            e.preventDefault();
            $header.toggleClass('search-opened');
            if ($header.hasClass('search-opened')) {
                if (!isOverlay) {
                    $('.overlay').addClass('visible');
                    isOverlay = true;
                }
            } else {
                if ($header.hasClass('profile-opened')) {
                    if (!isOverlay) {
                        $('.overlay').addClass('visible');
                        isOverlay = true;
                    }
                } else {
                    $('.overlay').removeClass('visible');
                    isOverlay = false;
                }
            }
        });

        /* Добавление смайла в текстовое поле */
        $(document).on('click.smile','.js-smile', function(e){
            e.preventDefault();
            e.stopPropagation();
            var $me = $(this),
                $panel = $me.closest('.smiles-panel'),
                $textarea = $me.closest('.js-smile-trigger').siblings('textarea'),
                emo = $me.find('i').attr('class').replace('icon-emo-', '');
            insertAtCaret($textarea, ':' + emo + ':');
            $panel.removeClass('visible');
            setTimeout(function(){
                $panel.removeClass('block');
            }, 300);
        });

        /* Показ панели со смайлами */
        $(document).on('click.smilePanel', '.js-smile-trigger', function(e){
            e.preventDefault();
            var $panel = $(this).find('.smiles-panel');
            $panel.addClass('block');
            setTimeout(function(){
                $('.smiles-panel').addClass('visible');
            }, 100);
        });

        /* Аккордеон */
        $('.accordeon-trigger').on('click', function(e){
            e.preventDefault();
            var $parent = $(this).closest('.accordeon-item');
            if ($parent.hasClass('collapsed')) {
                $('.accordeon-item').removeClass('expanded').addClass('collapsed');
                $parent.removeClass('collapsed').addClass('expanded');
            } else {
                $parent.removeClass('expanded').addClass('collapsed');
            }
        });


        return this;
    };

    app.initUI = function(){
        $('.header-select').chosen({
            disable_search: true,
            inherit_select_classes: true
        });
        if (sizeObserver.page === 'search') {
            require(['searchMap', 'searchResults'], function(searchMap, searchResults){
                /* Инициализация модулей для страницы Поиск */
                searchMap.init();
                if (!sizeObserver.isMobile) {
                    $('.scrollable').mCustomScrollbar(); // Кастомный скролл для панелей
                }
                searchResults.init();
            });
        }

        /* Инициализация попапов */
        if (!sizeObserver.isMobile) {
            $('.js-register-popup').fancybox({
                padding: 30,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic'
            });
            $('.js-auth-popup').fancybox({
                padding: [30,0,0,0],
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic'
            });
        } else {
            $('.js-register-popup').fancybox({
                padding: [20, 20, 20, 20],
                margin: [0,0,0,0],
                fitToView: false,
                maxWidth: 640,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic'
            });
            $('.js-auth-popup').fancybox({
                padding: [20, 0, 0, 0],
                margin: [0,0,0,0],
                fitToView: false,
                maxWidth: 640,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic'
            });
        }

        if (sizeObserver.page === 'index') {
            require(['bxslider'], function(){
                /* Инициализация слайдера на главной */
                $('.index-slider').bxSlider({
                    mode: 'fade',
                    controls: false,
                    auto: true,
                    pager: false
                });
            });
        }
        else {
            // everywhere except landing
            toggleSidebar(clientState.get('sidebarActive'));
        }

        /* Инициализация секций аккордеона */
        if ($('.accordeon-cnt').length) {
            $('.accordeon-inner').each(function(idx, elt){
                $(this).css({height: $(this).find('.accordeon-content').outerHeight(true)});
                $('.accordeon-item').addClass('collapsed');
            });
        }

        /* Инициализация тултипов */
        $('.js-info').tooltipster({
            theme: 'tooltipster-knackit',
            interactive: true
        });

        return this;
    };

    return app;
});


