/**
   Cache simple GET AJAX requests.
   Done callbacks are called immediately or when result is ready.
*/
define(['jquery',], function ($) {
    // 
    var assetFetcher = {
        table: {}, // cache of data for given user url
        requests: {}, // unique user url -> ajax request

        // translate a message and call callback with the resulting object
        fetch: function(url){
            var res = { }
            var fetcher = this;
            if(url in fetcher.table){ // resource is already loaded
                // immedate call version
                return {
                    done: function(callback){
                        callback(fetcher.table[url]);    
                        return this; // finally I meant 'this' to chain calls to done
                    }
                }
            }
            if(!(url in fetcher.requests)){ // never asked before
                // issue new AJAX request
                fetcher.requests[url] = $.ajax({
                    type: "GET",
                    url: url
                    //TODO: graceful error handling:
                    // retry failed requests?
                }).done(function(res){
                    fetcher.table[url] = res;
                });
            }
            return {
                done: function(callback){
                    fetcher.requests[url].done(function(){
                        callback(fetcher.table[url]);
                    });
                    return this; // chain calls
                }
            };
        }
    };
    return assetFetcher.fetch.bind(assetFetcher);
})