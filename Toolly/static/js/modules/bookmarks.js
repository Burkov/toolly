/*
    Bookmarks section of sidebar.
*/
define(['jquery', 'backbone', 'utils', 'clientState', 'asyncUserInfo'], 
function($, Backbone, utils, clientState, userInfo){
    var profBookmarks = global_context.bookmarks;
    var bookmarks = [];
    var $search = $('.contacts-search');
    for(var key in profBookmarks){
        if(profBookmarks.hasOwnProperty(key)){
            bookmarks.push({
                id: key,
                user_id: profBookmarks[key].user_id,
                note: profBookmarks[key].note
            })
        }
    }
    var filterFunction = utils.modelFilter($search, "name");

    var BookmarkModel = Backbone.Model.extend({
        decodeAppend: function(col){
            var self = this;
            return userInfo(this.get("user_id")).done(function(user){
                self.set({
                    name: user.name,
                    page: user.page
                })
                self.view = new BookmarkView({ model : self }).render();
                col.add(self);
            });
        },
        save: function(attrs, options) {
            // Shamelessly stolen from stackoverflow
            options || (options = {});
            attrs || (attrs = _.clone(this.attributes));

            // Filter the data to send to the server
            delete attrs.id;
            delete attrs.page;
            delete attrs.name;

            options.data = JSON.stringify(attrs);

            // Proxy the call to the original save function
            Backbone.Model.prototype.save.call(this, attrs, options);
        }
    })
    var BookmarkView = Backbone.View.extend({
        tagName: "div",
        className: "contact-card js-operate-item",

        template: utils.mustacheOf("#bookmark-tmpl"),

        events: {
            "click .js-add-contact-note" : 'toggleNote',
            "submit .contact-note-form-cnt" : 'changeNote',
            'click .js-remove-item' : "deleteNote"
        },

        initialize: function(){
            this.listenTo(this.model, "change", this.render);
        },

        toggleNote: function(e){
            e.preventDefault();
            this.$('.contact-note-form-cnt').slideToggle(200);
        },

        changeNote: function(e){
            e.preventDefault();
            this.model.set({
                note: $('.js-contact-note').val()
            })
        },

        deleteNote: function(){
            var self = this;
            this.$el.addClass('operating remove');
            setTimeout(function(){
                self.model.destroy();
            }, 300);

        },

        render: function(){
            var self = this;
            var data = this.model.toJSON();
            data.has_note = (data.note != "");
            this.$el.html(this.template(data));
            if(data.has_note)
                this.$('.contact-note').html(utils.formatMessage(data.note));
            addBookmarkButton(this.$('.action-remove-user'), this.model.get("user_id"));
            require(['chat'], function(chat){
                chat.button(self.$('.action-mail'), self.model.get("user_id"));
            })
            return this;
        }
    })
    var BookmarkListView = Backbone.View.extend({
        el: $('.contacts-list-cnt'),

        template: utils.mustacheOf("#bookmark-section-tmpl"),

        initialize: function(){
            this.listenTo(bookmarkCol, "add change remove", this.render);
        },

        render: function(){
            var self = this;
            var $sections = [];
            var firstLetter = 0;
            var $section = null;
            var col = bookmarkCol.filter(filterFunction);
            col.forEach(function(m){
                // next section?
                if(m.get("name")[0] != firstLetter){
                    firstLetter = m.get("name")[0];
                    if($section) // add previous section
                        $sections.push($section);
                    $section = $(self.template({letter : firstLetter }));
                }
                $section.append(m.view.$el);
            })
            // last section if any
            if($section)
                $sections.push($section);
            this.$el.children().detach();
            this.$el.append($sections);
        }
    })
    // A view to attach to "bookmark" buttons
    var BookmarkButtonView = Backbone.View.extend({
        events: {
            "click" : "toggleBookmark"
        },

        initialize: function(attrs){
            this.user_id = attrs.user_id;
            this.listenTo(bookmarkCol, "add change remove", this.render);
        },

        haveBookmark: function(){
            return bookmarkCol.findWhere({ user_id : this.user_id }) !== undefined;
        },

        toggleBookmark: function(){
            var bm = bookmarkCol.findWhere({ user_id : this.user_id });
            if(bm !== undefined){
                console.info("Remove ", bm)
                bm.view.deleteNote();
            }
            else {
                new BookmarkModel({
                    user_id: this.user_id,
                    note: ""
                }).decodeAppend(bookmarkCol);
            }
        },

        render: function(){
            if(this.haveBookmark())
                this.$el.addClass("action-remove-user").removeClass("action-add-user");
            else
                this.$el.removeClass("action-remove-user").addClass("action-add-user");
        }
    })
    var BookmarkCollection = Backbone.Collection.extend({
        comparator: "name"
    })
    var bookmarkCol = new BookmarkCollection();
    var mainView = new BookmarkListView();
    $search.keyup(function(e){
        mainView.render();
    })
    
    userInfo(global_context.user_pk).done(function(user){
        function setCallbacks(){
            // then install add/change/remove handlers
            bookmarkCol.on("add change", function(m){
                m.save();
            })
        }
        bookmarkCol.url = "/api/"+user.page+"/profile/bookmarks";
        // add bookmarks preloaded from global_context
        var loaded = 0;
        if(bookmarks.length){
            _.forEach(bookmarks, function(b){
            var m = new BookmarkModel(b).decodeAppend(bookmarkCol).done(function(){
                    loaded += 1
                    if(loaded == bookmarks.length){
                        setCallbacks();
                    }
                });
            })
        }
        else
            setCallbacks();
        
    })
    
    function addBookmarkButton($el, user_id){
        new BookmarkButtonView({ el : $el, user_id: user_id }).render();
    }
    // export bookmark button
    return {
        button: addBookmarkButton
    }
})