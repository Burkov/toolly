/*
    Simple K-V store for pairs of timestamps: last event date <-> last event read date.
    Plus counter views to insert into DOM.

    At start uses last login as read date if can't find information in localStorage.

    API:
    
    notifications.group("chat").event(some-id, time); // add/update event in a group
    notifications.group("chat").read(some-id); // set read time to NOW
    notifications.group("xyz").unread() - number of unread in in "xyz" group
    notifications.group("xyz").$counter - element to insert into DOM
    notifications.unread() - sum of unread events in all groups
    notifications.$counter - element for sum of events

*/
define(['jquery', 'underscore', 'backbone', 'clientState'], function($, _, Backbone, clientState) {
    var module = {}, groups = {};

    var last_login = global_context.last_login;
    if(!last_login)
        last_login = new Date().valueOf();
    console.log("Last login: ", last_login);

    function stateFor(grop, id){
        return "notification."+this.name+"."+id;
    }
    function count(arr, pred) {
        var cnt = 0;
        for(var i=0; i<arr.length; i++)
            if(pred(arr[i])){
                cnt++;
            }
        return cnt;
    }

    var NotificationView = Backbone.View.extend({
        tagName: "span",
        className: "counter",

        initialize: function (attrs) {
            this.collection = attrs.collection;
            this.listenTo(this.collection, "add change", this.render);
        },

        render: function(){
            var unread = this.collection.unread();
            this.$el.html(unread);
            this.$el.toggle(unread > 0);
            return this;
        }
    })

    var TotalView = Backbone.View.extend({
        tagName: "span",
        className: "counter",

        render: function(){
            var unread = module.unread();
            this.$el.html(unread);
            this.$el.toggle(unread > 0);
            return this;
        }
    })

    var NotificationGroup = Backbone.Collection.extend({

        initialize: function(attr){
            this.name = attr.name;
            this.view = new NotificationView({ collection: this }).render();
            this.$counter = this.view.$el;
        },

        event: function (id, time) {
            var item = this.findWhere({ id: id });
            if(item)
                item.set({ time : time });
            else
                this.add({
                id : id,
                time: time,
                read: clientState.get(stateFor(this.name, id)) || last_login
            });
        },

        read: function(id){
            var item = this.findWhere({ id : id });
            if(item){
                var now  = new Date().valueOf()
                item.set({
                    read: now
                })
                var key = stateFor(this.name, id)
                //
                clientState.set({
                    key : now
                })
            }
        },

        unread: function(){
            return count(this.models, function(m){
                return m.get("read") && (m.get("read") < m.get("time"));
            })
        }
    })

    module.group = function(name){
        if(!(name in groups)){
            groups[name] = new NotificationGroup({ name: name });
            totalView.listenTo(groups[name], "add change", totalView.render);
        }
        return groups[name];
    }

    module.unread = function(){
        var total = 0;
        _.each(groups, function (v, k) {
            total += v.unread();
        })
        return total;
    }

    module.$counter = function(){
        return totalView.$el;
    }

    var totalView = new TotalView().render();

    return module;
})