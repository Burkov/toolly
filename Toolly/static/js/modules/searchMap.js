define(['jquery', 'sizeObserver', 'gmapLoader', 'clientState'], function($, sizeObserver, gmapLoader, clientState){
	var searchMap = {},
		$mapCnt,
		map;

	searchMap.init = function(){
        console.groupCollapsed('Initialize searchMap');
        if (!sizeObserver.isMobile) {
            $mapCnt = $('.map-cnt');
        } else {
            $mapCnt = $('.mobile-map-cnt');
        }
		var self = this;
		searchMap.initEvents();
        console.groupEnd();
        clientState.set({
			map_ready : false
		});

		gmapLoader.done(function(){
            console.log('Init searchMap Google Map...');
            var data = clientState.get('map'),
				style = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#b5cbe4"}]},{"featureType":"landscape","stylers":[{"color":"#efefef"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#83a5b0"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#bdcdd3"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}],
        		mapOptions = {
		            center: new google.maps.LatLng(data.lat, data.lng),
		            zoom: data.zoom,
		            disableDefaultUI: true,
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            styles:style,
		            scrollwheel: true,
		            zoomControl: true,
    				zoomControlOptions: {
      					style: google.maps.ZoomControlStyle.SMALL,
      					position: google.maps.ControlPosition.LEFT_BOTTOM
    				}

		        },
		        callback;
		    /*controlPlusMinus = $(
		    	'<div class="map_control_plus_minus"><div><img src="/img/icon_zoom_out.png"/></div></div>').get();*/
			map = new google.maps.Map($mapCnt[0],mapOptions);
			//map.controls[google.maps.ControlPosition.LEFT_BOTTOM] = controlPlusMinus;
			self.map = map; // make accessible
			callback = function(){
				var center = self.getCenter();
				var zoom = self.getZoom();
				clientState.set({
					map_ready : true
				});
				clientState.set({
					map : {
						lat : center.lat,
						lng : center.lng,
						zoom : zoom
					}
				});
			};
			google.maps.event.addListener(map, 'bounds_changed', callback);
		});
	};

	searchMap.getBounds = function(){
		var bounds = this.map.getBounds();
		var ne = bounds.getNorthEast();
		var sw = bounds.getSouthWest();
		return {
			nw: { lat: ne.lat(), lng: sw.lng() },
			se: { lat: sw.lat(), lng: ne.lng() }
		};
	}

	searchMap.getZoom = function(){
		return this.map.getZoom();
	}

	searchMap.getCenter = function(){
		var c = this.map.getBounds().getCenter();
		return { lat: c.lat() , lng: c.lng() };
	}

	searchMap.setLocation = function(latlng){
		if(this.map){
			this.map.panTo(latlng);
		}
		else{
            clientState.set({
              map : {
              	lat : latlng.lat,
              	lng: latlng.lng,
              	zoom: clientState.get("map").zoom
              }
            })
            console.log("Changed to ", clientState.get("map"))
            console.log(clientState);
            //document.location = "/search";
		}
	}

    searchMap.setEventHandler = function(event, callback){

    }

	searchMap.initEvents = function(){
        console.log('Init searchMap events...');
		$(document).on('map.resize', function(){
			setTimeout(function(){
				google.maps.event.trigger(map, 'resize');
			}, 300);
		});

		return this;
	};

	return searchMap;
});
