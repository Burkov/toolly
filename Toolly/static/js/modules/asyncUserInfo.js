/**
    Get info by user id and lets attach done callback to use that info.
    They either run immediately or 
    Callback gets object of form:
    {
        name: "Full Name",
        page: "name as in /profile/<name>/"
    }
*/
define(['ajaxCache'], function (ajaxCache) {
    // 
    return function(id){
        return ajaxCache("/userinfo/"+id);
    }
})