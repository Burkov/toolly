define(['jquery', 'underscore', 'backbone', 'mustache', 'profileEdit', 'customScroll'], function($, _, Backbone, Mustache, profileEdit) {

    /**
     * This is a hack used to write multiline template html inside javascript.
     * Given a function, whose body is a commented html template, 
     * template_from_html extracts html from it and returns it as a string.
     * See: http://tomasz.janczuk.org/2013/05/multi-line-strings-in-javascript-and.html
     *
     * @param func {function} - function that contains commented-out html template
     */
    function templateFromFunction(func){
        return func.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
    }

    function centerImage($img){
        $img[0].onload = function(){  // use onload, cause otherwise image may haven't been loaded and naturalWidth=naturalHeight=0
            if ($img.get(0).naturalWidth / $img.get(0).naturalHeight >= 1) {
                $img.css('overflow', 'hidden');
                $img.css('height', '100%');
                $img.css('width', 'auto'); // this is required, otherwise scale isn't proportional
            }
            else{
                $img.css('overflow', 'hidden');
                $img.css('width', '100%');
                $img.css('height', 'auto'); // this is required, otherwise scale isn't proportional
            }
            $img.toggle(true);
        }
    }

    var ImageModel = Backbone.Model.extend({
        initialize: function(){
        },
        /*
         * @param data {FormData} - FormData, received from form with <input type="file" name="image_file"> 
         *  that contains user-selected image.
         */
        toJSON: function(){
            return {name: this.get('name'), fs: this.get('fs')};
        },
        save: function(data){
            // first create File object in the database {"name": "something"}
            var result = this.createFileObject();
            // bind postFileData to this and data
            result.done(_.bind(function(received_data){
                this.postFileData(received_data, data);
            }, this));
        },
        createFileObject: function(){
            var result = $.ajax({
                url: this.url(),
                type: 'post',
                data: JSON.stringify({ 'name': 'No name' }),
                contentType: 'application/json'
            });
            return result;
        },
        postFileData: function(received_data, data){
            this.set("id", received_data.id);
            this.set("name", 'No name');
            // then populate the "id" field of that File object with content of the real file
            var result = $.ajax({
                url: this.url()+"/"+"fs",
                type: 'put',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
            });
            // after posting the image, add this model to collection; collection.add
            // triggers callback, ImageUploadModalView.onUploadDone, set in ImageUploadModalView.onUpload
            result.done(_.bind(function() {this.collection.add(this)}, this)); 
            
            return result;
        },
        imageSrc: function(){
            /**
             *  Constructs the path to the image content as follows:
             *  this.url() points to something like /api/<pagename>/profile/portfolio/<album_id>/<image_id>
             *  we need path to the respective image in /img/<pagename>/<album_id>/<image_id> hierarchy
            **/

            // extract album_id and image_id from this.url() and create imageSrc path from them
            var urlComponents = this.url().replace(/([^\/])$/, '$1').split("/");
            var imageSrcPathEnding = urlComponents[urlComponents.length-3] + "/" + urlComponents[urlComponents.length-1] + "/";
            var imageSrcPath =  window.location.protocol + "//" + window.location.host + '/img/' + global_context.pagename + "/" + imageSrcPathEnding;
            return imageSrcPath;
        }
    });

    var ImageView = Backbone.View.extend({
        tagName: 'li',
        className: 'portfolio-item',
        template: templateFromFunction(function(){/*
            <div class="img-cnt">
                <a class="knackit-gallery-image" rel="gallery1" href="{{url}}">
                    <img src="{{url}}" alt="photo"/>
                </a>
                {{#is_editable}}
                <div class="portfolio-item-actions">
                    <a class="item-edit js-img-edit fancybox.ajax">Редактировать</a>
                    <a class="item-remove js-portfolio-img-remove">Удалить</a>
                </div>
                {{/is_editable}}
            </div>
        */}),
        render: function(){
            this.$el.html(Mustache.to_html(this.template, {
                url: this.model.imageSrc(),
                is_editable: profileEdit.get("enabled")
            }));

            this.$('.knackit-gallery-image img').toggle(false); // so that image doesn't jump upon centering
            centerImage(this.$('.knackit-gallery-image img'));

            return this;
        },
        events: {
            'click a.item-edit': 'editImage',
            'click a.item-remove': 'removeImage',
            'click a.knackit-gallery-image': 'showGallery',
        },
        editImage: function(e){
            this.model.collection.album.view.editAlbum(e);
        },
        removeImage: function(e){
            var self = this;            
            console.log(this.model.collection.album.url())
            if (this.model.collection.album.get("cover") == this.model.id){
                // first, remove the cover, than remove the model itself
                var response = $.ajax({
                    url: this.model.collection.album.url() + "/cover",
                    type: "DELETE",
                    success: function(){
                        self.model.collection.album.unset("cover");
                        self.model.collection.album.view.render();
                        self.model.destroy({success: function(){
                            self.remove();
                        }});
                    }
                });
            }
            else{
                self.model.destroy({success: function(){
                    self.remove();
                }});
            }
        },
        showGallery: function(e){
            e.preventDefault();
            e.stopPropagation();

            var images = [];
            for (i=0; i < this.model.collection.length; i++){
                images.push({type: "image", href: this.model.collection.at(i).imageSrc(), title: this.model.collection.at(i).get("name") || "Untitled"})
            }

            // get index of current image within selected album (selected album's selector hard-coded)
            var index = $(".knackit-selected-album img").index(e.target);

            // perform cyclical shift of images with slice so that clicked image is displayed first
            images = images.slice(index).concat(images.slice(0, index));

            $.fancybox.open(images, {});
        }
    });

    var ImagesCollection = Backbone.Collection.extend({
        model: ImageModel,
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output;
        }
    });

    var AlbumModel = Backbone.Model.extend({
        initialize: function(){
        },
        setId: function(data){
            this.set("id", data.id);
        },
        toJSON: function(){
            if (this.attributes.hasOwnProperty("cover")){
                return {title: this.get('title'), description: this.get('description'), files: this.get("files").toJSON(), cover: this.get("cover")};
            }
            else {
                return {title: this.get('title'), description: this.get('description'), files: this.get("files").toJSON()};
            }
            
        },
        save_cover: function(){
            var response;
            if (this.attributes.hasOwnProperty("cover")){
                response = $.ajax({
                    type: "PUT",
                    url: this.url().replace(/([^\/])$/, '$1/') + "cover",
                    contentType: "application/json",
                    data: JSON.stringify(this.get("cover"))
                });
            }
            else {
                response = $.ajax({
                    type: "DELETE",
                    url: this.url().replace(/([^\/])$/, '$1/') + "cover"
                });
            }

            return response;
        },
        save_image_names: function(){
            var responses = [];
            for (var i=0; i<this.get("files").length; i++){
                responses.push($.ajax({
                    type: "PUT",
                    url: this.get("files").at(i).url().replace(/([^\/])$/, '$1/') + "name",
                    contentType: "application/json",
                    data: JSON.stringify(this.get("files").at(i).get("name"))
                }));
            }
            return responses;
        }

    });


    var AlbumView = Backbone.View.extend({
        /*
         * This view represents the cover of the album, its title and description, nothing else.
         */
        tagName: 'li',
        className: 'portfolio-item',
        template: templateFromFunction(function(){/*
            <div class="img-cnt">
                <a class="knackit-select-album">
                    <img src="{{url}}" alt=""/>
                </a>
                {{#is_editable}}
                <div class="portfolio-item-actions">
                    <a class="item-edit">Редактировать</a>
                    <a class="item-remove">Удалить</a>
                </div>
                {{/is_editable}}
            </div>
            <div class="portfolio-item-descr">
                {{#is_editable}}
                <div class="common-form-cnt">
                    <fieldset>
                        <input type="text" name="title" class="knackit-title-input" value="{{title}}"/>
                    </fieldset>
                    <fieldset>
                        <input type="text" name="description" class="knackit-description-input" value="{{description}}"/>
                    </fieldset>
                    <fieldset>
                        <a class="btn knackit-album-save">Сохранить</a>
                        <a class="btn btn-secondary knackit-album-cancel">Отменить</a>
                    </fieldset>
                </div>
                {{/is_editable}}
                {{^is_editable}}
                    <p class="header"><a href="#">{{title}}</a></p>
                    <p>{{description}}</p>                
                {{/is_editable}}
            </div>
        */}),
        events: {
            'click .knackit-select-album': "selectAlbum",
            'click .item-edit': "editAlbum",
            'click .item-remove': "removeAlbum",
            'click .knackit-album-save': "saveAlbumTitle",
            'click .knackit-album-cancel': "cancelAlbumTitleEdit"
        },
        render: function(){
            var url = this.model.attributes.hasOwnProperty("cover") && this.model.get("cover") ? this.model.get("files").get(this.model.get("cover")).imageSrc() : "/img/album-placeholder.gif";
            this.$el.html(Mustache.to_html(this.template, {
                is_editable: profileEdit.get("enabled"),
                title: this.model.get("title"),
                description: this.model.get("description"),
                hasCover: this.model.attributes.hasOwnProperty("cover"),
                url: url
            }));

            this.$('.knackit-select-album img').toggle(false); // so that image doesn't jump upon centering
            centerImage(this.$('.knackit-select-album img'));
        },
        imgEditPopupTemplate: templateFromFunction(function(){/*
            <div class="popup-cnt img-edit-popup common-form-cnt knackit-img-edit-popup">
                <form action="" class="knackit-img-edit-popup-submit">
                    <div class="image-edit-cnt" style="position:relative">
                        <img src="{{cover_image_src}}" alt="dummy" style="position:absolute;"/>
                    </div>
                    <fieldset>
                        <input type="text" name="image-descr" id="image-descr" placeholder="Название фото"/>
                        <input class="css-checkbox" type="checkbox" name="make-cover" id="make-cover"/><label class="css-label" for="make-cover">Сделать обложкой</label>
                    </fieldset>
                    <div class="img-thumbs-cnt">
                        <ul class="img-thumbs-list">
                        </ul>
                    </div>
                    <fieldset class="form-submit-cnt">
                        <input type="submit" value="Сохранить"/><a class="btn btn-secondary" href="#">Отмена</a>
                    </fieldset>
                </form>
            </div>
        */}),
        editAlbum: function(e){

            var html = Mustache.to_html(this.imgEditPopupTemplate, {
                pagename: global_context.pagename,
                cover_image_src: this.model.attributes.hasOwnProperty("cover") && this.model.get("cover") ? this.model.get("files").get(this.model.get("cover")).imageSrc() : "/img/placeholder640x360.gif"
            });

            $.fancybox({
                padding: [60, 20, 20, 20],
                margin: [0,0,0,0],
                fitToView: true,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                content: html,
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic',
            });

            $(".knackit-img-edit-popup .image-edit-cnt img").toggle(false);
            this.resizeImage($(".knackit-img-edit-popup .image-edit-cnt img"));

            for (var i=0; i < this.model.get("files").length; i++){
                $('.knackit-img-edit-popup ul.img-thumbs-list').append($('<li class="img-thumb"><a href="#"><img src="' + this.model.get("files").at(i).imageSrc() + '" alt="image ' + i + '"/></a></li>'));
            }

            // initialize slider - commented-out, cause it doesn't work
            /*
            $('.knackit-img-edit-popup .img-thumbs-cnt').mCustomScrollbar({
                axis:'x',
                mouseWheel: {
                    axis: 'x'
                },
                advanced: {
                    updateOnContentResize: true,
                    autoExpandHorizontalScroll: true
                }
            });
            */

            // initialize temporary storage variables for image names and cover
            this.names = [];
            for (var i=0; i < this.model.get("files").length; i++){
                this.names.push(this.model.get("files").at(i).get("name"));
            }
            if (this.model.attributes.hasOwnProperty("cover") && this.model.get("cover")) {
                this.cover = this.model.get("cover");
                var cover_model = this.model.get("files").get(this.cover);
                this.index = this.model.get("files").indexOf(cover_model);
                this.editAlbumSetImageAction();                
            }
            else {
                this.cover = undefined;
                if (this.model.get("files").length > 0) {
                    this.index = 0;
                    this.editAlbumSetImageAction();
                }
            }

            // set event handlers
            $('.knackit-img-edit-popup-submit').submit(_.bind(this.editAlbumSave, this)); // submit button handler
            $('.knackit-img-edit-popup .btn.btn-secondary').click(_.bind(this.editAlbumCancel, this)); // cancel button handler
            $('.knackit-img-edit-popup ul.img-thumbs-list li img').click(_.bind(this.editAlbumSetImage, this)); // click on an image
            $('.knackit-img-edit-popup #image-descr').keyup(_.bind(this.editAlbumSetDescription, this)); // user entered the name of photo
            $('.knackit-img-edit-popup #make-cover').change(_.bind(this.editAlbumMakeCover, this)); // user put this image on album cover

        },
        editAlbumSave: function(e){
            e.stopPropagation();
            e.preventDefault();

            // save the results from temporary variables into the models
            for (var i=0; i<this.model.get("files").length;i++){
                this.model.get("files").at(i).set("name", this.names[i]);
            }
            if (this.cover){
                this.model.set("cover", this.cover);
            }
            else {
                this.model.unset("cover");
            }

            var self = this;
            var promises = this.model.save_image_names().push(this.model.save_cover()); // joint responses from requests, saving title and cover
            $.when( promises ).then(function(){
                self.render(); // in case the cover's changed
                $.fancybox.close(); // close the popup
            });

        },
        editAlbumCancel: function(e){
            $.fancybox.close(); // close the popup
        },
        resizeImage: function($img){
            // $img is jquery selector of our image
            // We need to resize the image to 640/360 constraints, creating white space if image dimensions are different
            $img[0].onload = function(){  // use onload, cause otherwise image may haven't been loaded and naturalWidth=naturalHeight=0
                console.log($img.get(0).naturalWidth, $img.get(0).naturalHeight, $img.get(0).naturalWidth/$img.get(0).naturalHeight);
                if ($img.get(0).naturalWidth / $img.get(0).naturalHeight >= 640/360) {
                    $img.width(640);  // this image is wider than our proportions, create white space to the left and right from it
                }
                else {
                    $img.height(360);  // this image is taller than our proportions, create white space at its top and bottom
                }

                // align image to center both vertically and horizontally with the following hack:
                $img.css("margin", "auto");
                $img.css("position", "absolute");
                $img.css("top", 0);
                $img.css("bottom", 0);
                $img.css("left", 0);
                $img.css("right", 0);

                $img.toggle(true);
            };
        },
        editAlbumSetImage: function(e){
            var image = e.target; // event target is <img>, not <li> or <a>
            this.index = $('.knackit-img-edit-popup ul.img-thumbs-list li img').index( image );

            this.editAlbumSetImageAction();
        },
        editAlbumSetImageAction: function(){
            // this performs the actual work when called either programmatically or by editAlbumSetImage

            // draw the selected image preview and resize it
            $(".knackit-img-edit-popup .image-edit-cnt").html('<img src="' + this.model.get("files").at(this.index).imageSrc() + '" alt="dummy"/>') // event handler won't be lost, it is delegated (attached to parent)
            $(".knackit-img-edit-popup .image-edit-cnt img").toggle(false);            
            this.resizeImage($(".knackit-img-edit-popup .image-edit-cnt img"));

            // set the values of popup inputs corresponding to the selected image
            $('.knackit-img-edit-popup #make-cover').prop("checked", false); // false by default
            if (this.cover == this.model.get("files").at(this.index).id){ $('.knackit-img-edit-popup #make-cover').prop("checked", true); } // true if cover
            $(".knackit-img-edit-popup #image-descr").val(this.names[this.index]); // set image name            
        },
        editAlbumMakeCover: function(e){
            if (!this.hasOwnProperty("index") || this.index == undefined) return;
            this.cover = this.model.get("files").at(this.index).id;
        },
        editAlbumSetDescription: function(e){
            if (!this.hasOwnProperty("index") || this.index == undefined) return;            
            this.names[this.index] = $(".knackit-img-edit-popup #image-descr").val();
        },
        removeAlbum: function(e){
            var self = this;
            $.ajax({
                url: this.model.url(),
                type: "DELETE"
            }).done(function(){
                var portfolioCollection = self.model.collection;
                var portfolioView = self.model.collection.view;
                portfolioCollection.remove(self.model);
                if (portfolioView.hasOwnProperty("selectedAlbum") && portfolioView.selectedAlbum == self.model){
                    for (var i=0; i < portfolioView.selectedAlbum.get("files").length; i++){
                        self.model.get("files").at(i).view.remove();
                        delete self.model.get("files").at(i).view
                    }
                    delete portfolioView.selectedAlbum;
                }
                self.remove();
                portfolioView.render();
            });
        },

        selectAlbum: function(e){
            var portfolioView = this.model.collection.view;

            // clear old selectedAlbum
            if (portfolioView.hasOwnProperty("selectedAlbum")){
                portfolioView.selectedAlbum.view.$('.knackit-select-album').removeClass('active');
                for (var i=0; i < portfolioView.selectedAlbum.get("files").length; i++){
                    portfolioView.selectedAlbum.get("files").at(i).view.remove();
                    delete portfolioView.selectedAlbum.get("files").at(i).view
                }
            }
            portfolioView.selectedAlbum = this.model;
            portfolioView.selectedAlbum.view.$('.knackit-select-album').addClass('active');

            // create views for this album
            for (var i=this.model.get("files").length-1; i>=0; i--){
                var imageView = new ImageView({model: this.model.get("files").at(i)});
                this.model.get("files").at(i).view = imageView;
                imageView.$el.prependTo(portfolioView.$('.knackit-selected-album'));
                imageView.render();
            }
        },

        saveAlbumTitle: function(){
            var self = this;
            var title = this.$('.knackit-title-input').val();
            var description = this.$('.knackit-description-input').val();

            var response1 = $.ajax({
                url: this.model.url().replace(/([^\/])$/, '$1/') + "title",
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(title)
            });

            var response2 = $.ajax({
                url: this.model.url().replace(/([^\/])$/, '$1/') + "description",
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(description)
            });

            $.when([response1, response2]).then(function(){
                self.model.set("title", title);
                self.model.set("description", description);
                self.cancelAlbumTitleEdit();
            });
        },

        cancelAlbumTitleEdit: function(){
            this.$('.knackit-title-input').val(this.model.get("title"));
            this.$('.knackit-description-input').val(this.model.get("description"));
        }
    });


    var PortfolioCollection = Backbone.Collection.extend({
        model: AlbumModel,
        initialize: function(){
            this.url = this.urlRoot; // portfolioCollection itself doesn't need url, but its AlbumModels do
        },
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, textStatus, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                // create array from files object
                var files = value.files;
                var files_models = [];
                var newImagesCollection = new ImagesCollection(); // we create it here and .set() later in order to set .collection property on each model
                for (var file_id in files){
                    var newImageModel = new ImageModel(files[file_id]);
                    newImageModel.set("id", file_id);
                    newImageModel.collection = newImagesCollection;
                    files_models.push(newImageModel);
                }
                newImagesCollection.set(files_models, {silent:true}); // don't fire event
                var newAlbum = new AlbumModel({files: newImagesCollection, title: value.title, description: value.description, cover: value.cover},
                                              {collection: this});
                newAlbum.set("id", key);
                newImagesCollection.url = newAlbum.url().replace(/([^\/])$/, '$1/') + "files/";
                newImagesCollection.album = newAlbum;
                models.push(newAlbum);
            }
            this.set(models);
        }
        //TODO MAKE SAVE METHOD THAT STORES THE WHOLE PortfolioCollection AS AN {}, NOT AS []
    });


    var PortfolioView = Backbone.View.extend({
        template: templateFromFunction(function(){/*
            <div class="portfolio-catalog-slider-cnt">
                <ul class="portfolio-gallery catalog-gallery">
                    {{#is_editable}}
                    <li class="portfolio-item">
                        <div class="img-cnt empty-cnt">
                            <label class="img-cnt empty-cnt" style="height: 100%; display: block;">
                                <input type="file" multiple name="album-photo" id="album-photo" style="display: none;">
                            </label>
                        </div>
                    </li>
                    {{/is_editable}}
                </ul>
            </div>
            <ul class="portfolio-gallery knackit-selected-album">
                {{#is_editable}}
                {{#has_selected_album}}
                <li class="portfolio-item">
                    <div class="img-cnt empty-cnt">
                        <label class="img-cnt empty-cnt" style="height: 100%; display: block;">
                            <input type="file" multiple name="album-photo" id="album-photo" style="display: none;">
                        </label>
                    </div>            
                </li>
                {{/has_selected_album}}
                {{/is_editable}}
            </ul>
        */}),
        initialize: function(){
            // initialize selectedAlbum
            if (this.collection.length > 0) {
                this.selectedAlbum = this.collection.at(0);
                // TODO create imageViews for its images!!!
                for (var i=0; i < this.selectedAlbum.get("files").length; i++){
                    var imageView = new ImageView({model: this.selectedAlbum.get("files").at(i)});
                    this.selectedAlbum.get("files").at(i).view = imageView;
                    this.$('.knackit-selected-album').prepend(imageView.$el);
                }
            }
            // listen to profileEdit editable
            this.listenTo(profileEdit, "change", this.render);
        },
        events:{
            "change .catalog-gallery #album-photo": "createAlbum",
            "change .knackit-selected-album #album-photo": "addImages"
        },
        render: function(){
            // detach all the nested views, then replace content, then attach again
            for (var i=this.collection.length-1; i>=0; i--){
                this.collection.at(i).view.$el.detach();
            }
            if (this.hasOwnProperty("selectedAlbum")) {
                for (var i=this.selectedAlbum.get("files").length-1; i>=0; i--){
                    this.selectedAlbum.get("files").at(i).view.$el.detach();
                }
            }

            var newHtml = Mustache.to_html(this.template, {is_editable: profileEdit.get("enabled"), has_selected_album: this.hasOwnProperty("selectedAlbum")});
            this.$el.html(newHtml);

            // attached and render the nested views
            for (var i=this.collection.length-1; i>=0; i--){
                this.collection.at(i).view.$el.prependTo(this.$(".catalog-gallery"));
                this.collection.at(i).view.render();
            }

            // attach custom scrollbar to albums
            this.$('.portfolio-catalog-slider-cnt').mCustomScrollbar({
                axis:'x',
                mouseWheel: {
                    axis: 'x'
                },
                advanced: {
                    updateOnContentResize: true,
                    autoExpandHorizontalScroll: true
                }
            });

            if (this.hasOwnProperty("selectedAlbum")) {
                for (var i=this.selectedAlbum.get("files").length-1; i>=0; i--){
                    this.selectedAlbum.get("files").at(i).view.$el.prependTo(this.$('.knackit-selected-album'));
                    this.selectedAlbum.get("files").at(i).view.render();
                }                
            }

            return this;
        },
        createAlbum: function(e){
            /**
             * This starts a long and ugly chain of async callbacks, resulting in chain saving of the album, its images and rendering of the views.
            **/

            var files = e.target.files;

            // create new Album and append it to portfolioCollection
            var album = new this.collection.model({title: "Альбом", description: "Описание альбома", files: new ImagesCollection()}, {collection: this.collection});
            album.get("files").album = album;
            this.collection.add(album);

            var response = album.save(null, {success: _.bind(function(data){
                this.albumSaved(data, album, files);
            }, this)});

        },
        albumSaved: function(data, album, files){
            // finish saving album
            album.setId(data);
            album.set("id", data.id);
            album.get("files").url = album.url().replace(/([^\/])$/, '$1/') + "files/";

            // create an album view for the album
            var albumView = new AlbumView({model: album});
            album.view = albumView;
            this.$('.catalog-gallery').append(albumView.$el);
            // set selectedAlbum to the new album
            this.selectedAlbum = album;

            // start saving files into the newly created album
            // when all the files are uploaded to the server, fileUploaded will create all the views required
            this.listenTo(album.get("files"), "add", _.bind(function(imageModel){this.fileUploaded(imageModel, album, files)}, this));
            this.uploadAlbumFiles(files, album.get("files"));
        },
        addImages: function(e){
            var files = e.target.files;
            var album = this.selectedAlbum;
            var imageModel = album.get("files");

            this.listenTo(album.get("files"), "add", _.bind(function(imageModel){this.fileUploaded(imageModel, album, files)}, this));
            this.uploadAlbumFiles(files, album.get("files"));
        },
        uploadAlbumFiles: function(fileList, imagesCollection){
            var data;
            for (var i = 0; i < fileList.length; i++){
                data = new FormData();
                data.append("image_file", fileList[i]);

                var imageModel = new ImageModel({}, {collection:imagesCollection});
                imageModel.save(data); // this function also adds imageModel to imagesCollection, thus calling fileUploaded callback
            }
        },        
        fileUploaded: function(imageModel, album, fileList){
            /** 
            * This function can be executed in 2 scenarios: creation of a new album (after uploadAlbumFiles) and 
            * addition of new images to selectedAlbum (after addImages).
            * 
            **/

            // In order to find out, whether all files were uploaded, we calculate the number of freshly uploaded imageModels - they don't have views yet.
            // If their number equals to the length of fileList, all the files were uploaded.
            var viewlessImageModels = 0;
            for (var i=0; i < album.get("files").length; i++){
                if (! album.get("files").at(i).hasOwnProperty("view")) viewlessImageModels = viewlessImageModels + 1;
            }

            if (viewlessImageModels == fileList.length){ // if all the files've been uploaded, (optionally) set album cover and create views
                if (album.get("files").length == fileList.length){ // this is creation of album, not addition of images to existing album
                    console.log("Creating a cover")
                    album.set("cover", album.get("files").at(0).id); // album.get("files").length > 0: otherwise user wouldn't be able to press ok in system FileChooserDialog
                    var self = this;
                    album.save_cover().done(function(){
                        self.createAndRenderViews(album);
                    });

                }
                else {
                    this.createAndRenderViews(album);
                }
            }

        },
        createAndRenderViews: function(album){
            // for each imageModel in album.get("files"), create a view in selectedAlbum
            for (var i=0; i<album.get("files").length; i++){
                if (! album.get("files").at(i).hasOwnProperty("view")){
                    var imageView = new ImageView({model: album.get("files").at(i)});
                    album.get("files").at(i).view = imageView;
                    this.$('.knackit-selected-album').prepend(imageView.$el);
                }
            }

            // draw all
            this.render();
        },
    });

    // "main" entry point

    var portfolio = {};

    portfolio.init = function(){
        if (global_context.hasOwnProperty("portfolio_url")){
            var portfolioCollection = new PortfolioCollection([]);
            portfolioCollection.url = global_context.portfolio_url;
    
            portfolioCollection.fetch({}).done(function(){
                var portfolioView = new PortfolioView({collection: portfolioCollection, el: $("div#knackit-portfolio").get(0)});
                portfolioCollection.view = portfolioView;

                // create albumViews for all the albums
                for (var i=0; i<portfolioCollection.length; i++){
                    var newAlbumView = new AlbumView({model: portfolioCollection.at(i)});
                    portfolioCollection.at(i).view = newAlbumView;
                    portfolioView.$('.catalog-gallery').append(newAlbumView.$el);
                    newAlbumView.render();
                }

                portfolioView.render();
            });
        }
    }

    return portfolio;

});