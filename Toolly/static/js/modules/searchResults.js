/**
 * Created by SKIff on 07.01.15.
 */
define(['jquery', 'moment','fancybox',  'datepicker'], function($, moment){
    var searchResults = {},
        mobileMapInited = false;


    searchResults.init = function(){
        console.groupCollapsed('Initialize searchResults');
        moment.lang('ru');
        Date.parseDate = function( input, format ){
            return moment(input,format).toDate();
        };
        Date.prototype.dateFormat = function( format ){
            return moment(this).format(format);
        };
        searchResults.initUI().initEvents();
        console.groupEnd();
    };

    searchResults.initEvents = function(){
        console.log('Init searchResults events...');

        /* Переключение между режимами "Карта/Список" на моильных устройствах */
        $('.js-search-view-switch').on('touchend', function(e){
            e.preventDefault();
            var $me = $(this),
                target = $me.attr('data-target');
            if (!$me.hasClass('active')) {
                $('.js-search-view-switch').removeClass('active');
                $me.addClass('active');
                if (target === 'map') {
                    $('.search-results-list').slideUp(200, function(){
                        $('.mobile-map-cnt').slideDown(200, function(){
                            if (!mobileMapInited) {
                                require(['searchMap'], function(searchMap){
                                    searchMap.init();
                                    mobileMapInited = true;
                                });
                            }
                        });
                    });
                } else {
                    $('.mobile-map-cnt').slideUp(200, function(){
                        $('.search-results-list').slideDown(200);
                    })
                }
            }
        });
        return this;
    };

    searchResults.initUI = function(){
        console.log('Init searchResults UI...');
        return this;
    };

    return searchResults;
});
