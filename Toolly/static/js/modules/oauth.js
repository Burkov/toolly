define(['jquery'], function($){
  //general information can be found here: https://developers.google.com/+/web/signin/
  //taken from https://developers.google.com/+/web/signin/redirect-uri-flow
  $(window).on('APP.loaded', function(){
    $("a.gp.btn-social").click(function googleSignInCallback(authResult){
      $(this).attr('href','https://accounts.google.com/o/oauth2/auth?scope=' +
        'https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login&' +
        'state=' + global_context.csrf + '&' +
        'redirect_uri=' + global_context.GOOGLE_OAUTH_REDIRECT_URI + '&'+ 
        'response_type=code&' +
        'client_id=' + global_context.GOOGLE_CLIENT_ID + '&' +
        'access_type=offline' + '&' +
        'approval_prompt=force');
        return true; // Continue with the new href.
        //http://alpha.toolly.ru{% url 'google_oauth2callback' %}&
    });
    //information on login flow can be found here:
    //https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow/v2.2#token
    $("a.fb.btn-social").click(function facebookSignInCallback(authResult){
      $(this).attr('href', 'https://www.facebook.com/dialog/oauth?state=' + global_context.csrf + '&' +
        'redirect_uri=' + global_context.FACEBOOK_OAUTH_REDIRECT_URI + '&' +
        'response_type=code&' +
        'client_id=' + global_context.FACEBOOK_CLIENT_ID + '');
    });
    $("a.vk.btn-social").click(function vkSignInCallback(authResult){
      $(this).attr('href', 'http://oauth.vk.com/authorize?state=' + global_context.csrf + '&' +
        'redirect_uri=' + global_context.VK_OAUTH_REDIRECT_URI + '&' + 
        'response_type=code&' +
        'client_id=' + global_context.VK_CLIENT_ID + '');
    });
    //twitter login flow is nuts! OAuth1 with hmac-sha1 signing and other crazy stuff!
    //see: https://dev.twitter.com/oauth/overview
    //http://stackoverflow.com/questions/332872/encode-url-in-javascript
    //to urlencode on client side, use escape(), encode() or encodeURIComponent()
  });
});