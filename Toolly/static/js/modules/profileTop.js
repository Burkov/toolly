define(['jquery', 'underscore', 'backbone', 'utils', 'profileEdit', 'fancybox'], function ($, _, Backbone, utils, profileEdit) {
    var $avatarCnt = $(".profile-user-info").find('.photo-cnt');
    var pagename = global_context.pagename;
    if(!global_context.is_owner){
        var $buttons = $('.profile-action-buttons');
        $buttons.show();
        if('user_pk' in global_context){
            require(['bookmarks','chat'], function (bookmarks, chat){
                chat.button($buttons.find('.action-mail'), global_context.owner_pk);
                bookmarks.button($buttons.find('.action-add-user'), global_context.owner_pk);
            })
        }
    }

    var AvatarModel = Backbone.Model.extend({
        save: function(){
            var data = new FormData();
            data.append("file", this.get("file"));
            $.ajax({
                type: 'PUT',
                url: '/api/'+pagename+'/profile/avatar',
                data: data,
                processData: false,
                contentType: false
            })
        }
    })

    var AvatarView = Backbone.View.extend({
        template: utils.mustacheOf('#avatar-cnt-tmpl'),

        events: {
            "click .js-img-edit" : 'editAvatar',
            "click .js-img-remove" : 'removeAvatar'
        },

        initialize: function(){
            this.listenTo(profileEdit, "change", this.render);
            this.popupTmpl = utils.mustacheOf('#avatar-popup-tmpl');
        },

        render: function(){
            this.$el.html(this.template({
                pagename: pagename,
                is_editable: profileEdit.get("enabled")
            }));
            return this;
        },

        removeAvatar: function(){
            $.ajax({
                type: 'DELETE',
                url: '/api/'+pagename+'/profile/avatar'

            }).done(function(){
                $avatarCnt.find('img').attr({
                    href: "/img/"+pagename+"/avatar/ts="+Math.random()
                })
            })
        },

        editAvatar: function(){
            var self = this;
            $.fancybox({
                padding: [60, 20, 20, 20],
                margin: [0,0,0,0],
                fitToView: true,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                content: this.popupTmpl({
                    pagename: pagename
                }),
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic',
                beforeShow: function(){
                    var $popup = $('#avatar-popup');
                    $img = $popup.find(".image-edit-cnt img");
                    $img.get(0).onload = function(){
                        var aspect = $img.get(0).naturalWidth / $img.get(0).naturalHeight;
                        if (aspect > 640/360) {
                            $img.width(640);
                            $img.height(Math.round(640/aspect));
                        }
                        else {
                            $img.width(Math.round(360*aspect));
                            $img.height(360);
                        }
                        // align image to center both vertically and horizontally with the following hack:
                        $img.css({
                            margin: "auto",
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0
                        });
                    };
                    $popup.submit('.knackit-avatar-image-upload', function(e){
                        e.preventDefault();
                        if(self.model.get("file")) self.model.save();
                        $.fancybox.close();
                    })
                    $popup.find('.profile-add-avatar').change(function(e){
                        var file = e.target.files[0];
                        self.model.set({ file : file });
                        var reader = new FileReader();
                        reader.readAsDataURL(e.target.files[0]);
                        reader.onloadend = function(){
                            $img.attr({ src : reader.result })
                            $avatarCnt.find('img').attr({ src: reader.result })
                        };
                    })
                    $popup.find('.avatar-cancel-image').click(function (){
                        $.fancybox.close();
                    });
                }
            })
        }
    })

    var avatarView = new AvatarView({el : $avatarCnt, model: new AvatarModel() }).render();
})