// Defines a model of on/off profile edit switch
define(['jquery', 'backbone'], function($, Backbone){
    var profileEdit = new Backbone.Model({
        enabled: false
    })
    if('user_pk' in global_context && global_context.is_owner){
        $('.control-cnt').show();
        // Setup events (might use a View to render button and attach event
        $('.js-editable-trigger').on('click', function (e) {
            e.preventDefault();
            profileEdit.set({
                enabled: !profileEdit.get('enabled')
            })
            // this works on all panes
            if(profileEdit.get('enabled')){
                $('.js-editable-trigger').addClass('active');
                $(".js-pane").addClass('editable');
                $('.js-editable-trigger').text("Закончить редактирование профиля")
            }
            else{
                $('.js-editable-trigger').removeClass('active');
                $('.js-editable-trigger').text("Редактировать профиль")
                $(".js-pane").removeClass('editable');
            }
        });
    }
    return profileEdit;
});