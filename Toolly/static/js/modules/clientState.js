define(['underscore', 'backbone'], function(_, Backbone){
    var stateRouter = { },
        state,
        user_id;
    _.extend(stateRouter, Backbone.Events);
    if('user_pk' in global_context){
        user_id = global_context.user_pk;
    }
    else{
        user_id = 'anonymous';
    }
    state = localStorage[user_id];
    if(state === undefined){
        state = { //setup defaults
            map : { // these control map view
                lat: 55.6,
                lng: 37.7,
                zoom: 13
            },
            autosearch: false, // should automatically trigger serach on drag end
            map_ready : false, //flip-flop switch turns on once map is loaded
            place : "", // place field of suggester
            query : "", // query field of search
            tags : [],  // tags list of search
            sidebarPage: 'contacts', // values are 'contacts', 'messages', 'events'
            sidebarActive : false, // is sidebar active
            chat : '', // '' - hide chat view; value is chat id to show in messages pane
            recentChats : [], // array of chats to switch between in messages pane
            editable : false // edit mode of profile
        }; // invisible from outside
    }
    else {
        state = JSON.parse(state);
    }
    
    stateRouter.set = function(hash){
        var changed = false;
        for (key in hash){
            if (hash.hasOwnProperty(key)){
                if(JSON.stringify(hash[key]) != JSON.stringify(state[key])){
                    state[key] = hash[key];
                    stateRouter.trigger(key, state[key]);
                    changed = true;
                }
            }
        }
        if(changed)
            localStorage[user_id] = JSON.stringify(state);
    }

    stateRouter.get = function(key){
        return state[key];
    }
    console.info("Client state: ", state);
    return stateRouter;
});
