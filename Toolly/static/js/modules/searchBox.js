define(['jquery', 'underscore', 'backbone', 'utils',  'moment', 'sizeObserver', 'searchMap', 'clientState'], 
    function($, _, Backbone, utils, moment, sizeObserver, searchMap, clientState){
    var searchBox = {},
        $search = $('.header-search'),
        $form = $('#header-search-form'),
        $resultsList = $('.search-results-list'),
        popupTmpl = utils.mustacheOf('#balloon-tmpl')
        markers = [], 
        infowindow = null;

    var autosearchModel = new Backbone.Model({
        enabled: clientState.get("autosearch")
    })

    autosearchModel.on("change", function(m){
        clientState.set({ autosearch : m.get("enabled") });
        console.log("Client autosearch:", clientState.get("autosearch"));
    });

    var MapSearchCheckBox = Backbone.View.extend({

        template: utils.mustacheOf('#map-checkbox-tmpl'),

        events:{
            "click .css-checkbox" : 'onCheck',
            "click .map-connect-button" : 'onButton'
        },

        initialize: function(){
            this.listenTo(this.model, "map", this.toButton);
        },

        onCheck: function(){
            autosearchModel.set({
                enabled: !autosearchModel.get("enabled")
            })
        },

        toButton: function(){
            if(!this.model.get("enabled")){ // not autosearch
                utils.turnInto(this, MapSearchButton);
            }
        },

        render: function(){
            this.$el.html(this.template({ checked: clientState.get("autosearch") }));
            return this;
        }

    })

    var MapSearchButton = Backbone.View.extend({
        tagName: "button",
        className: "map-connect-button",

        events: {
            "click" : 'onButton'
        },

        onButton: function(){
            refreshSearch();
            utils.turnInto(this, MapSearchCheckBox);
        },

        render: function(){
            this.$el.html("Искать в этом месте");
            return this;
        }
    })

    $('.map-wrapper').append(new MapSearchCheckBox({ model: autosearchModel})
        .render().$el);

    var CurrentTagView = Backbone.View.extend({
        tagName: "li",
        className: "tag-item",
        template: utils.mustacheOf("#search-current-tag-tmpl"),

        events: {
            "click" : 'remove'
        },

        remove: function(){
            var m = this.model.collection.remove(this.model);
        },

        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    })

    var SuggestedTagView = Backbone.View.extend({
        tagName: "li",
        className: "tag-item",
        template: utils.mustacheOf("#search-new-tag-tmpl"),

        events: {
            "click" : 'remove'
        },

        remove: function(){
            console.log(this.model);
            var m = this.model.collection.remove(this.model);
            currentTags.add(m);
        },

        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    })

    var CurrentTagsBoxView = Backbone.View.extend({
        
        template: utils.mustacheOf("#search-selected-tags-tmpl"),

        initialize: function(){
            this.listenTo(this.collection, "add remove reset", this.render);
        },

        render: function(){
            this.$el.toggle(this.collection.length != 0);//hide on empty
            this.$el.html(this.template());
            this.$('.tags-list').empty();
            this.$('.tags-list').append(this.collection.map(function(m){ 
                return new CurrentTagView({model : m}).render().$el; 
            }));
            return this;
        }
    })

    var SuggestedTagsBoxView = Backbone.View.extend({

        template: utils.mustacheOf("#search-add-tags-tmpl"),

        initialize: function(){
            this.listenTo(this.collection, "add remove reset", this.render);
        },
        render: function(){
            this.$el.toggle(this.collection.length != 0);//hide on empty
            this.$el.html(this.template());
            this.$('.tags-list').empty();
            this.$('.tags-list').append(this.collection.map(function(m){ 
                return new SuggestedTagView({model : m}).render().$el; 
            }));
            return this;
        }
    })

    var currentTags = new Backbone.Collection();
    var suggestedTags = new Backbone.Collection();
    currentTags.on("add remove reset", function () {
        clientState.set({
            tags: currentTags.toJSON()
        })
        
    })
    $(".search-tags-cnt").append([
        new CurrentTagsBoxView({ collection : currentTags }).$el,
        new SuggestedTagsBoxView({ collection: suggestedTags }).$el
    ]);

    currentTags.reset(clientState.get('tags'));

    currentTags.on("add remove", refreshSearch);

    var ResultView = Backbone.View.extend({
        tagName: "li",

        className: "search-result-item",

        template: utils.mustacheOf('#search-item-template'),

        render: function(){
            var _this = this;
            if(this.model.get("rating_info").deals_counter >= 5){
                this.model.set({
                    rating: Math.round(this.model.get("rating_info").avg_rate*10)/10.0,
                    has_rating: true
                })
            }
            var obj = this.model.toJSON();
            this.$el.html(this.template(obj));
            if('user_pk' in global_context){
                require(['bookmarks'], function(bookmarks){
                    bookmarks.button(_this.$('.action-add-user'), _this.model.get("user_id"));
                })
                require(['chat'], function(chat){
                    chat.button(_this.$('.action-mail'), _this.model.get("user_id"))
                })
                attachFancyBox(this.$('.js-search-popup'), obj);
            }
            return this;
        }
    });

    // attach fancybox
    function attachFancyBox($el, params){
        var tmpl = utils.mustacheOf("#order-service-tmpl");
        $el.click(function(e){
            var data = tmpl(params);
            e.preventDefault();
            $.fancybox({
                padding: 30,
                content: data,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic',
                beforeShow: function(){
                    var today = new Date();
                    $('.service-date').val(moment(today).format('D MMMM, YYYY'));
                    if(!sizeObserver.isMobile){
                        $('.service-date').datetimepicker({
                            lang:'ru',
                            timepicker:false,
                            format:'D MMMM, YYYY',
                            formatDate: 'D MMMM, YYYY',
                            minDate:0
                        });
                    }
                    $('.service-price').chosen({
                        inherit_select_classes: true,
                        placeholder_text_multiple: 'Выберите варианты...'
                    });
                    $('.service-price').on('change', function(){
                        var $me = $(this),
                            val = $me.val();
                        if (val) {
                            $.each(val, function(idx, elt){
                                if (elt === '---') {
                                    $me.find('option').each(function(idx, elt){
                                        if (idx > 0) {
                                            $(elt).removeAttr('selected');
                                            $(elt).attr('disabled', 'disabled');
                                        }
                                    });
                                }
                            });
                        } else {
                            $me.find('option').each(function(idx, elt){
                                $(elt).removeAttr('selected');
                                $(elt).removeAttr('disabled');
                            });
                        }
                        $me.trigger('chosen:updated');
                        $.fancybox.update();
                    });
                    $('.service-request-form').on('submit', function(e){
                        e.preventDefault();
                        var data = $(this).serializeArray();
                        var date;
                        var bullets = [];
                        var comment = "";
                        var service_id;
                        var provider_id;
                        _.forEach(data, function(arg){
                            if(arg.name == "bullets")
                                bullets.push(arg.value);
                            else if(arg.name == "service_id")
                                service_id = arg.value;
                            else if(arg.name == "provider_id")
                                provider_id = arg.value;
                            else if(arg.name == "comment")
                                comment = arg.value;
                            if(sizeObserver.isMobile){
                                if(arg.name == "service_date_mob")
                                    date = "\nЖелаемая дата " + moment().format("Do MM YYYY", arg.value);
                            }
                            else{
                                if(arg.name == "service_date")
                                    date = "\nЖелаемая дата " + moment().format("Do MM YYYY", arg.value);
                            }
                        })
                        $.ajax({
                            type: "POST",
                            url: "/deals/request",
                            data: JSON.stringify({
                                bullets: bullets,
                                service_id: service_id,
                                provider_id: provider_id,
                                comment: comment + date 
                            }),
                            contentType: 'application/json'
                        }).done(function(){
                            $.fancybox.close();
                        }).fail(function(jqXHR, textStatus, errorThrown){
                            console.error("Deals inaccessible?");
                        });
                    })
                }
            });
        });
    }

    function displayResults(result){
        var data = result.hits;
        // handle tags
        var tags = []
        if(result.extracted && result.extracted.similar){
            tags = result.extracted.similar;
        }
        if(tags.length < 8){
            result.tags.reverse();
            var additional = _.map(result.tags, function (r) {
                return r[0];
            });
            tags = tags.concat(additional);
        }
        tags = _.uniq(tags)
        tags = _.difference(tags, currentTags.pluck("name"));
        suggestedTags.reset(_.map(tags, function (tag) {
            return { name: tag };
        }));
        //cleanup
        _.each(markers, function(m){
            google.maps.event.clearListeners(m);
            m.setMap(null);
        })
        markers.length = 0;
        infowindow.close();
        // assign markers
        $resultsList.empty();
        for(var i=0; i<data.length; i++){
            var model = new Backbone.Model(data[i]);
            var viewEl = new ResultView({ model : model }).render().$el;
        
            var locs = data[i].location;
            if(locs.length){
                var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locs[0].center[1], locs[0].center[0]),
                        map : searchMap.map
                })
                markers.push(marker);
                var fn = (function (){
                    var m = marker;
                    var text = popupTmpl(model.toJSON());
                    var user_id = data[i].user_id;
                    return function(){
                        infowindow.setContent(text);
                        infowindow.open(searchMap.map, m);
                        if('user_pk' in global_context){
                            require(['chat'], function(chat){
                                chat.button($('.balloon-inner').find('.action-mail'),user_id); 
                            })
                            require(['bookmarks'], function(bookmarks){
                                bookmarks.button($('.balloon-inner').find('.action-add-user'), user_id);
                            })
                        }
                    }
                })();
                google.maps.event.addListener(marker, 'click', fn);
                (function(){
                    var cfn = fn;
                    viewEl.hover(function(){
                        cfn();
                    }, function(){
                        infowindow.close();
                    })
                })()
                google.maps.event.addListener(marker, 'click', (function(){
                    var view = viewEl;
                    return function(){
                        $('.search-results').mCustomScrollbar("scrollTo", view, {
                            scrollInertia: 900 // ms
                        });
                    }
                })());
            }
            $resultsList.append(viewEl);
        }
        
    }

    function search(text, tags, nw, se, center){
        $.ajax({
            type: "POST",
            url: '/search/users',
            data: JSON.stringify({
                "query" : text,
                "tags" : tags,
                "loc" : [+center.lng, +center.lat],
                "box_tl" : [nw.lng, nw.lat],
                "box_br" : [se.lng, se.lat],
                "from" : 0,
                "size" : 20
            }),
            contentType: 'application/json'
        }).done(displayResults).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO: nice fail notification
            console.error("Search inaccessible?");
        });
    }

    function refreshSearch(){
        var c = searchMap.getCenter();
        var bbox = searchMap.getBounds();
        var tags = currentTags.pluck("name");
        search(clientState.get('query'), tags, bbox.nw, bbox.se, c);
    }

    searchBox.init = function(){
        $search.val(clientState.get('query'));
        clientState.on('map_ready', function(map_ready){
            if(map_ready){
                console.log(searchMap.map);
                infowindow = new google.maps.InfoWindow();
                var mapCallback = function(){
                    if(autosearchModel.get("enabled")){
                        refreshSearch();
                    }
                    else{
                        autosearchModel.trigger("map");
                    }
                }
                searchMap.setEventHandler
                google.maps.event.addListener(searchMap.map, 'zoom', mapCallback);
                google.maps.event.addListener(searchMap.map, 'dragend', mapCallback);
                refreshSearch(); // once map is loaded - show initial results
                // now after intial search we empty the tags
                clientState.on("query", function (argument) {
                    console.log("Empty query!");
                    currentTags.reset(); 
                })
            }
        })
        if(sizeObserver.page != 'search'){
            $form.on('submit', function(e){
                e.preventDefault();
                clientState.set({
                    query : $search.val()
                })
                document.location = "/search";
            });
        }
        else{
            $search.val(clientState.get('query'));
            $form.on('submit', function(e){
                e.preventDefault();
                clientState.set({
                    query : $search.val()
                })
                refreshSearch();
                return false;
            });
        }
        return this;
    };

    searchBox.orderButton = attachFancyBox;


    return searchBox;
});
   
