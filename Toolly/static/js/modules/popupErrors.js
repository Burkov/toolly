require(['jquery', 'fancybox'], function($){

	$(window).on('APP.loaded', function(){
		// if user is not authenticated i.e. there exists a dom node created only for unauthenticated user
		// and there are errors in some form fields (checked by presense some error class in jquery selector)
		if ($('div#register-popup').length > 0 && $('div#register-popup ul.errors-list').length > 0){
		        $('.js-register-popup').trigger('click');
		}
	
		if ($('div#auth-popup').length > 0 && $('div#auth-popup ul.errors-list').length > 0){
		        $('.js-auth-popup').trigger('click');
		}
	});
});