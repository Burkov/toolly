define(['jquery', 'underscore', 'backbone', 'mustache', 'moment', 'sizeObserver', 'chosen', 'datepicker'], function($, _, Backbone, Mustache, moment, sizeObserver, chosen) {
	
	function mustacheOf(selector){
        return function(arg){
            return Mustache.render($(selector).html(), arg);
        };
    }


    function setDate($el, from, till, locale) {
        var $dateFrom = $el.find('.date-from'),
            $dateTo = $el.find('.date-to');

        $dateFrom.datetimepicker({
            lang: locale,
            timepicker:false,
            value:moment(from).format('D MMMM, YYYY'),
            format:'D MMMM, YYYY',
            scrollInput: false,
            formatDate: 'D MMMM, YYYY',
            closeOnDateSelect: true,
            onShow:function( ct, $input ){
                this.setOptions({
                    maxDate:$dateTo.val() ? $dateTo.val() : false
                })
            },

            onSelectDate: function (ct, $input) {
                $dateTo.datetimepicker("option", "minDate", ct);
            }
        });

        $dateTo.datetimepicker({
            lang: locale,
            timepicker:false,
            value:moment(till).format('D MMMM, YYYY'),
            format:'D MMMM, YYYY',
            scrollInput: false,
            formatDate: 'D MMMM, YYYY',
            closeOnDateSelect: true,
            onShow:function( ct, $input ){
                this.setOptions({
                   minDate:$dateFrom.val() ? $dateFrom.val() : false
                })
            },
            onSelectDate: function (ct, $input) {
                $dateFrom.datetimepicker("option", "maxDate", ct);
            }
        });
    }

    //global event aggregator
    var eventAgg = _.extend({}, Backbone.Events);

    //Common views
    var StaticItemView = Backbone.View.extend({
        events : {
            "click .header-edit-link" : "switch_to_edit"
        },

        initialize : function(options) {
            this.selector = options.selector;
            this.template = mustacheOf(this.selector);

            this.listenTo(aboutEdit, 'change', this.render);
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
        },

        render : function() {
            var tmpl = this.model.toJSON();

            if(this.selector == '#about-exp-static-tmpl' || this.selector == '#about-edu-static-tmpl')
            {
                tmpl.from = moment(tmpl.from).format('LL') 
                tmpl.till = moment(tmpl.till).format('LL') 
            }
            this.$el.html(this.template(tmpl));          
            return this;
        },

        switch_to_edit : function(e) {
            e.preventDefault();
            switch(this.selector) {
                case '#about-bio-static-tmpl' :
                    var edit = new EditBioView({model : this.model}).render().$el;
                    break;
                case '#about-exp-static-tmpl' :
                    var edit = new EditExpItemView({model : this.model}).render().$el;
                    break;
                case '#about-edu-static-tmpl' :
                    var edit = new EditEduItemView({model : this.model}).render().$el;
                    break;
                case '#about-lang-static-tmpl' :
                    var edit = new EditLangItemView({model : this.model}).render().$el;
                    break;
            }
            this.$el.after(edit);
            this.remove();
            removeAddEditBtn();
        }
    });
    
   //view for editable bio section

    var EditBioView = Backbone.View.extend({
        className: 'about-bio-section',

        template: mustacheOf('#about-bio-edit-tmpl'),

        events : {
            "click .bio-submit" : "submit",
            "click .bio-cancel" : "cancel"
        },

        initialize : function(options) {
            this.listenTo(aboutEdit, 'change', this.toStatic);
        },

        toStatic : function() {
            var static = new StaticItemView({model : this.model, className : "about-bio-section", selector : '#about-bio-static-tmpl'}).render().$el;
            this.$el.after(static);
            this.remove();
        },

        saveToModel : function() {
            this.model.set({"bio" : $("#bio-common").val()});
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        submit : function(e) {
            e.preventDefault();
            this.saveToModel();
            this.toStatic();
            this.model.save(this.model.get("bio"));
            renderAddEditBtn();
        },

        cancel : function(e) {
            e.preventDefault();
            this.toStatic();
            renderAddEditBtn();
        },
    });

    var BasicEditItemView = Backbone.View.extend({
        mobile : sizeObserver.isMobile,

        events : {
            "click .btn-submit" : "submit",
            "click .btn-cancel" : "cancel",
            "click .btn-delete" : "delete"
        },

        initialize : function(options) {
            this.listenTo(aboutEdit, 'change', function() {
                if(this.model.isNew())
                    this.model.destroy();
                else {
                    this.toStatic();
                }
            });

            this.listenTo(this.model, "destroy", this.remove);
        },

        toStatic : function() {
            var static_view = new StaticItemView({model : this.model, className : this.className, selector : this.static_selector}).render().$el;
            this.$el.after(static_view);
            this.remove();
        },

        render : function() {
            var tmpl = this.model.toJSON();
            tmpl.isMobile = this.mobile;
            this.$el.html(this.template(tmpl));
            this.editRender();
            return this;
        },

        submit : function(e) {
            e.preventDefault();
            this.saveToModel();
            this.toStatic();
            this.model.save();
        },

        cancel : function(e) {
            e.preventDefault();
            if(this.model.isNew())
                this.model.destroy();
            else 
                this.toStatic();
            renderAddEditBtn();
        },

        delete : function(e) {
            e.preventDefault();
            this.model.destroy();
            renderAddEditBtn();
        }
    });
    //view for editable experience section

    var EditExpItemView = BasicEditItemView.extend({
        className : "about-exp-section",

        template : mustacheOf('#about-exp-edit-tmpl'),

        static_selector : '#about-exp-static-tmpl',

        saveToModel : function() {
            if(!this.mobile)
                this.model.set({"kind" : this.$('#bio-qual').val(), "from" : moment(this.$('.date-from').val(), 'D MMMM, YYYY').valueOf(), 
                                "till" : moment(this.$('.date-to').val(), 'D MMMM, YYYY').valueOf(), "comment" : this.$('#bio-qual-comment').val()});
            else 
                this.model.set({"kind" : this.$('#bio-qual').val(), "from" : moment(this.$('#bio-date-from').val(), 'YYYY-MM-DD').valueOf(), 
                                "till" : moment(this.$('#bio-date-to').val(), 'YYYY-MM-DD').valueOf(), "comment" : this.$('#bio-qual-comment').val()});
            
        },

        editRender : function() {
            if(!this.mobile)
                setDate(this.$el, this.model.get("from"), this.model.get("till"), 'ru');
        }
    });

    
    //view for editable education section

    var EditEduItemView = BasicEditItemView.extend({
        className : "about-edu-section",

        template : mustacheOf('#about-edu-edit-tmpl'),

        static_selector : '#about-edu-static-tmpl',

        saveToModel : function() {
            if(!this.mobile)
                this.model.set({"org" : this.$('#bio-ed-org').val(), "from" : moment(this.$('.date-from').val(), 'D MMMM, YYYY').valueOf(), 
                                "till" : moment(this.$('.date-to').val(), 'D MMMM, YYYY').valueOf(), "comment" : this.$('#bio-ed-comment').val()});
            else
                this.model.set({"org" : this.$('#bio-ed-org').val(), "from" : moment(this.$('#bio-ed-from').val(), 'YYYY-MM-DD').valueOf(), 
                                "till" : moment(this.$('#bio-ed-to').val(), 'YYYY-MM-DD').valueOf(), "comment" : this.$('#bio-ed-comment').val()});
        },

        editRender : function() {
            if(!this.mobile)
                setDate(this.$el, this.model.get("from"), this.model.get("till"), 'ru');
        }
    });

    //view for editable language section

    var EditLangItemView = BasicEditItemView.extend({
        className : "about-lang-section",

        template : mustacheOf('#about-lang-edit-tmpl'),

        static_selector : '#about-lang-static-tmpl',

        saveToModel : function() {
            var lang_map = {ru : {"basic" : "Начальный уровень", "intermediate" : "Средний уровень", "advanced" : "Продвинутый уровень"}}
            var cur_lang = lang_map.ru
            this.model.set({localized : cur_lang[this.$('#bio-lang-level').val()], name : this.$('#bio-lang').val(), skill : this.$('#bio-lang-level').val()});
        },

        editRender : function() {
            //custom selector
            this.$('.single-select').chosen({
                disable_search: true,
                inherit_select_classes: true
            });
        }
    });

    var aboutEdit = new Backbone.Model({
        enabled : false
    });

    //callback for 'edit profile' button
    $('.js-editable-trigger').on('click', function (e) {
        e.preventDefault();
        aboutEdit.set({
            enabled: !aboutEdit.get('enabled')
        })
        if(aboutEdit.get('enabled'))
            renderAddEditBtn();
        else
            removeAddEditBtn();
    });

    function renderAddEditBtn() {
        $('.about-bio-section').children(".section-header").append(Mustache.render($('#header-edit-btn').html()))
        $('.about-exp-section').children(".section-header").append(Mustache.render($('#header-edit-btn').html()))
        $('.about-edu-section').children(".section-header").append(Mustache.render($('#header-edit-btn').html()))
        $('.about-lang-section').children(".section-header").append(Mustache.render($('#header-edit-btn').html()))
        $('.about-exp-view').append(Mustache.render($('#about-exp-add-item').html()));
        $('.about-edu-view').append(Mustache.render($('#about-edu-add-item').html()));
        $('.about-lang-view').append(Mustache.render($('#about-lang-add-item').html()));

        $('.about-exp-add-item').children('.add-more').on('click', function (e) {
            e.preventDefault();
            removeAddEditBtn();
            var tmp_model = new ExpModel();  
            exp_list.add(tmp_model);
            var new_edit = new EditExpItemView({model : tmp_model});
            $('.about-exp-view').append(new_edit.render().$el);
        });

        $('.about-edu-add-item').children('.add-more').on('click', function (e) {
            e.preventDefault();
            removeAddEditBtn();
            var tmp_model = new EduModel();  
            edu_list.add(tmp_model);
            var new_edit = new EditEduItemView({model : tmp_model});
            $('.about-edu-view').append(new_edit.render().$el);
        });

        $('.about-lang-add-item').children('.add-more').on('click', function (e) {
            e.preventDefault();
            removeAddEditBtn();
            var tmp_model = new LangModel();
            lang_list.add(tmp_model);  
            var new_edit = new EditLangItemView({model : tmp_model});
            $('.about-lang-view').append(new_edit.render().$el);
        });
    }

    function removeAddEditBtn() {
        $('.about-exp-add-item').off('click');
        $('.about-edu-add-item').off('click');
        $('.about-lang-add-item').off('click');

        $('.about-exp-add-item').empty();
        $('.about-exp-add-item').detach();
        $('.about-edu-add-item').empty();
        $('.about-edu-add-item').detach();
        $('.about-lang-add-item').empty();
        $('.about-lang-add-item').detach();

        $('.about-bio-section').find(".header-edit-link").remove();
        $('.about-exp-section').find(".header-edit-link").remove();
        $('.about-edu-section').find(".header-edit-link").remove();
        $('.about-lang-section').find(".header-edit-link").remove();
    }

    var BasicModel = Backbone.Model.extend({        
        save: function(attrs, options){

             // Shamelessly stolen from stackoverflow
            options || (options = {});
            attrs || (attrs = _.clone(this.attributes));

            // Filter the data to send to the server
            delete attrs.id;
            delete attrs.header;
            if(attrs.hasOwnProperty('localized'))                                             //remove unused attr from language section
                delete attrs.localized;
            
            console.log("Send to server : ", attrs);
            
            options.data = JSON.stringify(attrs);
            options.success = renderAddEditBtn;
            options.error = function(model, response, options) {
                model.fetch();
                renderAddEditBtn();
            }

            // Proxy the call to the original save function
            Backbone.Model.prototype.save.call(this, attrs, options);
        }
    });

    //page owner
    var owner = global_context.owner_page

    //Biography model
    var BioModel = BasicModel.extend({
                defaults : {
                    header : "",
                    bio : ""
                },
                url : "/api/" + owner + "/profile/extras/bio",
                save: function() {
                    return $.ajax({
                        type: "PUT",
                        contentType: "application/json",
                        url: this.url,
                        data: JSON.stringify(this.get("bio"))
                    })
                }
            });

    //experience section - model and collection
    var date = moment().valueOf();                                                          //current date in UNIX format(ms) to use in defaults

    var ExpModel = BasicModel.extend({
        defaults : {
            header : "",
            kind : "",
            comment : "",
            from : date,
            till : date
        }
    });

    var ExpList = Backbone.Collection.extend({
        model : ExpModel,
        url : "/api/" + owner + "/profile/extras/experience"
    });

    var exp_list = new ExpList();
    //Education section - model and collection

    var EduModel = BasicModel.extend({
        defaults : {
            header : "",
            org : "",
            comment : "",
            from : date,
            till : date,
            files : {}
        }
    });

    var EduList = Backbone.Collection.extend({
        model : EduModel,
        url : "/api/" + owner + "/profile/extras/education"
    });

    var edu_list = new EduList(); 
    //Language section - model and collection
    var LangModel = BasicModel.extend({
        defaults : {
            header : "",
            name : "",
            skill : "basic",
            localized : "Начальный уровень"
        }
    });

    var LangList = Backbone.Collection.extend({
        model : LangModel,
        url : "/api/" + owner + "/profile/extras/languages"

    });

    var lang_list = new LangList();

    function setHeaders(headers_arr) {
        _.each(headers_arr, function(elem, index, list) {

            var cur_col = elem.col_ref;
            var header_loc = elem.header;
            var selector = elem.selector;

            cur_col.on('add', function(model) {
                if(this.length == 1) {
                    model.set({header : header_loc});
                    $(selector + ' p:first').remove();
                }
            }, cur_col);

            cur_col.on('remove', function(model) {
                if(this.length != 0) 
                    this.at(0).set({header : header_loc});
                else 
                    $(selector).append(Mustache.render($('#about-header').html(), {header : header_loc}));
                
            }, cur_col);
        });
    }

    setHeaders([{col_ref : exp_list, header : 'Квалификация', selector : '.about-exp-view'}, 
                {col_ref : edu_list, header : 'Образование/курсы', selector : '.about-edu-view'},
                {col_ref : lang_list, header : 'Язык', selector : '.about-lang-view'}]);
    
    var AppView = Backbone.View.extend({
        el : $("#about_view"),

        initialize : function() {
        },  

        render : function() {
            var extras = global_context.owner_profile.extras;
            
            var bio_model = new BioModel({bio : extras.bio, header : "Общие сведения"});
            var view = new StaticItemView({model : bio_model, className : "about-bio-section", selector : '#about-bio-static-tmpl'});
            $('.about-bio-view').append(view.render().$el);

            _.each(extras.experience, function(value ,key ,list) {
                var model = new ExpModel(value);
                model.set({id : key});
                var view = new StaticItemView({model : model, className : "about-exp-section", selector : '#about-exp-static-tmpl'});
                $('.about-exp-view').append(view.render().$el);
                exp_list.add(model);
            });

            _.each(extras.education, function(value ,key ,list) {
                var model = new EduModel(value);
                model.set({id : key});
                var view = new StaticItemView({model : model, className : "about-edu-section", selector : '#about-edu-static-tmpl'});
                $('.about-edu-view').append(view.render().$el);
                edu_list.add(model);
            });

            _.each(extras.languages, function(value ,key ,list) {
                var model = new LangModel(value);
                model.set({id : key});
                var lang_map = {ru : {"basic" : "Начальный уровень", "intermediate" : "Средний уровень", "advanced" : "Продвинутый уровень"}}
                var cur_lang = lang_map.ru
                model.set({localized : cur_lang[value.skill]});
                var view = new StaticItemView({model : model, className : "about-lang-section", selector : '#about-lang-static-tmpl'});
                $('.about-lang-view').append(view.render().$el);
                lang_list.add(model);
            });
        }      
    });

    var app = new AppView

    app.render();
});