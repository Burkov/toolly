define(['jquery', 'searchMap', 'clientState', 'typeahead'], function($, searchMap, clientState){
    var locationSuggester = {},
        $search = $('.places-search'),
        locating = false;
    function lookupGIS(q, cb){
          //TODO: use Bloodhound remote
        $.ajax({
            type: "GET",
            url: '/suggest/places',
            data: { "query" : q , "field" : "suggest" }
        }).done(function(data) {
           cb(data);
        })
    }

    //
    function pinpoint(){
        // set timeout 
        if (navigator.geolocation && !locating) {
            locating = true;
            navigator.geolocation.getCurrentPosition(function (position){
                searchMap.setLocation({ 
                    lat: position.coords.latitude, 
                    lng: position.coords.longitude 
                });
                locating = false;
                //TODO: call geo coder to infer city name?
            }, function(error){
                locating = false;
            });
        }
    }

    locationSuggester.init = function(){
        var callback = function(e, item, name){
            console.log(item);
            var center = item.payload.center;
            clientState.set({
              place: item.text
            })
            // may redirect if not on a search page
            searchMap.setLocation({lng : center[0], lat: center[1] });
        }
        $('.city-select-locate-button').on("click", pinpoint);
        //.on('click', pinpoint);
        clientState.on('place', function(place){
            $('#city-select').val(place);
        });
        $('#city-select').on("focus", function(e){
            $(this).select();
        })
        .val(clientState.get('place'))
        .typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'location',
            displayKey: 'text',
            source: lookupGIS
        })
        .bind('typeahead:autocompleted', callback)
        .bind('typeahead:selected', callback)
        .css({ "vertical-align" : "" })
        
        return this;
    };


    return locationSuggester;
});
