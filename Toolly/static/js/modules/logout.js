define(['jquery'], function($){
	$(document).ready(function() {
		$("a.knackit-logout").click(function(){

			/*

			Fore some reason the following code doesn't work. Is it a bug in jquery .parseHTML code?

			var form = $('<form id="logmeout" method="post" action="/logout/">' + 
				'<input type="hidden" value="' + global_context.csrf + '" name="csrfmiddlewaretoken">' + 
                '<input type="hidden" name="next" value="' + global_context.path + '" />' +
                '<input id="button" name="submit" type="submit" value="Logout" />' +
              '</form>'); // parseHTML returns a list, we need 1 element only
			console.log(form instanceof HTMLElement);
			console.log(form);
			console.log('<form method="post" action="/logout/">' instanceof HTMLElement);
			console.log('<form method="post" action="/logout/">');
			$('a.knackit-logout').append(form);
			document.getElementById("logmeout").submit();

			*/

			// Thus we use native javascript code as described here:
			// https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Forms/Sending_forms_through_JavaScript
			var form = document.createElement("FORM");
			form.action = "/logout/";
			form.method = "POST";
			var next_input = document.createElement("input");
			form.appendChild(next_input);
			next_input.name = "next";
			next_input.value = global_context.path;
			var csrf_input = document.createElement("input");
			csrf_input.name = "csrfmiddlewaretoken";
			csrf_input.value = global_context.csrf;
			form.appendChild(csrf_input);
			form.style.display = "none";
			$('a.knackit-logout').append(form);
			form.submit();
		})
	});
})