/*
    Contacts part of Services profile page.
*/
define(['jquery', 'underscore', 'backbone', 'profileEdit', 'utils'], function($, _, Backbone, profileEdit, utils){
    //+++ data
    var prefered = global_context.owner_profile.main.preferred_contact_types;
    var contacts = global_context.owner_profile.main.contacts;
    var contactTypes = ['Telephone', 'Skype id', 'E-mail', 'VK', 'Facebook', 'Twitter', 'LinkedIn'];
    //--- data
    function selectorFor(type){
        switch(type){
            case 'Telephone':
                return '.cont-type-item.phone';
            case 'Skype id':
                return '.cont-type-item.skype';
            case 'E-mail':
                return '.cont-type-item.mail';
            case 'VK':
                return '.cont-type-item.vkontakte';
            case 'Facebook':
                return '.cont-type-item.facebook';
            case 'Twitter':
                return '.cont-type-item.twitter';
            case 'LinkedIn':
                return '.cont-type-item.linkedin';
        }
    }

    function placeholder(type){
        return new ContactModel({
            type: type
        }).resetPlaceholder();
    }

    var ContactModel = Backbone.Model.extend({
        //nothing so far - should have validate
        isPlaceholder: function(){
            return this.get("placeholder") == true;
        },
        resetPlaceholder: function(){
            this.set({
                content: "Добавить",
                placeholder: true
            })
            return this;
        },
        save: function(attrs, options){
             // Shamelessly stolen from stackoverflow
            options || (options = {});
            attrs || (attrs = _.clone(this.attributes));

            // Filter the data to send to the server
            delete attrs.id;

            options.data = JSON.stringify(attrs);

            // Proxy the call to the original save function
            Backbone.Model.prototype.save.call(this, attrs, options);
        }
    });
    var ContactsCollection = Backbone.Collection.extend({
        model: ContactModel,
        url: "/api/"+global_context.owner_page+"/profile/main/contacts"
    });

    var contactsCol = new ContactsCollection();
    var PrefferedModel = Backbone.Model.extend({
        url: "/api/"+global_context.owner_page+"/profile/main/preferred_contact_types",

        isPreffered: function (type) {
            return _.contains(this.get("items"), type);
        },
        toggle: function (type) {
            if (this.isPreffered(type)){
                this.set({
                    items: _.without(this.get("items"), type)
                });
            }
            else {
                this.get("items").push(type);
                this.set({
                    items: this.get("items") // trigger event
                });
            }
        },
        save: function(){
            return $.ajax({
                type: "PUT",
                contentType: "application/json",
                url: this.url,
                data: JSON.stringify(this.get("items"))
            })
        }
    });

    var preferedModel = new PrefferedModel({
        items: prefered
    });

    var StaticContactView = Backbone.View.extend({
        tagname: 'div',
        className: 'cont-type-inner',
        
        template: utils.mustacheOf("#cont-static-tmpl"),

        events: {
            "click .profile-contact-name" : "onEdit",
            "click .profile-remove" : "onDelete"
        },

        initialize: function() {
            this.listenTo(profileEdit, "change", this.render);
            this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "destroy", this.remove);
        },

        onEdit: function(){
            if(profileEdit.get("enabled")){
                var inp = new InputContactView({ model: this.model }).render().$el;
                this.$el.after(inp);
                this.remove();                
            }
        },

        onDelete: function(){
            if(profileEdit.get("enabled")){
                this.model.destroy();
            }
        },

        render: function(){
            var data = this.model.toJSON();
            data.edit = profileEdit.get("enabled");
            data.placeholder = this.model.isPlaceholder();
            this.$el.html(this.template(data));
            return this;
        }
    });

    var InputContactView  = Backbone.View.extend({
        tagname: 'div',
        className: 'input-group',
        
        template: utils.mustacheOf("#cont-input-tmpl"),

        events:{
            "click .confirm-edit" : "doneEdit",
            "click .cancel-edit" : "cancelEdit",
            "keypress" : "onEnter"
        },

        initialize: function() {
            this.listenTo(this.model, "destroy", this.remove);
            this.listenTo(profileEdit, "change", this.toStatic);
            if(this.model.isPlaceholder()){
                this.model.set({
                    content: ""
                })
            }
        },

        toStatic: function () {
            var inp = new StaticContactView({ model: this.model }).render().$el;
            this.$el.after(inp);
            this.remove();
        },

        onEnter: function(e){
            if(e.keyCode == 13)
                this.doneEdit();
        },

        doneEdit: function(){
            // add new placeholder if the old one is edited
            if(this.model.isPlaceholder()){
                this.model.unset("placeholder");
                this.model.collection.add(placeholder(this.model.get("type")));
            }
            this.model.set({
                content: this.$(':input').val()
            })
            this.model.save();
            this.toStatic();
        },

        cancelEdit: function(){
            if(this.model.isPlaceholder()){
                this.model.resetPlaceholder();
            }
            this.toStatic();
        },

        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    var PrefferedStarView = Backbone.View.extend({
        events:{
            "click" : "toggleDefault"
        },

        initialize: function(attrs){
            this.type = attrs.type;
        },

        toggleDefault: function(){
            this.model.toggle(this.type);
            this.model.save();
            this.render();
        },

        render: function(){
            if(this.model.isPreffered(this.type))
                this.$el.parent().addClass("default");
            else
                this.$el.parent().removeClass("default");
            this.$el.attr({ title : "Предпочтительный способ связи"});
            return this;
        }
    })

    function hideUnused(){
        //this depends on placeholders
        var presentTypes = _.uniq(contactsCol.pluck("type"));
        _.forEach(contactTypes, function (type) {
            $(selectorFor(type)).toggle(_.contains(presentTypes, type));
        })
    }

    function setPlaceholders(){
        if(profileEdit.get("enabled")){
            _.forEach(contactTypes, function(type){
                contactsCol.add(placeholder(type));
            })
        }
        else{
            _.forEach(contactsCol.where({id: undefined }), function(model){
                model.trigger("destroy", model, model.collection);
            });
        }
    }

    contactsCol.on("add", function (model) {
        var $el = new StaticContactView({ model : model }).render().$el;
        $(selectorFor(model.get("type"))).append($el);
    });
    profileEdit.on("change", setPlaceholders);
    profileEdit.on("change", hideUnused);
    

    // Initialization
    // hack in the types list section (could be a view but it's 100% static)
    $('.profile-cont-types-list').append(utils.mustacheOf('#cont-section-tmpl')({}));
    // 
    for(key in contacts){
        if(contacts.hasOwnProperty(key)){
            contactsCol.add({
                id: key,
                content: contacts[key].content,
                type: contacts[key].type
            });
        }
    }
    _.forEach(contactTypes, function(type){
        var $el = $(selectorFor(type)).children('.cont-type-status');        
        new PrefferedStarView({ 
            type : type,
            model: preferedModel,
            el : $el
        }).render();
    })
    hideUnused();
    return ;
});