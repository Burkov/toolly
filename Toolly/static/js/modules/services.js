define(['jquery', 'underscore', 'backbone', 'mustache', 'typeahead', 'profileEdit', 'moment', 'searchBox'], function($, _, Backbone, Mustache, typeahead, profileEdit, moment, searchBox) {

	/**
	 * This is a hack used to write multiline template html inside javascript.
	 * Given a function, whose body is a commented html template, 
	 * template_from_html extracts html from it and returns it as a string.
	 * See: http://tomasz.janczuk.org/2013/05/multi-line-strings-in-javascript-and.html
	 *
	 * @param func {function} - function that contains commented-out html template
	 */
	function templateFromFunction(func){
		return func.toString().match(/\/\*(([\n]|.)*)\*\//m)[1];
	}

    function centerImage($img){
        $img[0].onload = function(){  // use onload, cause otherwise image may haven't been loaded and naturalWidth=naturalHeight=0
            if ($img.get(0).naturalWidth / $img.get(0).naturalHeight >= 1) {
                $img.css('overflow', 'hidden');
                $img.css('height', '100%');
                $img.css('width', 'auto'); // this is required, otherwise scale isn't proportional
            }
            else{
                $img.css('overflow', 'hidden');
                $img.css('width', '100%');
                $img.css('height', 'auto'); // this is required, otherwise scale isn't proportional
            }
            $img.toggle(true);
        }
    }

	var PointView = Backbone.View.extend({
		template: templateFromFunction(function(){/*
            {{^is_editable}}
                <span>{{title}}</span>
                <span>{{price}}</span>            
            {{/is_editable}}
            {{#is_editable}}
                <div class="profile-service-edit-cnt common-form-cnt">
                    <form action="">
                        <fieldset class="service-name-cnt">
                            <label for="service-name">Наименование услуги</label>
                            <textarea name="service-name" id="service-name">{{title}}</textarea>
                        </fieldset>
                        <fieldset class="service-price-cnt">
                            <label for="service-price">Стоимость</label>
                            <input type="text" name="service-price" id="service-price" value="{{price}}"/>
                        </fieldset>
                        <fieldset class="service-submit-cnt">
                            <a class="btn knackit-save"">Сохранить</a>
                            <a class="btn btn-secondary knackit-discard">Удалить</a>
                            <a class="btn btn-secondary knackit-cancel">Отменить</a>
                        </fieldset>
                    </form>
                </div>
            {{/is_editable}}
		*/}),
		initialize: function(options){
			this.model = options.model;
		},
		render: function(){
            var index = this.model.view.pointViews.indexOf(this);
			var newHtml = Mustache.to_html(this.template, {
                is_editable: profileEdit.get("enabled"),
				title: this.model.get("points")[index]["title"],
				price: this.model.get("points")[index]["price"]
			})
            this.$el.html(newHtml);
		},
		events: {
			'click .knackit-discard': "discardPoint",
			'click .knackit-cancel': "cancelPoint",
			'click .knackit-save': "savePoint"
		},
        // TODO potential error here:
        // if user edits his profile in multiple browser tabs,
        // changes to one model might not be propagated to the other one.
        // As a result if we push a new element to a list in one tab
        // and then remove an element in another tab, we'll delete a wrong
        // element.
		discardPoint: function(){
            var points = this.model.get("points");
            var index = this.model.view.pointViews.indexOf(this);
            this.model.get("points").splice(index, 1);

            var self = this; // to propagate this into event handlers
            var response = this.model.save_points();
            response.done(function(){
                self.model.view.pointViews.splice(index, 1); // remove this view from pointViews
                self.remove();
            })
            return response;
        },
		cancelPoint: function(){},
		savePoint: function(){
            var points = this.model.get("points");
            var index = this.model.view.pointViews.indexOf(this);

            this.price = this.$('#service-price').val();
            this.title = this.$('#service-name').val();
            points[index] = {title: this.title, price: this.price};
            var response = this.model.save_points();
            return response;
        }
	});

	var TagView = Backbone.View.extend({
        template: templateFromFunction(function(){/*
		     {{tag_name}}{{#is_owner}}{{#is_editable}}<a class="tag-remove js-tag-remove" href="#"></a>{{/is_editable}}{{/is_owner}}
        */}),
        tagName: 'li',
        className: 'tag-item',
        /**
         * Expected parameters:
         * @param options {object} e.g. {service: ServiceModel, tag_name: "NameOfTag", tag_multiplicity: 1}
         */
        initialize: function(options){
            this.service = options.service;
            this.service_view = options.service_view;
            this.tag_name = options.tag_name;
            //this.tag_multiplicity = options.tag_multiplicity;
        },
        render: function(){
            var newHtml = Mustache.to_html(this.template, {
                tag_name: this.tag_name,
                is_editable: profileEdit.get("enabled"),    
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);
        },
        events: {
            "click a.js-tag-remove": "discardTag"
        },
        discardTag: function(e){
            e.preventDefault();
            var self = this; // to propagate this into event handlers
            var tags = this.service.get("tags");
            tags.splice(tags.indexOf(this.tag_name), 1); // remove tag from service.attributes.tags
            this.service_view.tagViews.splice(this.service_view.tagViews.indexOf(this), 1) ;// remove tagView from tagViews
            self.remove();
            $.ajax({
                type: "POST",
                url: global_context.discard_service_tag_url,
                data: JSON.stringify({id: this.service.id, tag: this.tag_name}),
                contentType: 'application/json; charset=utf-8',
            })
        }
	});

	var ServiceView = Backbone.View.extend({
		template: templateFromFunction(function(){/*
			<p class="section-header">Услуги</p>
		    <div class="profile-service-logo-cnt">
		        <div class="img-cnt">
                    {{#has_image}}<img alt="logo" src="/img/{{pagename}}/services/{{id}}/image/">{{/has_image}}
                    {{^has_image}}<img src="/img/service-placeholder.gif" alt="dummy"/>{{/has_image}}
		        </div>
                {{#is_editable}}                
		        <div class="profile-user-actions bottom">
		            <div class="profile-user-actions-inner"><a class="edit fancybox.ajax"></a><a class="remove"></a></div>
		        </div>
		        {{/is_editable}}
		    </div>
		    <div class="profile-service-inner">
                {{#is_editable}}            
		        <div class="profile-service-remove"><a class="btn btn-icon icon-remove" type="button"></a></div>
		        <div class="header">
		            <div class="input-group">
		                <input type="text" class="form-control knackit-service-name" placeholder="Введите заголовок..." value="{{name}}">
		                    <span class="input-group-btn">
		                        <a class="btn btn-icon icon-ok knackit-save-service-name" type="button"></a>
		                    </span>
		                    <span class="input-group-btn">
		                        <a class="btn btn-icon icon-cancel knackit-cancel-service-name" type="button"></a>
		                    </span>
		            </div>
		        </div>
		        {{/is_editable}}
		        {{^is_editable}}
		        <p class="header">{{name}}</p>
				<div class="profile-service-rating">{{rating}}</div>
		        {{/is_editable}}
		        <p class="profile-meta">Размещено {{ts}}</p>
		
		        <p class="small-header">Категории{{^is_editable}}<i class="js-info info-trigger tooltipstered"></i>{{/is_editable}}</p>

		        <ul class="tags-list">
		        </ul>
		        {{#is_editable}}
		        <div class="service-category-input">
		            <div class="input-group">
		                <input type="text" class="form-control knackit-tag-suggester" placeholder="Название метки...">
		                <span class="input-group-btn">
		                    <button class="btn knackit-tag-submit" type="button">Добавить</button>
		                </span>
		            </div>
		        </div>
		        {{/is_editable}}
		        {{^is_editable}}
                {{^is_owner}}
		        <div class="profile-service-order-btn">
					<a class="btn profile-order-button js-search-popup fancybox.ajax">Заказать услугу</a>
				</div>
                {{/is_owner}}
		        {{/is_editable}}
		    </div>
		    <div class="profile-service-info">
		        <div class="col12 knackit-editable-services-list">
		            <p class="small-header">Список услуг</p>
					{{#is_editable}}
			        <div class="add-more-cnt">
			            <a class="add-more">Добавить услугу</a>
			        </div>					
					{{/is_editable}}
					{{^is_editable}}
					<ul class="profile-services-list">
					</ul>
					{{/is_editable}}
		        </div>
		        <div class="col12">
		            <p class="small-header">Описание</p>
					{{#is_editable}}
		            <div class="profile-service-edit-cnt common-form-cnt">
		                <form action="">
		                    <fieldset class="service-descr-cnt">
		                        <textarea name="service-descr" id="service-descr">{{extra}}</textarea>
		                    </fieldset>
		                    <fieldset class="service-submit-cnt">
		                        <a class="btn knackit-extra-save" href="#">Сохранить</a>
		                        <a class="btn btn-secondary knackit-extra-cancel" href="#">Отменить</a>
		                    </fieldset>
		                </form>
		            </div>
		            {{/is_editable}}
		            {{^is_editable}}
		            <div class="profile-service-descr">
		            	<p>{{extra}}</p>
		            </div>
		            {{/is_editable}}
		        </div>
		    </div>

		*/}),
		className: "inner-section",
        initialize: function(options){
            this.$el.addClass("panel panel-default service-panel");

            // create tagViews for each tag in model
            this.tagViews = [];
            for (var i=0;i<this.model.get("tags").length;i++){
                var tagName = this.model.get("tags")[i];
                var newTagView = new TagView({service: this.model, service_view: this, tag_name: tagName});
                this.tagViews.push(newTagView);
            }

            // create a pointView for each point in model
            this.pointViews = [];
            for (var i=0; i<this.model.get("points").length; i++){
                var newPointView = new PointView({model: this.model});
                this.pointViews.push(newPointView);
            }
        },
        render: function(){

            // cut the child views out of html to insert them to the new html
            for (var i=0; i<this.tagViews.length; i++){
                this.tagViews[i].$el.detach();
            }
            for (var i=0; i<this.pointViews.length; i++){
                this.pointViews[i].$el.detach();
            }            

            var service_id = this.model.get("id");          //get current service id
            
            // reset view content
            var newHtml = Mustache.to_html(this.template, {
                name: this.model.get("name"),
                extra: this.model.get("extra"),
                has_image: Boolean(this.model.get("image")),
                id: service_id,
                is_editable: profileEdit.get("enabled"),
                ts : moment(new Date(parseInt(service_id.substring(0, 8), 16) * 1000)).calendar(),
                pagename: global_context.pagename,
                is_owner: global_context.is_owner
            });
            this.$el.html(newHtml);

            this.$('.img-cnt img').toggle(false);
            centerImage(this.$('.img-cnt img'));

            // insert the tag $el's back into the new content and render
            for (var i=0;i<this.tagViews.length;i++){
                this.$(".tags-list").append(this.tagViews[i].$el);
                this.tagViews[i].render();
            }

            // create and assign new $els for pointViews and render
            for (var i=this.pointViews.length-1; i>=0; i--){
                if (profileEdit.get("enabled")){
                    this.$(".knackit-editable-services-list p").after('<div class="profile-service-edit-cnt common-form-cnt"></div>');
                    var $el = this.$(".knackit-editable-services-list div.profile-service-edit-cnt.common-form-cnt").first();
                    this.pointViews[i].setElement($el);
                }
                else {
                    this.$("ul.profile-services-list").prepend('<li></li>');
                    var $el = this.$("ul.profile-services-list li").first();
                    this.pointViews[i].setElement($el);
                }
                this.pointViews[i].render();
            }

            // if editting enabled, execute tag suggester code
            if (profileEdit.get("enabled")){
                var self = this;
                var callback = function(event, item, name){ // function to be called upon autocomplete
                    self.$('.knackit-tag-suggester').val(item.text);
                };
    
                self.$('.knackit-tag-suggester').typeahead(
                {
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'tags',
                    displayKey: 'text',
                    source: self.askSuggester
                })
                .bind('typeahead:autocompleted', callback)
                .bind('typeahead:selected', callback)
                .css({ "vertical-align" : "" });                
            }
            if(global_context.hasOwnProperty("user_pk"))                                                // render order button
            {
                var obj = {};
                obj.user_id = global_context.owner_pk;
                obj.service_id = this.model.get("id");
                obj.pagename = global_context.pagename;
                obj.name = global_context.owner_name;
                obj.service = this.model.attributes;
                searchBox.orderButton(this.$('a.profile-order-button'), obj);
            }
        },

        events: {
            "click .knackit-tag-submit": "addTag",
            "click a.add-more": "addPoint",
            "click a.icon-remove": "discardService",
            "click a.knackit-extra-save": "saveExtra",
            "click a.knackit-extra-cancel": "cancelExtra",
            "click a.knackit-save-service-name": "saveName",
            "click a.knackit-cancel-service-name": "cancelName",
            "click a.edit": "openImageUploadPopup",
            "click a.remove": "removeImage",
            "keypress .knackit-tag-suggester" : "onEnter"
        },


        onEnter: function(e){
            if(e.keyCode == 13)
                this.addTag(e);
        },
        addTag: function(e){
            //e.stopPropagation();
            //e.preventDefault();
            var self = this; // for callbacks

            var newTagName = this.$(".knackit-tag-suggester").typeahead('val');
            this.$('.knackit-tag-suggester').typeahead('val', '');
            if (!_.contains(this.model.get("tags"), newTagName)){ //prevent duplicate tags in the same service
                var tags = self.model.get("tags");
                tags.push(newTagName);
                var newTagView = new TagView({service: self.model, service_view: self, tag_name: newTagName});//, tag_multiplicity: newTagMultiplicity});
                self.tagViews.push(newTagView);
                self.$('.tags-list').append(newTagView.$el);
                newTagView.render();
                this.saveTag(newTagName);
            }
            else {
                //TODO display an explicit warning about Tag duplication!!!
            }
        },
        saveTag: function(tag_name){
            var result = $.ajax({
                type: "POST",
                url: global_context.service_submit_tag_url,
                data: JSON.stringify({id: this.model.id, tag: tag_name}),
                contentType: 'application/json; charset=utf-8',
            });
            return result
        },
        askSuggester: function(query, callback){
            var result = $.ajax({
                type: "GET",
                url: '/suggest/tags',
                data: {query: query, field: "name"},
                success: function(data, textStatus, jqXHR){
                    console.log('success, data = ', data);
                    callback(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus + ', error = ', errorThrown);
                }
            });
        },
        addPoint: function(e){
            var self = this;
            var points = self.model.get("points");
            points.push({title:"", price:""});
            var response = self.model.save_points();
            response.done(function(){
                var newPointView = new PointView({model: self.model});                
                self.pointViews.push(newPointView);
                var i = self.pointViews.length-1;
                if (profileEdit.get("enabled")){
                    self.$(".knackit-editable-services-list p").after('<div class="profile-service-edit-cnt common-form-cnt"></div>');
                    var $el = self.$(".knackit-editable-services-list div.profile-service-edit-cnt.common-form-cnt").first();
                    self.pointViews[i].setElement($el);
                }
                else {
                    self.$("ul.profile-services-list").prepend('<li></li>');
                    var $el = self.$("ul.profile-services-list li").first();
                    self.pointViews[i].setElement($el);
                }

                newPointView.render();
            });
        },
        saveExtra: function(e){
            e.preventDefault();
            var extra = this.$("#service-descr").val();
            this.model.set("extra", extra);
            var response = $.ajax({
                url: this.model.url().replace(/([^\/])$/, '$1/') + "extra",
                type: "PUT",
                data: JSON.stringify(extra),
                contentType: 'application/json',
            });

            return response;
        },
        cancelExtra: function(e){
            e.preventDefault();
        },
        saveName: function(e){
            var name = this.$('.knackit-service-name').val();
            this.model.set("name", name);
            var response = $.ajax({
                url: this.model.url().replace(/([^\/])$/, '$1/') + "name",
                type: "PUT",
                data: JSON.stringify(name),
                contentType: 'application/json',
            });

            return response;
        },
        cancelName: function(e){
        },
        // --------- Fancybox-related content ----------
        imagePopupTemplate: templateFromFunction(function(){/*
        <div class="popup-cnt action-edit-popup common-form-cnt">
            <p class="form-header">Редактирование изображения услуги</p>
            <form class="knackit-service-image-upload {{id}}" action="">
                <div class="image-edit-cnt" style="position:relative">
                    {{#has_image}}<img src="/img/{{pagename}}/services/{{id}}/image/?ts={{bust}}" alt="dummy"/>{{/has_image}}
                    {{^has_image}}<img src="http://placehold.it/640x360" alt="dummy"/>{{/has_image}}
                </div>
                <div class="inner-section add-section">
                    <!-- Label tag around input is a hack used to make invisible input respond to clicks on div.inner-section.add-section -->
                    <label class="profile-add-logo" style="height: 100%">
                        <input id="profile-add-logo" type="file" name="profile-add-logo" style="display: none">
                    </label>
                </div>
                <fieldset class="form-submit-cnt">
                    <input type="submit" value="Сохранить"/><a class="btn btn-secondary knackit-cancel-service-image">Отмена</a>
                </fieldset>
            </form>
        </div>
        */}),

        openImageUploadPopup: function(e){
            var html = Mustache.to_html(this.imagePopupTemplate, {
                pagename: global_context.pagename,
                id: this.model.id, 
                has_image: Boolean(this.model.get("image")),
                bust: Math.random()
            });

            $.fancybox({
                padding: [60, 20, 20, 20],
                margin: [0,0,0,0],
                fitToView: true,
                tpl: {
                    closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                },
                content: html,
                openEffect  : 'drop',
                closeEffect : 'drop',
                nextEffect  : 'elastic',
                prevEffect  : 'elastic',
            });

            $('.knackit-service-image-upload.' + this.model.id + " .image-edit-cnt img").toggle(false);
            this.resizeImage($('.knackit-service-image-upload.' + this.model.id + " .image-edit-cnt img"));

            // attach the event handlers
            $('.knackit-service-image-upload.' + this.model.id + ' #profile-add-logo').change(_.bind(this.setImagePreview, this)); // show the preview of selected image
            $('.knackit-service-image-upload.' + this.model.id).submit(_.bind(this.saveImage, this)); // save the selected image to server
            $('.knackit-service-image-upload.' + this.model.id + ' .knackit-cancel-service-image').click(_.bind(this.cancelImageEdit, this)); // cancel the edit
        },
        resizeImage: function($img){
            // $img is jquery selector of our image
            // We need to resize the image to 640/360 constraints, creating white space if image dimensions are different
            $img[0].onload = function(){  // use onload, cause otherwise image may haven't been loaded and naturalWidth=naturalHeight=0
                console.log($img.get(0).naturalWidth, $img.get(0).naturalHeight, $img.get(0).naturalWidth/$img.get(0).naturalHeight);
                if ($img.get(0).naturalWidth / $img.get(0).naturalHeight >= 640/360) {
                    $img.width(640);  // this image is wider than our proportions, create white space to the left and right from it
                }
                else {
                    $img.height(360);  // this image is taller than our proportions, create white space at its top and bottom
                }

                // align image to center both vertically and horizontally with the following hack:
                $img.css("margin", "auto");
                $img.css("position", "absolute");
                $img.css("top", 0);
                $img.css("bottom", 0);
                $img.css("left", 0);
                $img.css("right", 0);

                $img.toggle(true);
            };
        },
        setImagePreview: function(e){
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);

            var self = this; // for event handler
            reader.onloadend = function(){
                $('.knackit-service-image-upload.' + self.model.id + ' .image-edit-cnt').html('<img src="' + reader.result + '" alt="dummy"/>');
                var $img = $('.knackit-service-image-upload.' + self.model.id + " .image-edit-cnt img");
                $img.toggle(false);
                self.resizeImage($img);
            }
        },
        saveImage: function(e){
            e.preventDefault();
            var data = new FormData(e.target);

            // if user clicked save without uploading an image - then input with file is empty - then bail out
            if (!$('.knackit-service-image-upload.' + this.model.id + ' #profile-add-logo').val()) {
                $.fancybox.close();
                return;
            }

            var self = this;
            var result = $.ajax({
                url: self.model.url()+"/"+"image",
                type: 'put',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
            });
            result.done(function(){
                self.model.set("image", data);
                var img = Mustache.render('<img src="/img/{{pagename}}/services/{{id}}/image/?ts='+Math.random()+'" alt="logo"/>', { 
                    pagename: global_context.pagename,
                    id: self.model.get("id")
                })
                self.$('.img-cnt').html(img);

                self.$('.img-cnt img').toggle(false);
                centerImage(self.$('.img-cnt img'));
                //self.$('.img-cnt img').replaceWith($('.knackit-service-image-upload.' + self.model.id + ' .image-edit-cnt img').clone()); // replace the old image with new one
                $.fancybox.close(); // close the popup
            });
        },
        cancelImageEdit: function(e){
            $.fancybox.close(); // close the popup
        },        
        removeImage: function(e){
            console.log(this.model.url()+"/"+"image");
            var self = this;
            var result = $.ajax({
                type: "DELETE",
                url: this.model.url()+"/"+"image"
            });
            result.done(function(){
                self.model.set("image", "");
                self.$('.img-cnt').html('<img src="/img/service-placeholder.gif" alt="dummy"/>'); // replace the old image with dummy

                self.$('.img-cnt img').toggle(false);
                centerImage(self.$('.img-cnt img'));
                $.fancybox.close(); // close the popup                
            });
        },

        // --------- End of Fancybox-related content ----------

        discardService: function(e){
            var self = this; // to let .done callback see this
            $.ajax({
                type: "POST",
                url: global_context.discard_service_url,
                data: JSON.stringify({"id":this.model.id}),
                contentType: 'application/json; charset=utf-8'
            }).done(function(){
                //TODO write a destructor, preventing memory leaks
                self.remove();
            });
        },
	});

	var ServiceModel = Backbone.Model.extend({
		setId: function(model, response, options){
            model.set("id", response.id);
        },
        toJSON: function(){
            // we don't want ServiceModel to send id
            return {name: this.get("name"), tags: this.get("tags"), points: this.get("points"), extra: this.get("extra"), image: this.get("image")};
        },
        save_points: function(){
            var response = $.ajax({
                url: this.url().replace(/([^\/])$/, '$1/') + "points",
                data: JSON.stringify(this.get("points")),
                type: "PUT",
                contentType: "application/json; charset=utf-8"
            });

            return response
        }
	});

	var ServicesCollection = Backbone.Collection.extend({
		model: ServiceModel,
        fetch: function(options){
            var response = this.sync('read', this, options);
            response.done(_.bind(this.onFetchDone, this));
            return response;
        },
        onFetchDone: function(data, text_status, jqXHR){
            var models = [];
            for (var key in data){
                var value = data[key];
                //each service is
                //{"name":"programmer", "tags":["html","css","js"], "points":["freelance: 2k$", "full-time: 3k$"], "extra":"text description"}
                var serviceModel = new ServiceModel({name: value.name,
                                                      tags: value.tags,
                                                      points: value.points,
                                                      extra: value.extra,
                                                      image: value.image},
                                                      {collecion: this});
                serviceModel.set("id", key); //this should automatically set serviceModel.url() as well
                models.push(serviceModel);
            }
            this.set(models);
            this.comparator = 'id';
            this.sort();
        },
        toJSON: function(){
            var output = {};
            this.forEach(function(model){
                output[model.id] = model;
            });
            return output
        }
	});

	var services = {};

	services.init = function(){

	    if (global_context.hasOwnProperty("services_url")){ // this ensures, that this code is executed only in profile_view
	        var servicesCollection = new ServicesCollection([]);
	        servicesCollection.url = global_context.services_url;
            
            var services = global_context.owner_profile.main.services;

            console.log("Services : ", services);
            _.each(services, function (value, key, list) {
                var model = new ServiceModel(value);
                model.set({id : key});
                servicesCollection.add(model);
                model.view = new ServiceView({model: model});
                $('.services-view').append(model.view.$el);
                model.view.render(); 
            });

	        var addService = function(e){
	            var newService = new servicesCollection.model({name: "", tags: [], points: [], extra: "", image: ""},
	                                                       {collection: servicesCollection}); // settings collection's not necessary, cause collection.add does this anyways
	            servicesCollection.add(newService);
	            var response = newService.save(null, {success: newService.setId});
	            response.done(_.bind(function(data){
	                newService.set("id", data.id);
	                var newServiceView = new ServiceView({model: newService, id: newService.get("id")});
	                newService.view = newServiceView;
	                newServiceView.render();
                    $('.services-view').append(newServiceView.$el);
	            }, this));
	        };

            // add button
	        $(".inner-section.add-section").click(addService);
            $(".inner-section.add-section").toggle(profileEdit.get("enabled"));

            var profileEditChange = function(){
                servicesCollection.forEach(function(model){
                    model.view.render();
                })
                console.log("profileEdit enabled = ", profileEdit.get("enabled"));
                $(".inner-section.add-section").toggle(profileEdit.get("enabled"));
            }

            profileEdit.on("change", profileEditChange);
	    }
	}

	return services;
});
