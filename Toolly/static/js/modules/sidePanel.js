/**
 * Created by d.ezhkov on 27.12.2014.
 */
define(['jquery', 'sizeObserver','clientState', 'raty', 'dot', 'customScroll'], function($, sizeObserver, clientState){
    var sidePanel = {},
        isOperating = false;

    sidePanel.init = function(){
        sidePanel.initUI().initEvents();

        return this;
    };

    function switchPage(pageId){
        var $triggers = $('.js-pane-trigger'),
            $panes = $('.js-pane'),
            i, $trigger, $pane;
        for (i=0; i<$triggers.length; i++){
            $trigger = $($triggers[i]);
            $pane = $($panes[i]);
            if($trigger.attr('data-pane') == pageId)
                $trigger.addClass('active');
            else if($trigger.attr('data-pane'))
                $trigger.removeClass('active');
            if($pane.attr('data-pane') == pageId)
                $pane.addClass('active');
            else if($pane.attr('data-pane'))
                $pane.removeClass('active');
        }
    }
    function switchChat(chatId){
        // show chat block
        if(chatId == '')
            $('.chat').removeClass('active');
        else
            $('.chat').addClass('active');
    }
    function expandTab($clickedTrigger){
        var $panesControl = $clickedTrigger.closest('.js-panes-control'); /* Находим нужный контейнер */
        var $tabsItems = $panesControl.find('.js-tab-item');
        var $panesTriggers = $panesControl.find('.js-pane-trigger'); /* Находим табы в нужном контейнере */
        var $panesContainer = $('#' + $panesControl.attr('data-panes')); /* Находим связанные контейнеры с контентом */
        var $activePane = $panesContainer.children('.js-pane.active').eq(0); /* Находим активный контейнер */
        var paneId = $clickedTrigger.attr('href');
        var $paneToShow = $(paneId); /* Находим контейнер, который нужно показать */
        if (!$clickedTrigger.hasClass('active')) { /* Если кликнут неактивный таб, то */
            $panesTriggers.removeClass('active'); /* убираем активные классы */
            $tabsItems.removeClass('active');
            $clickedTrigger.addClass('active');
            $clickedTrigger.closest('.js-tab-item').addClass('active');
            $paneToShow.addClass('active');
            $activePane.removeClass('active');
        }
    }

    clientState.on('sidebarPage', switchPage);
    clientState.on('chat', switchChat);

    sidePanel.initEvents = function(){
        if ($('.js-panes-control').length) {
            $('.js-pane-trigger').click(function(e){
                var $clickedTrigger = $(this); // Находим кликнутый таб
                if($clickedTrigger.attr('data-pane') == undefined){ // не sidebar
                    expandTab($clickedTrigger);
                }
                else{
                    e.preventDefault();
                    clientState.set({
                        sidebarPage : $clickedTrigger.attr('data-pane')
                    })
                }
            });
        }

        $('#utils').on('click', '.js-remove-item', function(e){
            e.preventDefault();
            var $me = $(this),
                $parent = $me.closest('.remove');
            $parent.addClass('operating');
            setTimeout(function(){
                $parent.remove();
            }, 300);
        });

        /* Переключение режима показа/удаления элементов в навигаторе*/
        $('#utils').on('click', '.js-utils-operate', function(e){
            e.preventDefault();
            var $me = $(this),
                operate = $me.attr('data-operate'),
                $itemsCnt = $me.closest('.js-pane').find('.utils-inner-content'),
                $items = $itemsCnt.find('.js-operate-item');
            if (!isOperating) {
                $items.each(function(idx, elt){
                    var $elt = $(elt),
                        isReadOnly = $elt.attr('data-ro');
                    if (!isReadOnly || isReadOnly !== 'true') {
                        $elt.addClass(operate);
                    }
                });
                isOperating = true;
            } else {
                $items.each(function(idx, elt){
                    var $elt = $(elt),
                        isReadOnly = $elt.attr('data-ro');
                    if (!isReadOnly || isReadOnly !== 'true') {
                        $elt.removeClass(operate);
                    }
                });
                isOperating = false;
            }
        });

        $('#utils').on('click', '.js-message-expander', function(e){
            e.preventDefault();
            var $me = $(this),
                $msg = $me.siblings('.message'),
                orig = $me.attr('title');
            //$msg.html(orig);
            $msg.trigger('destroy');
            $msg.css('height','auto');
            $me.remove();
        });

        return this;
    };

    sidePanel.initUI = function(){
        var h = 0;
            

        switchPage(clientState.get('sidebarPage'));
        switchChat(clientState.get('chat'));

        /* Инициализируем скролл в навигаторе */
        $('.chat-content').mCustomScrollbar({
            callbacks:{
                onUpdate: function(){
                    console.log("scrolled" );
                    $(this).mCustomScrollbar("scrollTo", "bottom", {
                     scrollInertia: 0,
                    });
                }
            }
        })
        $('.utils-inner-content').mCustomScrollbar();
        
        return this;
    };

    return sidePanel;

});