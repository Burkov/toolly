/**
 * Created by SKIff on 11.01.15.
 */
define(['jquery', 'sizeObserver', 'gmapLoader', 'moment', 'datepicker', 'contacts', 'address', 'about', 'settings', 'profileTop'], function($, sizeObserver, gmapLoader, moment){
    var profile = {},
        gallerySlider = null,
        sliderInited = false,
        isFancyboxInited = false;

    function openTab(tab){
        switch(tab){
            case "#services_view":
            case "#about_view":
            case "#portfolio_view":
            case "#settings_view":
                $("a[href='"+tab+"']").click();
                break;
        }
    }

    $(window).on('hashchange', function(e){
        e.preventDefault(e);
        openTab(window.location.hash);
    })
    if(window.location.hash){
        openTab(window.location.hash);
        console.log(window.location.hash);
    }

    profile.init = function(){
        console.log('Initialize lk');

        $(document).on('pane.switch', function(e, paneId){
            //if (paneId == '#portfolio_view') {
            //    if (!sliderInited) {
            //        if (!sizeObserver.isMobile) {
            //            require(['bxslider'], function(){
            //                gallerySlider = $('.catalog-gallery').bxSlider({
            //                    infiniteLoop: false,
            //                    pager: false,
            //                    controls: true,
            //                    slideWidth: 215,
            //                    slideMargin: 6,
            //                    maxSlides: 4,
            //                    minSlides: 4
            //                });
            //                sliderInited = true;
            //            });
            //        }
            //    } else {
            //        if (!sizeObserver.isMobile) {
            //            gallerySlider.reloadSlider();
            //        }
            //    }
            //}
            if (paneId == '#portfolio_edit' && !isFancyboxInited) {
                $('.js-img-edit').fancybox({
                    padding: [60, 20, 20, 20],
                    margin: [0,0,0,0],
                    fitToView: false,
                    tpl: {
                        closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
                    },
                    openEffect  : 'drop',
                    closeEffect : 'drop',
                    nextEffect  : 'elastic',
                    prevEffect  : 'elastic',
                    beforeShow  : function() {
                        /* Do some picture actions */
                    }
                });
                isFancyboxInited = true;
            }
        });
        profile.initUI();
    };

    profile.initUI = function(){

        /* Попап галереи */
        $('.js-gallery-image').fancybox({
            padding: [0, 0, 0, 0],
            fitToView: true,
            tpl: {
                closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
            },
            openEffect  : 'drop',
            closeEffect : 'drop',
            nextEffect  : 'elastic',
            prevEffect  : 'elastic'
        });

        /* Попап редактирования изображения */
        $('.js-img-edit').fancybox({
            padding: [60, 20, 20, 20],
            margin: [0,0,0,0],
            fitToView: false,
            maxWidth: 600,
            tpl: {
                closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
            },
            openEffect  : 'drop',
            closeEffect : 'drop',
            nextEffect  : 'elastic',
            prevEffect  : 'elastic',
            beforeShow  : function() {
                /* Do some picture actions */
            }
        });

        /* Попап смены пароля */
        $('.js-changepass-popup').fancybox({
            padding: 30,
            tpl: {
                closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
            },
            openEffect  : 'drop',
            closeEffect : 'drop',
            nextEffect  : 'elastic',
            prevEffect  : 'elastic'
        });

        return this;
    };

    return profile;
});
