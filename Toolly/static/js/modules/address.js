/*
    Address + map part of Services profile page.
*/
define(['jquery', 'underscore', 'backbone', 'profileEdit', 'utils', 'gmapLoader'], function($, _, Backbone, profileEdit, utils, gmapLoader){
    //+++data
    var address = global_context.owner_profile.main.address;
    var centerLocation = global_context.owner_profile.main.location;
    //---data
    //
    if(centerLocation.length){
        centerLocation = centerLocation[0].center;
    }
    else{
        //default location 
        centerLocation = [37.63, 55.7542];
    }
    var location = {
        lng: centerLocation[0],
        lat: centerLocation[1]
    }
    var $mapCnt = $('.profile-address-map');

    var module = function(){
        // hackish way to postprocess geocoder address
        function reorderAddress(argument) {
            var args = argument.split(",");
            args.reverse();
            return args.join(", ")
        }
        function editLocationPopup(){
            if(profileEdit.get("enabled")){
                $.fancybox({
                    padding: [0, 0, 0, 0],
                    fitToView: true,
                    content:
                    '<div class="popup-cnt address-map-popup">' +
                        '<div class="set-map-cnt">' +
                        '</div>' +
                        '<div class="fixed-marker"></div>' +
                        '<div class="map-caption-cnt">'+
                            'Перемещайте карту, пока маркер не окажется в нужном месте.' +
                        '</div>'+
                    '</div>',
                    minWidth: 760,
                    minHeight: 560,
                    tpl: {
                        closeBtn: '<div class="fancybox__close solid"><a class="fbx__close" href="#">&nbsp;</a></div>'
                    },
                    caption : {
                        type : 'over'
                    },
                    openEffect  : 'drop',
                    closeEffect : 'drop',
                    nextEffect  : 'elastic',
                    prevEffect  : 'elastic',
                    afterShow: function(){
                        var loc = locationModel.toJSON();
                        mapOptions = {
                            center: new google.maps.LatLng(loc.lat, loc.lng),
                            zoom: 12,
                            disableDefaultUI: true,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            styles:style,
                            scrollwheel:true,
                            zoomControl: true
                        };
                        mapPopup = new google.maps.Map($('.set-map-cnt')[0],mapOptions);
                        google.maps.event.addListener(mapPopup, 'dragend', moveCallback);
                        google.maps.event.addListener(mapPopup, 'zoom', moveCallback);
                    }
                });
            }
        }
        var StaticAddressView = Backbone.View.extend({
            el: $('.profile-address'),

            events: {
                "click .address-line" : 'toEdit',
                "keypress" : "onEnter",
                "click .confirm-edit" : 'save',
                "click .cancel-edit" : 'cancel'
            },

            initialize: function(){
                this.listenTo(profileEdit, "change", this.onChange);
                this.listenTo(this.model, "change", this.render);
                this.model.view = this;
            },

            onChange: function(){
                if(!profileEdit.get("enabled")){
                    this.editable = false;
                }
                this.render(); // also to enable placehodler
            },

            onEnter: function(e){
                if(e.keyCode == 13)
                    this.save();
            },

            save: function(){
                this.model.set({
                    address: this.$(":input").val()
                })
                //this.model.save();
                this.editable = false;
                this.render();
            },

            cancel: function(){
                this.editable = false;
                this.render();
            },

            toEdit: function(){
                if(profileEdit.get("enabled")){
                    this.editable = true;
                    this.render();
                    this.autocomplete = new google.maps.places.Autocomplete(this.$(":input")[0], 
                        { types : ["geocode"] });
                    this.autocomplete.setBounds(map.getBounds());
                    //this.autocomplete
                    this.$(':input').focus();
                    this.$(':input').select();
                }
            },

            render: function(){
                var data = this.model.toJSON();
                if(this.editable){
                    this.template = utils.mustacheOf("#address-edit-tmpl");
                }
                else
                    this.template = utils.mustacheOf("#address-tmpl");
                if(profileEdit.get("enabled")){
                    if(data.address == undefined || data.address == ""){
                        data.address = "Добавить адрес";
                    }
                }
                this.$el.html(this.template(data));
                return this;
            }
        })

        var AddressModel = Backbone.Model.extend({
            save: function(){
                return $.ajax({
                    type: "PUT",
                    url: "/api/"+global_context.owner_page+"/profile/main/address",
                    contentType: "application/json",
                    data: JSON.stringify(this.get("address"))
                })
            }
        })
        var addressModel = new AddressModel({
            address: address
        })

        var LocationModel = Backbone.Model.extend({
            save: function(){
                var obj = this.toJSON();
                var toSave = [{
                    center: [ obj.lng, obj.lat ],
                    shape: {
                        "type": "Point",
                        "coordinates": [obj.lng, obj.lat]
                    }
                }]
                return $.ajax({
                    type: "PUT",
                    url: "/api/"+global_context.owner_page+"/profile/main/location",
                    contentType: "application/json",
                    data: JSON.stringify(toSave)
                })
            }
        })

        var LocationView = Backbone.View.extend({
            initialize: function(){
                this.listenTo(this.model, "change", this.render);
                var loc = this.model.toJSON();
                this.marker = new google.maps.Marker({
                      position: new google.maps.LatLng(loc.lat,loc.lng),
                      map: map,
                      icon: "/img/icon_pin_set.png"
                });
                google.maps.event.addDomListener(this.marker, 'click', editLocationPopup);
            },

            render: function(){
                var loc = this.model.toJSON();
                this.marker.setPosition(new google.maps.LatLng(loc.lat,loc.lng))
            }
        })

        var locationModel = new LocationModel(location)
        locationModel.on("change", locationModel.save);
        addressModel.on("change", addressModel.save);
        console.log('Init lk Google Map...');
        var style = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#b5cbe4"}]},{"featureType":"landscape","stylers":[{"color":"#efefef"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#83a5b0"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#bdcdd3"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}],
            mapOptions = {
                center: new google.maps.LatLng(locationModel.get("lat"), locationModel.get("lng")),
                zoom: 12,
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles:style,
                scrollwheel:false,
                zoomControl: false
            }, 
            map, mapPopup, geocoder, autocomplete;

        map = new google.maps.Map($mapCnt[0],mapOptions);
        new StaticAddressView({
            model: addressModel
        }).render()
        //depends on map
        new LocationView({
            model: locationModel
        })
        geocoder = new google.maps.Geocoder();
        var addressCallback = function(){
            geocoder.geocode({ 'address': addressModel.get("address") }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK) {
                    var loc = results[0].geometry.location;
                    locationModel.set({
                        lat: loc.lat(),
                        lng: loc.lng()
                    })
                    // silent to break recursion
                    addressModel.set({
                        address : reorderAddress(results[0].formatted_address)
                    }, { silent : true })
                    addressModel.view.render();
                }
                else{
                    console.error("Geocoder failed due to: " +status)
                }
            })
        }
        addressModel.on("change", addressCallback);
        locationModel.on("change", function (m) {
            map.panTo(new google.maps.LatLng(m.get("lat"), m.get("lng")));
        })
        var moveCallback = function(){
            var latlng = mapPopup.getCenter();
            locationModel.set({
                lat: latlng.lat(),
                lng: latlng.lng()
            });
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    addressModel.set({
                        address: reorderAddress(results[0].formatted_address)
                    });
                } else {
                  console.error('Geocoder failed due to: ' + status);
                }
            });

        }
        google.maps.event.addListener(map, 'click', editLocationPopup);
    }
    if ($mapCnt.length) {
        gmapLoader.done(module);
    }
})