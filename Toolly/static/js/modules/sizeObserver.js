/**
 * Created by d.ezhkov on 23.12.2014.
 */
define(['jquery', 'device'], function($){
    var so = {},
        $fullSizedWrapper = $('#site-wrapper'),
        $fullSizedInner = $('.full-sized-site-inner'),
        $utilsSidebar = $('.utils-sidebar'),
        $utilsHeader = $('.utils-tabs'),
        $utils = $('#utils'),
        $utilsInner = $('.utils-inner-content'),
        $searchResultsCnt = $('.search-results-inner'),
        $searchResults = $('.search-results'),
        $searchTags = $('.search-tags-cnt'),
        $footer = $('#footer'),
        $header = $('.global-header'),
        $page = $('.js-page').attr('data-page'),
        UTILS_OUTER_PADDING = 10,
        UTILS_INNER_PADDING = 15;


    so.headerHeight = 0;
    so.footerHeight = 0;
    so.contentHeight = $fullSizedInner.height();
    so.rightBlockInnerHeight = 0;
    so.leftBlockInnerHeight = 0;
    so.utilsSidebarHeight = 0;
    so.utilsHeaderHeight = $utilsHeader.outerHeight(true);
    so.utilsInnerHeight = 0;
    so.page = $page;
    so.vpWidth = $(window).width();
    so.vpHeight = $(window).height();
    so.isPhablet = false;
    so.isMobile = false;

    so.init = function(){

        if (device.tablet() && so.vpWidth < 641) {
            so.isPhablet = true;
        }

        so.isMobile = device.mobile() || so.isPhablet;

        console.log('Init sizeObserver');
        if (device.mobile()) {
            //$('meta[name="viewport"]').attr('content','width=640');
        }
        if (so.isMobile) {
            console.log('Device is mobile');
            UTILS_OUTER_PADDING = 20;
            UTILS_INNER_PADDING = 30;
        }
        so.headerHeight = $header.outerHeight();
        so.footerHeight = $footer.outerHeight();

        so.initEvents();

        if (!so.isMobile) {
            $(window).trigger('resize');
        }

        if (so.page == 'index') {
            if (so.isMobile) {
                $(window).trigger('resize');
            }
        }
        return this;
    };

    so.initMobile = function(){
        console.log('Init mobile sizeObserver');
        UTILS_OUTER_PADDING = 20;
        UTILS_INNER_PADDING = 30;
        so.headerHeight = $header.outerHeight();
        so.footerHeight = $footer.outerHeight();
        $(document).on('pane.switch', so.recalcMobileSidePanel);
        if (so.page == 'search') {
            $(document).trigger('pane.switch');
        }
    };

    so.initEvents = function(){
        $(window).on('resize', so.recalcOnResize);
        $(document).on('pane.switch', so.recalcSidePanel);
        return this;
    };

    so.recalcOnResize = function(){
        console.log('Window resize');
        so.vpHeight = $(window).height();
        so.vpWidth = $(window).width();


        if (so.page == 'index' || so.page == '404'){
            if (so.isMobile) {
                if (so.page == 'index') {
                    $fullSizedInner.height($('.header-search-cnt').outerHeight(true) + $('.welcome-block').outerHeight(true));
                    $('.index-slider').height($fullSizedInner.height() + so.headerHeight);
                }
                if (so.page == '404') {
                    //$fullSizedInner.height($('.header-search-cnt').outerHeight(true) + $('.page404-content').outerHeight(true));
                }
            } else {
                $fullSizedWrapper.height(so.vpHeight - so.footerHeight);
                $fullSizedInner.height(so.vpHeight - so.footerHeight - so.headerHeight + $('.header-search-cnt').outerHeight());
                $('.index-slider').height(so.vpHeight - so.footerHeight + 1);
            }
        }

        if (!so.isMobile && $page == 'search') {
            so.recalcSearchPanel();
        }

        so.recalcSidePanel();
        return this;
    };

    so.recalcSidePanel = function(){
        so.utilsSidebarHeight = so.vpHeight - so.headerHeight - so.footerHeight - 2*UTILS_OUTER_PADDING;
        if (so.isMobile) {
            so.utilsSidebarHeight = so.vpHeight - so.headerHeight - 2*UTILS_OUTER_PADDING;
        }
        //if ($page == 'lk') {
        //    so.utilsSidebarHeight = so.vpHeight - so.headerHeight - 2*UTILS_OUTER_PADDING;
        //}
        so.utilsInnerHeight = so.utilsSidebarHeight - so.utilsHeaderHeight;
        $utils.height(so.utilsInnerHeight);
        $utilsInner.each(function(idx, elt){
            var $elt = $(elt),
                $topActions = $elt.siblings('.utils-top-actions');
            if ($elt.hasClass('chat-content')) {
                $elt.height(so.utilsInnerHeight - 2*UTILS_INNER_PADDING - $topActions.outerHeight(true) - $('.chat-message-form').outerHeight());
            } else {
                $elt.height(so.utilsInnerHeight - 2*UTILS_INNER_PADDING - $topActions.outerHeight(true));
            }


        });
    };

    so.recalcSearchPanel = function(){
        $fullSizedWrapper.height(so.vpHeight - so.footerHeight);
        $fullSizedInner.height($fullSizedWrapper.outerHeight() - so.headerHeight);
        $searchResultsCnt.height($fullSizedInner.outerHeight());
        $searchResults.height($searchResultsCnt.outerHeight() - $searchTags.outerHeight());
    };

    return so;
});