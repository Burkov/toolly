define(['jquery', 'backbone', 'mustache', 'moment', 'clientState', 'asyncUserInfo', 'utils', 'notifications'], function($, Backbone, Mustache, moment, clientState, userInfo, utils, notifications){
    var chat = {},
        $chats = $('.chat-list')
        $messages = $('.chat-messages-list'),
        $search = $('.chats-search');

    var filterFunction = utils.modelFilter($search, "user_name");
    // chat notation for dates
    function toChatTime(date){
        return moment(date).calendar();
    }


    var ChatMessageModel = Backbone.Model.extend({
        defaults: {
            send_time : 0, 
            sender: 0,
            msg: ""
        },

        decode: function(){
            var _this = this;
            return userInfo(this.get("sender")).done(function(user){
                _this.set({
                    sender_name : user.name,
                    sender_page: user.page,
                    date : toChatTime(_this.get("send_time")),
                    my: _this.get("sender") == global_context.user_pk
                });
            });
        }
    });

    var ChatMessageView = Backbone.View.extend({
        tagName: "li",

        className: "chat-item js-operate-item",
        template: $("#chat-message-template").html(),

        events: {
            'click .js-remove-item' : 'remove'
        },

        remove: function(){
            // signal
            this.model.trigger('hide', this.model.get("sender"), this.model.get("send_time"))
        },

        initialize: function(){
            this.listenTo(this.model, "change", this.render);
            this.model.view = this;
        },

        render: function(){
            var templateMessage = this.template;
            var msg = Mustache.to_html(templateMessage,  this.model.toJSON());
            this.$el.html(msg);
            // format message after escaping
            this.$('.chat-message-text').html(
                utils.formatMessage(this.model.get("msg"))
            )
            if(this.model.get("my"))
                this.$el.addClass("my-message");
            return this;
        }
    });


    var MessageList = Backbone.Collection.extend({
        model: ChatMessageModel,
        comparator: 'send_time'
    });
    /**
        A model of a chat instance:
        a list of messages plus few extra methods
    */
    var ChatModel = Backbone.Model.extend({
        defaults: function(){
            var col = new MessageList();
            return {
                messages: col,
                id: 'BAD_CHAT_ID', // for debugging
                ts: 0, // no deleted history 
                kind: 'dialogue'
            };
        },

        constructor: function(){
            var _this = this;
            Backbone.Model.apply(this, arguments);
            this.listenTo(this.get('messages'), "add", function(){
                _this.trigger("change");
            });
            this.listenTo(this.get("messages"), "hide", function(sender, ts){
                console.log("HIDE:", sender, ts);
                $.ajax({
                    type: "PUT",
                    url: '/chat/'+this.get("kind")+'/'+_this.id+"/hide?"
                    + $.param({
                        sender: sender,
                        send_time: ts
                    })
                })
            })
        },

        load: function(){
            var _this = this;
            var result = $.Deferred();
            $.ajax({
              type: "GET",
              url: '/chat/'+this.get("kind")+'/'+this.id+'/history',
              data: {
                ts : this.get("ts")
              },
              success: function(data){
                  _this.refresh(data).done(function(){
                    result.resolve();
                  });
              }
              //TODO: handle failure with events?
            });
            return result;
        },

        post: function(msg){
            $.ajax({
                type: "POST",
                url: '/chat/'+this.get("kind")+'/'+this.id+'/post',
                data: JSON.stringify({ 'msg' : msg }),
                contentType: 'application/json'
            }).done(function () {
                //TODO: trigger some event
            }).fail(function(jqXHR, textStatus, errorThrown) {
                //TODO: handle failure with events?
                console.error("Chat inaccessible: "+errorThrown);
            });
        },

        deleteHistory: function(){
            $.ajax({
                type: 'PUT',
                url: '/chat/'+this.get("kind")+'/'+this.id+"/delete?ts="+moment().valueOf()
            })
            this.trigger('destroy', this, this.collection);
        },

        lastMessage: function(){
            var messages = this.get("messages");
            if(messages.length == 0)
                return {
                    send_time : 0,
                    sender : 0,
                    msg : ''
                };
            else
                return messages.at(messages.length-1).toJSON();
        },

        // update with array of "recent" messages
        // they may overlap with current array
        refresh: function(recentMessages){
            var i = 0;
            var _this = this;
            var messages = this.get("messages")
            // what equal messages means
            function equal(m1, m2){
                return m1.send_time == m2.send_time && m1.msg == m2.msg;
            }
            //console.log("REFRESH:", messages, recentMessages)
            if(messages.length){
                // need to check for stale messages in recentMessages
                last = messages.at(messages.length-1).toJSON();
                for(i=0; i<recentMessages.length; i++)
                    if(equal(last, recentMessages[i]))
                        break;
                // not found - all fresh
                if(i == recentMessages.length)
                    i = 0;
                else
                    i++; // all after i are fresh
            }
            // decode & insert all fresh messages
            recentMessages = recentMessages.slice(i);
            if(recentMessages.length == 0)
                return;
            var decoding = $.Deferred();
            var decoded = 0;
            recentMessages = _.map(recentMessages, function(m){
                var mod = new ChatMessageModel(m)
                mod.decode().done(function(){
                    new ChatMessageView({model: mod}).render();
                    decoded += 1;
                    if(decoded == recentMessages.length)
                        decoding.resolve();
                })
                return mod;
            })
            decoding.done(function(){
                console.log("Done load for ", _this.get("id"))
                messages.add(recentMessages);
            })
            return decoding;
        }
    })

    // A full display of a singular chat as 
    // a list of messages with the new message area
    var ChatView = Backbone.View.extend({
        el: '.chat',

        initialize: function() {
            this.listenTo(this.model.get("messages"), "add", this.appendMessage);
            this.listenTo(this.model.get("messages"), "change", this.render);
            _.bindAll(this, 'postOnEnter', 'postOnSubmit', 
                'fireBackEvent', 'appendMessage');
            this.render();
        },

        appendMessage: function(message){
            this.render();
        },

        postMessage: function(){
            var el = this.$("#chat-message")
            if(!/^\s*$/.test(el.val())){
                this.model.post(el.val())
                el.val("")
            }
        },

        // keypress callback
        postOnEnter: function(e){
            if(e.which == 13 && !e.shiftKey){
                this.postMessage();
                return false;
            }
            return true;
        },

        postOnSubmit: function(e){
            this.postMessage();
            return false;
        },

        fireBackEvent: function(e){
            this.model.trigger("chatBack", this.model.get("id"));
        },

        events: {
            "submit .chat-message-form" : "postOnSubmit",
            "keypress #chat-message" : "postOnEnter",
            "click .js-close-chat" : "fireBackEvent"
        },

        render: function() {
            var $els = _.map(this.model.get("messages").models, function(m){
                return m.view.$el;
            })
            this.$(".chat-messages-list").children().detach();
            this.$(".chat-messages-list").append($els);
            return this;
        }
    })

    // A short display of chat 
    // only the last message and (if not your own) sender avatar
    var ChatMiniView = Backbone.View.extend({
        tagName: "li",

        className: "chat-item js-operate-item",

        template: $('#chat-room-template').html(),


        initialize: function() {
            this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, "destroy", this.remove);
            _.bindAll(this, 'openChat');
        },

        openChat: function(e){
            //console.info("Trigger chatEnter")
            this.model.trigger("chatEnter", this.model.get("id"));     
            return true;
        },

        openProfile: function(){
            document.location = "/profile/"+this.model.get("user_page");
        },

        removeChat: function(){
            this.model.deleteHistory();
            console.log("removeChat")
        },

        events: {
            "click .js-chat-trigger" : "openChat",
            'click .profile-link' : "openProfile",
            "click .js-remove-item" : "removeChat"
        },
        
        render: function(){
            var tmpl = this.template;
            var last = this.model.lastMessage();
            var obj = {
                user_page : this.model.get("user_page"),
                user_name : this.model.get("user_name"),
                poster_name: last.sender_name,
                poster_page: last.sender_page,
                date : toChatTime(last.send_time), 
                msg : last.msg
            }
            var content = Mustache.to_html(tmpl, { data: obj });
            this.$el.html(content);
            return this;
        }
    });

    var ChatCollection = Backbone.Collection.extend({
        model: ChatModel,
        comparator: function(chat){
            return -chat.lastMessage().send_time;
        }
    })

    var ChatListModel = Backbone.Model.extend({
        defaults: {
            chats: new ChatCollection(),
            user: "BAD_USER",
            poll: null
        },

        constructor: function(){
            var _this = this;
            _this.timeout = 1000;
            Backbone.Model.apply(this, arguments);
            // forward all events from chat models (that are forwarded to collection)
            this.listenTo(this.get("chats"), "change", function(arg){
                _this.trigger("change", arg);
            });
            this.listenTo(this.get("chats"), "chatEnter", function(arg){
                _this.trigger("chatEnter", arg);
            });
            this.listenTo(this.get("chats"), "chatBack", function(arg){
                _this.trigger("chatBack", arg);
            });
            this.info = $.Deferred();
            $.ajax({
                url:"/chat/info",
                type:"GET",
                success: function(data){
                    var chats = _this.get("chats")
                    var usr = _this.get("user")
                    var dialogs = data.user_info.dialogs;
                    var num_dialogs = _.keys(dialogs).length;
                    if(num_dialogs == 0)
                        _this.info.resolve();
                    var dialogs_ts = data.user_info.dialogs_ts;
                    var msg;
                    for (var k in dialogs){
                        if (dialogs.hasOwnProperty(k)){
                            var id = k.split("-")
                            if(id[0] != usr)
                                id = id[0];
                            else if(id[1] != usr)
                                id = id[1];
                            msg = {
                                sender: dialogs[k][0],
                                send_time: dialogs[k][1],
                                msg: dialogs[k][2]
                            };
                            (function(){ // encapsulate id capture
                                var cid = id;
                                var ts = dialogs_ts[k] || 0;
                                userInfo(cid).done(function(user){
                                    var newChat = new ChatModel({
                                        id: cid,
                                        kind: "dialogue",
                                        ts: ts,
                                        user_name: user.name,
                                        user_page: user.page
                                    })
                                    newChat.load().done(function(){
                                        chats.add(newChat);
                                        if(chats.length == num_dialogs){
                                            _this.info.resolve();
                                        }
                                    });
                                });
                            }());
                        }
                    }
                    _this.longPoll()
                }
            })
        },

        longPoll: function(){
            var dialogs = { };
            var chats = this.get("chats");
            chats.forEach(function(e){
                //console.log("POLL: "+new Date(e.get("lastMessage").send_time).toTimeString());
                var ts = e.lastMessage().send_time + 1;
                if(ts == 1) // no messages use history deletion timestamp instead
                    ts = e.get("ts")+1;
                dialogs[e.get("id")] = ts;

            });
            var requestData = 
              '{ "dialogs": '+ JSON.stringify(dialogs) + ', "chats": {} }';
            //console.log(requestData)
            var _this = this;
            var request = $.ajax({
                url:"/chat/refresh",
                type:"POST",
                data: requestData,
                contentType:"application/json; charset=utf-8",
                success: function(data){
                    var chat = null;
                    $.each(data, function(index, val) {
                        chat = _this.get("chats").find(function(model){
                            return index.search(model.get("id")) >= 0;
                        });
                        if(chat){
                            chat.refresh(val);
                        } else{
                            var ids = index.split("-");
                            var id;
                            if(ids[0] == global_context.user_pk)
                                id = ids[1];
                            else
                                id = ids[0];
                            userInfo(id).done(function(user){
                                chat = new ChatModel({
                                    id: id,
                                    user_name: user.name,
                                    user_page: user.page,
                                    kind: "dialogue"
                                })
                                chat.refresh(val).done(function(){
                                    chats.add(chat);
                                });
                            });
                        }
                    });
                    _this.timeout = 1000;
                    _this.longPoll();
                },
                error: function(error){
                    //if(error.status >= 400 && error.status < 500)
                    console.error(JSON.stringify("Chat long-poll failed: ", error.status));
                    window.setTimeout(_.bind(_this.longPoll, _this), _this.timeout);
                    if(_this.timeout < 30000){
                        _this.timeout = Math.round(_this.timeout*1.5+100);
                    }
                }
            });
            this.set({ poll : request });
        },

        addNew: function(model){
            this.get("chats").add(model);
        },

        loading: function(){
            return this.info;
        }
    })

    var ChatListView = Backbone.View.extend({
        tagName: "div",

        className: "panel panel-default",

        initialize: function(){
            this.listenTo(this.model.get("chats"), "add", this.addView)
        },

        addView: function(obj){
            obj.view = new ChatMiniView({ model: obj }).render()
            this.render();
        },

        render: function(){
            var $els = this.model.get("chats").filter(filterFunction).map(function(m){
                return m.view.$el;
            })
            this.$el.children().detach();
            this.$el.append($els);
            return this;
        }
        
    })

    var QuickSwitchModel = Backbone.Model.extend({
        initialize: function(){
            var parts = this.get("user_name").split(" ");
            this.set({
                user_name: _.map(parts, function(s){ return s[0]; }).join("")
            })
            this.view = new QuickSwitchTagView({ model : this }).render();
        }
    })

    var QuickSwitchCollection = Backbone.Collection.extend({
        model: QuickSwitchModel
    })

    var QuickSwitchTagView = Backbone.View.extend({
        tagName: "li",
        className: "tag-item",

        events: {
            'click' : 'onClick'
        },

        onClick: function(){
            chat.openChat(this.model.get("id"));
        },

        render: function(){
            this.$el.html(this.model.get("user_name"));
            return this;
        }
    })

    var QuickSwitchView = Backbone.View.extend({
        el: $('.chat-persons'),

        initialize: function(){
            this.listenTo(this.model.get("recent"), "add remove", this.render);
        },

        render: function(){
            var $els = this.model.get("recent").map(function(m){
                return m.view.$el;
            })
            console.log("Render:", $els);
            this.$el.children().detach();
            this.$el.append($els);
            return this;
        }
    })

    chat.init = function(){
        if("user_pk" in global_context){ // chat needs authenticated user
            var chats = new ChatListModel({ user: global_context.user_pk });
            var chatListView = new ChatListView({ model: chats });
            var quickSwitchModel = new Backbone.Model({
                recent: new QuickSwitchCollection()
            })
            new QuickSwitchView({ model : quickSwitchModel }).render();
            var currentChat = null;
            function openChat(chatId){
                var which;
                if(chatId == ''){
                    // remove empty chats from list
                    var emptyChats = chats.get("chats").filter(function(chat){
                        return chat.get("messages").length == 0;
                    });
                    _.each(emptyChats, function(m){
                       m.trigger('destroy', m, m.collection);
                    });
                }
                else{
                    which = chats.get("chats").find(function(chat){
                        return chat.get("id") == chatId;
                    });
                    userInfo(chatId).done(function(user){
                        if(!which){ // new chat
                            which = new ChatModel({
                                id : chatId, 
                                user_name: user.name,
                                user_page: user.page
                            })
                            // which.load(); // no need to load new chat
                            chats.addNew(which);
                        }
                        
                        if(!quickSwitchModel.get("recent").findWhere({ id: chatId})){
                            quickSwitchModel.get("recent").add({
                                id: chatId,
                                user_name: user.name
                            });
                        }
                        if(currentChat){
                            // one is for DOM events the other for backbone events
                            currentChat.undelegateEvents();
                            currentChat.stopListening();
                        }
                        currentChat = new ChatView({ model: which }).render(); 
                    })
                }
            }
            chats.loading().done(function(){
                openChat(clientState.get('chat'));
                clientState.on('chat', openChat);
                chats.on("chatEnter", function(chatId){
                    // hide chat block
                    clientState.set({ 
                        chat : chatId
                    })
                });
                chats.on("chatBack", function(chatId){
                    // hide chat block
                    clientState.set({ 
                        chat : ''
                    })
                });
                $search.keyup(function(e){
                    chatListView.render();
                })
                $chats.append(chatListView.render().$el);
            });            
        }
    };

    chat.openChat = function(chat_id){
        clientState.set({
            chat: chat_id,
            sidebarActive : true,
            sidebarPage: 'messages'
        });
    }

    // attach button to 
    chat.button = function($el, user_id){
        $el.click(function(e){
            e.preventDefault();
            chat.openChat(user_id);
        });
    }

    return chat;
});
   