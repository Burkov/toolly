// The machinery of events pane of sidebar
define(['jquery', 'underscore', 'backbone', 'moment', 'sizeObserver', 'asyncUserInfo', 'utils', 'ajaxCache', 'raty'],
function($, _, Backbone,  moment, sizeObserver, userInfo, utils, ajaxCache){
    var PENDING = "PENDING",
        IN_PROGRESS = "IN PROGRESS"
        CANCELLED = "CANCELLED",
        REVOKED = "REVOKED",
        COMPLETE = "COMPLETED",
        RATED = "RATED";
    var starOn = '',
        starOff = '';
    var $search = $('.events-search');
    var filterFunction = utils.modelFilter($search, "name");
    // AJAX me a service name
    function fetchService(userpage, service_id){
        return ajaxCache("/api/"+userpage+"/profile/main/services/"+service_id+"/name");
    }
    // 
    function putAjax(url, obj){
        return $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify(obj),
            contentType:"application/json; charset=utf-8",
        });
    }
    function statusText(status){
        switch(status){
            case PENDING:
                return "Ожидает подтверждения";
            case IN_PROGRESS:
                return "В работе";
            case CANCELLED:
                return "Отказано";
            case REVOKED:
                return "Отозвана";
            case COMPLETE:
                return "Завершена";
            case RATED:
                return "Оценена";
        }
    }
    function attachView(model){
        if(model instanceof ProviderDealModel){
            model.view = new ProviderView({ model: model}).render();
        }
        else if(model instanceof ClientDealModel){
            model.view = new ClientView({ model: model}).render();
        }
    }

    if (sizeObserver.isMobile) {
        MESSAGE_HEIGHT_TO_COLLAPSE = 80;
        starOn = '/img/icon_star_on@2x.png';
        starOff = '/img/icon_star_off@2x.png';
    } else {
        MESSAGE_HEIGHT_TO_COLLAPSE = 40;
        starOn = '/img/icon_star_on.png';
        starOff = '/img/icon_star_off.png'
    }

    var ProviderDealModel = Backbone.Model.extend({
        /*CQLSH:
            provider_id text,
            deal_id  uuid,
            created timestamp,
            last_changed timestamp,
            client_id text,
            service_id text,
            status text,
            bullets set<text>,
            rating int,
            review text,
            comment text,
            thank boolean
        */
        // get extra info by AJAX and put into collection once ready
        decodeAppend: function(collection){
            var self = this;
            userInfo(self.get("provider_id")).done(function(me){
                userInfo(self.get("client_id")).done(function(user){
                    fetchService(me.page, self.get("service_id")).done(function(service){
                        self.set({
                            name: user.name,
                            page: user.page,
                            reputation: 0, // TODO: fetch ratings from sysrate
                            date: moment(self.get("last_changed")).calendar(),
                            service_name: service
                        });
                        attachView(self);
                        collection.add(self);
                    });
                });
            });
        }
    });
    var ClientDealModel = Backbone.Model.extend({
        /*
            client_id text,
            <ditto as provider>
        */
        // get extra info by AJAX and put into collection once ready
        decodeAppend: function(collection){
            var self = this;
            userInfo(this.get("provider_id")).done(function(user){
                fetchService(user.page, self.get("service_id")).done(function(service){
                    self.set({
                        name: user.name,
                        page: user.page,
                        date: moment(self.get("last_changed")).calendar(),
                        service_name: service
                    });
                    attachView(self);
                    collection.add(self);
                });
            })
        }
    });

    var ProviderView = Backbone.View.extend({
        tagName: "li",
        className: "event-item js-operate-item",

        events: {
            "click .js-event-accept" : 'accept',
            "click .js-event-decline": 'decline',
            "click .js-event-complete" : 'complete',
            "click .js-event-thanks" : 'thank',
            "click .js-event-no-thanks" : 'nothank'
        },

        initialize: function(){
            this.listenTo(this.model, "change", this.render);
            this.model.view = this;
        },

        render: function(){
            switch(this.model.get("status")){
                case PENDING:
                    this.template = utils.mustacheOf("#event-prov-pending-tmpl");
                    break;
                case IN_PROGRESS:
                    this.template = utils.mustacheOf("#event-prov-in-progress-tmpl");
                    break;
                case CANCELLED:
                case REVOKED:
                case COMPLETE:
                    this.template = utils.mustacheOf("#event-prov-status-tmpl");
                    break;
                case RATED:
                    if(this.model.get("thank") === null)
                        this.template = utils.mustacheOf("#event-prov-rated-tmpl");
                    else
                        this.template = utils.mustacheOf("#event-prov-status-tmpl");
            }
            var obj = this.model.toJSON();
            console.log(obj);
            obj.status = statusText(obj.status);
            this.$el.html(this.template(obj));
            this.$('.message').html(utils.formatMessage(obj.comment));
            this.$el.attr({"data-ro" : true });
            return this;
        },

        accept: function(){
            console.info("Accepted", this.model.toJSON());
            console.info(global_context.user_pk);
            putAjax("/deals/"+this.model.get("deal_id")+"/accept", { comment : "TODO" })
        },
        decline: function(){
            console.info("Declined", this.model.toJSON());
            putAjax("/deals/"+this.model.get("deal_id")+"/decline", { comment : "TODO" })
        },
        complete: function(){
            console.info("Complete", this.model.toJSON());
            putAjax("/deals/"+this.model.get("deal_id")+"/complete", { comment : "TODO" })
        },
        thank: function(){
            console.info("Thank", this.model.toJSON());
            console.info(this.model.get("thank") === null);
            if(this.model.get("thank") === null){
                putAjax("/deals/"+this.model.get("deal_id")+"/thanks", { yes : true });
                this.model.set({
                    thank : true
                })
            }
        },
        nothank: function(){
            console.info("No thanks", this.model.toJSON());
            if(this.model.get("thank") === null){
                putAjax("/deals/"+this.model.get("deal_id")+"/thanks", { yes : false });
                this.model.set({
                    thank : false
                })
            }
        }
    })
 
    // View for cases where client simply observes status
    var ClientView = Backbone.View.extend({
        tagName: "li",
        className: "event-item js-operate-item",

        events: {
            "click .js-event-revoke" : 'revoke',
            "click .js-event-rate": 'rate'
        },

        initialize: function(){
            this.listenTo(this.model, "change", this.render)
            this.model.view = this;
        },

        render: function(){
            switch(this.model.get("status")){
                case PENDING:
                    this.template = utils.mustacheOf("#event-client-pending-tmpl");
                    break;
                case IN_PROGRESS:
                    this.template = utils.mustacheOf("#event-client-status-tmpl");
                    break;
                case CANCELLED:
                case REVOKED:
                case RATED:
                    this.template = utils.mustacheOf("#event-client-status-tmpl");
                    break;
                case COMPLETE:
                    this.template = utils.mustacheOf("#event-client-complete-tmpl");
            }
            var obj = this.model.toJSON();
            obj.status = statusText(obj.status);
            this.$el.html(this.template(obj));
            this.$el.attr({"data-ro" : true });
            if(this.model.get("status") == COMPLETE){
                this.$('.event-rating').raty({
                    number: 10,
                    starOff: starOff,
                    starOn: starOn
                })
            }
            return this;
        },

        revoke: function(){
            console.info("Revoked", this.model.toJSON());
            putAjax("/deals/"+this.model.get("deal_id")+"/revoke", { comment : "TODO" });
        },

        rate: function(){
            console.info("Rated", this.model.toJSON());
            if(this.$('.event-rating').raty('score')){

                putAjax("/deals/"+this.model.get("deal_id")+"/rate", { 
                    review: "", 
                    rating : this.$('.event-rating').raty('score')
                });
            }
        }
    })
    // Any emitted info - can be seen by anybody
    var InfoView = Backbone.View.extend({
        tagName: "li",
        className: "event-item js-operate-item item-info",

        template: utils.mustacheOf("#event-info-tmpl"),

    })

    var EventsCollection = Backbone.Collection.extend({
        comparator: function(m){ return -m.get("last_changed"); }
    })
    var eventsCol = new EventsCollection();
    var EventListView = Backbone.View.extend({
        el: $('.events-list'),

        initialize: function(){
            this.listenTo(eventsCol, "add change remove", this.render);
        },

        render: function(){
            var $els = eventsCol.filter(filterFunction).map(function(m){
                return m.view.$el;
            })
            this.$el.children().detach();
            this.$el.append($els);
        }
    })
    var eventListView = new EventListView()
    $search.keyup(function(e){
        eventListView.render();
    })
    // Init from REST
    $.ajax({
        url: "/deals",
        type: "GET",
    }).done(function(data){
        var ts = 0;
        _.forEach(data.client, function(deal){
            if(deal.last_changed > ts)
                ts = deal.last_changed;
            new ClientDealModel(deal).decodeAppend(eventsCol);
        })
        _.forEach(data.provider, function(deal){
            if(deal.last_changed > ts)
                ts = deal.last_changed;
            new ProviderDealModel(deal).decodeAppend(eventsCol);
        })
        longPoll(ts+1);
        console.log(data);
    })
    // refresh views if the actual text changed
    setInterval(function(){
        eventsCol.forEach(function(m){
            m.set({
                date: moment(m.get("last_changed")).calendar()
            });
        })
    }, 3600000);
    // setup long poll
    function longPoll(start){
        var timeout = 1000;
        function refresh(ts){
            var _ts = ts;
            $.ajax({
                url:"/deals/refresh",
                type:"GET",
                data: { ts: _ts },
                contentType:"application/json; charset=utf-8",
            }).done(function(data){
                var ts = _ts;
                console.log("Sysrate polled: ", ts, data);
                _.forEach(data, function(msg){
                    if(ts < msg.ts)
                        ts = msg.ts+1;
                    switch(msg.msg_type){
                        case "create":
                            if(msg.sender == global_context.user_pk){
                                new ClientDealModel({
                                    bullets: msg.bullets,
                                    client_id: msg.sender,
                                    provider_id: msg.provider_id,
                                    comment: msg.comment,
                                    deal_id: msg.deal_id,
                                    service_id: msg.service_id,
                                    status: PENDING,
                                    // timestamps
                                    created: msg.ts,
                                    last_changed: msg.ts
                                }).decodeAppend(eventsCol);
                            }
                            else if(msg.provider_id == global_context.user_pk){
                                new ProviderDealModel({
                                    bullets: msg.bullets,
                                    client_id: msg.sender,
                                    provider_id: msg.provider_id,
                                    comment: msg.comment,
                                    deal_id: msg.deal_id,
                                    service_id: msg.service_id,
                                    status: PENDING,
                                    // timestamps
                                    created: msg.ts,
                                    last_changed: msg.ts
                                }).decodeAppend(eventsCol);
                            }
                            break;
                        case "accept":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                status: IN_PROGRESS,
                                last_changed: msg.ts
                            })
                            break;
                        case "revoke":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                status: REVOKED,
                                last_changed: msg.ts
                            })
                            break;
                        case "decline":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                status: CANCELLED,
                                last_changed: msg.ts
                            })
                            break;
                        case "complete":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                status: COMPLETE,
                                last_changed: msg.ts
                            })
                            break;
                        case "rate":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                status: RATED,
                                last_changed: msg.ts
                            })
                            break;
                        case "thanks":
                            eventsCol.findWhere({ deal_id : msg.deal_id}).set({
                                thank: msg.yes,
                                last_changed: msg.ts
                            })
                            break;
                    }
                    console.log(msg.msg_type, msg);
                })
                timeout = 1000;
                refresh(ts);
            })
            .fail(function(error){
                console.error("Sysrate poll failed:", error.status);
                window.setTimeout(function(){ refresh(_ts) }, timeout);
                if(timeout < 30000){
                    timeout = Math.round(timeout*1.5+100);
                }
            })
        }
        refresh(start);
    }
    return ;
});