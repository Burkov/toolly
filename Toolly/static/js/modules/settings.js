define(['jquery', 'underscore', 'backbone', 'mustache'], function($, _, Backbone, Mustache) {

/*
    var pn = global_context.pagename;
    var mail = "test@mail.ru";
    var name = "Test teste";

	var set_model = new SettingsModel({pagename : pn, mail : mail, name : name});
	var view = new SettingsView({model : set_model});

	$('.settings-section').append(view.render().$el);
*/


    var show_popup = function(e) {
        if (e) e.preventDefault(); // this may be invoked not by click, but manually
        $.fancybox({
            padding: 30,
            content : Mustache.render($("#fancybox-pass-popup").html()),
            tpl: {
                closeBtn: '<div class="fancybox__close"><a class="fbx__close" href="#">&nbsp;</a></div>'
            },
            openEffect  : 'drop',
            closeEffect : 'drop',
            nextEffect  : 'elastic',
            prevEffect  : 'elastic'
        });
        $('.js-show-pass').on('click', function(e){
            e.preventDefault();
            var $me = $(this),
                $input = $me.prev('input'),
                type = $input.attr('type');
            if (type === 'text') {
                $input.attr('type', 'password');
            } else {
                $input.attr('type', 'text');
            }
        });
    }

    $(".change-pass-link").on("click", show_popup);

    // if form errors occurred, open the popup upon app load
    $(window).on('APP.loaded', function(){
        var $selected = $('<div/>').html($("#fancybox-pass-popup").html()).contents(); // this creates a temp div, inserts our html in it and extracts content
        if ($selected.find("ul.errors-list").length > 0){
            show_popup();
        }
    });

    
});