// Grab bag of helpers
define(['mustache'], function (Mustache) {
    var mod = {
        mustacheOf: function(selector){
            return function(arg){
                return Mustache.render($(selector).html(), arg);
            };
        },

        turnInto: function(thisView, ThatViewType){
            var v = new ThatViewType({ model: thisView.model });
            v.render();
            thisView.$el.after(v.$el);
            thisView.remove();
        },

        // Get HTML out of plain text message, adding <br/> in place of line ends,
        // turning links into <a href=...></a> tags and replace :emoji: with some elements.
        formatMessage: function(text){
            text = text.replace(/(https?|ftp):\/\/([^\s\/$.?#]\S*)/g, function(m, p1, p2){
                return '<a href="'+m+'">'+p2+'</a>'
            });
            text = text.replace(/:([a-z]+?):/g, '<i class="icon-emo-$1"></i>')
            text = text.replace(/\n/g, "<br/>");
            return text;
        },

        // Simple filter for backbone model and a value of an input field
        modelFilter: function($searchInput, attr){
            return function(model){
                var query = $searchInput.val().toLowerCase();
                if(query == null || query == "")
                    return true;
                else{
                    return model.get(attr).toLowerCase().search(query) >= 0;
                }
            }
        }
    }
    return mod;
})