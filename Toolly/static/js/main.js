requirejs.config({
    "paths": {
        "app":          "app",
        "jquery":       "libs/jquery-1.11.1.min",
        "mustache":     "libs/mustache",
        "underscore":   "libs/underscore-1.7.0.min",
        "typeahead":    "libs/typeahead",
        "backbone":     "libs/backbone-1.1.2.min",
        "utils":        "libs/utils",
        "raty":         "plugins/raty/jquery.raty",
        "dot":          "plugins/dot/jquery.dotdotdot.min",
        "device":       "addons/device.min",
        "customScroll": "plugins/scrollbar/jquery.mCustomScrollbar.concat.min",
        "fancybox":     "plugins/fancybox3/jquery.fancybox",
        "mask":         "plugins/maskedinput/jquery.maskedinput.min",
        "moment":       "plugins/momentjs/moment-with-locales.min",
        "datepicker":   "plugins/datepicker/jquery.datetimepicker",
        "chosen":       "plugins/chosen/chosen.jquery.min",
        "bxslider":     "plugins/bxslider/jquery.bxslider.min",
        //"parsley":      "plugins/parsley/parsley.min",
        //"interact":     "plugins/interact/interact.min",
        "tooltip":      "plugins/tooltipster/jquery.tooltipster.min",
        "sizeObserver": "modules/sizeObserver",
        "sidePanel":    "modules/sidePanel",
        "gmapLoader":   "modules/gmapLoader",
        "searchMap":    "modules/searchMap",
		"searchResults":"modules/searchResults",
		"profile":      "modules/profile",
        "gmaps":        "//maps.google.com/maps/api/js?sensor=true",
        //=============== Our code ===========
        "clientState" : "modules/clientState",        
        "settings":     "modules/settings",
        "about":        "modules/about",
        "locationSuggester":    "modules/locationSuggester",
        "searchBox": "modules/searchBox",
        "chat": "modules/chat",
        "logout": "modules/logout",
        "oauth": "modules/oauth",
        "popupErrors": "modules/popupErrors",
        "contacts":  "modules/contacts",
        "address": "modules/address",
        "services": "modules/services",
        "portfolio": "modules/portfolio",
        "events":  "modules/events",
        "bookmarks": "modules/bookmarks",
        "profileEdit": "modules/profileEdit",
        "profileTop": "modules/profileTop",
        "asyncUserInfo": "modules/asyncUserInfo",
        "ajaxCache": "modules/ajaxCache",
        "notifications": "modules/notifications"
    },
    "shim": {
        'typeahead': ['jquery'],
        'datepicker':['jquery'],
        'chosen':['jquery'],
        'dot':['jquery'],
        'bxslider':['jquery'],
        'tooltip':['jquery'],
        'moment':['jquery'],
        'mask':['jquery'],
        'fancybox':['jquery'],
        'raty':['jquery'],
		'customScroll':['jquery'],

        'device': {
            exports: 'device'
        },
        //'ymaps': {
        //    exports: 'ymaps'
        //},
        //'parsley': {
        //    exports: 'parsley'
        //},
        //'mask': {
        //    exports: 'mask'
        //}
    },
// bust commented-out, cause it forces refresh of cached script upon every page refresh
// hence not allowing us to set breakpoints for debugging purposes; instead we set
// Cache-Control: no-store http header on our frontend/static server to prevent scripts 
// cacheing.
//    urlArgs: "bust=" + (new Date()).getTime()
});

// Load the main app module to start the app
requirejs(['jquery','app'], function ($, app) {
    console.log('start scripts');
    app.
        init();
    $(window).trigger('APP.loaded');
});
