#!/usr/bin/env python

import json
import logging
from logging import info, error, basicConfig
import os
import socket
from signal import SIGKILL, SIGHUP
import subprocess
import sys
import threading

basicConfig(format='[FRONTEND][%(levelname)s] %(message)s',level=logging.INFO)
# bind and listen for Service Discovery events
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serv.bind(("0.0.0.0", 10000))
serv.listen(1)

DISCOVERED_SERVICES = json.loads(os.environ['HOSTS'])
if len(sys.argv) != 2:
    print "Need exactly 1 argument - config file path"
    exit(1)
configFile = sys.argv[1]

routing = """
server{
    listen 80;
    return 301 https://$host$uri;
}

server {
    listen 443 ssl;
    
    # capture sub-domain
    server_name ~^(?<sub>[a-zA-Z0-9_-]+).knackit.foo$;

    # static files directory
    root /data;

    location /static {

    }

    location /css/ {
        root /data/static/;
    }

    location /img/ {
        root /data/static/;
        try_files $uri @django;
    }

    location /js/ {
        root /data/static/;
        add_header Cache-Control no-store;
    }

    location /js/libs {
        root /data/static/;
        add_header Cache-Control max-age=3600000;
    }

    location /js/plugins {
        root /data/static/;
        add_header Cache-Control max-age=3600000;
    }

    location /js/addons {
        root /data/static/;
        add_header Cache-Control max-age=3600000;
    }

    # Chat service
    location /chat {
        proxy_pass http://chat;
    }

    # Sysrate service
    location /deals {
        proxy_pass http://sysrate;
    }

    # Search engine
    location /search/users {
        proxy_pass http://search;
    }

    # Suggester
    location /suggest/ {
        proxy_pass http://suggester;
    }

    # Tag-manager
    location /get_tags/ {
        proxy_pass http://tagman;
    }

    # Django location to use in try_files
    location @django {
        proxy_set_header Host $host; 
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_pass http://django;
    }

    # Everyting else gets to the Django
    location ~ ^/$ {
        proxy_set_header Host $host; 
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_pass http://django;
        if ($sub){
            proxy_pass http://django/profile/$sub/;
        }
    }

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_pass http://django;
    }
        
    # an exact match speeds up the processing
    location = /robots.txt {
        access_log off;
    }

    # ditto
    location = /favicon.ico {
        root /data/static;
        error_page 404 = @empty;
        access_log off;
        expires 30d;
    }
}
"""

# mapping: zpath ==> (upstream_name, port, {path -> ip})
mapping = {
    "/toolly/chat" : ("chat", 8080, {}),
    "/toolly/search" : ("search", 8080, {}),
    "/toolly/sysrate" : ("sysrate", 8080, {}),
    "/toolly/suggester" : ("suggester", 8081, {}),
    "/toolly/tag-manager" : ("tagman", 8080, {}),
    "/toolly/web" : ("django", 8080, {})
}

def getInstances(prefix):
    keys = filter(lambda x: x.startswith(prefix), DISCOVERED_SERVICES.keys())
    hosts = [(k, DISCOVERED_SERVICES[k]['addr']) for k in keys]
    return dict(hosts)

# populate mapping
for k in mapping.keys():
    mapping[k] = mapping[k][0], mapping[k][1], getInstances(k)

def writeConfig(filename):
    with open(filename, "w") as f:
        for v in mapping.values():
            servers = map(lambda x: "server %s:%s;" % (x, v[1]), v[2].values())
            upstream = "upstream %s{\n%s\n}\n" % (v[0] , "\n".join(servers))
            f.write(upstream)
        f.write(routing)

def addHost(path, ip):
    for k in mapping.keys():
        if path.startswith(k):
            mapping[k][2][path] = ip
            break

def delHost(path):
    for k in mapping.keys():
        if path.startswith(k):
            del mapping[k][2][path]
            break

def processEvents(f):
    for line in f:
        parts = line.split(":", 2) # NEW:/toolly/coo/coo2:{ "addr" : "1.2.3.4" }
        if len(parts) != 3:
            error("Bad event - expected 3 ':' separated fields")
            continue
        
        event,path,data = parts[0],parts[1],parts[2]

        if event == "NEW":
            try:
                data = json.loads(parts[2])
            except ValueError as e:
                error("Bad event: %s ", e)
                continue
            addHost(path, data['addr'])
        elif event == "CHG":
            pass
        elif event == "DEL":
            delHost(path)
        info("%s:%s DATA=%s", event, path, data)
        writeConfig(configFile)
        nginx.send_signal(SIGHUP)

def socketHandler():
    sock, _ = serv.accept()
    f = sock.makefile("r")
    try:
        try:
            processEvents(f)
        finally:
            f.close()
    finally:
        sock.close()
        
writeConfig(configFile)
nginx = subprocess.Popen(["nginx"])
t = threading.Thread(target=socketHandler)
t.daemon = True
t.start()
info("Waiting on NGINX")
status = nginx.wait()
info("NGINX terminated")
