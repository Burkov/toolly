#!/usr/bin/env python

import json
import logging
from logging import info, error, basicConfig
import os
import socket
from signal import SIGKILL, SIGHUP
import subprocess
import string
import sys
import threading

basicConfig(format='[BACKEND][%(levelname)s] %(message)s',level=logging.INFO)
# bind and listen for Service Discovery events
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serv.bind(("0.0.0.0", 10000))
serv.listen(1)

DISCOVERED_SERVICES = json.loads(os.environ['HOSTS'])
if len(sys.argv) != 2:
    print "Need exactly 1 argument - config file path"
    exit(1)

configFile = open(sys.argv[1], "w")

def writeConfig():
    configFile.seek(0)
    payload = json.dumps(DISCOVERED_SERVICES, separators=(',',':'))
    # MUST keep constant length of file for mmaping to work 
    # 16K should be enough for everybody (TM)
    payload = string.ljust(payload, 4 * 4096, " ")
    configFile.write(payload)
    configFile.flush()

def processEvents(f):
    for line in f:
        parts = line.split(":", 2) # NEW:/toolly/coo/coo2:{ "addr" : "1.2.3.4" }
        if len(parts) != 3:
            error("Bad event - expected 3 ':' separated fields")
            continue
        try:
            event,path,data = parts[0],parts[1],json.loads(parts[2])
        except ValueError as e:
            error("Bad event: %s ", e)
            continue
        if event == "NEW":
            DISCOVERED_SERVICES[path] = data
        elif event == "CHG":
            pass
        elif event == "DEL":
            if path in DISCOVERED_SERVICES:
                del DISCOVERED_SERVICES[path]
        info("%s:%s DATA=%s", event, path, data)
        writeConfig()

def socketHandler():
    sock, _ = serv.accept()
    f = sock.makefile("r")
    try:
        try:
            processEvents(f)
        finally:
            f.close()
    finally:
        sock.close()
        
writeConfig()
nginx = subprocess.Popen(["./start.sh"])
t = threading.Thread(target=socketHandler)
t.daemon = True
t.start()
info("Waiting on uWSGI")
status = nginx.wait()
info("uWSGI terminated")
