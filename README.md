# README #

This is the web part of Toolly project, written mostly in django

### How to deploy ###

In order to try this project in test mode, using django built-in web and static content server, do the following:

Clone the repository to your folder of choice:

    git clone https://bitbucket.org/toolly/toolly-web.git

You'll need to install the following pre-requisites:

  * mongodb (tested with 2.4.9) - deb-package
  * PIL or Pillow - already installed or pip archive
  * django1.7 - pip archive
  * requests - pip archive 
  * sendgrid - pip archive
  * mongoengine 0.8.7 - pip archive
  * python-magic 0.4.6+ - pip archive, deb-package's obsolete and WON'T WORK!
  * libmagic1 - deb-package


`Mongodb2.4.9` is available in Mint repository(just `sudo aptitude install mongodb`). Not sure about Debian.

`PIL` is installed by default on Debian-based distrbutions. Optionally, you could use its fork, Pillow, instead of PIL. Install it with `pip` from PyPI then.

`libmagic1` is available in Mint repository as well.

For installation of other packages (python packages, django, mongoengine, requests, sendgrid and python-magic), use `pip`, python analogue of `apt`. By default it searches for packages on web, in python repository, called PyPI (Python Packages Index). You can use `pip search mypackage` to search PyPI for packages. It can also install packages from local directory with `-e` flag.

You can also use `virtualenv` in combination with `pip`. `virtualenv` is a script that creates a special folder, called python virtual environment, in order to install python modules and packages to that folder instead of the system location of python modules (`/usr/lib/python/site-packages` for modules, installed with `pip/distutils` or `usr/lib/python/dist-packages` for modules installed via deb-packages), so that you can have multiple different versions of django installed in multiple virtualenv's and they won't conflict with each other. You can choose one virtual environment and activate it by running `source ENV/bin/activate` shell script, where ENV is the name of virtual environment folder (you can choose your own). This script temporarily substitutes your system `python` and `pip` commands with modified ones that search for/install python modules in your ENV folder instead of the system location. Note that you can't easily move `ENV` folder in the filesystem after creation because it's location is hard-coded in ENV/bin/activate script, which will break if you move the ENV folder.

So, in the folder, where you deploy Toolly, e.g. in `/home/you/www`, you say:

    virtualenv ENV --system-site-packages

where "ENV" is the name of yout virtual environment folder and --system-site-packages flag tells virtualenv to use python packages, installed system-wide along with those installed in virtual environment only. Thus we'll be able to use PIL,installed system-wide. If you don't want to use pip packages, installed system-wide, you'll have to manually install Pillow or PIL from PyPI.

Activate that environment with:

    source ENV/bin/activate

Now, download django from github:

    git clone -b stable/1.7.x https://github.com/django/django.git

Install it into the virtualenv path via:

    pip install -e django/

"django/" is the folder, where you cloned django, -e flag tells pip to install django not from web, but from local directory.

Note that you SHOULDN'T have another version of django installed system-wide on that machine, or they'll conflict!

Install mongoengine from PyPI with:

    pip install mongoengine

Likewise, install python-magic from PyPI:

    pip install python-magic

Note, that python-magic is also available from deb-packages, but they've changed the API, so older versions from deb-packages won't work.

That's it. Now, you're ready to roll. Start the test django server from toplevel Toolly folder with:

    python manage.py runserver

It should start complaining about `MongoUserManager.get_query_set` method should be renamed `get_queryset` and that's ok.

### How to deploy frontend ###
The frontend part lives in static/base subfolder of the repository. Its most important contents are base.css and base.js files. base.js is concatenated from its sources in static/base/src/js 
folder with grunt.

In order to build the frontend, you'll need to do the following in "static/base" folder:

Install nodeenv (analogue to python virtualenv)

    pip install nodeenv

Create a new node virtual environment (e.g. named NODEENV) in static/base folder (note that you'll need g++ for compiling node.js)

    sudo aptitude install g++
    nodeenv NODEENV

Install grunt in that NODEENV

    source NODENV/bin/activate
    npm install -g grunt-cli

This installs grunt-cli in the NODENV folder (flag "-g" means global)

    npm install grunt --save-dev

This installs grunt locally, creating a "node_modules" folder .

Install required grunt modules, such as grunt-contrib-concat, grunt-contrib-jshint, grunt-contrib-uglify etc.

    npm install grunt-contrib-concat --save-dev
    npm install grunt-contrib-jshint --save-dev
    npm install grunt-contrib-uglify --save-dev

Now, to build base.js just say:

    grunt

I've also included `watch` into the list of grunt tasks, so that grunt automatically re-creates base.js as you modify and save your source filles. In order to start grunt in the watch mode, you need to install `grunt-contrib-watch` package:

    npm install grunt-contrib-watch --save-dev

After that, you can start grunt in watch mode:

    grunt watch

and it will automatically re-build your project when you modify and save any file.