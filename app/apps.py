from django.apps import AppConfig

class WebAppConfig(AppConfig):

    name = 'app'
    verbose_name = 'app'

    def ready(self):
        import app.signals.propagate_auth
