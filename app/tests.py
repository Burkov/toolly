import os

# The following is a failed attempt to create a django test suite.
# It doesn't work, cursing about 
# AttributeError: 'DatabaseWrapper' object has no attribute 'Database'

# to run the test, say:
# > python manage.py test

#from django.conf import settings
#from django.test import TestCase
#from django.test.runner import DiscoverRunner
#from mongoengine.connection import connect, disconnect, get_connection
#
#from app.models import User, Profile
#
#
#test_database_name = "test_database"
#
#class MongoTestRunner(DiscoverRunner):
#    def setup_databases(self, **kwargs):
#        # disconnect from the default database, connect to the test database
#        disconnect()
#        connect(test_database_name, host=settings.MONGODB_URI)
#        return super(MongoTestRunner, self).setup_databases(**kwargs)
#
#    def teardown_databases(self, old_config, **kwargs):
#        # remove the test database after tests
#        connection = get_connection()
#        connection.drop_database(test_database_name)
#        disconnect()
#        super(MongoTestRunner, self).teardown_databases(old_config, **kwargs)
#
#
#class AvatarReplaceTestCase(TestCase):
#    '''Idea stolen from:
#    http://nubits.org/post/django-mongodb-mongoengine-testing-with-a-custom-mongo-test-case/
#    '''
#    def setUp(self):
#        # create a new user like in UserCreationForm.save()
#        profile = Profile(main=MainSection(services={}, location=[]), extras={}, portfolio={})
#        avatar = open(os.path.join(settings.STATICFILES_DIRS[0], 'anonymous_user.png'), 'rb').read()
#        profile.avatar.put(avatar, content_type='image/png')
#        new_user = User(username='test', password='test', profile=profile)
#        new_user.save()
#
#    def test_replace(self):
#        # This replaces an avatar in the user profile with another image
#        # (actually, the same, but it doesn't matter).
#        new_avatar = open(os.path.join(settings.STATICFILE_DIRS[0], 'anonymous_user.png'), 'rb').read()
#        test_user = User.objects.get(username='test')
#        test_user.profile.avatar.replace(new_avatar, content_type='image/png')
#        self.assertEqual(test_user.profile.avatar.read(), new_avatar)


# This is a pure python unittest testset for validators.
# to run the test, say in this folder
# > python -m unittest -v tests

import unittest
from validators import check_schema, geoValidator, strValidator, fileValidator, contactTypeValidator, contactContentValidator

class TestContactValidator(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_contact_type(self):
    	self.assertTrue(contactTypeValidator()("Telephone"))

    def test_contact_content_telephone(self):
    	self.assertTrue(contactContentValidator()("Telephone", "+7(901)123-45-67"))

    def test_contact_content_telephone_wrong(self):
    	self.assertFalse(contactContentValidator()("Telephone", "sometext"))

    def test_contact_content_skype_id(self):
    	self.assertTrue(contactContentValidator()("Skype id", "Ivan.Petrov"))

    def test_contact_content_skype_id_wrong(self):
    	self.assertFalse(contactContentValidator()("Skype id", "TooLongForSkypeIdOver32Characters"))

    def test_contact_content_address(self):
    	self.assertTrue(contactContentValidator()("Address", "This should return True anyway"))

    # address can't be wrong

    def test_contact_content_email(self):
    	self.assertTrue(contactContentValidator()("E-mail", "neck@theendoftherope.com"))

    def test_contact_content_email_wrong(self):
    	self.assertFalse(contactContentValidator()("E-mail", "something not e-mailish"))

    def test_contact_content_website(self):
    	self.assertTrue(contactContentValidator()("Web site", "http://www.mysite.blogspot.com"))

    def test_contact_content_website_wrong(self):
    	self.assertFalse(contactContentValidator()("Web site", "https://domainmissing"))

    def test_contact_content_facebook(self):
    	self.assertTrue(contactContentValidator()("Facebook", "https://facebook.com/My/Friends/Profile"))

    def test_contact_content_facebook_wrong(self):
    	self.assertFalse(contactContentValidator()("Facebook", "https://www.nonfacebooksite.com"))

    def test_contact_content_vk(self):
    	self.assertTrue(contactContentValidator()("VK", "https://vk.com/My/Friends/Profile"))

    def test_contact_content_facebook_wrong(self):
    	self.assertFalse(contactContentValidator()("VK", "https://vkontakte.ru/will/fail/although/may/be/we/shold/make/it/pass"))

    def test_contact_content_twitter(self):
    	self.assertTrue(contactContentValidator()("Twitter", "https://TWITTER.COM/My/Friends/Profile"))

    def test_contact_content_twitter_wrong(self):
    	self.assertFalse(contactContentValidator()("Twitter", "https://ThisAintTwitter.com"))

class TestCheckSchema(unittest.TestCase):
    def test_post_album(self):
        check_schema({"title": "New Album", "files": {}}, "portfolio", request_type="POST")
        self.assertTrue(True)

    def test_put_album_title(self):
        check_schema("Album title", "portfolio/54ab009985eb245f9d43b357/title", request_type="PUT")
        self.assertTrue(True)

    def test_post_file(self):
        check_schema({"name":"file_name"}, "portfolio/54ab009985eb245f9d43b357/files/", request_type="POST")
        self.assertTrue(True)

    def test_put_file_fs(self):
        check_schema("54ab009985eb245f9d43b357", "portfolio/54ab009985eb245f9d43b357/files/54ab009985eb245f9d43b357/fs", request_type="PUT")
        self.assertTrue(True)

    def test_post_service(self):
        check_schema({"name": "Service name", "tags": [], "points": [], "extra": ""}, "main/services/", request_type="POST")
        self.assertTrue(True)

    def test_post_tag(self):
        check_schema("New tag", "main/services/54ab009985eb245f9d43b357/tags", request_type="POST")
        self.assertTrue(True)