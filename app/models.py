import datetime

from django.db import models

import mongoengine.django.auth
from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import *

#TODO!!! reverse_delete_rule

class File(EmbeddedDocument):
    name = StringField()
    fs = FileField() # object in GridFS

class Album(EmbeddedDocument):
    title = StringField() # album title
    description = StringField() # album description
    cover = StringField() # album cover is an image object (File), one of the images of this album
    files = MapField(EmbeddedDocumentField(File)) # list of images in GridFS

class MainSection(EmbeddedDocument):
    services = DictField() #each service is {"name":"programmer", "tags":["html","css","js"], "points":["freelance: 2k$", "full-time: 3k$"], "extra":"text description", "image": ObjectId}
    location = ListField(DictField()) #each location is e.g. {"center": [37.17185494, 39.19193]}
    contacts = DictField() # keys in the dict are ObjectIDs, values are {"type": "phone/url/email" "content": "+7-xxx, http://, abs@mail.com"}
    preferred_contact_types = ListField(StringField()) # subset of contact types that user prefers (e.g. telephone, vk)

class Profile(EmbeddedDocument):
    # where first album is slideshow, rolling on the profile page of profile owner
    main = EmbeddedDocumentField(MainSection)
    extras = DictField()
    avatar = FileField() # user avatar, image in GridFS
    portfolio = MapField(EmbeddedDocumentField(Album)) # dict of {ObjectID: album}, containing albums with images
    bookmarks = DictField() # ObjectId: {"user_id" : <ObjectId of user>, "note" : "Text string}

class User(mongoengine.django.auth.User):
    name = StringField()
    code = StringField() # activation code sent to e-mail when user first creates the account or when he requests password recovery
    username_code = StringField() # when user tries to change username (=e-mail), an activation code is generated and writtend to this field until user follows link in confirmation e-mail
    new_username = StringField() # when user tries to change username (=e-mail), a new_username is stored in this field until user follows link in confirmation e-mail
    new_password = StringField() # when user recovers password, this stores temporary password, until user logs in - then it is set as permanent password
    pagename = StringField() # url of profile page, e.g. if user.pagename = "Ivan", his profile will be seen as Ivan.knackit.com
    profile = EmbeddedDocumentField(Profile) #
    oauth = StringField() # oauth provider; possible values:  "" (if login is not via oauth), "google+", "facebook", "vk", "twitter"
    oauth_uid = StringField() # oauth userId, identifying this user in oauth provider's database
    refresh_token = StringField() # empty string, if user logged in via login/password; otherwise value of refresh_token obtained from oauth provider
