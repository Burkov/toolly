from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth.signals import user_logged_in, user_logged_out
from datetime import datetime
import random
import requests
from string import strip
from math import ceil

'''
    Try all coordinators in turn starting from 
    random index, and going in full circle wrapping the array.
'''
def propogate(mtd, session, userid=None):
    coordinators = settings.SERVICES.coo()
    timeout = settings.COORDINATORS_TIMEOUT
    if len(coordinators) == 0:
        print "No coordinators online!"
        return False
    random.shuffle(coordinators)
    retries = 0
    key = session.session_key
    for coo in coordinators:
        url = "%s/sessions/%s" % (coo, key)
        try:
            if mtd == 'PUT':
                # no escaping required 
                exp = session.get_expiry_date().replace(tzinfo=None)
                epoch = (exp - datetime(1970, 1, 1)).total_seconds()
                epoch = ceil(epoch)
                body = """{ "user" : "%s", "expire_date" : %s }""" \
                    % (userid, epoch)
                headers = {'Content-Type': 'application/json'}
                r = requests.put(url, data=body, headers=headers, timeout=timeout)
            elif mtd == 'DELETE':
                r = requests.delete(url, timeout=timeout)
            if r.status_code >= 200 and r.status_code < 300:
                return True
        except requests.RequestException:
            pass # try next coordinator
    return False

@receiver(user_logged_in)
def add_session(**kwargs):
    user = kwargs['user']
    session = kwargs['request'].session
    ok = propogate('PUT', session, user.id)
    if not ok:
        print "Failed to propogate session for '%s'" % user

@receiver(user_logged_out)
def remove_session(**kwargs):
    user = kwargs['user']
    session = kwargs['request'].session
    ok = propogate('DELETE', session)
    if not ok:
        print "Failed to propogate removal of session for '%s'" % user