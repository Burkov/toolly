import re

# Upon PUT or POST requests, we have to validate the inserted object.
# In order to do so, we store a schema of our database in api_profile_schema object here
# and use check_schema function that applies a number of validators to its fields.

class geoValidator(object):
    def __call__(self, obj, path):
        # TODO
        return True

class intValidator(object):
    def __call__(self, obj, path):
        if type(obj) == int: return True
        else: raise TypeError("Object %s is not an integer" % obj)

class strValidator(object):
    def __call__(self, obj, path):
        if isinstance(obj, basestring): return True
        else: raise TypeError("Object %s is not a string" % obj)

class fileValidator(object):
    def __call__(self, obj, path):
        # actually, file is also represented with ObjectId of an object
        # in GridFS, so nothing special here
        if re.match("^[0-9a-fA-F]{24}$", str(obj)): return True
        else: return False

class objectIdValidator(object):
    def __call__(self, path_component, path):
        if re.match("^[0-9a-fA-F]{24}$", path_component): return True
        else: return False

class contactValidator(object):
    def __call__(self, obj, path):
        # check that obj is a dict-like (mapping) type, raise exception otherwise
        if not hasattr(obj,  "__getitem__") or not hasattr(obj, "__contains__") or not hasattr(obj, "keys"): return False
        # check that it has only 2 keys: "type" and "content"
        keys = obj.keys()
        if len(keys) != 2 or not "type" in keys or not "content" in keys: return False
        if not contactTypeValidator()(obj["type"], path): return False
        return contactContentValidator()(obj["type"], obj["content"])


contact_types = ['Telephone', 'Skype id', 'Address', 'E-mail', 'Web site', 'Facebook', 'VK', 'Twitter'] #, 'LinkedIn'

class contactTypeValidator(object):
    def __call__(self, contact_type, path):
        # check that contact type is one of the allowed contact types
        if contact_type in contact_types: return True
        else: return False

class contactContentValidator(object):
    def __call__(self, contact_type, content):
        # check that contact's content is appropriate for contact type
        if contact_type == "Telephone":
            # see http://stackoverflow.com/questions/123559/a-comprehensive-regex-for-phone-number-validation?page=1&tab=active#tab-top
            match_object = re.match("^[\d\ \+\-\(\)]*$", content)
            if match_object: return True # if we need to check length, we can do it with "and match_object.end() == len(content)"
            else: return False
        elif contact_type == "Skype id":
            match_object = re.match("^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$", content)
            if match_object: return True
            else: return False
        elif contact_type == "Address":
            return True # to hell with it, address can be anything
        elif contact_type == "E-mail":
            # see http://stackoverflow.com/questions/201323/using-a-regular-expression-to-validate-an-email-address
            match_object = re.match("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", content, flags=re.IGNORECASE)
            if match_object: return True
            else: return False
        elif contact_type == "Web site":
            match_object = re.match("(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})", content, flags=re.IGNORECASE)
            if match_object: return True
            else: return False
        elif contact_type == "Facebook":
            match_object = re.match("https?://(?:www.)?facebook.com/\S*", content, flags=re.IGNORECASE) # created by me, just any non-whitespace stuff on facebook domain
            if match_object: return True
            else: return False
        elif contact_type == "VK":
            match_object = re.match("https?://(?:www.)?vk.com/\S*", content, flags=re.IGNORECASE)
            if match_object: return True
            else: return False
        elif contact_type == "Twitter":
            match_object = re.match("https?://(?:www.)?twitter.com/\S*", content, flags=re.IGNORECASE)
            if match_object: return True
            else: return False        
        #elif contact_type == "LinkedIn":
        #    pass
        else: # no contact_type yet
            return True

# api_profile_schema is a tree, where each node is either a dict or a list or a validator

api_profile_schema = {
    "main": {
        "services": {
            objectIdValidator(): {
                "name": strValidator(),
                "tags": [strValidator()],
                "points": [{
                    "title": strValidator(), 
                    "price": strValidator()
                }],
                "extra": strValidator(),
                "image": fileValidator()
            }
        },
        "address": strValidator(),
        "location": [geoValidator()],
        "contacts": {
            objectIdValidator(): contactValidator()
        },
        "preferred_contact_types": [contactTypeValidator()]
    },
    "extras": {
        "bio" : strValidator(),
        "experience" : {
            objectIdValidator() : {
                "kind" : strValidator(),
                "comment" : strValidator(),
                "from" : intValidator(),
                "till" : intValidator()
            }
        },
        "education" : {
            objectIdValidator() : {
                "org" : strValidator(),
                "comment" : strValidator(),
                "from" : intValidator(),
                "till" : intValidator(),
                "files" : {
                    objectIdValidator() : {
                        "name": strValidator(),
                        "fs": fileValidator()
                    }
                }
            }
        },
        "languages" :{
            objectIdValidator() : {
                "name" : strValidator(), 
                "skill" : strValidator()
            }
        }
    },
    "avatar": fileValidator(),
    "portfolio": {
        objectIdValidator(): {
            "title": strValidator(),
            "description": strValidator(),
            "cover": objectIdValidator(),
            "files": {
                objectIdValidator(): {
                    "name": strValidator(),
                    "fs": fileValidator()
                }
            }
        }
    },
    "bookmarks" : {
        objectIdValidator(): {
            "user_id" : objectIdValidator(),
            "note" : strValidator()
        }
    }
}

def check_schema(obj, path, request_type):
    '''
        obj - object, coming from request, that we're going to insert to the database
        path - url, where we're goint to insert obj
        request_type - "POST" or "PUT"; POST adds a child to resource at path, while PUT replaces that resource with obj
    '''
    # we don't have to remove the first 3 components of path: /api/<profile_owner>/profile/, cause they were already removed by api_profile_view
    path_components = path.strip("/").split("/")
    # get the schema_component, corresponding to the path, where we're going to insert the object
    schema_component = api_profile_schema
    for index, path_component in enumerate(path_components):
        # if we get KeyError, let check_schema crash and KeyError be caught by check_schema's caller
        if type(schema_component) is dict:
            if len(schema_component.keys()) == 1 and type(schema_component.keys()[0]) == objectIdValidator: # if it's a dynamically assigned ObjectId
                schema_component.keys()[0](path_component, "/".join(path_components[:index+1])) # check that it really is an ObjectId
                schema_component = schema_component[schema_component.keys()[0]] # go to the next component
            else:
                schema_component = schema_component[path_component]
        elif type(schema_component) is list:
            schema_component = schema_component[0]
    if request_type == "POST": # if we create an entry in dict or list
        if type(schema_component) == dict:
            if len(schema_component.keys()) == 1 and type(schema_component.keys()[0]) == objectIdValidator: # if dict's keys are dynamically assigned ObjectIds - ok
                schema_component = schema_component[schema_component.keys()[0]]
                traverse_validators(schema_component, obj, path + "/" + "<NewObjectId>" + "/")
            else: # this dict is not supposed to contain dynamically created keys, bail out
                raise KeyError("Resource at path '%s' is not a collection, thus you can't create a new entry in it." % path)
        elif type(schema_component) == list: # if it's a list collection - ok
            schema_component = schema_component[0]
            traverse_validators(schema_component, obj, path + "/" + "<LastElement>" + "/")
        else: # it's not a collection, bail out
            raise KeyError("Resource at path '%s' is not a collection, thus you can't create a new entry in it." % path)
        # check that next schema_component's key is objectIdValidator
        # run traverse_validators on that component
    elif request_type == "PUT":
        # now, validate components of obj, so that they pass validators in schema
        # if any component contradicts the schema, Error is raised in this function and should be caught by the caller
        traverse_validators(schema_component, obj, path + "/")


def traverse_validators(schema_component, obj, path_to_schema_component):
    '''special case of breadth-first tree traversal for schema tree

    Recursively looks for validators in schema_components and calls them on corresponding obj_components;
    if 
        - validator(obj_component) fails or
        - types of schema_component and corresponding obj_component mismatch or
        - set of keys in obj_component is not a subset of set of keys in schema_component
    raises an exception, pointing to the path to schema_component, where failure occured
    '''

    # queue of breadth-first traversal consists of 3-tuples of (schema_component, obj_component, path_to_schema_component)
    queue = [(schema_component, obj, path_to_schema_component)]
    while len(queue) > 0:
        schema_component, obj_component, path_to_schema_component = queue.pop(0)
        if type(schema_component) == dict:
            # check that obj_component is a dict-like (mapping) type, raise exception otherwise
            if not hasattr(obj_component,  "__getitem__") or not hasattr(obj_component, "__contains__"):
                raise KeyError("Object component at %s is expected to be mapping, but it is not. obj = %s" % (path_to_schema_component, obj))
            # check that set of keys in obj_component is a subset of the set of keys in schema_component
            if not set(obj_component.keys()).issubset(set(schema_component.keys())):
                # We don't require equality of key sets, but only a subset because
                # e.g. when we upload an image to an album, we make 2 requests: POST new file to album.files with its name only
                # and then push image content in a separate request
                raise KeyError("Object component's keys at %s are not a subset of schema_component's keys. obj = %s" % (path_to_schema_component, obj))
            # push the children of current node to the queue
            for key in obj_component:
                # if schema_component[key] is a dict {ObjectId: <something>, ObjectId: <something> ...}, split it into parts
                # WARNING: I assume, it can't be recurrent like {ObjectId: {ObjectId: <something>}}
                if hasattr(schema_component[key], "keys") and isinstance(schema_component[key].keys()[0], objectIdValidator):
                    for inner_key in obj_component[key].keys():
                        schema_component_inner_key = schema_component[key].keys()[0]
                        queue.append((schema_component[key][schema_component_inner_key], obj_component[key][inner_key], path_to_schema_component + key + "/" + inner_key + "/"))
                else:
                    queue.append((schema_component[key], obj_component[key], path_to_schema_component + key + "/"))
        elif type(schema_component) == list:
            if not type(obj_component) == list:
                raise TypeError("Object component at %s is expected to be list, but it is not. obj = %s" % (path_to_schema_component, obj))
            for index, element in enumerate(obj_component): # push all the members of obj_component list to the queue
                queue.append((schema_component[0], element, path_to_schema_component + str(index) + "/"))
        else:
            schema_component(obj_component, path_to_schema_component) # this will raise error if something's wrong