import os
import re

from collections import OrderedDict

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.text import capfirst
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, get_user_model

from models import User, Profile, MainSection
from django.conf import settings

from common import create_empty_profile

def make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    '''
    Stolen from mongoengine, as User.objects.make_random_password() doesn't work:
    'QuerySet' object has no attribute 'make_random_password'.
    "Generates a random password with the given length and given allowed_chars"
    '''
    # Note that default value of allowed_chars does not have "I" or letters
    # that look like it -- just to avoid confusion.
    from random import choice
    return ''.join([choice(allowed_chars) for i in range(length)])

def make_random_pagename():
    ''' Generate 8-[a-z0-9] (lowercase) pagename and ensures it is unique'''
    while True: # after 36^8 users this will loop forever :)
        pagename = make_random_password(length=8).lower()
        try:
            User.objects.get(pagename=pagename)
        except User.DoesNotExist:
            return pagename


# UserCreationForm, AuthenticationFormand SetPasswordForm are stolen from
# django.contrib.auth.forms almost verbatim with modifications 
# having to do with:
# 1) storing models via mongoengine
# 2) styling widgets with our CSS styles

class UserCreationForm(forms.Form):
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    name = forms.CharField(label=_("Name"), widget=forms.TextInput(attrs={'placeholder': _("Name"), 'id': 'register-name'}))
    username = forms.RegexField(label=_("Username"),
                regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                help_text=_("Required. E-mail. Letter, digits"
                " @/./+/-/_ characters only."),
                error_messages={'invalid': _("This value should be an e-mail address like 'John.Smith@mail-service.com'. It may contain only letters, numbers and @/./+/-/_ characters.")},
                widget=forms.TextInput(attrs={'id':'register-email', 'placeholder': _("E-mail")}))
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'id':'register-pass', 'placeholder': _("Password")}))
    password2 = forms.CharField(label=_("Password confirmation"), widget=forms.PasswordInput(attrs={'id':'register-pass2', 'placeholder': _("Password confirmation")}), help_text="Enter the same password as above, for verification.")

    def clean_username(self):
        username = self.cleaned_data["username"]
        username = username.lower()
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'],
                                    code='duplicate_username'
                                   )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(self.error_messages['password_mismatch'],
                                        code='password_mismatch'
                                       )
        return password2

    def save(self):
        #TODO django.contrib.auth.forms.UserCreationForm had a commit option. What do i do with it?
        username = self.cleaned_data["username"]
        name = self.cleaned_data["name"]
        password = make_password(self.cleaned_data["password1"])
        #image = DefaultUserAvatar.objects.findOne().avatar
        code = make_random_password(length=32).lower()
        profile = create_empty_profile()
        new_user = User(username=username, password=password, name=name, code=code, username_code="", new_username="", new_password="", pagename=make_random_pagename(), profile=profile, oauth="", oauth_uid="", refresh_token="")
        new_user.is_active = False        
        new_user.save()

        return new_user


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.RegexField(label=_("Username"),
                regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                help_text=_("Required. E-mail. Letter, digits"
                " and @/./+/-/_ characters only."),
                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")},
                widget=forms.TextInput(attrs={'id':'auth-email', 'placeholder': _("E-mail")}), required=True)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'id': 'auth-pass', 'placeholder': _("Password")}), required=True)

    error_messages = {
        'invalid_login': _("Please enter a correct e-mail and password. "
                           "Note that password may be case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            username = username.lower()
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

class SetPasswordForm(forms.Form):
    """
    A form that lets a user set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput(attrs={'id':'change-newpass', 'placeholder': _("Password")}))
    new_password2 = forms.CharField(label=_("New password confirmation"),
                                    widget=forms.PasswordInput(attrs={'id':'change-newpass2', 'placeholder': _("Password confirmation")}))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user


class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change his/her password by entering
    their old password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
    })
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput(attrs={'id': 'change-pass', 'placeholder': _("Old password")}))

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)


class PasswordRecoveryForm(forms.Form):
    """
    A form that sends a password recovery e-mail to the user.
    """
    # TODO check that user account is active
    error_messages = {'non_existing_user': _("Please enter a correct e-mail of existing user account.")}

    username = forms.RegexField(label=_("E-mail"),
                regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                help_text=_("Required. E-mail. Letter, digits"
                " and @/./+/-/_ characters only."),
                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")},
                widget=forms.TextInput(attrs={"id": "forgot-pass-email", "placeholder": _("E-mail")}))

    def clean_username(self):
        username = self.cleaned_data["username"]
        username = username.lower()
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError(self.error_messages['non_existing_user'],
                                    code='non_existing_user')
        return username

class AvatarForm(forms.Form):
    avatar_file = forms.FileField(label="Select a file", help_text="Image file to use as your photo", required=False)
