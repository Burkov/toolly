# -*- coding: utf-8 -*-

import sys
import os
import base64
import magic
import re
import traceback
import inspect

import logging
# create logger with 'spam_application'
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handler to the logger
logger.addHandler(ch)
logger.info('Executing views.py')

from random import choice, shuffle
from hashlib import sha1
import hmac
import time
import urllib
import calendar
import datetime
import pymongo, gridfs, uuid
from bson import ObjectId, json_util
import json
import requests
import urlparse

from mongoengine.django.auth import User
from mongoengine.queryset import DoesNotExist

from django.conf import settings
DEBUG = settings.DEBUG # to avoid from django.conf.settings import DEBUG, which doesn't work
PRODUCTION = settings.PRODUCTION
from django.shortcuts import render, resolve_url
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden, HttpResponseBadRequest, JsonResponse, HttpResponseNotFound, HttpResponseNotAllowed
from django.template.response import TemplateResponse
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.middleware.csrf import get_token

from django.contrib.auth import login, authenticate, logout
from django.utils.http import is_safe_url
from django.utils.html import escape
from django.forms.formsets import formset_factory

from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.http import require_POST, require_GET

from forms import UserCreationForm, AuthenticationForm, SetPasswordForm, PasswordRecoveryForm, PasswordChangeForm, AvatarForm
from forms import make_random_password, make_random_pagename
from models import User, Album, File, Profile, MainSection
from validators import contact_types, check_schema, traverse_validators, geoValidator, strValidator, fileValidator, contactTypeValidator, contactContentValidator

from common import create_empty_profile
import sendgrid

class ModifiedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, datetime.datetime):
            return str(o)
        else:
            return json.JSONEncoder.default(self, o)

# UTC time in milliseconds
def unix_epoch(dt):
    return calendar.timegm(dt.timetuple())*1000

# fabricate "auto" login link for emails
def create_login_hyperlink(request, user):
    return "https://%s%s" % (get_current_site(request).name, reverse("login", kwargs={"pagename": user.pagename, "code": user.code}))


def basic_context(request):
    current_site = get_current_site(request)

    return {
        'user': request.user,
        'site': current_site,
        'site_name': current_site.name,
        'js_context': {
            "path": request.get_full_path(),
            "csrf": get_token(request)
        },
    }

# add user info to js_context
def set_authed_js_context(request, js_context):
    pass
##### RESTful API views for dynamic modification of user data #####


@require_GET
def userinfo_view(request, user_id):
    try:
        user = User.objects.get(id=user_id)
        return JsonResponse({"name": user.name, "page": user.pagename})
    except:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        raise Http404

def parse_put_request_body(request):
    '''This function is used to create analogues of request.POST and request.FILES for PUT
    request. E.g. when we upload a file with PUT request, we need this function to get the
    file content out of request.body. It is a rip-off of django.http.request._load_post_and_files and
    django.http.multipartparser.parse.

    returns a tuple (POST, FILES)
    '''

    from django.utils.datastructures import MultiValueDict
    from django.utils.encoding import force_text
    from django.utils.text import unescape_entities
    from django.utils import six
    from django.core.files.uploadhandler import StopUpload, SkipFile, StopFutureHandlers
    from django.http import QueryDict
    from django.http.multipartparser import Parser, MultiPartParserError, parse_header, LazyStream, ChunkIter, exhaust
    from io import BytesIO
    import cgi

    RAW = "raw"
    FILE = "file"
    FIELD = "field"
    _BASE64_DECODE_ERROR = TypeError if six.PY2 else binascii.Error

    input_data = BytesIO(request.body) # input data stands for self._input_data in django.http.multipartparser
    handlers = request.upload_handlers # handlers stands for upload_handlers or self._upload_handlers in django.http.multipartparser
    encoding = settings.DEFAULT_CHARSET
    content_type = request.META.get('HTTP_CONTENT_TYPE', request.META.get('CONTENT_TYPE', ''))

    # check content-type; if it's not multipart/form-data, return quickly
    if content_type.startswith('application/x-www-form-urlencoded'): # return POST data and empty FILES right away
        POST, FILES = QueryDict(request.body, encoding=encoding), MultiValueDict()
        return POST, FILES
    if not content_type.startswith('multipart/form-data'): # if this is not multipart/form-data and not x-www-form-urlencoded, return empty data
        POST, FILES = QueryDict('', encoding=encoding), MultiValueDict()
        return POST, FILES

    # so, request's content_type is multipart/form-data, brace yourself and parse it

    # Content-Length should contain the length of the body we are about
    # to receive.
    try:
        content_length = int(request.META.get('HTTP_CONTENT_LENGTH', request.META.get('CONTENT_LENGTH', 0)))
    except (ValueError, TypeError):
        content_length = 0
    if content_length < 0:
        # This means we shouldn't continue...raise an error.
        raise MultiPartParserError("Invalid content length: %r" % content_length)

    # HTTP spec says that Content-Length >= 0 is valid
    # handling content-length == 0 before continuing
    if content_length == 0:
        return QueryDict('', encoding=encoding), MultiValueDict()

    # boundary is the separator between parts of form-data/multipart http request body, stolen from mutilpartparser
    ctypes, opts = parse_header(content_type.encode('ascii'))
    boundary = opts.get('boundary')
    if not boundary or not cgi.valid_boundary(boundary):
        raise MultiPartParserError('Invalid boundary in multipart: %s' % boundary)

    # calculate chunk size (piece of file, read in one step), stolen from multipartparser
    # For compatibility with low-level network APIs (with 32-bit integers),
    # the chunk size should be < 2^31, but still divisible by 4.
    possible_sizes = [x.chunk_size for x in handlers if x.chunk_size]
    chunk_size = min([2 ** 31 - 4] + possible_sizes)

    # See if any of the handlers take care of the parsing.
    # This allows overriding everything if need be.
    for handler in handlers:
        result = handler.handle_raw_input(input_data,
                                      request.META,
                                      content_length,
                                      boundary,
                                      encoding)
        # Check to see if it was handled
        if result is not None: return result[0], result[1]

    # create the data structures to be used later
    POST = QueryDict('', mutable=True)
    FILES = MultiValueDict()

    # Instantiate the parser and stream:
    stream = LazyStream(ChunkIter(input_data, chunk_size))

    # Whether or not to signal a file-completion at the beginning of the loop.
    old_field_name = None
    counters = [0] * len(handlers)

    try:
        for item_type, meta_data, field_stream in Parser(stream, boundary):
            if old_field_name:
                # We run this at the beginning of the next loop
                # since we cannot be sure a file is complete until
                # we hit the next boundary/part of the multipart content.
                handle_file_complete(old_field_name, counters, handlers, encoding, FILES)
                old_field_name = None

            try:
                disposition = meta_data['content-disposition'][1]
                field_name = disposition['name'].strip()
            except (KeyError, IndexError, AttributeError):
                continue

            transfer_encoding = meta_data.get('content-transfer-encoding')
            if transfer_encoding is not None:
                transfer_encoding = transfer_encoding[0].strip()
            field_name = force_text(field_name, encoding, errors='replace')

            if item_type == FIELD:
                # This is a post field, we can just set it in the post
                if transfer_encoding == 'base64':
                    raw_data = field_stream.read()
                    try:
                        data = base64.b64decode(raw_data)
                    except _BASE64_DECODE_ERROR:
                        data = raw_data
                else:
                    data = field_stream.read()

                POST.appendlist(field_name,
                                      force_text(data, encoding, errors='replace'))

            elif item_type == FILE:
                # This is a file, use the handler...
                file_name = disposition.get('filename')
                if not file_name:
                    continue
                file_name = force_text(file_name, encoding, errors='replace')
                file_name = IE_sanitize(unescape_entities(file_name))

                content_type, content_type_extra = meta_data.get('content-type', ('', {}))
                content_type = content_type.strip()
                charset = content_type_extra.get('charset')

                try:
                    content_length = int(meta_data.get('content-length')[0])
                except (IndexError, TypeError, ValueError):
                    content_length = None

                counters = [0] * len(handlers)
                try:
                    for handler in handlers:
                        try:
                            handler.new_file(field_name, file_name,
                                             content_type, content_length,
                                             charset, content_type_extra)
                        except StopFutureHandlers:
                            break

                    for chunk in field_stream:
                        if transfer_encoding == 'base64':
                            # We only special-case base64 transfer encoding
                            # We should always read base64 streams by multiple of 4
                            over_bytes = len(chunk) % 4
                            if over_bytes:
                                over_chunk = field_stream.read(4 - over_bytes)
                                chunk += over_chunk

                            try:
                                chunk = base64.b64decode(chunk)
                            except Exception as e:
                                # Since this is only a chunk, any error is an unfixable error.
                                msg = "Could not decode base64 data: %r" % e
                                six.reraise(MultiPartParserError, MultiPartParserError(msg), sys.exc_info()[2])

                        for i, handler in enumerate(handlers):
                            chunk_length = len(chunk)
                            chunk = handler.receive_data_chunk(chunk,
                                                                   counters[i])
                            counters[i] += chunk_length
                            if chunk is None:
                                # If the chunk received by the handler is None, then don't continue.
                                break

                except SkipFile:
                    # Just use up the rest of this file...
                    exhaust(field_stream)
                else:
                    # Handle file upload completions on next iteration.
                    old_field_name = field_name
            else:
                # If this is neither a FIELD or a FILE, just exhaust the stream.
                exhaust(stream)
    except StopUpload as e:
        if not e.connection_reset:
            exhaust(input_data)
    else:
        # Make sure that the request data is all fed
        exhaust(input_data)

    # Signal that the upload has completed.
    for handler in handlers:
        retval = handler.upload_complete()
        if retval:
            break

    return POST, FILES

def handle_file_complete(old_field_name, counters, handlers, encoding, FILES):
    """
    Handle all the signaling that takes place when a file is complete.
    """
    from django.utils.encoding import force_text    

    for i, handler in enumerate(handlers):
        file_obj = handler.file_complete(counters[i])
        if file_obj:
            # If it returns a file object, then set the files dict.
            FILES.appendlist(
                force_text(old_field_name, encoding, errors='replace'),
                file_obj)
            break

def IE_sanitize(filename):
    """Cleanup filename from Internet Explorer full paths."""
    return filename and filename[filename.rfind("\\") + 1:].strip()


@csrf_exempt
def api_profile_view(request, pagename, path):
    db = settings.MONGO_DB.Toolly
    path_components = path.strip("/").split("/")
    root = db.user.find_one({"pagename": pagename.lower()})
    if root is None:
        raise Http404
    output_json = root["profile"]
    if request.method != "GET" and len(path_components) > 0:
        resource = path_components[-1]
        path_components = path_components[0:-1]
    try:
        for component in path_components:
            if type(output_json) is list:
                if component != "": output_json = output_json[int(component)]
            elif type(output_json) is dict:
                if component != "": output_json = output_json[component]
                
        if request.method == "GET":
            return HttpResponse(ModifiedJSONEncoder().encode(output_json), "application/json")

        # check that user is logged in and is username owner
        # only GET may be anonymous
        if request.user.is_anonymous() or request.user.pagename != pagename.lower(): return HttpResponseForbidden()

        if request.method == "POST":
            # get to the last path component
            if type(output_json) == list:
                output_json = output_json[int(resource)]
            else:
                output_json = output_json[resource]
            # check if user data is a json or x-www-form-urlencoded
            try: # if it is a json
                obj = json.loads(request.body)
            except ValueError as e: # if it's an x-www-form-urlencoded
                key = request.FILES.keys()[0]
                uploaded_file = request.FILES[key]
                uploaded_file_content = uploaded_file.read()
                mime_type = magic_mime(uploaded_file_content)

                fs = gridfs.GridFS(db)
                obj = fs.put(uploaded_file_content, content_type=mime_type)
                pk = "id" # if it's a file upload request, key name is "id"
            # check that obj fits into the schema
            try:
                check_schema(obj, path, request_type="POST")
            except Exception as e:
                logger.error(traceback.format_exc(sys.exc_info()[2]))
                return HttpResponseBadRequest(e)
            # create a child of the resource and return its ObjectId or index
            if type(output_json) == list:
                output_json.append(obj)
                if DEBUG: print "POST %s TO %s " % ( obj, ".".join(["profile"] + path_components + [resource]) )
                db.user.update({ "pagename": pagename.lower()}, { "$push" : { ".".join(["profile"] + path_components + [resource]) : obj } })
                return HttpResponse(ModifiedJSONEncoder().encode({"id": len(output_json)-1}), "application/json")
            elif type(output_json) == dict:
                if not 'pk' in vars(): # if it's not a file upload, generate pk as ObjectId
                    pk = str(ObjectId())
                    output_json[pk] = obj
                else: # if it is a file upload, set pk to ObjectId of created file after adding file to output_json
                    output_json[pk] = obj
                    pk = obj
                if DEBUG: print "POST %s TO %s " % ( obj, ".".join(["profile"] + path_components + [resource] + [pk] ))
                db.user.update({ "pagename": pagename.lower()}, { "$set" : { ".".join(["profile"] + path_components + [resource] + [pk]) : obj } })

                return HttpResponse(ModifiedJSONEncoder().encode({"id": pk}), "application/json")
            else:
                return HttpResponseBadRequest("Resourse is not a collection, can't create item")

        elif request.method == "PUT":
            # check if user data is a json or x-www-form-urlencoded
            try: # if it is a json
                obj = json.loads(request.body)
                # parse it as a JSON, even if it is form data!
            except ValueError as e: # if it's an x-www-form-urlencoded
                # Django doesn't parse body into request.POST and request.FILES dict-likes
                # for requests, other than POST. So for PUT I've stolen the piece responsible
                # for parsing of request.body and made parse_put_request_body function 
                # out of it.
                POST, FILES = parse_put_request_body(request)
                key = FILES.keys()[0]
                uploaded_file = FILES[key]
                uploaded_file_content = uploaded_file.read()
                mime_type = magic_mime(uploaded_file_content)

                fs = gridfs.GridFS(db)
                obj = fs.put(uploaded_file_content, content_type=mime_type)
                pk = "id" # if it's a file upload request, key name is "id"            
            # check that obj adheres to the schema
            try:
                check_schema(obj, path, request_type="PUT")
            except Exception as e:
                logger.error(traceback.format_exc(sys.exc_info()[2]))                
                return HttpResponseBadRequest(e)            
            if type(output_json) is list:
                index = int(resource)
                if index > len(output_json):
                    raise Http404
                elif index == len(output_json):
                    output_json.append(obj) # TODO: resize list, not append!
                else:
                    output_json[index] = obj
            elif type(output_json) is dict:
                output_json[resource] = obj
            if DEBUG: print "PUT %s TO %s " % ( obj, ".".join(["profile"] + path_components + [resource] ))
            db.user.update({ "pagename": pagename.lower()}, { "$set" : { ".".join(["profile"] + path_components + [resource]) : obj } })
            return HttpResponse("{}", "application/json")

        elif request.method == "DELETE":
            if type(output_json) is list:
                index = int(resource)
                if index >= len(output_json):
                    raise Http404
                else:
                    if DEBUG: print "DELETE %s " % (".".join(["profile"] + path_components + [resource] ))
                    db.user.update({ "pagename": pagename.lower()}, {"$unset" : { ".".join(["profile"] + path_components + [resource]) : ""}})
            elif type(output_json) is dict:
                if DEBUG: print "DELETE %s " % (".".join(["profile"] + path_components + [resource] ))
                db.user.update({ "pagename": pagename.lower()}, {"$unset" : { ".".join(["profile"] + path_components + [resource]) : ""}})
            return HttpResponse("{}", "application/json")
    except KeyError as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponse("Not found!", status=404)


##### Services-related views that require additional requests to tag_manager #####


# accepts array of { "name" : "some tag!", "counter" : "-1" } or +1
def tagmanager_update_tags(tags):
    headers = {'Content-Type': 'application/json'}
    body = json.dumps(tags)
    tagman = settings.SERVICES.tagman()
    shuffle(tagman)
    for t in tagman:
        try:
            r = requests.post("%s/update_tags/tags/tag" % t, data=body, headers=headers, timeout=1.5)
            if DEBUG: print  r.status_code, r.text.encode("utf-8")
            if r.status_code == 200:
                return True
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return False
    return False

@require_POST
@csrf_exempt
def service_submit_tag_view(request, pagename):
    # check that user is logged in and is profile owner
    if request.user.is_anonymous(): return HttpResponseForbidden()
    else:
        if request.user.pagename != pagename.lower(): return HttpResponseForbidden()

    obj = json.loads(request.body)
    if DEBUG: print "obj = %s" % obj
    if "id" not in obj:
        return HttpResponseBadRequest("id not in request.POST", content_type="text/plain")
    else:
        id = obj["id"]
        try:
            service = request.user.profile.main.services[id]
        except Exception as e:
            return HttpResponseBadRequest(e)

    if "tag" not in obj:
        if DEBUG: print "No tag specified in request"
        return HttpResponseBadRequest("No tag specified in request")
    tag = obj["tag"]
    # if DEBUG: print "tag = %s" % tag
    # if DEBUG: print "service['tags'] = %s" % service["tags"]
    if tag in service["tags"]:
        if DEBUG: print "Tag is already present"
        return HttpResponseBadRequest("Tag '%s' is already present in tags list" % tag) # front-end is meant to check the presense of tag

    try: # try sending data to tag_manager        
        # TODO check correctness of bulk save
        service["tags"].append(tag)
        request.user.profile.main.services[id] = service
        request.user.save()
        if tagmanager_update_tags([{ "name": tag, "counter": 1 }]):
            # if successful, save changes in service and quit
            return JsonResponse({"id": id, "tag": tag})
        else:
            return HttpResponse("Failed to deliver tag to tag manager", status=500, content_type="text/plain")
    except Exception as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponse(str(e)+traceback.format_exc(), status=500, content_type="text/plain")


@require_POST
@csrf_exempt
def discard_service_tag_view(request, pagename):
    # check that user is logged in and is profile owner
    if request.user.is_anonymous(): return HttpResponseForbidden()
    else:
        if request.user.pagename != pagename.lower(): return HttpResponseForbidden()    

    obj = json.loads(request.body)
    if DEBUG: print "obj = %s" % obj
    if "id" not in obj:
        return HttpResponseBadRequest("id not in request.POST", content_type="text/plain")
    else:
        id = obj["id"]
        if "tag" in obj:
            tag = obj["tag"]
            try: # check if service with given id is there
                service = request.user.profile.main.services[id]
                # remove tag from the database
                service["tags"].pop(service["tags"].index(tag))
                request.user.profile.main.services[id] = service
                request.user.save()
            except:
                logger.error(traceback.format_exc(sys.exc_info()[2]))
                return HttpResponseBadRequest("No primary key %s for service in the database" % id)
            if tagmanager_update_tags([{ "name": tag, "counter": -1 }]):
                return JsonResponse({"tag": tag})
            else:
                return HttpResponse("Failed to deliver tag to tag manager", status=500, content_type="text/plain")
        else:
            return HttpResponseBadRequest("No tag field in input data")

@require_POST
@csrf_exempt
def discard_service_view(request, pagename):
    # check that user is logged in and is profile owner
    if request.user.is_anonymous(): return HttpResponseForbidden()
    else:
        if request.user.pagename != pagename.lower(): return HttpResponseForbidden()

    obj = json.loads(request.body)
    if "id" in obj:
        try:
            db = settings.MONGO_DB.Toolly
            root = db.user.find_one({"pagename":request.user.pagename})
            # send request to tag-manager and clear all tags
            headers = {'Content-Type': 'application/json'}
            tags = []
            service = root["profile"]["main"]["services"][obj["id"]]
            for tag in service["tags"]:
                tags.append({ "name": tag, "counter": -1 })
            tagmanager_update_tags(tags)
            del root["profile"]["main"]["services"][obj["id"]]
            db.user.update({ "pagename": pagename.lower()}, root)
            return HttpResponse(obj["id"], content_type="text/plain")
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseBadRequest("Bad request: %s" % e, content_type="text/plain")


##### Images-related views #####


def magic_mime(image_data):
    mime = magic.Magic(mime=True)
    return mime.from_buffer(image_data)

@csrf_protect
@never_cache
def avatar_view(request, pagename):
    '''This view displays user avatar as /img/{{ pagename }}/avatar
    '''
    if request.method == "POST":
        # check that user is logged in and is pagename owner
        if request.user.is_anonymous() or request.user.pagename != pagename.lower(): return HttpResponseForbidden()
        if DEBUG: print request.FILES
        uploaded_file = request.FILES['avatar_file']    
        image_data = uploaded_file.read()
        mime_type = magic_mime(image_data)
        request.user.profile.avatar.replace(image_data, content_type=mime_type)
        request.user.save()
        return HttpResponse(status=200)
    else:
        try:
            image_data = User.objects.get(pagename=pagename.lower()).profile.avatar.read()
            if image_data == None:
                image_data = open(os.path.join(settings.STATICFILES_DIRS[0], "anonymous_user.png"), 'rb').read()
            mime_type = magic_mime(image_data)
            return HttpResponse(image_data, content_type=mime_type)
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            raise Http404

@csrf_protect
@never_cache
def image_view(request, pagename, album_id, image_id):
    '''This view displays portfolio images as
        /img/{{ pagename }}/{{ album_id }}/{{ image_id }}
    '''

    if request.method == "POST":
        # check that user is logged in and is username owner
        if request.user.is_anonymous() or request.user.pagename != pagename.lower(): return HttpResponseForbidden()
        # gallery update
        if DEBUG: print "request.POST = %s" % request.POST
        if DEBUG: print "request.FILES = %s" % request.FILES

        try:
            album = request.user.profile.portfolio[album_id]
        except ValueError as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            raise Http404

        if request.method == "DELETE":
            removed_file = request.user.profile.portfolio(id=image_id)
            request.user.profile.portfolio[album_id].files.remove(removed_file)
            request.user.save()
            removed_file.delete()
            return HttpResponse("Ok", content_type="text/plain")
        else:
            uploaded_file = request.FILES['image_file'] # filename corresponds to form field name
            uploaded_file_content = uploaded_file.read()
            mime_type = magic_mime(image_data)   
    
            new_file_id = str(ObjectId())
            request.user.profile.portfolio[album_id].files[new_file_id] = File(name="")
            request.user.profile.portfolio[album_id].files[new_file_id].fs.put(uploaded_file_content, content_type=mime_type)
            request.user.save()
            return HttpResponse("%s" % new_file.id, content_type="text/plain")

    else: # if this is GET request, just show the image
        try:
            db = settings.MONGO_DB.Toolly
            file_object = db.user.find_one({"pagename":pagename.lower()})["profile"]["portfolio"][album_id]["files"][image_id]
            fs = gridfs.GridFS(db)
            image = fs.get(file_object["fs"])
            image_data = image.read()
            mime_type = magic_mime(image_data)
        except:
            raise Http404
        return HttpResponse(image_data, content_type=mime_type)

@require_GET
def service_image_view(request, pagename, service_id):
    '''This view displays service image, corresponding to user
     with pagename=pagename and service ObjectId=service_id
    '''
    db = settings.MONGO_DB.Toolly
    user = db.user.find_one({"pagename":pagename.lower()})
    try:
        imageId = user["profile"]["main"]["services"][service_id]["image"]
        if imageId == "":
            return avatar_view(request, pagename) # it's GET only no worries
    except KeyError: # fallback to user's avatar
        return avatar_view(request, pagename) 
    fs = gridfs.GridFS(db)
    image = fs.get(imageId)
    image_data = image.read()
    mime_type = magic_mime(image_data)
    return HttpResponse(image_data, content_type=mime_type)


##### Views for submitting forms causing page reload #####


@require_GET
@never_cache
def login_view(request, pagename, code):
    """
    Performs a password-less login for each of 2 scenarios:
    1) User first registered on the website, gets an e-mail with link with validation code
        and follows that link. The link leads here, and this view makes user account active.
    2) User (or someone else) sent request for password recovery and if user followed that
        link, he comes here and gets logged-in.
    """
    # we don't allow already logged-in user to the login page
    if not request.user.is_anonymous():
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":request.user.pagename}))

    redirect_to = request.POST.get('next',
                                   request.GET.get('next', ''))

    try:
        user = User.objects.get(pagename=pagename.lower())
    except User.DoesNotExist:
        logger.error(traceback.format_exc(sys.exc_info()[2]))        
        raise Http404

    # if code is empty or wrong, kick him
    if user.code == "" or user.code != code:
        return HttpResponseForbidden("Activation code in the hyperlink is wrong or outdated.")

    # the code is right, activate user account, if it's not active yet
    if not user.is_active: user.is_active = True
    # code is one-time only, clear it
    user.code = ""
    # if user was changing password, set his new password
    if user.new_password != "":
        user.set_password(user.new_password)
        user.new_password = ""
    user.save()
    # don't call authenticate, instead set authentication backend manually so that django doesn't complain
    user.backend = 'mongoengine.django.auth.MongoEngineBackend'
    login(request, user)
    # redirect freshly registered user to his profile page
    return HttpResponseRedirect(reverse("profile", kwargs={"pagename":user.pagename}))

@sensitive_post_parameters('password1', 'password2', 'password')
@csrf_protect
def email_sent_view(request, pagename):
    """A message page, shown when user first registered or when user requested password recovery.
    """
    context = basic_context(request)
    context.update({ 'pagename': pagename.lower() })

    topbar_result = process_unauthenticated_topbar_forms(request, context)
    if type(topbar_result) == HttpResponseRedirect:
        return topbar_result

    context['js_context'] = json.dumps(context['js_context'])

    return TemplateResponse(request, 'email_sent.html', context)

@csrf_protect
@require_POST
def logout_view(request):
    redirect_to = request.POST.get("next", request.GET.get("next", ''))
    logout(request)
    if not is_safe_url(redirect_to):
        redirect_to = resolve_url('/')
    return HttpResponseRedirect(redirect_to)

@csrf_exempt
@require_POST
def recover_password_view(request):
    # works with page reload and redirects back to the calling page
    if not request.user.is_anonymous:
        return HttpResponseBadRequest("You're already logged in! Just change your password, don't recover it.")

    password_recovery_form = PasswordRecoveryForm(request.POST) # create bound UserCreationForm
    if password_recovery_form.is_valid(): # if it really is a valid e-mail of existing user
        username = password_recovery_form.cleaned_data['username']
        user = User.objects.get(username=username) # no try here, else form would've been invalid
        user.code = make_random_password(length=32).lower()
        user.new_password = make_random_password(length=32).lower()
        user.save()

        hyperlink = create_login_hyperlink(request, user)

        client = settings.SEND_GRID
        message = sendgrid.Mail()

        message.add_to(username) # add recepient (there can be multiple)
        message.set_from("no-reply@knackit.com")
        message.set_subject(u'Восстановление пароля')
        message.set_html(u'''
        <html>
            <head>
                <title>Password recovery e-mail from Knackit.com</title>
            </head>
            <body>
                <h1>Восстановление пароля на Knackit.com</h1>
                <p>Вы (или кто-то еще) отправили запрос на восстановление пароля к Вашему аккаунту на Knackit.com.</p>
                <br>
                <p>Если Вы перейдете ссылке ниже, для Вашего аккуанта будет установлен временный пароль: %s.</p>
                <br>
                <p>Кроме того, вы попадете на сайт без ввода пароля. Чтобы изменить пароль, перейдите на вкладку
                настроек в личном кабинете.
                </p>
                <a href="%s">Нажмите сюда, чтобы войти на Knackit.com без пароля.</a>
            </body>
        </html>
        ''' % (user.new_password, hyperlink))

        client.send(message)
        # redirect user to the email sent page
        redirect_to = reverse("email_sent", kwargs={"pagename": user.pagename})

        return HttpResponseRedirect(redirect_to)

@csrf_protect
@require_POST
def change_pagename_view(request):
    if request.user.is_anonymous():
        return HttpResponseForbidden("Please, login to change your pagename.")

    new_pagename = request.POST["pagename"].lower()
    is_valid = re.match("^\w+$", new_pagename, flags=re.IGNORECASE)
    if not is_valid:
        return HttpResponseBadRequest("New pagename should consists of alphanumeric characters only, got: %s" % new_pagename)

    try:
        User.objects.get(pagename=new_pagename.lower())
    except User.DoesNotExist:
        request.user.pagename = new_pagename
        request.user.save()

    return HttpResponseRedirect(reverse("profile", kwargs={"pagename": request.user.pagename}))


@csrf_protect
@require_POST
def change_username_view(request):
    if request.user.is_anonymous():
        return HttpResponseForbidden("Please, login to change your pagename.")

    new_username = request.POST["username"].lower()

    # check with regex that it looks like e-mail at all
    is_valid = re.match("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", new_username, flags=re.IGNORECASE)
    if not is_valid:
        return HttpResponseBadRequest("New username %s is not a valid e-mail" % new_username)

    try:
        User.objects.get(pagename=request.POST["username"].lower())
    except User.DoesNotExist:
        request.user.username_code = make_random_password(length=32).lower()
        request.user.new_username = new_username
        request.user.save()

        hyperlink = get_current_site(request).domain + reverse("change_username_confirmation", kwargs={"username_code": request.user.username_code})

        client = settings.SEND_GRID
        message = sendgrid.Mail()

        print request.user.new_username

        message.add_to(request.user.new_username) # add recepient (there can be multiple)
        message.set_from("no-reply@knackit.com")
        message.set_subject(u'Смена адреса электронной почты')
        message.set_html(u'''
        <html>
            <head>
                <title>E-mail change letter from Knackit.com</title>
            </head>
            <body>
                <h1>Смена адреса электронной почты на Knackit.com</h1>
                <p>Вы отправили запрос на смену адреса электронной почты Вашего аккаунта на Knackit.com.
                Щелкинте на ссылке ниже, чтобы привязать Ваш аккаунт на Knackit.com к этому адресу электронной почты.
                </p>
                <a href="%s">Нажмите сюда, чтобы сменить адрес электронной почты на Knackit.com.</a>
            </body>
        </html>
        ''' % hyperlink)

        client.send(message)
        # redirect user to the email sent page
        redirect_to = reverse("email_sent", kwargs={"pagename": request.user.pagename})

        return HttpResponseRedirect(redirect_to)


def change_username_confirmation_view(request, username_code):
    if request.user.is_anonymous():
        return HttpResponseForbidden("Please, login to finish changing your username.")

    if request.user.username_code == "" or request.user.username_code.lower() != username_code.lower():
        return HttpResponseBadRequest("The confirmation hyperlink is broken or obsolete.")

    request.user.username = request.user.new_username
    request.user.new_username = ""
    request.user.username_code = ""
    request.user.save()

    return HttpResponseRedirect(reverse("profile", kwargs={"pagename": request.user.pagename}))


@csrf_protect
@require_POST
def change_name_view(request):
    if request.user.is_anonymous():
        return HttpResponseForbidden("Please, login to change your pagename.")

    new_name = escape(request.POST["account-firstname"])
    request.user.name = new_name
    request.user.save()

    return HttpResponseRedirect(reverse("profile", kwargs={"pagename": request.user.pagename}))


##### OAuth-related views #####


def google_oauth2callback_view(request):
    # description of possible sign-in flows
    # https://developers.google.com/+/web/signin/
    GOOGLE_CLIENT_ID = settings.GOOGLE_CLIENT_ID
    GOOGLE_CLIENT_SECRET = settings.GOOGLE_CLIENT_SECRET
    # check that csrf in request and in cookie are the same
    cookie_csrf = get_token(request)
    if request.GET['state'] != cookie_csrf:
        return HttpResponseForbidden("csrf tokens in cookie and in GET parameters don't match: cookie_csrf = %s, request_csrf = %s" % (cookie_csrf, request.GET['state']))
    # For overall exchange scheme, see:        
    # https://github.com/reddit/reddit/wiki/OAuth2-Python-Example
    # from https://github.com/StartTheShift/pyoauth2
    redirect_uri = "https://knackit.com/google_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/google_oauth2callback"
    params = {"grant_type": "authorization_code",
              "client_id": GOOGLE_CLIENT_ID,
              "client_secret": GOOGLE_CLIENT_SECRET,
              "redirect_uri": redirect_uri,
              "code": request.GET['code']}
    r = requests.post("https://accounts.google.com/o/oauth2/token", data=params)
    #if DEBUG: print "params = %s" % params
    #if DEBUG: print "response headers = %s" % r.headers
    #return HttpResponse("%s" % r.content, content_type="text/plain")

    # If we need to get a new access token with refresh token, see:
    # http://stackoverflow.com/questions/24026431/google-oauth2-0-405-error

    # Content should look like {"access_token": "", 
    #                           "token_type": "Bearer",
    #                           "expirse_in": 3600,
    #                           "id_token": "A Long JWT (JSON Web Token) string"}
    # Upon first request it should also contain "refresh_token":"" - refresh_token
    # that you should permanently store for the current user.
    # If you lost the refresh_token, that user can send it again by logging into
    # his account and revoking the access to user data for toolly:
    # https://www.google.com/settings/u/1/security
    # OR
    # you can add "approval_prompt=force" to the first (redirected) request so that it returns refresh_token
    # no matter what: 
    # https://developers.google.com/accounts/docs/OAuth2WebServer#formingtheurl
    try:
        response_json = r.json()
    except Exception as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponseNotFound("Can not get access token from Google")

    else: # else create an account for him, unless he already has one
        response_json = r.json()
        uid_request_params = {"access_token": response_json["access_token"]}
        uid_request = requests.get("https://www.googleapis.com/oauth2/v2/userinfo", params=uid_request_params)
        try:
            uid_response_json = uid_request.json()
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseNotFound("Can not get user information by OAuth access token from Google")
        try: # if user already exists, log him in
            user = User.objects.get(oauth="google+", oauth_uid=uid_response_json["id"])
            if user == None: raise User.DoesNotExist # in case mongoengine runs crazy and doesn't raise exception
            # no authentication, as we use oauth instead of login/password
            user.backend = 'mongoengine.django.auth.MongoEngineBackend'
            login(request, user)
            # redirect freshly registered user to his profile page
            return HttpResponseRedirect(reverse("profile", kwargs={"pagename": user.pagename}))
        except User.DoesNotExist: # create user, if he doesn't exist
            profile = create_empty_profile()
            username = base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
            new_user = User(username=username, password=make_random_password(), name=uid_response_json['name'], code="", username_code="", new_username="", pagename=make_random_pagename(), profile=profile, oauth="google+", oauth_uid=uid_response_json["id"], refresh_token=response_json["refresh_token"])
            if "picture" in uid_response_json:
                try:
                    picture_request = requests.get(uid_response_json["picture"], stream=True)
                    avatar_content = picture_request.raw.read(decode_content=True)
                except Exception as e:
                    logger.error(traceback.format_exc(sys.exc_info()[2]))
                    return HttpResponseNotFound("Can't download avatar from Google")
            else:
                avatar_content = open(os.path.join(settings.STATICFILES_DIRS[0], "anonymous_user.png"), 'rb').read()
            new_user.profile.avatar.put(avatar_content, content_type="image/png")
            new_user.save()
            # no authentication, as we use oauth instead of login/password
            new_user.backend = 'mongoengine.django.auth.MongoEngineBackend'
            login(request, new_user)
            # redirect freshly registered user to his profile page
            return HttpResponseRedirect(reverse("profile", kwargs={"pagename":new_user.pagename}))

def facebook_oauth2callback_view(request):
    # Overall flow of FB authentication is described here:
    # https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow/v2.2
    # http://ishalyapin.ru/%D0%B0%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F-%D1%87%D0%B5%D1%80%D0%B5%D0%B7-facebook-%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D1%80-%D0%B4%D0%BB%D1%8F-django/
    FACEBOOK_CLIENT_ID = settings.FACEBOOK_CLIENT_ID
    FACEBOOK_CLIENT_SECRET = settings.FACEBOOK_CLIENT_SECRET
    # check that csrf in request and in cookie are the same
    cookie_csrf = get_token(request)
    if request.GET['state'] != cookie_csrf:
        return HttpResponseForbidden("csrf tokens in cookie and in GET parameters don't match: cookie_csrf = %s, request_csrf = %s" % (cookie_csrf, request.GET['state']))
    # Our dashboard with CLIENT_ID, CLIENT_SECRET etc. is here:
    # https://developers.facebook.com/apps/319619584891125/dashboard/

    # second request to FB is GET, not POST in contradiction with OAUTH2 RFC
    redirect_uri = "https://knackit.com/facebook_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/facebook_oauth2callback"
    params = {"grant_type": "authorization_code",
              "client_id": FACEBOOK_CLIENT_ID,
              "client_secret": FACEBOOK_CLIENT_SECRET,
              "redirect_uri": redirect_uri,
              "code": request.GET['code']}
    r = requests.get("https://graph.facebook.com/oauth/access_token", params=params)
    response = dict(urlparse.parse_qsl(r.text))
    try:
        access_token = response["access_token"] # FB doesn't send refresh_token, so forget about it
    except Exception as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponseNotFound("Can not get access token from Facebook")

    # third request: get user data by access_token
    uid_request_params = {"access_token": access_token}
    uid_request = requests.get("https://graph.facebook.com/me", params=uid_request_params)    
    uid_response_json = uid_request.json()

    try: # if user already exists, log him in
        user = User.objects.get(oauth="facebook", oauth_uid=uid_response_json["id"])
        if user == None: raise User.DoesNotExist # in case mongoengine runs crazy and doesn't raise exception
        # no authentication, as we use oauth instead of login/password
        user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename": user.pagename}))
    except User.DoesNotExist: # create user, if he doesn't exist
        profile = create_empty_profile()
        username = base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
        new_user = User(username=username, password=make_random_password(), name=uid_response_json['name'], code="", username_code="", new_username="", new_password="", pagename=make_random_pagename(), profile=profile, oauth="facebook", oauth_uid=uid_response_json["id"], refresh_token="")
        # download avatar picture
        try:
            picture_request = requests.get("https://graph.facebook.com/%s/picture?type=large" % uid_response_json["id"], stream=True)
            avatar_content = picture_request.raw.read(decode_content=True)
            new_user.profile.avatar.put(avatar_content, content_type="image/png")
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseBadRequest("Can not download avatar from Facebook")
        new_user.save()
        # no authentication, as we use oauth instead of login/password
        new_user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, new_user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":new_user.pagename}))

def vk_oauth2callback_view(request):
    # General manual on OAUTH for VK is here:
    # https://vk.com/dev/auth_mobile
    VK_CLIENT_ID = settings.VK_CLIENT_ID
    VK_CLIENT_SECRET = settings.VK_CLIENT_SECRET
    # check that csrf in request and in cookie are the same
    cookie_csrf = get_token(request)
    if request.GET['state'] != cookie_csrf:
        return HttpResponseForbidden("csrf tokens in cookie and in GET parameters don't match: cookie_csrf = %s, request_csrf = %s" % (cookie_csrf, request.GET['state']))
    # Our dashboard with CLIENT_ID, CLIENT_SECRET etc. is here:
    # http://vk.com/editapp?id=4652014
    # second request to VK is GET, not POST in contradiction with OAUTH2 RFC
    redirect_uri = "https://knackit.com/vk_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/vk_oauth2callback"
    params = {"grant_type": "authorization_code",
              "client_id": VK_CLIENT_ID,
              "client_secret": VK_CLIENT_SECRET,
              "redirect_uri": redirect_uri,
              "code": request.GET['code']}
    r = requests.get("https://oauth.vk.com/access_token", params=params) # url taken from http://habrahabr.ru/sandbox/80273/
    try:
        response = json.loads(r.text) # in contrast with FB, VK's response here is json, not form-data. At least something original from Durov!
        uid = str(response["user_id"])
        access_token = response["access_token"]
    except Exception as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponseBadRequest("Can not download access token and user id from VK")
    try: # if user already exists, log him in
        user = User.objects.get(oauth="vk", oauth_uid=uid)
        if user == None: raise User.DoesNotExist # in case mongoengine runs crazy and doesn't raise exception
        # no authentication, as we use oauth instead of login/password
        user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename": user.pagename}))
    except User.DoesNotExist: # create user, if he doesn't exist
        try:
            users_get_params = {"uids": uid, "fields":"first_name,last_name,photo_max_orig"} # see parameters here: https://vk.com/dev/users.get
            users_get_request = requests.get("https://api.vk.com/method/users.get", params=users_get_params)
            users_get_request_json = users_get_request.json()['response'][0]
            name = users_get_request_json["first_name"] + " " + users_get_request_json["last_name"]
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseNotFound("Can not download user data from VK")
        profile = create_empty_profile()
        username = base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
        new_user = User(username=username, password=make_random_password(), name=name, code="", username_code="", new_username="", new_password="", pagename=make_random_pagename(), profile=profile, oauth="vk", oauth_uid=uid, refresh_token="")
        # download avatar picture
        try:
            picture_request = requests.get(users_get_request_json['photo_max_orig'], stream=True)
            avatar_content = picture_request.raw.read(decode_content=True)
        except Exception as e:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseNotFound("Can not download user avatar from VK")
        new_user.profile.avatar.put(avatar_content, content_type="image/png")
        new_user.save()
        # no authentication, as we use oauth instead of login/password
        new_user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, new_user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":new_user.pagename}))

def twitter_oauth_submit_button_view(request):
    # https://dev.twitter.com/web/sign-in/implementing
    # https://dev.twitter.com/oauth/overview/creating-signatures
    # https://dev.twitter.com/oauth/overview/authorizing-requests
    # http://stackoverflow.com/questions/8338661/implementaion-hmac-sha1-in-python
    # https://dev.twitter.com/web/sign-in/implementing
    # https://dev.twitter.com/oauth/overview/creating-signatures

    # we don't allow already registered user to this url
    if not request.user.is_anonymous():
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":request.user.pagename.lower()}))

    TWITTER_CLIENT_SECRET = settings.TWITTER_CLIENT_SECRET
    TWITTER_CLIENT_ID = settings.TWITTER_CLIENT_ID

    urlencode = urllib.quote_plus

    nonce = make_nonce()
    timestamp = int(time.time())
    # encode with client secret, NOTE the ampersand sign added to secret! Ampersand comes from the fact, that encoding key
    # should normally be "TWITTER_CLIENT_SECRET&OAUTH_TOKEN_SECRET", but as we don't have OAUTH_TOKEN_SECRET yet, we should
    # use just "TWITTER_CLIENT_SECRET&".
    # also note the urlencode() around TWITTER_CLIENT_SECRET
    hmaced = hmac.new(urlencode(TWITTER_CLIENT_SECRET)+"&", make_request_token_basestring(nonce=nonce, timestamp=timestamp), sha1)
    signature = hmaced.digest().encode("base64").rstrip("\n")

    redirect_uri = "https://knackit.com/twitter_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/twitter_oauth2callback"
    oauth_header = ('OAuth ' + 
                   'oauth_callback="' + urlencode(redirect_uri) + '", ' +
                   'oauth_consumer_key="' + urlencode(TWITTER_CLIENT_ID) + '", ' +                   
                   'oauth_nonce="' + urlencode(nonce) + '", ' +
                   'oauth_signature="' + urlencode(signature) + '", ' +
                   'oauth_signature_method="' + urlencode('HMAC-SHA1') + '", ' +
                   'oauth_timestamp="' + urlencode(str(timestamp)) + '", ' +
                   'oauth_version="1.0"')
    
    headers = {'Authorization': oauth_header}
    try:
        r = requests.post("https://api.twitter.com/oauth/request_token", headers=headers, data={})
        if DEBUG: print "oauth_header = %s" % oauth_header
        if DEBUG: print "r.request.headers = %s" % r.request.headers # http://stackoverflow.com/questions/10588644/how-can-i-see-the-entire-http-request-thats-being-sent-by-my-python-application
        if DEBUG: print "r.content = %s" % r.content
        response = dict(urlparse.parse_qsl(r.text))
    except Exception as e:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponseBadRequest()
    if "oauth_token" in response:
        return HttpResponseRedirect("https://api.twitter.com/oauth/authenticate?oauth_token=%s" % response["oauth_token"])
    else:
        raise Http404("Failed to connect to Twitter")

def make_request_token_basestring(nonce, timestamp, request_type="POST", url="https://api.twitter.com/oauth/request_token"):
    urlencode = urllib.quote_plus

    redirect_uri = "https://knackit.com/twitter_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/twitter_oauth2callback"
    params = [("oauth_callback", redirect_uri),
              ("oauth_consumer_key", settings.TWITTER_CLIENT_ID),
              ("oauth_nonce", nonce),
              ("oauth_signature_method", "HMAC-SHA1"),
              ("oauth_timestamp", str(timestamp)),
              ("oauth_version", "1.0")]
    oauth_params_string = "&".join([(urlencode(key) + "=" + urlencode(value)) for key, value in params])
    base_string = urlencode(request_type) + "&" + urlencode(url) + "&" + urlencode(oauth_params_string)
    if DEBUG: print "base_string = %s" % base_string
    return base_string

def make_nonce(length=32, charset='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    return "".join([choice(charset) for i in range(length)])

def make_access_token_basestring(nonce, timestamp, oauth_token, oauth_verifier, request_type="POST", url="https://api.twitter.com/oauth/access_token"):
    urlencode = urllib.quote_plus

    params = [("oauth_consumer_key", settings.TWITTER_CLIENT_ID),
              ("oauth_nonce", nonce),
              ("oauth_signature_method", "HMAC-SHA1"),
              ("oauth_timestamp", str(timestamp)),
              ("oauth_token", oauth_token),
              ("oauth_verifier", oauth_verifier),
              ("oauth_version", "1.0")]
    oauth_params_string = "&".join([(urlencode(key) + "=" + urlencode(value)) for key, value in params])
    base_string = urlencode(request_type) + "&" + urlencode(url) + "&" + urlencode(oauth_params_string)
    return base_string

def twitter_oauth2callback_view(request):
    # Unlike other OAuth providers, utilizing OAuth2, Twitter can use OAuth2 only for
    # user-less requests. When we need to ask something on behalf of the user, Twitter
    # wants us to use the complicated OAuth1 protocol, where OAuth client (i.e. Toolly)
    # sends cryptographically signed requests to OAuth provider (i.e. Twitter).
    # General flow of OAuth1 exchange with Twitter is described here:
    # https://dev.twitter.com/oauth/overview
    # We can't use application-only auth via OAuth2, cause we need user context :(
    TWITTER_CLIENT_ID = settings.TWITTER_CLIENT_ID
    TWITTER_CLIENT_SECRET = settings.TWITTER_CLIENT_SECRET
    # Twitter Application Management with CLIENT_ID, CLIENT_SECRET and all urls is here:
    # https://apps.twitter.com/app/7297707
    oauth_token = request.GET["oauth_token"]
    oauth_verifier = request.GET["oauth_verifier"]
    # TODO: check that oauth_token corresponds to the results of twitter_oauth_submit_button_view

    urlencode = urllib.quote_plus

    nonce = make_nonce()
    timestamp = int(time.time())
    # encode with "TWITTER_CLIENT_SECRET&" again
    hmaced = hmac.new(urlencode(TWITTER_CLIENT_SECRET)+"&", make_access_token_basestring(nonce=nonce, 
        timestamp=timestamp, oauth_token=oauth_token, oauth_verifier=oauth_verifier), sha1)
    signature = hmaced.digest().encode("base64").rstrip("\n")

    oauth_header = ('OAuth ' + 
                   'oauth_consumer_key="' + urlencode(TWITTER_CLIENT_ID) + '", ' +
                   'oauth_nonce="' + urlencode(nonce) + '", ' +
                   'oauth_signature="' + urlencode(signature) + '", ' +
                   'oauth_signature_method="' + urlencode('HMAC-SHA1') + '", ' +
                   'oauth_timestamp="' + urlencode(str(timestamp)) + '", ' +
                   'oauth_token="' + urlencode(oauth_token) + '", ' +
                   'oauth_version="1.0"')

    # send request to oauth / access_token endpoint
    headers = {'Authorization': oauth_header}
    try:
        r = requests.post("https://api.twitter.com/oauth/access_token", headers=headers, data={"oauth_verifier":oauth_verifier})
        response = dict(urlparse.parse_qsl(r.text)) 
        # sample response: {u'oauth_token': u'2894449875-NCWmlOiNtq1sXiyqvkpv71EuUPeKhb2mUW93qDK',
        # u'oauth_token_secret': u'ljkBZZBbpjz2bNk0MqjEC9HYf5kt4W556BSEYoxkYoCjH',
        # u'screen_name': u'highload22',
        # u'user_id': u'2894449875'}

        # get uid, oauth_token and oauth_token_secret
        uid = str(response["user_id"])
        access_token = response["oauth_token"]
        access_token_secret = response["oauth_token_secret"]
    except:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        return HttpResponseBadRequest("Can not get access_token from Twitter")

    try: # if user already exists, log him in
        user = User.objects.get(oauth="twitter", oauth_uid=uid)
        if user == None: raise User.DoesNotExist # in case mongoengine runs crazy and doesn't raise exception
        # no authentication, as we use oauth instead of login/password
        user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":user.pagename.lower()}))
    except User.DoesNotExist: # create user, if he doesn't exist
        # in order to obtain user data, we make a request to lookup.json endpoint
        # with all those annoying Authentication headers
        users_get_nonce = make_nonce()
        users_get_timestamp = int(time.time())

        users_get_params = [("oauth_consumer_key", TWITTER_CLIENT_ID),
                ("oauth_nonce", users_get_nonce),
                ("oauth_signature_method", "HMAC-SHA1"),
                ("oauth_timestamp", str(users_get_timestamp)),
                ("oauth_token", access_token),
                ("oauth_version", "1.0"),
                ("user_id", uid)]
        users_get_params_string = "&".join([(urlencode(key) + "=" + urlencode(value)) for key, value in users_get_params])
        users_get_basestring = urlencode("GET") + "&" + urlencode("https://api.twitter.com/1.1/users/lookup.json") + "&" + urlencode(users_get_params_string)
        users_get_hmaced = hmac.new(urlencode(TWITTER_CLIENT_SECRET)+"&"+urlencode(access_token_secret), users_get_basestring, sha1)
        users_get_signature = users_get_hmaced.digest().encode("base64").rstrip("\n")
        users_get_header = ('OAuth ' + 
                   'oauth_consumer_key="' + urlencode(TWITTER_CLIENT_ID) + '", ' +
                   'oauth_nonce="' + urlencode(users_get_nonce) + '", ' +
                   'oauth_signature="' + urlencode(users_get_signature) + '", ' +
                   'oauth_signature_method="' + urlencode('HMAC-SHA1') + '", ' +
                   'oauth_timestamp="' + urlencode(str(users_get_timestamp)) + '", ' +
                   'oauth_token="' + urlencode(access_token) + '", ' +
                   'oauth_version="1.0"')
        users_get_headers = {'Authorization': users_get_header}
        users_get_params = {"user_id": uid}
        try:
            users_get_request = requests.get("https://api.twitter.com/1.1/users/lookup.json", params=users_get_params, headers=users_get_headers)
            users_get_request_json = users_get_request.json()[0]
            name = users_get_request_json["name"]
        except:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseBadRequest("Can not get user data from Twitter")
        profile = create_empty_profile()
        username = base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
        new_user = User(username=username, password=make_random_password(), name=name, code="", username_code="", new_username="", new_password="", pagename=make_random_pagename(), profile=profile, oauth="twitter", oauth_uid=uid, refresh_token=access_token+"|"+access_token_secret)
        # download avatar picture
        try:
            picture_request = requests.get(users_get_request_json['profile_image_url'], stream=True)
            avatar_content = picture_request.raw.read(decode_content=True)
            new_user.profile.avatar.put(avatar_content, content_type="image/png")
            new_user.save()
        except:
            logger.error(traceback.format_exc(sys.exc_info()[2]))
            return HttpResponseBadRequest("Can not get user avatar from Twitter")
        # no authentication, as we use oauth instead of login/password
        new_user.backend = 'mongoengine.django.auth.MongoEngineBackend'
        login(request, new_user)
        # redirect freshly registered user to his profile page
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":new_user.pagename}))


##### Views, related to the real pages #####


@sensitive_post_parameters('password1', 'password2', 'password')
@csrf_protect
@never_cache
def index(request):
    # we don't allow already registered user to the index page
    if not request.user.is_anonymous():
        return HttpResponseRedirect(reverse("profile", kwargs={"pagename":request.user.pagename}))

    context = basic_context(request)

    topbar_result = process_unauthenticated_topbar_forms(request, context)
    if type(topbar_result) == HttpResponseRedirect:
        return topbar_result

    context['js_context'] = ModifiedJSONEncoder().encode(context['js_context'])

    return TemplateResponse(request, 'index.html', context)


@sensitive_post_parameters('password1', 'password2', 'password')
@csrf_protect
@never_cache
def profile_view(request, pagename, template_name):
    # check that requested url corresponds to a registered user, 404 otherwise
    try:
        owner = User.objects.get(pagename=pagename.lower())
    except User.DoesNotExist:
        logger.error(traceback.format_exc(sys.exc_info()[2]))
        raise Http404
    db = settings.MONGO_DB.Toolly
    doc = db.user.find_one({"pagename":pagename.lower()})
    # [[image.id for image in album.images] for album in profile.gallery],

    # create 'is_owner' boolean variable for template context, indicating
    # if user is the owner of profile and can edit it

    if request.user.is_anonymous() or not request.user.pagename.lower() == pagename.lower(): is_owner = False # note that request.user is django.utils.functional.SimpleLazyObject
    else: is_owner = True

    context = basic_context(request)
    context.update({
        'owner': owner,
        'pagename': pagename,
        'is_owner': is_owner
    })

    context['js_context'] = js_context = {
        "path": request.get_full_path(),
        "portfolio_url": reverse("api_profile", kwargs={"pagename": pagename, "path":"portfolio"}),
        "services_url": reverse("api_profile", kwargs={"pagename": pagename, "path":"main/services"}),
        "discard_service_url": reverse("discard_service", kwargs={"pagename": pagename}),
        "discard_service_tag_url": reverse("discard_service_tag", kwargs={"pagename": pagename}),
        "service_submit_tag_url": reverse("service_submit_tag", kwargs={"pagename": pagename}),
        "pagename": pagename,
        "is_owner": is_owner,
        "owner_name": owner.name,
        "owner_pk" : str(owner.id),
        "owner_page" : pagename,
        "owner_profile" : {
            "main": doc['profile']['main'],
            "extras": doc['profile']['extras'],
            "portfolio": doc['profile']['portfolio']
        },
        "csrf": get_token(request)
    }
    if not request.user.is_anonymous():
        doc = db.user.find_one({ "username" : request.user.username })
        context['js_context']['bookmarks'] = doc['profile']['bookmarks']

    # avatar
    if request.method == 'POST':
        if 'avatar-btn' in request.POST:
            if DEBUG: print request.FILES
            avatar_form = AvatarForm(request.POST, request.FILES)
            if avatar_form.is_valid():
                new_avatar_file = request.FILES['avatar_file']
                new_avatar = new_avatar_file.read()
                if DEBUG: print "magic_mime = %s" % magic_mime(new_avatar)
                request.user.profile.avatar.replace(new_avatar, content_type="image/png")
                if DEBUG: print "new_avatar = %s ..." % request.user.profile.avatar.read()[0:100]
                request.user.save()
            else:
                if DEBUG: print "invalid form"
                if DEBUG: print avatar_form.errors
                #TODO meaningful error message
        else:
            avatar_form = AvatarForm()
    else:
        avatar_form = AvatarForm()

    context["avatar_form"] = avatar_form
    if not request.user.is_anonymous():
        js_context["user_pk"] = str(request.user.id)
        js_context["last_login"] = unix_epoch(request.user.last_login)

    # PasswordChangeForm
    if is_owner: # only do this if user is GETting/POSTing his own profile
        if request.method == 'POST':
            password_change_form = PasswordChangeForm(request.user, request.POST)
            if password_change_form.is_valid():
                password_change_form.save()
        else:
            password_change_form = PasswordChangeForm(owner) # here used to be owner argument
        context["password_change_form"] = password_change_form


    topbar_result = process_unauthenticated_topbar_forms(request, context)
    if type(topbar_result) == HttpResponseRedirect:
        return topbar_result

    context['js_context'] = ModifiedJSONEncoder().encode(js_context)

    return TemplateResponse(request, template_name, context)

def process_unauthenticated_topbar_forms(request, context):
    '''Call this function in each view that has topbar with login and register
     forms to process the data of those forms.

    Returns HttpResponseRedirect, if user decides to login.
    
    Modifies context, adding the following names to it:
        "auth_form"
        "user_creation_form"
        "next"
    '''
    redirect_to = request.POST.get("next", request.GET.get("next", ''))

    if request.method == "POST":
        if DEBUG: print "process_authentication_tobar_forms: request.POST = %s" % request.POST
        # check, which submit button was pressed; parse the respective form
        if "Create account" in request.POST: # if user's attempting to register
            auth_form = AuthenticationForm() # create auth form unbound
            password_recovery_form = PasswordRecoveryForm() # create password recovery form unbound
            user_creation_form = UserCreationForm(request.POST) # create bound UserCreationForm
            if user_creation_form.is_valid():
                new_user = user_creation_form.save() # create new user account with is_active=False and random code for account activation

                hyperlink = create_login_hyperlink(request, new_user)

                client = settings.SEND_GRID        
                message = sendgrid.Mail()
        
                message.add_to(new_user.username) # add recepient (there can be multiple)
                message.set_from("no-reply@knackit.com")
                message.set_subject(u'Регистрация на Knackit.com')
                message.set_html(u'''
                <html>
                    <head>
                        <title>Registration e-mail from Knackit.com</title>
                    </head>
                    <body>
                        <h1>Регистрация на Knackit.com</h1>
                        <p>Вы (или кто-то еще) отправили запрос на регистрацию аккаунта на Knackit.com.
                        Перейдите по ссылке ниже, чтобы завершить регистрацию и войти в Ваш аккаунт.
                        </p>
                        <a href="%s">Нажмите сюда, чтобы завершить регистрацию.</a>
                    </body>
                </html>
                ''' % hyperlink)
        
                client.send(message)
                # redirect user to the email sent page
                redirect_to = reverse("email_sent", kwargs={"pagename": new_user.pagename})
                return HttpResponseRedirect(redirect_to)

        elif "Login" in request.POST: # if user is trying to login
            user_creation_form = UserCreationForm() # create user_creation_form unbound
            password_recovery_form = PasswordRecoveryForm() # create password recovery form unbound
            auth_form = AuthenticationForm(request, data=request.POST) # create bound AuthenticationForm
            if auth_form.is_valid():
                redirect_to = request.POST.get("next", '')
                # if redirection url is empty, return to the same page, we came from
                if not request.POST.get("next", ''):
                    redirect_to = request.path
                # Ensure the user-originating redirection url is safe.
                if not is_safe_url(url=redirect_to):
                    redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
                # Okay, security check complete. Log the user in.
                login(request, auth_form.get_user())
                return HttpResponseRedirect(redirect_to)
        else: # none of topbar forms was submitted, create all forms unbound
            user_creation_form = UserCreationForm()
            auth_form = AuthenticationForm()
            password_recovery_form = PasswordRecoveryForm()
    else: # if it's an idempotent request (GET etc.), create all forms unbound
        auth_form = AuthenticationForm()
        user_creation_form = UserCreationForm()
        password_recovery_form = PasswordRecoveryForm()

    context["auth_form"] = auth_form
    context["user_creation_form"] = user_creation_form
    context["password_recovery_form"] = password_recovery_form
    context["next"] = redirect_to

    context['js_context']["GOOGLE_CLIENT_ID"] = settings.GOOGLE_CLIENT_ID
    context['js_context']["FACEBOOK_CLIENT_ID"] = settings.FACEBOOK_CLIENT_ID
    context['js_context']["VK_CLIENT_ID"] = settings.VK_CLIENT_ID
    context["TWITTER_CLIENT_ID"] = settings.TWITTER_CLIENT_ID # twitter OAuth 1 works in a different way

    context['js_context']["GOOGLE_OAUTH_REDIRECT_URI"] = "https://knackit.com/google_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/google_oauth2callback"
    context['js_context']["FACEBOOK_OAUTH_REDIRECT_URI"] = "https://knackit.com/facebook_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/facebook_oauth2callback"
    context['js_context']["VK_OAUTH_REDIRECT_URI"] = "https://knackit.com/vk_oauth2callback" if PRODUCTION else "https://sabotage.knackit.com/vk_oauth2callback"

@sensitive_post_parameters('password1', 'password2', 'password')
@csrf_protect
@never_cache
def search_view(request, template_name):
    context = basic_context(request)
    if not request.user.is_anonymous():
        db = settings.MONGO_DB.Toolly
        doc = db.user.find_one({ "username" : request.user.username })
        context['js_context']["user_pk"] = str(request.user.id)
        context["last_login"] = unix_epoch(request.user.last_login)
        context['js_context']['bookmarks'] = doc['profile']['bookmarks']

    topbar_result = process_unauthenticated_topbar_forms(request, context)
    if type(topbar_result) == HttpResponseRedirect:
        return topbar_result

    context['js_context'] = ModifiedJSONEncoder().encode(context['js_context'])

    return TemplateResponse(request, template_name, context)
