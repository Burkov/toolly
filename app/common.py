from models import MainSection, Profile

def create_empty_profile():
    return Profile(main=MainSection(services={}, location=[], contacts={}, preferred_contact_types=[]), extras={"experience": {}, "bio":"", "education": {}, "languages":{} }, portfolio={}, bookmarks={})
