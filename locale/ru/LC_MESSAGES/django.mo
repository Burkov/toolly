��          |      �          )   !     K     R     W     `  U   v  %   �     �  F        S  A   \  �  �  V   T  !   �     �     �  '   �  �   	  Y   �  4   T  X   �     �  x                           
                       	            A user with that username already exists. E-mail Name Password Password confirmation Please enter a correct e-mail and password. Note that password may be case-sensitive. The two password fields didn't match. This account is inactive. This value may contain only letters, numbers and @/./+/-/_ characters. Username Your old password was entered incorrectly. Please enter it again. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-02-09 21:37+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Этот адрес электронной почты уже используется. Электронная почта Имя Пароль Подтверждение пароля Пожалуйста, введите правильный адрес электронной почты и пароль. Обратите внимание, что пароль может быть чувствителен к регистру. Поле подтвержения пароля не совпадает с паролем. Этот аккаунт не активирован. Это поле может содержать буквы, цифры, и знаки @/./ Имя пользователя Старый пароль введен некорректно. Пожалуйста, попробуйте еще раз. 