#!/bin/bash
if [ "x$PRODUCTION" != "x" ] ; then
    PROD="--env PRODUCTION=1 --env NDEBUG=1"
fi

if [ "x$HOSTS" != "x" ] ; then
	exec uwsgi --ini uwsgi.ini --env "HOSTS=$HOSTS" --env "DJANGO_SETTINGS_MODULE=Toolly.settings" $PROD
else
	exec uwsgi --ini uwsgi.ini
fi
